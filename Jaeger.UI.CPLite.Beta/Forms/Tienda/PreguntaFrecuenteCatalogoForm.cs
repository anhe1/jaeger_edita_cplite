﻿using System;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    public class PreguntaFrecuenteCatalogoForm : UI.Tienda.Forms.PreguntaFrecuenteCatalogoForm {

        public PreguntaFrecuenteCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.GFaq.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += this.PreguntaFrecuenteCatalogoForm_Load;
        }

        private void PreguntaFrecuenteCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new Aplication.CPLite.Tienda.Services.PreguntasFrecuentesService();
        }

        public override void Consultar() {
            this.datos = this.service.GetList(true);
        }
    }
}
