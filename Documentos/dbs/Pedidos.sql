select pddcln_id NoPedido, pddcln_sttsdcs_id IdStatus, (case pddcln_sttsdcs_id when 0 then 'Cancelado' when 1 then 'Pendiente' when 2 then 'Por Surtir' when 3 then 'Detenido' when 4 then 'Entregado'  else '' end) Status,
drctr_clv Clave, drctr_nom Cliente, pddcln_fecped FechaPedido, PDDCLN_FCENTRG FechaEntrega, PDDCLN_MTDENV MetodoEnvio,  pddcln_sbttl$ SubTotal, 0 Envio, .16 TasaIVA, pddcln_iva$ TrasladoIVA, pddcln_ttl$ Total, PDDCLN_RQIRFCTR Facturar, pddcln_obsrv Nota, pddcln_fn, pddcln_usu_n Creo
from pddcln
left join drctr on pddcln_drctr_id = drctr_id
where PDDCLN_VNDDR_ID = 134