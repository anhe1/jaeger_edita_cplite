﻿using System;
using System.Linq;
using Telerik.WinControls;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class ReciboCobroForm : UI.Banco.Forms.MovimientoBancarioForm {
        private bool IsReady = false;
        public ReciboCobroForm(Aplication.Banco.IBancoService service) : base(service) {
            this.Load += ReciboCobroForm_Load;
        }

        public ReciboCobroForm(MovimientoBancarioDetailModel movimiento, Aplication.Banco.IBancoService service) : base(movimiento, service) {
            this.Load += ReciboCobroForm_Load;
        }

        private void ReciboCobroForm_Load(object sender, EventArgs e) {
            this.TMovimiento.ShowHerramientas = false;
        }

        protected override void TMovimiento_Imprimir_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento != null) {
                this.TMovimiento.Movimiento.FormatoImpreso = MovimientoBancarioFormatoImpreso.ReciboCobro;
                var imprimir = new UI.Banco.Forms.ReporteForm(new MovimientoBancarioPrinter(this.TMovimiento.Movimiento));
                imprimir.Show();
            }
        }

        protected override void BindingCompleted(object sender, EventArgs e) {
            base.BindingCompleted(sender, e);
            if (this.IsReady == false) {
                ConceptoMovimiento_SelectedIndexChanged(sender, e);
                ConceptoMovimiento_DropDownClosed(sender, null);
                this.IsReady = true;
            }
        }

        protected override void TComprobante_Buscar_Click(object sender, EventArgs e) {
            // para esta version solo podemos utilizar remisiones
            if (this.TComprobante.GetTipoComprobante() == MovimientoBancarioTipoComprobanteEnum.CFDI) {
                RadMessageBox.Show(this, "No existe documentos relacionados al tipo de movimiento", "Banco", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Error);
            } else {
                base.TComprobante_Buscar_Click(sender, e);
            }
        }

        protected override void GetTiposMovimiento() {
            this._TiposMovimiento = new System.ComponentModel.BindingList<MovimientoConceptoDetailModel>(
                this.TMovimiento.Service.GetList<MovimientoConceptoDetailModel>(Aplication.Banco.BancoConceptoService.Query().TipoOperacion(BancoTipoOperacionEnum.ReciboCobro).OnlyActive().Build()
                ).ToList());
        }
    }
}
