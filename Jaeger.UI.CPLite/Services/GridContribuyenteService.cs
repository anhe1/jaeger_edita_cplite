﻿using System.Windows.Forms;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.CPLite.Services {
    public static class GridContribuyenteService {
        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] { ColIdDirectorio, GridCommonService.ColCheckActivo, GridCommonService.ColClave, ColNombre, GridCommonService.ColRFC, ColValidaRFC, ColClaveRegimenFiscal, ColDomicilioFiscal, ColDiasCredito, ColCredito,
                ColSobreCredito, ColFactorDescuento, ColDiasEntrega, ColFactorIVAPactado, ColTelefono, GridDomicilioFiscalService.ColIdTipo, GridDomicilioFiscalService.ColCalle, 
                GridDomicilioFiscalService.ColNoExterior, GridDomicilioFiscalService.ColNoInterior, GridDomicilioFiscalService.ColColonia, GridDomicilioFiscalService.ColDelegacion, GridDomicilioFiscalService.ColCodigoPostal, 
                GridDomicilioFiscalService.ColEstado, GridDomicilioFiscalService.ColCiudad, GridDomicilioFiscalService.ColPais, GridCommonService.ColCorreo, GridCommonService.ColCreo, GridCommonService.ColFechaNuevo
            };
        }

        public static GridViewDataColumn[] GetBusqueda() {
            return new GridViewDataColumn[] {
                ColIdDirectorio, GridTelerikCommon.ColClave, ColNombre, GridTelerikCommon.ColRFC, GridTelerikCommon.ColCorreo
            };
        }

        /// <summary>
        /// columna para indice del directorio
        /// </summary>
        public static GridViewTextBoxColumn ColIdDirectorio {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Id",
                    HeaderText = "Id",
                    Width = 50,
                    Name = "IdDirectorio",
                    IsVisible = false,
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Clave",
                    HeaderText = "Clave",
                    Name = "Clave",
                    Width = 65
                };
            }
        }

        /// <summary>
        /// obtener columna del directorio Nombre
        /// </summary>
        public static GridViewTextBoxColumn ColNombre {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nombre",
                    HeaderText = "Nombre",
                    Name = "Nombre",
                    Width = 280
                };
            }
        }

        /// <summary>
        /// obtener columna del directorio Nombre
        /// </summary>
        public static GridViewTextBoxColumn ColCliente {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nombre",
                    HeaderText = "Cliente",
                    Name = "Nombre",
                    Width = 280
                };
            }
        }

        public static GridViewTextBoxColumn ColRFC {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RFC",
                    HeaderText = "RFC",
                    Name = "RFC",
                    TextImageRelation = TextImageRelation.TextBeforeImage,
                    Width = 90,
                    MaxLength = 15,
                    ColumnCharacterCasing = CharacterCasing.Upper
                };
            }
        }

        public static GridViewTextBoxColumn ColDiasCredito {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "DiasCredito",
                    HeaderText = "Días \r\nCrédito",
                    Name = "DiasCredito",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 65,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ColCredito {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Credito",
                    FormatString = "{0:N2}",
                    HeaderText = "Lim. Crédito",
                    Name = "Credito",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColSobreCredito {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "SobreCredito",
                    FormatString = "{0:N2}",
                    HeaderText = "Sob. Crédito",
                    Name = "SobreCredito",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColDiasEntrega {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "DiasEntrega",
                    HeaderText = "Días Pac.",
                    Name = "DiasEntrega",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 65
                };
            }
        }

        public static ExpressionFormattingObject RegistroActivo() {
            return new ExpressionFormattingObject {
                ApplyToRow = true,
                CellBackColor = Color.Empty,
                CellForeColor = Color.Empty,
                Expression = "Activo = false",
                Name = "Registro Activo",
                RowBackColor = Color.Empty,
                RowFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Italic, GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = Color.Gray,
            };
        }

        public static GridViewCheckBoxColumn ColValidaRFC {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "ValidaRFC",
                    HeaderText = "Val.",
                    Name = "ValidaRFC",
                    Width = 25
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveRegimenFiscal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "RegimenFiscal",
                    HeaderText = "Régimen",
                    IsVisible = false,
                    Name = "RegimenFiscal",
                    ReadOnly = true,
                    TextAlignment = ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColDomicilioFiscal {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "DomicilioFiscalF",
                    HeaderText = "Dom. Fiscal",
                    IsVisible = false,
                    Name = "DomicilioFiscalF",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColFactorDescuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "FactorDescuento",
                    FormatString = "{0:N4}",
                    HeaderText = "F. Pac.",
                    Name = "FactorDescuento",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColFactorIVAPactado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "FactorIvaPactado",
                    FormatString = "{0:P0}",
                    HeaderText = "%IVA",
                    Name = "FactorIvaPactado",
                    TextAlignment = ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 110,
                    ReadOnly = true,
                };
            }
        }
    }
}
