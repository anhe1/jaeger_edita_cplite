﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Herramientas.Configuracion.Profile {
    /// <summary>
    /// catalogo de usuarios
    /// </summary>
    public class UsersCatalogForm : Kaiju.Forms.UsersCatalogForm {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="menuElement">MenuElement</param>
        public UsersCatalogForm(UIMenuElement menuElement) : base() {
            this.Load += UsersCatalogForm_Load;
            this.TUser.Permisos = menuElement.Action;
        }

        private void UsersCatalogForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.CPLite.Services.ProfileService();
        }
    }
}
