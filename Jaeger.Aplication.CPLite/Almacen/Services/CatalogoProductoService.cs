﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Almacen.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.DataAccess.FB.Almacen.Repositories;

namespace Jaeger.Aplication.CPLite.Almacen.Services {
    public class CatalogoProductoService : Aplication.Tienda.Services.CatalogoProductoService, Aplication.Almacen.Contracts.ICatalogoProductoService, Aplication.Tienda.Contracts.ICatalogoProductosService {
        #region declaraciones
        protected internal ISqlCategoriaRepository categoriaRepository;
        protected internal ISqlEspecificacionRepository especificacionRepository;
        #endregion

        public CatalogoProductoService(AlmacenEnum almacen) : base(almacen) {
            this.s3.Folder = "almacen/pt";
        }

        public override void OnLoad() {
            this.categoriaRepository = new SqlFbCategoriaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.especificacionRepository = new SqlFbEspecificacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.productoRepository = new DataAccess.FB.CPLite.Repositories.SqlFbProdServRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.modelosRepository = new DataAccess.FB.CPLite.Repositories.SqlFbModelosRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.unidadRepository = new SqlFbUnidadRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.modeloEspecificacionRepository = new SqlFbModeloEspecificacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.mediosRepository = new SqlFbMediosRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.s3 = new Base.Services.EditaBucketService();
            this.sqlProdServXRepository = new DataAccess.FB.CPLite.Repositories.SqlFbProdServRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.publicacionRepository = new DataAccess.FB.CPLite.Repositories.SqlFbPublicacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.clasificacionRepository = new DataAccess.FB.Almacen.Repositories.SqlFbClasificacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.modeloExistenciaRepository = new DataAccess.FB.Almacen.Repositories.SqlFbModeloExistenciaRepository(GeneralService.Configuration.DataBase);
        }

        public override BindingList<EspecificacionModel> GetEspecificaciones() {
            return new BindingList<EspecificacionModel>(this.especificacionRepository.GetList().ToList());
        }

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            if (typeof(T1) == typeof(ProductoServicioXDetailModel)) {
                var response = this.sqlProdServXRepository.GetList<T1>(conditionals).ToList();
                if (response != null) {
                    var ids = response.Select(x => (x as ProductoServicioXDetailModel).IdProducto).ToList();
                    if (ids.Count > 0) {
                        var d0 = new List<IConditional>();
                        var activo = conditionals.Where(it => it.FieldName.ToLower().EndsWith("_a")).FirstOrDefault();
                        if (activo != null) { d0 = new List<IConditional>() { new Conditional("CTMDL_A", "1") }; }
                        var modelos = this.GetModelos<ModeloXDetailModel>(d0);

                        for (int i = 0; i < response.Count(); i++) {
                            (response[i] as ProductoServicioXDetailModel).Modelos = new BindingList<ModeloXDetailModel>(modelos.Where(it => it.IdProducto == (response[i] as ProductoServicioXDetailModel).IdProducto).ToList());
                        }
                    }
                }
                return response;
            } else if (typeof(T1) == typeof(ModeloXDetailModel)) {
                return this.GetModelos<T1>(conditionals).ToList();
            } else if (typeof(T1) == typeof(ModeloYEspecificacionModel)) {
                return this.modeloEspecificacionRepository.GetList<T1>(conditionals);
            } else if (typeof(T1) == typeof(MedioModel)) {
                return this.mediosRepository.GetList<T1>(conditionals); 
            } else if (typeof(T1) == typeof(CategoriaModel) | typeof(T1) == typeof(CategoriaSingle)) {
                return this.categoriaRepository.GetList<T1>(conditionals);
            }
            return base.GetList<T1>(conditionals);
        }

        private IEnumerable<T> GetModelos<T>(List<IConditional> conditionals) where T : class, new() {
            var modelos = this.modelosRepository.GetList<T>(conditionals).ToList();
            if (modelos != null) {
                var ids1 = modelos.Select(x => (x as ModeloXDetailModel).IdModelo).ToList();
                if (ids1.Count > 0) {
                    var disponibles = this.GetList<ModeloYEspecificacionModel>(
                            new List<IConditional> {
                            new Conditional("CTMDLE_CTMDL_ID", string.Join(",", ids1), ConditionalTypeEnum.In),
                            new Conditional("CTMDLE_A", "1")
                            });

                    var imagenes = this.GetList<MedioModel>(Aplication.Almacen.Services.MediosService.Query().IdModelo(ids1.ToArray()).Build());
                    var query = this.publicacionRepository.GetList<PublicacionModel>(new PublicacionQueryBuilder().IsActive(ids1.ToArray()).Build());

                    for (int i = 0; i < modelos.Count(); i++) {
                        if (imagenes != null) {
                            (modelos[i] as ModeloXDetailModel).Medios = new BindingList<MedioModel>(imagenes.Where(it => it.IdModelo == (modelos[i] as ModeloXDetailModel).IdModelo).ToList());
                        }
                        if (disponibles != null) {
                            (modelos[i] as ModeloXDetailModel).Disponibles = new BindingList<ModeloYEspecificacionModel>(disponibles.Where(it => it.IdModelo == (modelos[i] as ModeloXDetailModel).IdModelo).Where(it => it.Activo == true).ToList());
                        }
                        if (query != null) {
                            (modelos[i] as ModeloXDetailModel).Publicacion = query.Where(it => it.IdModelo == (modelos[i] as ModeloXDetailModel).IdModelo).FirstOrDefault();
                        }
                    }
                }
            }
            return modelos;
        }

        public override ModeloModel Save(ModeloModel model) {
            return base.Save(model);
        }

        public override List<ModeloXDetailModel> Save(List<ModeloXDetailModel> items) {
            for (int i = 0; i < items.Count; i++) {
                items[i] = (ModeloXDetailModel)this.modelosRepository.Save(items[i] as ModeloModel);
            }
            return items;
        }
    }
}
