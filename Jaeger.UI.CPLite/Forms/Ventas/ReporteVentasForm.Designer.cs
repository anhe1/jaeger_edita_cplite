﻿using System.Windows.Forms;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class ReporteVentasForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReporteVentasForm));
            this.TResumen = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.chart1 = new Telerik.WinControls.UI.RadChartView();
            this.gridRemisiones = new Telerik.WinControls.UI.RadGridView();
            this.splitContainer1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemisiones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemisiones.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.PanelContainer.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TResumen
            // 
            this.TResumen.Dock = System.Windows.Forms.DockStyle.Top;
            this.TResumen.Location = new System.Drawing.Point(0, 0);
            this.TResumen.Name = "TResumen";
            this.TResumen.ShowActualizar = true;
            this.TResumen.ShowAutosuma = false;
            this.TResumen.ShowCancelar = false;
            this.TResumen.ShowCerrar = true;
            this.TResumen.ShowEditar = false;
            this.TResumen.ShowEjercicio = true;
            this.TResumen.ShowExportarExcel = true;
            this.TResumen.ShowFiltro = true;
            this.TResumen.ShowHerramientas = true;
            this.TResumen.ShowImprimir = false;
            this.TResumen.ShowNuevo = false;
            this.TResumen.ShowPeriodo = true;
            this.TResumen.Size = new System.Drawing.Size(1254, 30);
            this.TResumen.TabIndex = 0;
            // 
            // chart1
            // 
            this.chart1.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelFormat = "{0:N0}";
            linearAxis1.TickOrigin = null;
            this.chart1.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(0, 30);
            this.chart1.Name = "chart1";
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LegendTitle = "Venta";
            barSeries1.VerticalAxis = linearAxis1;
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LegendTitle = "PorCobrar";
            barSeries2.VerticalAxis = linearAxis1;
            barSeries3.HorizontalAxis = categoricalAxis1;
            barSeries3.LegendTitle = "Cobrado";
            barSeries3.VerticalAxis = linearAxis1;
            barSeries4.HorizontalAxis = categoricalAxis1;
            barSeries4.LegendTitle = "Saldo";
            barSeries4.VerticalAxis = linearAxis1;
            this.chart1.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2,
            barSeries3,
            barSeries4});
            this.chart1.ShowGrid = false;
            this.chart1.ShowLegend = true;
            this.chart1.Size = new System.Drawing.Size(1254, 325);
            this.chart1.TabIndex = 0;
            // 
            // gridRemisiones
            // 
            this.gridRemisiones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridRemisiones.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridRemisiones.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridRemisiones.Name = "gridRemisiones";
            this.gridRemisiones.ShowGroupPanel = false;
            this.gridRemisiones.Size = new System.Drawing.Size(1252, 189);
            this.gridRemisiones.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContainer1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Up;
            this.splitContainer1.Location = new System.Drawing.Point(0, 355);
            this.splitContainer1.MinimumSize = new System.Drawing.Size(25, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 355, 1254, 217);
            // 
            // splitContainer1.PanelContainer
            // 
            this.splitContainer1.PanelContainer.Controls.Add(this.gridRemisiones);
            this.splitContainer1.PanelContainer.Size = new System.Drawing.Size(1252, 189);
            // 
            // 
            // 
            this.splitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitContainer1.Size = new System.Drawing.Size(1254, 217);
            this.splitContainer1.TabIndex = 3;
            this.splitContainer1.TabStop = false;
            // 
            // ReporteVentasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 572);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.TResumen);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReporteVentasForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Ventas: Por vendedor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReporteVentasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemisiones.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRemisiones)).EndInit();
            this.splitContainer1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarCommonControl TResumen;
        private Telerik.WinControls.UI.RadChartView chart1;
        public Telerik.WinControls.UI.RadGridView gridRemisiones;
        private Telerik.WinControls.UI.RadCollapsiblePanel splitContainer1;
    }
}