﻿using System.Collections.Generic;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.CPLite.Contracts {
    public interface IVentasService {
        IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
