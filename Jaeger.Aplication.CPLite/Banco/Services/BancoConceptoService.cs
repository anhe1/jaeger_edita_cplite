﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.Banco.Repositories;

namespace Jaeger.Aplication.CPLite.Banco.Services {
    public class BancoConceptoService : Aplication.Banco.BancoConceptoService, Aplication.Banco.IBancoConceptoService {

        public BancoConceptoService() : base() { }

        public override void OnLoad() {
            this.conceptoRepository = new SqlFbMovimientoConceptoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}