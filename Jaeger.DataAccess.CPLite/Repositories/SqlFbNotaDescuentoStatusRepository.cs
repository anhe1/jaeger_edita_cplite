﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    /// <summary>
    /// repositorio de status de notas descuento
    /// </summary>
    public class SqlFbNotaDescuentoStatusRepository : Abstractions.RepositoryMaster<NotaDescuentoStatusModel>, ISqlNotaDescuentoStatusRepository {
        public SqlFbNotaDescuentoStatusRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public NotaDescuentoStatusModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<NotaDescuentoStatusModel> GetList() {
            throw new NotImplementedException();
        }

        public int Insert(NotaDescuentoStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NTDSCS (NTDSCS_ID, NTDSCS_A, NTDSCS_NTDSC_ID, NTDSCS_NTDSC_STTS_ID, NTDSCS_NTDSC_STTSA_ID, NTDSCS_DRCTR_ID, NTDSCS_CVMTV, NTDSCS_NOTA, NTDSCS_USR_N, NTDSCS_FN)
                                          VALUES (@NTDSCS_ID,@NTDSCS_A,@NTDSCS_NTDSC_ID,@NTDSCS_NTDSC_STTS_ID,@NTDSCS_NTDSC_STTSA_ID,@NTDSCS_DRCTR_ID,@NTDSCS_CVMTV,@NTDSCS_NOTA,@NTDSCS_USR_N,@NTDSCS_FN) RETURNING NTDSCS_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NTDSCS_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_ID", item.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_STTS_ID", item.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_STTSA_ID", item.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_CVMTV", item.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_USR_N", item.User);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_FN", item.FechaNuevo);

            item.IdAuto = this.ExecuteScalar(sqlCommand);
            if (item.IdAuto > 0)
                return item.IdAuto;
            return 0;
        }

        public int Update(NotaDescuentoStatusModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NTDSCS SET NTDSCS_A = @NTDSCS_A, NTDSCS_NTDSC_ID = @NTDSCS_NTDSC_ID, NTDSCS_NTDSC_STTS_ID = @NTDSCS_NTDSC_STTS_ID, NTDSCS_NTDSC_STTSA_ID = @NTDSCS_NTDSC_STTSA_ID, NTDSCS_DRCTR_ID = @NTDSCS_DRCTR_ID, NTDSCS_CVMTV = @NTDSCS_CVMTV, NTDSCS_NOTA = @NTDSCS_NOTA, NTDSCS_USR_N = @NTDSCS_USR_N, NTDSCS_FN = @NTDSCS_FN WHERE NTDSCS_ID = @NTDSCS_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NTDSCS_ID", item.IdAuto);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_ID", item.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_STTS_ID", item.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_STTSA_ID", item.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_CVMTV", item.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_USR_N", item.User);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_FN", item.FechaNuevo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT NTDSCS.* FROM NTDSCS @wcondiciones ORDER BY NTDSCS_NTDSC_ID ASC"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
        #endregion

        public bool Save(NotaDescuentoStatusModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO 
NTDSCS ( NTDSCS_NTDSC_ID, NTDSCS_NTDSC_STTS_ID, NTDSCS_NTDSC_STTSA_ID, NTDSCS_DRCTR_ID, NTDSCS_CVMTV, NTDSCS_NOTA, NTDSCS_USR_N, NTDSCS_FN) 
VALUES (@NTDSCS_NTDSC_ID,@NTDSCS_NTDSC_STTS_ID,@NTDSCS_NTDSC_STTSA_ID,@NTDSCS_DRCTR_ID,@NTDSCS_CVMTV,@NTDSCS_NOTA,@NTDSCS_USR_N, CURRENT_TIMESTAMP)
MATCHING  (NTDSCS_NTDSC_ID, NTDSCS_NTDSC_STTS_ID, NTDSCS_NTDSC_STTSA_ID, NTDSCS_DRCTR_ID, NTDSCS_CVMTV, NTDSCS_NOTA);"
            };

            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_ID", model.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_STTS_ID", model.IdStatusA);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NTDSC_STTSA_ID", model.IdStatusB);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_DRCTR_ID", model.IdCliente);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_CVMTV", model.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_NOTA", model.Nota);
            sqlCommand.Parameters.AddWithValue("@NTDSCS_USR_N", model.User);

            var itsOk = this.ExecuteTransaction(sqlCommand) > 0;
            if (itsOk && model.IdStatusB == 0) {

                sqlCommand = new FbCommand {
                    CommandText = @"UPDATE NTDSC SET NTDSC_STTS_ID = 0, NTDSC_USU_M = @user, NTDSC_USU_C = @cancela, NTDSC_FM = @date, NTDSC_FCCNCL = @date WHERE NTDSC_ID = @index"
                };

                sqlCommand.Parameters.AddWithValue("@index", model.IdNota);
                sqlCommand.Parameters.AddWithValue("@cancela", model.User);
                sqlCommand.Parameters.AddWithValue("@date", DateTime.Now);
                sqlCommand.Parameters.AddWithValue("@user", this.User);

                return this.ExecuteTransaction(sqlCommand) > 0;
            }
            return itsOk;
        }
    }
}
