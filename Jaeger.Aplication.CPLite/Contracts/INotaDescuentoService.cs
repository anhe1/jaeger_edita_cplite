﻿using System.ComponentModel;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Aplication.CPLite.Contracts {
    /// <summary>
    /// Nota de Descuento
    /// </summary>
    public interface INotaDescuentoService {
        NotaDescuentoDetailModel GetById(int id);

        NotaDescuentoDetailModel Save(NotaDescuentoDetailModel conceptoModel);

        bool Cancelar(NotaDescuentoDetailModel model);
        bool Cancelar(NotaDescuentoStatusModel model);

        string Verificar(NotaDescuentoDetailModel model);

        BindingList<NotaDescuentoRelacionDetailModel> GetPartidas();
    }
}
