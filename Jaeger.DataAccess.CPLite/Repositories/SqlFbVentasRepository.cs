﻿using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbVentasRepository : Abstractions.RepositoryMaster<VentasResumenModel>, ISqlVentasRepository {
        public SqlFbVentasRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var isRemision = conditionals.Select(it => it.FieldName.ToLower().Contains("rmsn")).FirstOrDefault();
            var sqlCommand = new FbCommand();
            if (isRemision) {
                sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM RMSN LEFT JOIN MVAPT ON MVAPT.MVAPT_RMSN_ID = RMSN.RMSN_ID AND MVAPT.MVAPT_DOC_ID = 26 AND MVAPT.MVAPT_A > 0 @wcondiciones"
                };
            } else {
                sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM PDCLN LEFT JOIN PDCLP ON PDCLP.PDCLP_ID = PDCLN.PDCLN_ID AND PDCLP_A > 0 LEFT JOIN DRCTR ON DRCTR.DRCTR_ID = PDCLN.PDCLN_DRCTR_ID @wcondiciones"
                };
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
