SET AUTODDL ON;

DROP VIEW VWALLRMS;

/**************** DROPPING COMPLETE ***************/

CREATE VIEW VWALLRMS (VWALLRMS_ID, VWALLRMS_FOLIO, VWALLRMS_NOM, VWALLRMS_FCEMSN, VWALLRMS_SBTTL$, VWALLRMS_IMPST, VWALLRMS_IMPST$, VWALLRMS_CBRD$, VWALLRMS_DSCNT$)
AS                   
select 
    rem.ctlrms_id vwallrms_id, 
    rem.ctlrms_folio vwallrms_folio, 
    dir.drctr_nom vwallrms_nom, 
    rem.ctlrms_fcentrg vwallrms_fcEmsn, 
    rem.ctlrms_sbttl$ vwallrms_sbttl$, 
    rem.ctlrms_impst vwallrms_impst, 
    rem.ctlrms_impst$ vwallrms_impst$, 
    rem.ctlrms_cbrd$ vwallrms_cbrd$, 
    (rem.ctlrms_sbttl$ + rem.ctlrms_impst$) - rem.ctlrms_cbrd$ vwallrms_dscnt$
from ctlrms rem
left join drctr dir on rem.ctlrms_drctr_id = dir.drctr_id;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWALLRMS TO  SYSDBA WITH GRANT OPTION;
