SET TERM ^ ;
CREATE PROCEDURE APLICAR_RECIBOS_CORRECCION (
    INPUT_START DOM_ID DEFAULT 0,
    INPUT_END DOM_ID DEFAULT 0,
    INPUT_UPDATE DOM_ID DEFAULT 0 )
RETURNS (
    OUT_FOLIO DOM_ID )
AS
declare poliza dom_id;
BEGIN
/*
Elabora: anhe 1212/2012
Proposito: correcion de los recibos de cobro para guardar el historial de lo que se debio cobrar en cada abono
Revisiones:
*/
    for
        select cntbl_poliza
        from cntbl
        where 
                cntbl_folio between :input_start and :input_end 
            and cntbl_doc_id = 28
            and cntbl_sttsdcs_id = 3
        order by cntbl_poliza asc
        into
            : poliza
    do
        begin
            --execute procedure CBRNZ_RECIBOS_COBROS1 ( :out_folio, 30, 1, 0 );
            for 
                SELECT p.RLCNCBRNZ_REM_ID 
                FROM CBRNZ_RECIBOS_COBROS1_OUT(:poliza, 30, :INPUT_UPDATE ) p
                into :out_folio
            do
                begin
                    suspend;
                end
        end
        
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE APLICAR_RECIBOS_CORRECCION TO  SYSDBA;

