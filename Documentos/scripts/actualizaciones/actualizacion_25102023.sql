/*
*** ACTUALIZACIÓN 25/10/2023
*/
SET TERM ^ ;
ALTER TRIGGER TRG_RMSN_PERIODO ACTIVE
BEFORE INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 05052023 2249
    Purpose: al insertar un nuevo registro las columnas de ejercicio y periodo se obtiene de la fecha de emision
    el ejercicio y periodo correspondiente
------------------------------------------------------------------------------------------------------------- */
    IF (NEW.RMSN_FECEMS <> OLD.RMSN_FECEMS) THEN
        BEGIN
            NEW.RMSN_ANIO = EXTRACT(YEAR FROM NEW.RMSN_FECEMS);
            NEW.RMSN_MES = EXTRACT(MONTH FROM NEW.RMSN_FECEMS);
        END
    IF (NEW.RMSN_ANIO IS NULL) THEN
        BEGIN 
            NEW.RMSN_ANIO = EXTRACT(YEAR FROM NEW.RMSN_FECEMS);
            NEW.RMSN_MES = EXTRACT(MONTH FROM NEW.RMSN_FECEMS);
        END
END^
SET TERM ; ^