﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class RemisionPorClienteForm : UI.Almacen.PT.Forms.RemisionadoClienteForm {
        public RemisionPorClienteForm(UIMenuElement menuElement) : base(menuElement, new Aplication.CPLite.Services.RemisionadoService()) {
            this.Load += RemisionPorClienteForm_Load;
        }

        private void RemisionPorClienteForm_Load(object sender, System.EventArgs e) {
            this.Text = "Clientes: Remisiones por cliente";
        }
    }
}
