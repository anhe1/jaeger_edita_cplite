﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbPrecioPartidaRepository : Abstractions.RepositoryMaster<PrecioPartidaModel>, ISqlPrecioPartidaRepository {
        public SqlFbPrecioPartidaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(PrecioPartidaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTPRCP (CTPRCP_ID, CTPRCP_A, CTPRCP_CTPRC_ID, CTPRCP_CTESPC_ID, CTPRCP_UNITC, CTPRCP_UNIT, CTPRCP_OBSRV, CTPRCP_FN, CTPRCP_USU_N) VALUES (@CTPRCP_ID, @CTPRCP_A, @CTPRCP_CTPRC_ID, @CTPRCP_CTESPC_ID, @CTPRCP_UNITC, @CTPRCP_UNIT, @CTPRCP_OBSRV, @CTPRCP_FN, @CTPRCP_USU_N)"
            };
            sqlCommand.Parameters.AddWithValue("@CTPRCP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_UNITC", item.UnitarioC);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_UNIT", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_USU_N", item.Creo);
            item.IdPartida = this.ExecuteScalar(sqlCommand);
            if (item.IdPartida > 0)
                return item.IdPartida;
            return 0;
        }

        public int Update(PrecioPartidaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTPRCP SET CTPRCP_A = @CTPRCP_A, CTPRCP_CTPRC_ID = @CTPRCP_CTPRC_ID, CTPRCP_CTESPC_ID = @CTPRCP_CTESPC_ID, CTPRCP_UNITC = @CTPRCP_UNITC, CTPRCP_UNIT = @CTPRCP_UNIT, CTPRCP_OBSRV = @CTPRCP_OBSRV, CTPRCP_FM = @CTPRCP_FM, CTPRCP_USU_M = @CTPRCP_USU_M WHERE ((CTPRCP_ID = @CTPRCP_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@CTPRCP_ID", item.IdPartida);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_UNITC", item.UnitarioC);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_UNIT", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTPRCP_USU_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM CTPRCP WHERE ((CTPRCP_ID = @CTPRCP_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTPRCP_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PrecioPartidaModel GetById(int index) {
            return this.GetList(new List<IConditional>() { new Conditional("CTPRCP_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<PrecioPartidaModel> GetList() {
            return this.GetList(new List<IConditional>());
        }
        #endregion

        public PrecioPartidaModel Save(PrecioPartidaModel model) {
            if (model.IdPartida == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdPartida = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            return model;
        }

        public IEnumerable<PrecioPartidaModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CTPRCP @wcondiciones ORDER BY CTPRCP_ID ASC"
            };
            return this.GetMapper<PrecioPartidaModel>(sqlCommand, conditionals);
        }
    }
}
