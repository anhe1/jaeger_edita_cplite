SET TERM ^ ;
CREATE PROCEDURE COMISION_VENDEDOR (
    INPUT_VNDDR_ID DOM_ID DEFAULT 0,
    INPUT_DSCNT DOM_MONEDA DEFAULT 0,
    INPUT_DSTRNS DOM_ID DEFAULT 0,
    INPUT_FECHA DOM_FH )
RETURNS (
    OUTPUT_ID DOM_ID,
    OUTPUT_NOM DOM_STR100,
    OUTPUT_FACTOR1 DOM_MONEDA,
    OUTPUT_FACNOM1 DOM_STR100,
    OUTPUT_FACTOR2 DOM_MONEDA,
    OUTPUT_FACNOM2 DOM_STR100 )
AS
declare variable comision_id dom_id;
declare variable comision_nom dom_str100;
declare variable comision_factor1 dom_moneda;
declare variable comision_facnom1 dom_str100;
declare variable comision_factor2 dom_moneda;
declare variable comision_facnom2 dom_str100;
declare variable comision_descnto dom_moneda;
BEGIN
/*
    proposito: con este procedimiento calculamos la comision correspondiente al vendedor, esta divida en diferentes consultas para mostrar un mayor numero
    de posibles errores cuando no existe coincidencia con los catalogos.
        comision = catalogo de comisiones
        descuentos = comision por descuentos al cliente
        cartera = multas a la cartera vencida
        vendedor = comision asiganda al vendedor
*/
    if (input_dstrns<=0 ) then input_dstrns=1; -- en caso de que la cantidad de dias sea menor o igual a cero
    if (input_dstrns<0 ) then input_dstrns=1; -- en caso de que la cantidad de dias sea menor o igual a cero
    comision_id=0;
    comision_descnto =abs(1-:input_dscnt);
    if ( :comision_descnto <0 ) then 
        begin
            comision_descnto=0;
        end
    -- comision asignada al vendedor
    select first 1 comision.cmsdsc_id, comision.CMSDSC_NOM
    from   cmsdsc comision, rlcdrc vendedor
    where  comision.cmsdsc_a=1 
        and vendedor.rlcdrc_a=1
        and comision.cmsdsc_doc_id = 1016 
        and vendedor.rlcdrc_cmsn_id=comision.cmsdsc_id
        and vendedor.rlcdrc_vnddr_id=:input_vnddr_id                                    --< id del vendedor
        and :input_fecha between comision.cmsdsc_fchini and comision.cmsdsc_fchvig      --< fecha de emision del documento
    order by comision.cmsdsc_id asc, vendedor.rlcdrc_vnddr_id asc
    into
        :comision_id, :comision_nom;
    
    if ( :comision_id is not null and :comision_id>0 ) then
        begin
            output_id = :comision_id;
            output_nom = :comision_nom;
            comision_factor1=null;
            -- tabla de comisiones por descuentos al cliente
            select first 1 descuentos.tbrngs_factor,descuentos.tbrngs_nom
            from tbrngs descuentos
            where   
                 descuentos.tbrngs_a=1
                and descuentos.tbrngs_doc_id=1037
                and :comision_descnto between descuentos.tbrngs_rng1 and descuentos.tbrngs_rng2      --< % de descuento
                and descuentos.tbrngs_cmsdsc_id = :comision_id                                  --< id de comision asignada
            order by descuentos.tbrngs_rng1 asc
            into :comision_factor1, :comision_facnom1;
            if ( :comision_factor1 is not null ) then
                begin
                    output_factor1=:comision_factor1;
                    output_facnom1=:comision_facnom1;
                    -- tabla de multas a la cartera vencida
                    comision_factor2=null;
                    select first 1 cartera.tbrngs_factor,cartera.tbrngs_nom
                    from   tbrngs cartera 
                    where  cartera.tbrngs_a=1
                        and cartera.tbrngs_cmsdsc_id=:comision_id
                        and cartera.tbrngs_doc_id=1038
                        and :input_dstrns between cartera.tbrngs_rng1 and cartera.tbrngs_rng2           --< % dias transcurridos
                    into :comision_factor2, :comision_facnom2;
                    if ( :comision_factor2 is null ) then
                        begin
                            output_factor2=0;
                            output_facnom2='fuera de rango';
                        end
                    else
                        begin
                            output_factor2=:comision_factor2;
                            output_facnom2=:comision_facnom2;
                        end
                end
            else
                begin
                    output_factor1 = 0;
                    output_facnom1 = 'no existe nivel para el valor buscado.';
                    output_facnom2='fuera de rango';
                end
        end
    else
        begin
            output_id=-1;
            output_nom = 'no se asigno catalogo';
        end
/*
    select first 1 comision.cmsdsc_id,comision.cmsdsc_nom,
        descuentos.tbrngs_factor,descuentos.tbrngs_nom,
        cartera.tbrngs_factor,cartera.tbrngs_nom
    from    cmsdsc comision, tbrngs descuentos, tbrngs cartera , rlcdrc vendedor
    where   comision.cmsdsc_a=1 
        and descuentos.tbrngs_a=1
        and cartera.tbrngs_a=1
        and vendedor.rlcdrc_a=1
        and comision.cmsdsc_doc_id = 1016 
        and descuentos.tbrngs_doc_id=1037
        and cartera.tbrngs_doc_id=1038
        and vendedor.rlcdrc_doc_id = 1016 
        and comision.cmsdsc_id=descuentos.tbrngs_cmsdsc_id
        and comision.cmsdsc_id=cartera.tbrngs_cmsdsc_id
        and vendedor.rlcdrc_cmsn_id=comision.cmsdsc_id
        and vendedor.rlcdrc_vnddr_id=:input_vnddr_id                                    --< id del vendedor
        and :input_dscnt between descuentos.tbrngs_rng1 and descuentos.tbrngs_rng2      --< % de descuento
        and :input_dstrns between cartera.tbrngs_rng1 and cartera.tbrngs_rng2           --< % dias transcurridos
        and :input_fecha between comision.cmsdsc_fchini and comision.cmsdsc_fchvig      --< fecha de emision del documento
    order by comision.cmsdsc_id asc, vendedor.rlcdrc_vnddr_id asc, descuentos.tbrngs_rng1 asc, cartera.tbrngs_rng1 asc
    into
        :comision_id ,:comision_nom ,:comision_factor1 ,:comision_facnom1, :comision_factor2 ,:comision_facnom2;
    
    if (:comision_id is null or :comision_id <=0 ) then
        begin
            output_id = -1;
            output_nom = 'Sin catalogo';
            output_factor1 =0;
            output_facnom1 ='No hay conincidencia';
            output_factor2 =0;
            output_facnom2 ='calculado a cero';
        end 
    ELSE
        begin
            output_id = :comision_id;
            output_nom = :comision_nom;
            output_factor1 =:comision_factor1;
            output_facnom1 =:comision_facnom1;
            output_factor2 =:comision_factor2;
            output_facnom2 =:comision_facnom2;
        end */
SUSPEND;    
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE COMISION_VENDEDOR TO  SYSDBA;

