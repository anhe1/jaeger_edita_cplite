/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 13072022 1421
    Purpose: procedimiento para obtener listado de precios por id del catalogo
------------------------------------------------------------------------------------------------------------- */
SET TERM ^ ;
CREATE PROCEDURE LPRECIO (
    INPUT_PRECIO_ID Integer,
    INPUT_DRCTR_ID Integer DEFAULT 0)
RETURNS (
    ESPEC_ID Integer,
    PRECIO_ID Integer,
    PARTIDA_ID Integer,
    VUNITARIO Numeric(18,4),
    VCOSTO Numeric(18,4) )
AS
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 13072022 1421
    Purpose: procedimiento para obtner listado de precios por id del catalogo
------------------------------------------------------------------------------------------------------------- */
    -- SI ID DEL DIRECTORIO ES MAYOR A CERO ENTONCES BUSCAMOS EL CATALOGO EN LA RELACION
    IF (:INPUT_DRCTR_ID > 0) THEN
        BEGIN
            SELECT FIRST 1 rel.RLCDRC_CTLPRCS_ID
            FROM rlcdrc rel
            where rel.rlcdrc_doc_id = 1086              -- CATALOGO DE RELACIONES DE LISTA DE PRECIOS 
            and rel.rlcdrc_drctr_id = :INPUT_DRCTR_ID   -- INDICE DEL DIRECTORIO
            AND rel.rlcdrc_a = 1                        -- SOLO REGISTROS ACTIVOS
                INTO :INPUT_PRECIO_ID;
        END
    -- VERIFICAMOS UN INDICE VALIDO, SINO ASIGNAMOS LA LISTA PREDETERMINADA
    IF (:INPUT_PRECIO_ID = 0) THEN
        BEGIN
            INPUT_PRECIO_ID = 1;
        END 
        
    -- CREAMOS LA CONSULTA DONDE LA LISTA PREDETERMINADA ES SOBRE ESCRITA POR LAS EXCEPCIONES DE LA LISTA DE ASIGNADA AL CLIENTE
    /*SELECT A.CTPRCP_ID, b.CTPRCP_ID, A.CTPRCP_A, A.CTPRCP_CTPRC_ID, B.CTPRCP_CTPRC_ID, A.CTPRCP_CTESPC_ID, B.CTPRCP_CTESPC_ID, A.CTPRCP_UNITC, A.CTPRCP_UNIT AS CTPRCP_UNITA, B.CTPRCP_UNIT AS CTPRCP_UNITB, */
    FOR
        SELECT
        IIF(B.CTPRCP_CTESPC_ID > 0, B.CTPRCP_CTESPC_ID, A.CTPRCP_CTESPC_ID) AS ESPEC_ID,
        IIF(B.CTPRCP_CTPRC_ID > 0, B.CTPRCP_CTPRC_ID, A.CTPRCP_CTPRC_ID) PRECIO_ID, 
        IIF(B.CTPRCP_ID > 0, B.CTPRCP_ID, A.CTPRCP_ID) PARTIDA_ID, 
        IIF(B.CTPRCP_UNIT > 0, B.CTPRCP_UNIT, A.CTPRCP_UNIT) UNITARIO, 
        A.CTPRCP_UNITC AS COSTO
        FROM CTPRCP A
        LEFT JOIN CTPRCP B ON A.CTPRCP_CTESPC_ID = B.CTPRCP_CTESPC_ID AND B.CTPRCP_A = 1 AND B.CTPRCP_CTPRC_ID = :INPUT_PRECIO_ID -- INDICE DEL CATALOGO DE PRECIOS
        WHERE A.CTPRCP_CTPRC_ID = 1 -- AQUI ES UNO QUE SIEMPRE ES LA LISTA PREDETERMINADA
        AND A.CTPRCP_A = 1          -- SOLO REGISTROS ACTIVOS
        ORDER BY A.CTPRCP_CTESPC_ID
        INTO :ESPEC_ID, :PRECIO_ID, :PARTIDA_ID, :VUNITARIO, :VCOSTO
    DO
        BEGIN
            -- RETORNAMOS LA LISTA
            SUSPEND;
        END
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE LPRECIO TO  SYSDBA;