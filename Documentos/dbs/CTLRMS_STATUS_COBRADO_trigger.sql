SET TERM ^ ;
CREATE TRIGGER CTLRMS_STATUS_COBRADO FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
declare variable descripcion varchar(120);
declare variable comisionde varchar(120);
declare variable multade varchar(120);
declare variable comisionPagar numeric(18,4);
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: anhe 191120211138
    Purpose: Cambio de STATUS
    Para obtener el cambio de status 'Cobrado' se considera:
------------------------------------------------------------------------------------------------------------- */
	if (new.ctlrms_doc_id = 26 
        and new.ctlrms_fcentrg is not null 
         and new.ctlrms_fccbrnz is not null 
          and new.ctlrms_sttsdcs_id <> 0 
           and old.ctlrms_sttsdcs_id = 4 
            and new.ctlrms_aut_id = 0) then
        begin
            
            SELECT p.DESCRIPCION, p.COMISIONDE, p.MULTADEL, p.COMISIONPAGAR
            FROM COMISION_CALCULAR(new.CTLRMS_CBRD$, new.CTLRMS_SBTTLIVA, new.CTLRMS_DSTRSA, new.CTLRMS_CTLGVDDR_ID) p
            into :descripcion, :comisionde, :multade, :comisionPagar;
            new.ctlrms_cmsn = :descripcion || ' Com: ' || :comisionde || ' Multa ' || :multade;
            new.ctlrms_cmsn$ = :comisionPagar;
        end
END^
SET TERM ; ^
