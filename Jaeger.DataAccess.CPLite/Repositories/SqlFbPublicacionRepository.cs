﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbPublicacionRepository : Abstractions.RepositoryMaster<PublicacionModel>, ISqlPublicacionRepository {
        public SqlFbPublicacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) { 
            this.User = user;
        }

        #region CRUD
        public int Insert(PublicacionModel item) {
            throw new NotImplementedException();
        }

        public int Update(PublicacionModel item) {
            throw new NotImplementedException();
        }

        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public PublicacionModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<PublicacionModel> GetList() {
            throw new NotImplementedException();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT CTMDLT.* FROM CTMDLT @condiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public bool Saveable(PublicacionModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO CTMDLT (CTMDLT_CTMDL_ID, CTMDLT_CTCLS_ID, CTMDLT_VIS, CTMDLT_SEC, CTMDLT_DSCRC, CTMDLT_DSCR, CTMDLT_ETQTS, CTMDLT_USR_M, CTMDLT_FM) 
                                                     VALUES (@CTMDLT_CTMDL_ID,@CTMDLT_CTCLS_ID,@CTMDLT_VIS,@CTMDLT_SEC,@CTMDLT_DSCRC,@CTMDLT_DSCR,@CTMDLT_ETQTS,@CTMDLT_USR_M,@CTMDLT_FM) MATCHING(CTMDLT_CTMDL_ID);"
            };
            model.Modifica = this.User;
            model.FechaModifica = DateTime.Now;
            sqlCommand.Parameters.AddWithValue("@CTMDLT_CTMDL_ID", model.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_CTCLS_ID", model.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_VIS", model.IsAvailable);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_SEC", model.Secuencia);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_DSCRC", model.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_DSCR", model.Contenido);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_ETQTS", model.Etiquetas);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_USR_M", model.Modifica);
            sqlCommand.Parameters.AddWithValue("@CTMDLT_FM", model.FechaModifica);
            return ExecuteTransaction(sqlCommand) > 0;
        }
    }
}
