﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class ResumenForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.SpinEjericio = new Telerik.WinControls.UI.RadSpinEditor();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.TResumen = new Telerik.WinControls.UI.CommandBarStripElement();
            this.LblResumen = new Telerik.WinControls.UI.CommandBarLabel();
            this.Resumen = new Telerik.WinControls.UI.CommandBarDropDownList();
            this.LblEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.Ejercicio = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Exportar = new Telerik.WinControls.UI.CommandBarButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadPivotGrid();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.radCommandBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEjericio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Controls.Add(this.SpinEjericio);
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1004, 55);
            this.radCommandBar1.TabIndex = 0;
            // 
            // SpinEjericio
            // 
            this.SpinEjericio.Location = new System.Drawing.Point(518, 4);
            this.SpinEjericio.Name = "SpinEjericio";
            this.SpinEjericio.Size = new System.Drawing.Size(55, 20);
            this.SpinEjericio.TabIndex = 4;
            this.SpinEjericio.TabStop = false;
            this.SpinEjericio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.TResumen});
            // 
            // TResumen
            // 
            this.TResumen.DisplayName = "commandBarStripElement1";
            this.TResumen.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.LblResumen,
            this.Resumen,
            this.LblEjercicio,
            this.Ejercicio,
            this.Actualizar,
            this.Exportar,
            this.Cerrar});
            this.TResumen.Name = "TResumen";
            this.TResumen.StretchHorizontally = true;
            // 
            // LblResumen
            // 
            this.LblResumen.DisplayName = "Resumen por:";
            this.LblResumen.Name = "LblResumen";
            this.LblResumen.Text = "Resumen por:";
            // 
            // Resumen
            // 
            this.Resumen.DisplayName = "commandBarDropDownList1";
            this.Resumen.DropDownAnimationEnabled = true;
            this.Resumen.MaxDropDownItems = 0;
            this.Resumen.Name = "Resumen";
            this.Resumen.Text = "";
            // 
            // LblEjercicio
            // 
            this.LblEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.LblEjercicio.Name = "LblEjercicio";
            this.LblEjercicio.Text = "Ejercicio:";
            // 
            // Ejercicio
            // 
            this.Ejercicio.DisplayName = "Ejercicio";
            this.Ejercicio.MaxSize = new System.Drawing.Size(55, 20);
            this.Ejercicio.MinSize = new System.Drawing.Size(55, 20);
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Text = "Selecciona";
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.CPLite.Properties.Resources.refresh_16;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Exportar
            // 
            this.Exportar.DisplayName = "Exportar";
            this.Exportar.DrawText = true;
            this.Exportar.Image = global::Jaeger.UI.CPLite.Properties.Resources.xls_16px;
            this.Exportar.Name = "Exportar";
            this.Exportar.Text = "Exportar";
            this.Exportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.CPLite.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // GridData
            // 
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 55);
            this.GridData.Name = "GridData";
            this.GridData.Size = new System.Drawing.Size(1004, 624);
            this.GridData.TabIndex = 1;
            // 
            // ResumenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 679);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "ResumenForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Ventas: Resumen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RemisionResumenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.radCommandBar1.ResumeLayout(false);
            this.radCommandBar1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEjericio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement TResumen;
        private Telerik.WinControls.UI.CommandBarLabel LblResumen;
        private Telerik.WinControls.UI.CommandBarLabel LblEjercicio;
        private Telerik.WinControls.UI.CommandBarHostItem Ejercicio;
        private Telerik.WinControls.UI.CommandBarButton Actualizar;
        private Telerik.WinControls.UI.CommandBarButton Exportar;
        private Telerik.WinControls.UI.CommandBarButton Cerrar;
        private Telerik.WinControls.UI.RadPivotGrid GridData;
        private Telerik.WinControls.UI.RadSpinEditor SpinEjericio;
        private Telerik.WinControls.UI.CommandBarDropDownList Resumen;
    }
}