﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbCuponRepository : Abstractions.RepositoryMaster<CuponModel>, ISqlCuponRepository {
        public SqlFbCuponRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { 
                CommandText = @"DELETE FROM TWCPN WHERE ((TWCPN_ID = @TWCPN_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@TWCPN_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CuponModel GetById(int index) {
            return this.GetList<CuponModel>(new List<IConditional> { new Conditional("TWCPN_ID", index.ToString()) }).FirstOrDefault(); 
        }

        public int Insert(CuponModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"INSERT INTO TWCPN (TWCPN_A, TWCPN_DESC, TWCPN_CLV, TWCPN_NOM, TWCPN_VGINI, TWCPN_VGTER, TWCPN_DESCNT, TWCPN_UMAX, TWCPN_UACT, TWCPN_MIN, TWCPN_FN, TWCPN_USR_N) VALUES (@TWCPN_ID, @TWCPN_A, @TWCPN_DESC, @TWCPN_CLV, @TWCPN_NOM, @TWCPN_VGINI, @TWCPN_VGTER, @TWCPN_DESCNT, @TWCPN_UMAX, @TWCPN_UACT, @TWCPN_MIN, @TWCPN_FN, @TWCPN_USR_N) RETURNING TWCPN_ID"
            };
            sqlCommand.Parameters.AddWithValue("@TWCPN_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@TWCPN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@TWCPN_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@TWCPN_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@TWCPN_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@TWCPN_VGINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@TWCPN_VGTER", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@TWCPN_DESCNT", item.Valor);
            sqlCommand.Parameters.AddWithValue("@TWCPN_UMAX", item.UsosMaximos);
            sqlCommand.Parameters.AddWithValue("@TWCPN_UACT", item.UsoActual);
            sqlCommand.Parameters.AddWithValue("@TWCPN_MIN", item.MinmoCompra);
            sqlCommand.Parameters.AddWithValue("@TWCPN_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@TWCPN_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@TWCPN_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@TWCPN_USR_M", item.Modifica);
            
            item.IdCupon = this.ExecuteScalar(sqlCommand);
            if (item.IdCupon > 0)
                return item.IdCupon;
            return 0;
        }

        public int Update(CuponModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"UPDATE TWCPN SET TWCPN_A = @TWCPN_A, TWCPN_DESC = @TWCPN_DESC, TWCPN_CLV = @TWCPN_CLV, TWCPN_NOM = @TWCPN_NOM, TWCPN_VGINI = @TWCPN_VGINI, TWCPN_VGTER = @TWCPN_VGTER, TWCPN_DESCNT = @TWCPN_DESCNT, TWCPN_UMAX = @TWCPN_UMAX, TWCPN_UACT = @TWCPN_UACT, TWCPN_MIN = @TWCPN_MIN, TWCPN_FM = @TWCPN_FM, TWCPN_USR_M = @TWCPN_USR_M WHERE (TWCPN_ID = @TWCPN_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@TWCPN_ID", item.IdCupon);
            sqlCommand.Parameters.AddWithValue("@TWCPN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@TWCPN_DESC", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@TWCPN_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@TWCPN_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@TWCPN_VGINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@TWCPN_VGTER", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@TWCPN_DESCNT", item.Valor);
            sqlCommand.Parameters.AddWithValue("@TWCPN_UMAX", item.UsosMaximos);
            sqlCommand.Parameters.AddWithValue("@TWCPN_UACT", item.UsoActual);
            sqlCommand.Parameters.AddWithValue("@TWCPN_MIN", item.MinmoCompra);
            sqlCommand.Parameters.AddWithValue("@TWCPN_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@TWCPN_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<CuponModel> GetList() {
            return this.GetList<CuponModel>(new List<IConditional>());
        }
        #endregion

        public CuponModel Save(CuponModel model) {
            if (model.IdCupon == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdCupon = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            throw new NotImplementedException();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM TWCPN @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}