﻿namespace Jaeger.UI.CPLite.Forms.Empresa {
    partial class AutorizarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutorizarForm));
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.Importe = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.IdStatusAutorizado = new Telerik.WinControls.UI.RadDropDownList();
            this.label3 = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new Telerik.WinControls.UI.RadLabel();
            this.IdAfectacion = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatusAutorizado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdAfectacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Etiqueta = "";
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = false;
            this.ToolBar.ShowAutorizar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = false;
            this.ToolBar.ShowExportarExcel = false;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowGuardar = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImagen = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowRemover = false;
            this.ToolBar.Size = new System.Drawing.Size(488, 30);
            this.ToolBar.TabIndex = 14;
            this.ToolBar.Guardar.Click += this.ToolBar_Guardar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_Cerrar_Click;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(324, 39);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(48, 18);
            this.radLabel1.TabIndex = 381;
            this.radLabel1.Text = "Importe:";
            // 
            // Importe
            // 
            this.Importe.Location = new System.Drawing.Point(378, 38);
            this.Importe.Name = "Importe";
            this.Importe.Size = new System.Drawing.Size(95, 20);
            this.Importe.TabIndex = 380;
            this.Importe.TabStop = false;
            this.Importe.Value = "0";
            // 
            // IdStatusAutorizado
            // 
            this.IdStatusAutorizado.Location = new System.Drawing.Point(223, 38);
            this.IdStatusAutorizado.Name = "IdStatusAutorizado";
            this.IdStatusAutorizado.Size = new System.Drawing.Size(95, 20);
            this.IdStatusAutorizado.TabIndex = 379;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(180, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 378;
            this.label3.Text = "Status:";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(79, 64);
            this.Nota.MaxLength = 100;
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(394, 20);
            this.Nota.TabIndex = 376;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 18);
            this.label4.TabIndex = 377;
            this.label4.Text = "Nota:";
            // 
            // IdAfectacion
            // 
            this.IdAfectacion.Location = new System.Drawing.Point(79, 38);
            this.IdAfectacion.Name = "IdAfectacion";
            this.IdAfectacion.Size = new System.Drawing.Size(95, 20);
            this.IdAfectacion.TabIndex = 383;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(12, 39);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(61, 18);
            this.radLabel2.TabIndex = 382;
            this.radLabel2.Text = "Afectación:";
            // 
            // AutorizarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 97);
            this.Controls.Add(this.IdAfectacion);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.Importe);
            this.Controls.Add(this.IdStatusAutorizado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Nota);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AutorizarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Autorización";
            this.Load += new System.EventHandler(this.AutorizarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatusAutorizado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdAfectacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Common.Forms.ToolBarStandarControl ToolBar;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCalculatorDropDown Importe;
        private Telerik.WinControls.UI.RadDropDownList IdStatusAutorizado;
        private Telerik.WinControls.UI.RadLabel label3;
        public Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel label4;
        private Telerik.WinControls.UI.RadDropDownList IdAfectacion;
        private Telerik.WinControls.UI.RadLabel radLabel2;
    }
}