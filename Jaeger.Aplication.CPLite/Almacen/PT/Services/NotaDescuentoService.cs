﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.ValueObjects;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.DataAccess.FB.CPLite.Repositories;

namespace Jaeger.Aplication.CPLite.Almacen.PT.Services {
    /// <summary>
    /// Nota de Descuento
    /// </summary>
    public class NotaDescuentoService : INotaDescuentoService {
        #region declaraciones
        protected ISqlNotaDescuentoRepository notaDescuentoRepository;
        protected ISqlNotaDescuentoRelacionRepository relacionRepository;
        protected ISqlNotaDescuentoStatusRepository statusRepository;
        #endregion

        public NotaDescuentoService() {
            this.notaDescuentoRepository = new SqlFbNotaDescuentoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.relacionRepository = new SqlFbNotaDescuentoRelacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.statusRepository = new SqlFbNotaDescuentoStatusRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public NotaDescuentoDetailModel GetById(int index) {
            return new NotaDescuentoDetailModel();
        }

        public NotaDescuentoDetailModel Save(NotaDescuentoDetailModel model) {
            model = this.notaDescuentoRepository.Save(model);
            if (model.IdNota > 0) {
                for (int i = 0; i < model.Partidas.Count; i++) {
                    model.Partidas[i].IdNota = model.IdNota;
                    if (this.relacionRepository.Saveable(model.Partidas[i]) > 0) {


                    }
                }
            }
            return model;
        }

        public bool Cancelar(NotaDescuentoDetailModel model) {
            model.Cancela = ConfigService.Piloto.Clave;
            model.FechaCancela = DateTime.Now;
            return this.notaDescuentoRepository.Cancelar(model);
        }

        public bool Cancelar(NotaDescuentoStatusModel model) {
            var isOk = this.statusRepository.Save(model);
            return isOk;
        }

        public string Verificar(NotaDescuentoDetailModel model) {
            if (model.Partidas.Count > 0) {
                var ids = model.Partidas.Select(it => it.IdComprobante).ToList();
                var resultado = this.relacionRepository.GetList<NotaDescuentoView>(new List<Conditional> {
                        new Conditional("NTDSCP_ALMPT_ID", string.Join(",",ids), ConditionalTypeEnum.In),
                        new Conditional("NTDSCP_A", "1")
                    });

                if (resultado != null) {
                    return "Algunas notas de devolución ya se encuentran relacionadas con otra nota de descuento, no es posible crear esta nota de crédito. ";
                }
            }
            return null;
        }

        public BindingList<NotaDescuentoRelacionDetailModel> GetPartidas() {
            return new BindingList<NotaDescuentoRelacionDetailModel>(
                this.relacionRepository.GetList<NotaDescuentoRelacionDetailModel>(
                    new List<Conditional>() {
                        new Conditional("ALMPT_STTS_ID", ((int)NotaDescuentoStatusEnum.Impreso).ToString())
                    }).ToList());
        }

        #region metodos publicos
        public static NotaDescuentoDetailModel GetNew() {
            return new NotaDescuentoDetailModel();
        }

        /// <summary>
        /// obtener listado de status de remisiones
        /// </summary>
        public static List<StatusModel> GetStatus(bool all = true) {
            var response = EnumerationExtension.GetEnumToClass<StatusModel, NotaDescuentoStatusEnum>().ToList();
            if (all == false) {
                // Cancelado = 0, Pendiente = 1, Autorizado = 3, Aplicado = 4
                int[] idStatus = new int[] { 0, 1, 3, 4 };
                return response.Where(it => idStatus.Contains(it.Id)).ToList();
            }
            return response;
        }

        /// <summary>
        /// listado de motivos para la nota de descuento
        /// </summary>
        public static List<NotaDescuentoMotivoModel> GetMotivos() {
            return EnumerationExtension.GetEnumToClass<NotaDescuentoMotivoModel, NotaDescuentoMotivoEnum>().ToList();
        }

        /// <summary>
        /// listado de motivos para la nota de descuento
        /// </summary>
        public static NotaDescuentoMotivoModel GetMotivos(int index) {
            var d1 = EnumerationExtension.GetEnumToClass<NotaDescuentoMotivoModel, NotaDescuentoMotivoEnum>().ToList();
            return d1.Where(it => it.Id == index).FirstOrDefault();
        }

        public static List<NotaDescuentoMotivoCancelacion> GetMotivosCancelacion() {
            return EnumerationExtension.GetEnumToClass<NotaDescuentoMotivoCancelacion, NotaDescuentoMotivoCancelacionEnum>().ToList();
        }
        #endregion
    }
}
