﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.CPLite.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Ventas.ValueObjects;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    internal class NotaDescuentoGridControl : Common.Forms.GridCommonControl {
        protected internal GridViewTemplate _Partidas = new GridViewTemplate() { Caption = "Documentos relacionados" };
        protected internal GridViewTemplate _Autorizacion = new GridViewTemplate() { Caption = "Autorización" };
        protected internal GridViewTemplate _NotaDevolucion = new GridViewTemplate() { Caption = "Nota de devolución" };
        public NotaDescuentoGridControl() : base() {
            this.Load += NotaDescuentoGridControl_Load;
        }

        private void NotaDescuentoGridControl_Load(object sender, EventArgs e) {
            INotaDescuentoGridBuilder view = new NotaDescuentoGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
            this._Partidas.Columns.AddRange(view.Templetes().Documentos().Build());
            this.GridData.Templates.Add(this._Partidas);
            this._Partidas.Standard();
            this.GridData.RowSourceNeeded += GridData_RowSourceNeeded;
            this._Autorizacion.HierarchyDataProvider = new GridViewEventDataProvider(this._Autorizacion);
            this._Partidas.HierarchyDataProvider = new GridViewEventDataProvider(this._Partidas);
        }

        #region acciones del grid
        protected override void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var seleccionado = this.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
            if (seleccionado != null) {
                if (e.Template.Caption == this._Autorizacion.Caption) {
                    var _row = e.Template.Rows.NewRow();
                } else if (e.Template.Caption == this._Partidas.Caption) {
                    if (seleccionado.Partidas.Count > 0) {
                        foreach (var item in seleccionado.Partidas) {
                            var _row = e.Template.Rows.NewRow();
                            _row.Cells["Folio"].Value = item.Folio;
                            _row.Cells["IdPedido"].Value = item.IdPedido;
                            _row.Cells["Clave"].Value = item.Clave;
                            _row.Cells["Nombre"].Value = item.Receptor;
                            _row.Cells["FechaEmision"].Value = item.FechaEmision;
                            _row.Cells["Total"].Value = item.GTotal;
                            _row.Cells["Creo"].Value = item.Creo;
                            _row.Cells["FechaNuevo"].Value = item.FechaNuevo;
                            e.SourceCollection.Add(_row);
                        }
                    }
                }
            }
        }

        protected override void GridData_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "IdStatus") {
                    var seleccionado = (NotaDescuentoDetailModel)this.GridData.ReturnRowSelected();
                    switch (seleccionado.Status) {
                        case NotaDescuentoStatusEnum.Cancelado:
                            e.Cancel = true;
                            break;
                        case NotaDescuentoStatusEnum.Autoizado:
                            break;
                        case NotaDescuentoStatusEnum.Aplicado:
                            break;
                        default:
                            break;
                    }
                } else if (e.Column.Name == "Referencia") {
                    var seleccionado = (NotaDescuentoDetailModel)this.GridData.ReturnRowSelected();
                    if (seleccionado.Referencia != null) {
                        if (seleccionado.Referencia.Length > 0) {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        protected override void GridData_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if ((string)e.Column.Tag == "Actualizar") {
                        using (var espera = new Waiting1Form(this.Aplicar)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.Tag != null) {
                            if ((bool)this.Tag == false) {
                                MessageBox.Show("Error");
                                this.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        protected override void GridData_CellValidating(object sender, CellValidatingEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "IdStatus") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                        } else {
                            if ((int)e.Value == (int)NotaDescuentoStatusEnum.Cancelado) {
                                if (this.Cancelar.Enabled == false) {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                    RadMessageBox.Show(this, "Properties.Resources.Msg_Banco_Movimiento_Cancelar_Error", "Información", MessageBoxButtons.OK);
                                    return;
                                }

                                if (RadMessageBox.Show(this, "Properties.Resources.Msg_Banco_Movimiento_Cancelar", "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                                    e.Cancel = true;
                                    this.GridData.CancelEdit();
                                    return;
                                } else {
                                    if ((int)e.OldValue != (int)NotaDescuentoStatusEnum.Cancelado) {
                                        e.Cancel = false;
                                        e.Column.Tag = "Actualizar";
                                    }
                                }
                            } else if ((int)e.Value == (int)NotaDescuentoStatusEnum.Aplicado) {
                                var seleccionado = e.Row.DataBoundItem as NotaDescuentoDetailModel;
                                if (seleccionado.IsEditable == false) {
                                    e.Column.Tag = "Actualizar";
                                    e.Cancel = false;
                                } else {
                                    RadMessageBox.Show(this, "Properties.Resources.Msg_Banco_Status_Error", "Información", MessageBoxButtons.OK, RadMessageIcon.Error, MessageBoxDefaultButton.Button1);
                                    e.Cancel = true;
                                }
                                return;
                            } else if ((int)e.Value == (int)NotaDescuentoStatusEnum.Pendiente) {
                                return;
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
