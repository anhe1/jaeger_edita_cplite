﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Aplication.CPLite.Ventas.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class PedidoCatalogoForm : UI.Tienda.Forms.PedidoCatalogoForm {
        protected internal IVendedoresService _Service;
        protected internal RadMenuItem menuContextualRemisionar = new RadMenuItem { Text = "Remisionar" };
        protected internal List<RemisionDetailModel> remisiones;

        public PedidoCatalogoForm(UIMenuElement menuElement) : base() {
            this.TPedido.IsCosto = false;
            this.TPedido.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += this.PedidoCatalogoForm_Load;
        }

        private void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Pedidos";
            this.TPedido.Service = new Aplication.CPLite.Tienda.Services.PedidosService();
            this._Service = new VendedoresService();
            this.menuContextualRemisionar.Click += this.MenuContextualRemisionar_Click;
            this.menuContextual.Items.Add(this.menuContextualRemisionar);
        }
       
        private void MenuContextualRemisionar_Click(object sender, EventArgs e) {
            //if (this.dataGridPedidos.CurrentRow != null) {
            //    var seleccionado = this.dataGridPedidos.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
            //    var _Partidas = seleccionado.Partidas.OrderBy(it => it.Producto).OrderBy(it => it.Tamanio).ToList();
            //    if (seleccionado != null) {
            //        var _remision = new RemisionDetailModel();
            //        _remision.IdCliente = seleccionado.IdDirectorio;
            //        _remision.IdPedido = seleccionado.IdPedido;
            //        _remision.IdDomicilio = seleccionado.IdDireccion;
            //        _remision.IdVendedor = seleccionado.IdVendedor;
            //        foreach (var item in _Partidas) {
            //            var _concepto = new RemisionConceptoDetailModel {
            //                IdModelo = item.IdModelo,
            //                Activo = true,
            //                CantidadS = item.Cantidad,
            //                Especificacion = item.Especificacion,
            //                FechaNuevo = DateTime.Now,
            //                Creo = ConfigService.Piloto.Clave,
            //                IdProducto = item.IdProducto,
            //                IdUnidad = item.IdUnidad,
            //                Marca = item.Marca,
            //                Tamanio = item.Tamanio,
            //                Producto = item.Producto,
            //                Catalogo = item.Catalogo,
            //                Unidad = item.Unidad
            //            };
            //            _remision.Conceptos.Add(_concepto);
            //        }
            //        //var _nuevo = new Almacen.RemisionFiscalForm(_remision, 0) { MdiParent = this.ParentForm };
            //        //_nuevo.Show();
            //    }
            //}
        }

        public override void Vendedores() {
            this.TPedido.Vendedores = this._Service.GetList(false).ToList();
        }
    }
}
