﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Almacen.PT.Services;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Ventas.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class NotaDescuentoCatalogoForm : RadForm {
        #region declaraciones
        protected INotasDescuentoService service;
        private BindingList<NotaDescuentoDetailModel> _DataSource;
        private Domain.Base.ValueObjects.UIAction _permisos;
        #endregion

        #region constructor
        public NotaDescuentoCatalogoForm() {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public NotaDescuentoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }
        #endregion

        private void NotaDescuentoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Cliente: Nota de Descuento";
            this.service = new NotasDescuentoService();
            this.TNota.Permisos = this._permisos;

            this.TNota.Actualizar.Click += this.TNota_Actualizar_Click;
            this.TNota.Nuevo.Click += this.TNota_Nuevo_Click;
            this.TNota.Editar.Click += this.TNota_Editar_Click;
            this.TNota.Cancelar.Click += this.TNota_Cancelar_Click;
            this.TNota.Imprimir.Click += this.TNota_Imprimir_Click;
            this.TNota.Cerrar.Click += this.TNota_Cerrar_Click;

            this.TNota.GridData.CellBeginEdit += new GridViewCellCancelEventHandler(this.RadGridMovimiento_CellBeginEdit);
            this.TNota.GridData.CellEndEdit += new GridViewCellEventHandler(this.RadGridMovimiento_CellEndEdit);
            this.TNota.GridData.CellValidating += new CellValidatingEventHandler(this.RadGridMovimiento_CellValidating);
        }

        #region barra de herramientas
        private void TNota_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TNota.GridData.DataSource = this._DataSource;
        }

        private void TNota_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TNota_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new NotaDescuentoForm(null);
            _nuevo.ShowDialog(this);
        }

        private void TNota_Editar_Click(object sender, EventArgs e) {
            if (this.TNota.GridData.CurrentRow != null) {
                var _seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (_seleccionado != null) {
                    var _editar = new NotaDescuentoForm(_seleccionado);
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TNota_Cancelar_Click(object sender, EventArgs e) {
            if (this.TNota.GridData.CurrentRow != null) {
                var _seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (_seleccionado == null)
                    return;
                if (_seleccionado.Status == NotaDescuentoStatusEnum.Cancelado) { return; }
                if (RadMessageBox.Show(this, "¿Esta seguro de cancelar? Esta acción no se puede revertir.", "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                    var cancelarForm = new NotaDescuentoCancelarForm(_seleccionado);
                    cancelarForm.Selected += CancelarForm_Selected;
                    cancelarForm.ShowDialog(this);
                }
            }
        }

        private void CancelarForm_Selected(object sender, NotaDescuentoStatusModel e) {
            if (e != null) {
                var seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (seleccionado != null) {
                    seleccionado.IdStatus = e.IdStatusB;
                    seleccionado.FechaCancela = e.FechaNuevo;
                    seleccionado.Cancela = e.User;
                    seleccionado.TagStatus = e;
                }
                using (var espera = new Waiting1Form(this.Cancelando)) {
                    espera.Text = "Solicitando cancelación ...";
                    espera.ShowDialog(this);
                }
            }
        }

        private void TNota_Imprimir_Click(object sender, EventArgs e) {
            if (this.TNota.GridData.CurrentRow != null) {
                var _seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (_seleccionado != null) {
                    var motivo = NotaDescuentoService.GetMotivos(_seleccionado.IdMotivo);
                    var printer = new NotaDescuentoPrinter(_seleccionado) {
                        CvMotivo = motivo.Descriptor
                    };
                    var _editar = new ReporteForm(printer);
                    _editar.ShowDialog(this);
                }
            }
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
        }

        private void RadGridMovimiento_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "IdStatus") {
                    var seleccionado = (NotaDescuentoDetailModel)this.TNota.GridData.ReturnRowSelected();
                    switch (seleccionado.Status) {
                        case NotaDescuentoStatusEnum.Cancelado:
                            e.Cancel = true;
                            break;
                        case NotaDescuentoStatusEnum.Autoizado:
                            break;
                        case NotaDescuentoStatusEnum.Aplicado:
                            break;
                        default:
                            break;
                    }
                } else if (e.Column.Name == "Referencia") {
                    var seleccionado = (NotaDescuentoDetailModel)this.TNota.GridData.ReturnRowSelected();
                    if (seleccionado.Referencia != null) {
                        if (seleccionado.Referencia.Length > 0) {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void RadGridMovimiento_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if ((string)e.Column.Tag == "Actualizar") {
                        using (var espera = new Waiting1Form(this.Aplicar)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.TNota.Tag != null) {
                            if ((bool)this.TNota.Tag == false) {
                                MessageBox.Show("Error");
                                this.TNota.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        private void RadGridMovimiento_CellValidating(object sender, CellValidatingEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "IdStatus") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                        } else {
                            if ((int)e.Value == (int)NotaDescuentoStatusEnum.Cancelado) {
                                if (this.TNota.Cancelar.Enabled == false) {
                                    e.Cancel = true;
                                    this.TNota.GridData.CancelEdit();
                                    RadMessageBox.Show(this, "Este documento actualmente esta cancelado.", "Información", MessageBoxButtons.OK);
                                    return;
                                }

                                if (RadMessageBox.Show(this, "¿Esta seguro de cancelar? Esta acción no se puede revertir.", "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                                    e.Cancel = true;
                                    this.TNota.GridData.CancelEdit();
                                    return;
                                } else {
                                    if ((int)e.OldValue != (int)NotaDescuentoStatusEnum.Cancelado) {
                                        e.Cancel = false;
                                        e.Column.Tag = "Actualizar";
                                    }
                                }
                            } else if ((int)e.Value == (int)NotaDescuentoStatusEnum.Aplicado) {
                                var seleccionado = e.Row.DataBoundItem as NotaDescuentoDetailModel;
                                if (seleccionado.IsEditable == false) {
                                    e.Column.Tag = "Actualizar";
                                    e.Cancel = false;
                                } else {
                                    RadMessageBox.Show(this, "Properties.Resources.Msg_Banco_Status_Error", "Información", MessageBoxButtons.OK, RadMessageIcon.Error, MessageBoxDefaultButton.Button1);
                                    e.Cancel = true;
                                }
                                return;
                            } else if ((int)e.Value == (int)NotaDescuentoStatusEnum.Pendiente) {
                                return;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this._DataSource = this.service.GetList<NotaDescuentoDetailModel>(new Domain.Ventas.Builder.NotaDescuentoQueryBuilder().Year(this.TNota.GetEjercicio()).Month(this.TNota.GetPeriodo()).Build());
        }

        private void Aplicar() {
            var _seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
            this.Tag = this.service.Cancelar(_seleccionado);
        }

        public virtual void Cancelando() {
            var _seleccionado = this.TNota.GridData.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
            this.Tag = this.service.Cancelar(_seleccionado.TagStatus);
        }
        #endregion
    }
}
