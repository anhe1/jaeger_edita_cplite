﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.CPLite {
    static class Program {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ManagerPathService.CreatePaths();
            
            using (var login = new Forms.LoginForm(args)) {
                login.ShowDialog();
            }

            if (!(ConfigService.Synapsis == null) & !(ConfigService.Piloto == null)) {
                Application.Run(new Forms.MainRibbonForm());
            }
        }
    }
}
