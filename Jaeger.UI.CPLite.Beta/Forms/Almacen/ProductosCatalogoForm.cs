﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Almacen.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class ProductosCatalogoForm : UI.Tienda.Forms.ProductosCatalogoForm {
        #region declaraciones
        protected internal UIAction _permisos;
        protected internal Aplication.Almacen.Services.ImportacionService ImportacionService;
        protected internal RadMenuItem _Importar = new RadMenuItem { Text = "Importar" };
        #endregion

        public ProductosCatalogoForm(UIMenuElement menuElement) : base() {
            this._permisos = new UIAction(menuElement.Permisos);
            this.Load += this.ProductosCatalogoForm_Load;
        }

        private void ProductosCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Catálogo de Productos";
            this.TProductos.Herramientas.Items.Add(this._Importar);
            // solo se podra modificar la categoria por el formulario de edicion
            //this.GProducto.AllowEditRow = this._permisos.Editar;
            this.GModelos.AllowEditRow = this._permisos.Editar;
            this.gVariante.AllowEditRow = this._permisos.Editar;
            this.TProductos.Nuevo.Enabled = this._permisos.Agregar;
            this.TProductos.Editar.Enabled = this._permisos.Editar;
            this.TProductos.Remover.Enabled = this._permisos.Remover;
            this.TModelos.Nuevo.Enabled = this._permisos.Agregar;
            this.TModelos.Editar.Enabled = this._permisos.Editar;
            this.TModelos.Remover.Enabled = this._permisos.Remover;
            this.TModelos.Autorizar.Enabled = this._permisos.Autorizar;
            this.TDisponible.Nuevo.Enabled = this._permisos.Agregar;
            this.TDisponible.Editar.Enabled = this._permisos.Editar;
            this.TDisponible.Remover.Enabled = this._permisos.Remover;
            this._Importar.Click += Importar_Click;
            this.service = new CatalogoProductoService(AlmacenEnum.PT);
            this.ImportacionService = new Aplication.Almacen.Services.ImportacionService();
        }

        private void Importar_Click(object sender, EventArgs e) {
            var importar = new ModelosImportarForm(this.service);
            importar.ShowDialog(this);
        }
    }
}
