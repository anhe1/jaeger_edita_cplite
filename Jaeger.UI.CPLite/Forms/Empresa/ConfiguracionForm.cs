﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Empresa {
    public class ConfiguracionForm : UI.Empresa.Forms.ConfiguracionForm {
        public ConfiguracionForm(UIMenuElement menuElement) : base() { 
            this.radPropertyGrid1.ReadOnly = true;
        }
    }
}
