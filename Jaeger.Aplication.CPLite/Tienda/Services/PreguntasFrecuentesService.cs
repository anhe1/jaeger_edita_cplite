﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.DataAccess.FB.CPLite.Repositories;

namespace Jaeger.Aplication.CPLite.Tienda.Services {
    public class PreguntasFrecuentesService : Jaeger.Aplication.Tienda.Services.PreguntasFrecuentesService, IPreguntasFrecuentesService {

        public PreguntasFrecuentesService() {
            this.sqlRepository = new SqlFbFAQRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}