﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Ventas.ValueObjects;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.UI.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class NotaDescuentoCatalogoForm : RadForm {
        protected INotasDescuentoService service;
        private BindingList<NotaDescuentoDetailModel> notas;
        private Domain.Base.ValueObjects.UIAction _permisos;

        #region constructor
        public NotaDescuentoCatalogoForm() {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction();
        }

        public NotaDescuentoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }
        #endregion

        private void NotaDescuentoCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new NotasDescuentoService();
            this.dataGridNota.TelerikGridCommon();
            this.dataGridNota.Columns.AddRange(GridNotaDescuentoService.GetNotaDescuentoColumns());

            this.gridNotaDevolucion.TelerikTemplateCommon();
            this.gridNotaDevolucion.Columns.AddRange(GridValeAlmacenService.GetDevolucionColumnsView());
            this.gridAutorizacion.TelerikTemplateCommon();
            this.gridAutorizacion.Columns.AddRange(GridNotaDescuentoService.GetColumnsStatus());
            this.dataGridNota.RowSourceNeeded += GridData_RowSourceNeeded;
            this.gridAutorizacion.HierarchyDataProvider = new GridViewEventDataProvider(this.gridAutorizacion);
            this.gridNotaDevolucion.HierarchyDataProvider = new GridViewEventDataProvider(this.gridNotaDevolucion);
            this.TNota.Nuevo.Enabled = this._permisos.Agregar;
            this.TNota.Cancelar.Enabled = this._permisos.Cancelar;
            this.TNota.Editar.Enabled = this._permisos.Editar;
            this.gridAutorizacion.AllowEditRow = this._permisos.Editar;

            var idMotivo = this.dataGridNota.Columns["IdMotivo"] as GridViewComboBoxColumn;
            idMotivo.DataSource = this.service.GetMotivos();
            idMotivo.DisplayMember = "Descriptor";
            idMotivo.ValueMember = "Id";

            this.dataGridNota.CellBeginEdit += new GridViewCellCancelEventHandler(this.RadGridMovimiento_CellBeginEdit);
            this.dataGridNota.CellEndEdit += new GridViewCellEventHandler(this.RadGridMovimiento_CellEndEdit);
            this.dataGridNota.CellValidating += new CellValidatingEventHandler(this.RadGridMovimiento_CellValidating);
        }

        #region barra de herramientas
        private void TNota_Actualizar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            var IdStatus = this.dataGridNota.Columns["IdStatus"] as GridViewComboBoxColumn;
            IdStatus.DataSource = this.service.GetStatus();
            IdStatus.DisplayMember = "Descripcion";
            IdStatus.ValueMember = "Id";

            var IdStatusA = this.gridAutorizacion.Columns["IdStatus"] as GridViewComboBoxColumn;
            IdStatusA.DataSource = this.service.GetStatus();
            IdStatusA.DisplayMember = "Descripcion";
            IdStatusA.ValueMember = "Id";

            this.dataGridNota.DataSource = this.notas;
        }

        private void TNota_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TNota_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new NotaDescuentoForm(null);
            _nuevo.ShowDialog(this);
        }

        private void TNota_Editar_Click(object sender, EventArgs e) {
            if (this.dataGridNota.CurrentRow != null) {
                var _seleccionado = this.dataGridNota.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (_seleccionado != null) {
                    var _editar = new NotaDescuentoForm(_seleccionado);
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TNota_Cancelar_Click(object sender, EventArgs e) {
            if (this.dataGridNota.CurrentRow != null) {
                var _seleccionado = this.dataGridNota.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (_seleccionado.Status == NotaDescuentoStatusEnum.Cancelado) { return; }
                if (RadMessageBox.Show(this, Properties.Resources.Msg_Banco_Movimiento_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                    _seleccionado.Status = NotaDescuentoStatusEnum.Cancelado;
                    using (var espera = new Waiting1Form(this.Aplicar)) {
                        espera.Text = "Cancelando ...";
                        espera.ShowDialog(this);
                    }
                    if (this.TNota.Tag != null) {
                        if ((bool)this.TNota.Tag == false) {
                            MessageBox.Show("Error");
                            this.TNota.Tag = null;
                        }
                    }
                }
            }
        }

        private void TNota_Imprimir_Click(object sender, EventArgs e) {
            if (this.dataGridNota.CurrentRow != null) {
                var _seleccionado = this.dataGridNota.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
                if (_seleccionado != null) {
                    var motivo = NotaDescuentoService.GetMotivos(_seleccionado.IdMotivo);
                    var printer = new NotaDescuentoPrinter(_seleccionado);
                    printer.CvMotivo = motivo.Descriptor;
                    var _editar = new ReporteForm(printer);
                    _editar.ShowDialog(this);
                }
            }
        }

        private void TNota_Filtro_Click(object sender, EventArgs e) {
            this.dataGridNota.ActivateFilterRow(this.TNota.Filtro.ToggleState);
        }
        #endregion

        #region acciones del grid
        public virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var seleccionado = this.dataGridNota.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
            if (seleccionado != null) {
                if (e.Template.Caption == this.gridAutorizacion.Caption) {
                    var _row = e.Template.Rows.NewRow();
                    _row.Cells[GridCommonService.ColCboIdStatus.Name].Value = seleccionado.IdStatus;
                    _row.Cells[GridRemisionService.ClaveDeMotivo.Name].Value = seleccionado.CvMotivo;
                    _row.Cells[GridCommonService.ColNota.Name].Value = seleccionado.NotaCancelacion;
                    _row.Cells[GridCommonService.ColCreo.Name].Value = seleccionado.Cancela;
                    _row.Cells[GridCommonService.ColFechaNuevo.Name].Value = seleccionado.FechaCancela;
                    e.SourceCollection.Add(_row);
                } else if (e.Template.Caption == this.gridNotaDevolucion.Caption) {
                    if (seleccionado.Partidas.Count > 0) {
                        foreach (var item in seleccionado.Partidas) {
                            var _row = e.Template.Rows.NewRow();
                            _row.Cells[GridCommonService.ColFolio.Name].Value = item.Folio;
                            _row.Cells[GridCommonService.ColIdPedido.Name].Value = item.IdPedido;
                            _row.Cells[GridValeAlmacenService.ColClave.Name].Value = item.Clave;
                            _row.Cells[GridValeAlmacenService.ColRepcetor.Name].Value = item.Receptor;
                            _row.Cells[GridCommonService.ColFechaEmision.Name].Value = item.FechaEmision;
                            _row.Cells[GridCommonService.ColTotal.Name].Value = item.GTotal;
                            _row.Cells[GridCommonService.ColCreo.Name].Value = item.Creo;
                            _row.Cells[GridCommonService.ColFechaNuevo.Name].Value = item.FechaNuevo;
                            e.SourceCollection.Add(_row);
                        }
                    }
                }
            }
        }

        private void RadGridMovimiento_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "IdStatus") {
                    var seleccionado = (NotaDescuentoDetailModel)this.dataGridNota.ReturnRowSelected();
                    switch (seleccionado.Status) {
                        case NotaDescuentoStatusEnum.Cancelado:
                            e.Cancel = true;
                            break;
                        case NotaDescuentoStatusEnum.Autoizado:
                            break;
                        case NotaDescuentoStatusEnum.Aplicado:
                            break;
                        default:
                            break;
                    }
                }
                else if (e.Column.Name == "Referencia") {
                    var seleccionado = (NotaDescuentoDetailModel)this.dataGridNota.ReturnRowSelected();
                    if (seleccionado.Referencia != null) {
                        if (seleccionado.Referencia.Length > 0) {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void RadGridMovimiento_CellEndEdit(object sender, GridViewCellEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "Status") {
                    if ((string)e.Column.Tag == "Actualizar") {
                        using (var espera = new Waiting1Form(this.Aplicar)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.TNota.Tag != null) {
                            if ((bool)this.TNota.Tag == false) {
                                MessageBox.Show("Error");
                                this.TNota.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        private void RadGridMovimiento_CellValidating(object sender, CellValidatingEventArgs e) {
            if (!(e.Row is GridViewFilteringRowInfo)) {
                if (e.Column.Name == "IdStatus") {
                    if (e.ActiveEditor != null) {
                        if (e.OldValue == e.Value) {
                            e.Cancel = true;
                        }
                        else {
                            if ((int)e.Value == (int)NotaDescuentoStatusEnum.Cancelado) {
                                if (this.TNota.Cancelar.Enabled == false) {
                                    e.Cancel = true;
                                    this.dataGridNota.CancelEdit();
                                    RadMessageBox.Show(this, Properties.Resources.Msg_Banco_Movimiento_Cancelar_Error, "Información", MessageBoxButtons.OK);
                                    return;
                                }

                                if (RadMessageBox.Show(this, Properties.Resources.Msg_Banco_Movimiento_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info, MessageBoxDefaultButton.Button2) == DialogResult.No) {
                                    e.Cancel = true;
                                    this.dataGridNota.CancelEdit();
                                    return;
                                }
                                else {
                                    if ((int)e.OldValue != (int)NotaDescuentoStatusEnum.Cancelado) {
                                        e.Cancel = false;
                                        e.Column.Tag = "Actualizar";
                                    }
                                }
                            }
                            else if ((int)e.Value == (int)NotaDescuentoStatusEnum.Aplicado) {
                                var seleccionado = e.Row.DataBoundItem as NotaDescuentoDetailModel;
                                if (seleccionado.IsEditable == false) {
                                    e.Column.Tag = "Actualizar";
                                    e.Cancel = false;
                                }
                                else {
                                    RadMessageBox.Show(this, Properties.Resources.Msg_Banco_Status_Error, "Información", MessageBoxButtons.OK, RadMessageIcon.Error, MessageBoxDefaultButton.Button1);
                                    e.Cancel = true;
                                }
                                return;
                            }
                            else if ((int)e.Value == (int)NotaDescuentoStatusEnum.Pendiente) {
                                return;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.notas = this.service.GetList(this.TNota.GetEjercicio(), this.TNota.GetMes());
        }

        private void Aplicar() {
            var _seleccionado = this.dataGridNota.CurrentRow.DataBoundItem as NotaDescuentoDetailModel;
            this.Tag = this.service.Cancelar(_seleccionado);
        }
        #endregion
    }
}
