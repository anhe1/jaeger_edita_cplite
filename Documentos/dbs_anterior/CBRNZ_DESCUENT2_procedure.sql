SET TERM ^ ;
CREATE PROCEDURE CBRNZ_DESCUENT2 (
    INPUT_DRCTR_ID DOM_ID DEFAULT 0,
    INPUT_DIASTRA DOM_ID DEFAULT 0,
    INPUT_CATALOG DOM_ID DEFAULT 0,
    INPUT_FECEMSN DOM_FH,
    INPUT_AVGCMPR DOM_MONEDA DEFAULT 0 )
RETURNS (
    OUTPUT_CMSDSC_ID DOM_ID,
    OUTPUT_CATALOGO DOM_STR50,
    OUTPUT_FCHINI DOM_FH,
    OUTPUT_FCHVIG DOM_FH,
    OUTPUT_DESCUENT DOM_STR50,
    OUTPUT_FACTOR DOM_MONEDA,
    OUTPUT_COMPRA_RNG1 DOM_MONEDA,
    OUTPUT_COMPRA_RNG2 DOM_MONEDA,
    OUTPUT_DIAS_RNG1 DOM_MONEDA,
    OUTPUT_DIAS_RNG2 DOM_MONEDA )
AS
begin
/*
elabora: anhe 10112012
proposito: determinar el tipo de descuento que fue asigando y que descpues de los parametros introducidos,
revisiones:
*/
    if ( input_diastra <= 0 ) then input_diastra = 1;
    if ( input_avgcmpr <= 0 ) then input_avgcmpr = 1;
    select first 1 des.cmsdsc_id, des.cmsdsc_nom, des.cmsdsc_fchini, des.cmsdsc_fchvig, tbl2.tbrngs_nom, tbl2.tbrngs_factor, tbl1.tbrngs_rng1, tbl1.tbrngs_rng2, tbl2.tbrngs_rng1, tbl2.tbrngs_rng2
    from    rlcdrc rel , cmsdsc des, tbrngs tbl1, tbrngs tbl2
    where   rel.rlcdrc_doc_id = 1039
        and des.cmsdsc_doc_id = 1039                                    --< catalogo de descuentos
        and tbl1.tbrngs_doc_id = 1040                                   --< $ rangos de montos de compra
        and tbl2.tbrngs_doc_id = 1041                                   --< % por d�as de cobranza descuento a aplicar
        and des.cmsdsc_id = rel.rlcdrc_dscn_id                          --< relacion de directorio y catalogo asignado
        and tbl1.tbrngs_cmsdsc_id = des.cmsdsc_id                       --< relacion descuentos y rangos de compra
        and tbl1.tbrngs_id = tbl2.tbrngs_tbrngs_id                      --< dependencias entre ambas tablas
        and rel.rlcdrc_a = 1
        and des.cmsdsc_a = 1
        and tbl1.tbrngs_a = 1
        and tbl2.tbrngs_a = 1
        and rel.rlcdrc_drctr_id = :input_drctr_id                       --< indice del directorio
        and des.cmsdsc_opc_id1 = :input_catalog                         --< catalogo buscado
        and :input_avgcmpr between tbl1.tbrngs_rng1 and tbl1.tbrngs_rng2  --< promedio a buscar
        and :input_diastra between tbl2.tbrngs_rng1 and tbl2.tbrngs_rng2  --< d�as transcurridos para la cobranza
        and :input_fecemsn between des.cmsdsc_fchini and des.cmsdsc_fchvig   --< dentro de la vigencia
    order by rel.rlcdrc_id desc
    into :output_cmsdsc_id , 
         :output_catalogo , 
         :output_fchini , 
         :output_fchvig , 
         :output_descuent , 
         :output_factor , 
         :output_compra_rng1 , 
         :output_compra_rng2 , 
         :output_dias_rng1 , 
         :output_dias_rng2 ;
    
    if ( output_cmsdsc_id is null or output_cmsdsc_id = 0 ) then        -- si no encontramos uno usamos el predeterminado
        begin
            select first 1 des.cmsdsc_id, des.cmsdsc_nom, des.cmsdsc_fchini, des.cmsdsc_fchvig, tbl2.tbrngs_nom, tbl2.tbrngs_factor, tbl1.tbrngs_rng1, tbl1.tbrngs_rng2, tbl2.tbrngs_rng1, tbl2.tbrngs_rng2
            from    cmsdsc des, tbrngs tbl1, tbrngs tbl2
            where   des.cmsdsc_doc_id = 1039                                    --< catalogo de descuentos
                and tbl1.tbrngs_doc_id = 1040                                   --< $ rangos de montos de compra
                and tbl2.tbrngs_doc_id = 1041                                   --< % por d�as de cobranza descuento a aplicar
                and tbl1.tbrngs_cmsdsc_id = des.cmsdsc_id                       --< relacion descuentos y rangos de compra
                and tbl1.tbrngs_id = tbl2.tbrngs_tbrngs_id                      --< dependencias entre ambas tablas
                and des.cmsdsc_a = 1
                and tbl1.tbrngs_a = 1
                and tbl2.tbrngs_a = 1
                and des.cmsdsc_opc_id1 = :input_catalog                             --< catalogo buscado
                and des.cmsdsc_sec_id = 1                                           --< indice del predeterminado
                and :input_avgcmpr between tbl1.tbrngs_rng1 and tbl1.tbrngs_rng2    --< promedio a buscar
                and :input_diastra between tbl2.tbrngs_rng1 and tbl2.tbrngs_rng2    --< d�as transcurridos para la cobranza
                and :input_fecemsn between des.cmsdsc_fchini and des.cmsdsc_fchvig  --< dentro de la vigencia
            into :output_cmsdsc_id , 
                 :output_catalogo , 
                 :output_fchini , 
                 :output_fchvig , 
                 :output_descuent , 
                 :output_factor , 
                 :output_compra_rng1 , 
                 :output_compra_rng2 , 
                 :output_dias_rng1 , 
                 :output_dias_rng2 ;

            if ( output_cmsdsc_id is null or output_cmsdsc_id = 0 ) then
                begin
                     output_cmsdsc_id = -1;
                     output_catalogo = 'no asignado';
                     --output_fchini = null;
                     --output_fchvig = null; 
                     output_descuent = ' sin descuento / fuera de la vigencia';
                     output_factor = 1;
                     output_compra_rng1 = 0; 
                     output_compra_rng2 = 0;
                     output_dias_rng1 = 0; 
                     output_dias_rng2 = 0;
                end
        end
            
    suspend;
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_DESCUENT2 TO  SYSDBA;

