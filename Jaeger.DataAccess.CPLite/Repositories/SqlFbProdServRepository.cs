﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbProdServRepository : FB.Almacen.Repositories.SqlFbProdServRepository, ISqlProdServXRepository {
        public SqlFbProdServRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion, user) {
            this.User = user;
        }

        #region CRUD
        public int Delete(ProductoServicioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTPRD SET CTPRD_A = @CTPRD_A, CTPRD_USR_N = @CTPRD_USR_N, CTPRD_FN = @CTPRD_FN WHERE (CTPRD_ID = @CTPRD_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@CTPRD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTPRD_FN", item.FechaNuevo);
            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public override IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CTPRD @wcondiciones ORDER BY CTPRD.CTPRD_NOM ASC"
            };
            if (typeof(T1) == typeof(ProductoModeloExistenciaModel) | typeof(T1) == typeof(ProductoXModelo)) {
                sqlCommand.CommandText = @"SELECT VWCATE.*, CTPRD.*, CTMDL.*, CTMDLX.*, (CTPRD.CTPRD_A*CTMDL.CTMDL_A) AS ACTIVO , CTMDLE.*, CTESPC.CTESPC_NOM, CTUND.CTUND_NOM
FROM CTPRD 
LEFT JOIN CTMDL ON CTMDL.CTMDL_CTPRD_ID = CTPRD.CTPRD_ID 
LEFT JOIN CTMDLE ON CTMDLE.CTMDLE_CTMDL_ID = CTMDL.CTMDL_ID
LEFT JOIN CTESPC ON CTESPC.CTESPC_ID = CTMDLE.CTMDLE_CTESPC_ID
LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID = CTMDL.CTMDL_ID AND CTMDLX.CTMDLX_CTPRD_ID = CTPRD.CTPRD_ID AND CTMDLX.CTMDLX_CTESPC_ID = CTMDLE.CTMDLE_CTESPC_ID
LEFT JOIN CTUND ON CTUND.CTUND_ID = CTMDL.CTMDL_UNDDA 
LEFT JOIN VWCATE ON VWCATE.IDCATEGORIA = CTPRD.CTPRD_CTCLS_ID  @wcondiciones";

                return GetMapper<T1>(sqlCommand, conditionals);
            }
            return base.GetList<T1>(conditionals);
        }
        public ProductoServicioXDetailModel Save(ProductoServicioXDetailModel model) {
            if (model.IdProducto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdProducto = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}
