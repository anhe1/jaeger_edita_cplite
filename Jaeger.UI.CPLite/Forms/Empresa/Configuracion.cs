﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Empresa {
    public class Configuracion : UI.Empresa.Forms.Configuracion {
        public Configuracion(UIMenuElement element) : base(element) {
            this._Service = new Aplication.CPLite.Services.ConfiguracionService();
            this.Load += Configuracion_Load;
        }

        private void Configuracion_Load(object sender, EventArgs e) {
        }
    }
}
