﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Herramientas.Configuracion {
    public class ConfigurationForm : UI.Empresa.Forms.ConfigurationForm {
        public ConfigurationForm(UIMenuElement menuElement) : base() { 
            this.SynapsisProperty.ReadOnly = true;
        }
    }
}
