﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class EstadoCuentaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            this.TReporte = new Jaeger.UI.CPLite.Forms.Ventas.EstadoCuentaControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TReporte
            // 
            this.TReporte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TReporte.Location = new System.Drawing.Point(0, 0);
            this.TReporte.Name = "TReporte";
            this.TReporte.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TReporte.Permisos = uiAction1;
            this.TReporte.ShowActualizar = true;
            this.TReporte.ShowAgrupar = false;
            this.TReporte.ShowAutosuma = false;
            this.TReporte.ShowCancelar = false;
            this.TReporte.ShowCerrar = true;
            this.TReporte.ShowEditar = true;
            this.TReporte.ShowEjercicio = true;
            this.TReporte.ShowExportarExcel = false;
            this.TReporte.ShowFiltro = true;
            this.TReporte.ShowHerramientas = false;
            this.TReporte.ShowImprimir = false;
            this.TReporte.ShowItem = false;
            this.TReporte.ShowNuevo = true;
            this.TReporte.ShowPeriodo = true;
            this.TReporte.ShowSeleccionMultiple = true;
            this.TReporte.Size = new System.Drawing.Size(1118, 482);
            this.TReporte.TabIndex = 0;
            // 
            // EstadoCuentaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 482);
            this.Controls.Add(this.TReporte);
            this.Name = "EstadoCuentaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Ventas: Estado de Cuenta";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EstadoCuentaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private EstadoCuentaControl TReporte;
    }
}