﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    /// <summary>
    /// Repositorio para notas de descuento (NTDSC)
    /// </summary>
    public class SqlFbNotaDescuentoRepository : Abstractions.RepositoryMaster<NotaDescuentoModel>, ISqlNotaDescuentoRepository {
        protected ISqlNotaDescuentoRelacionRepository relacionRepository;
        protected ISqlNotaDescuentoStatusRepository statusRepository;

        public SqlFbNotaDescuentoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.relacionRepository = new SqlFbNotaDescuentoRelacionRepository(configuracion, user);
            this.statusRepository = new SqlFbNotaDescuentoStatusRepository(configuracion, user);
        }

        #region CRUD
        public int Insert(NotaDescuentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NTDSC (NTDSC_ID, NTDSC_A, NTDSC_STTS_ID, NTDSC_FOLIO, NTDSC_DRCTR_ID, NTDSC_CTMTV_ID, NTDSC_NOM, NTDSC_RFC, NTDSC_CLV, NTDSC_REF, NTDSC_CNTCT, NTDSC_IMPORTE, NTDSC_OBSRV, NTDSC_UUID, NTDSC_SBTTL, NTDSC_DSCNT, NTDSC_TRSIVA, NTDSC_GTOTAL, NTDSC_USU_N, NTDSC_FN) 
VALUES (@NTDSC_ID, @NTDSC_A, @NTDSC_STTS_ID, @NTDSC_FOLIO, @NTDSC_DRCTR_ID, @NTDSC_CTMTV_ID, @NTDSC_NOM, @NTDSC_RFC, @NTDSC_CLV, @NTDSC_REF, @NTDSC_CNTCT, @NTDSC_IMPORTE, @NTDSC_OBSRV, @NTDSC_UUID, @NTDSC_SBTTL, @NTDSC_DSCNT, @NTDSC_TRSIVA, @NTDSC_GTOTAL, @NTDSC_USU_N, @NTDSC_FN)
RETURNING NTDSC_ID;"
            };

            item.Folio = this.Max("NTDSC_FOLIO");
            item.FechaNuevo = DateTime.Now;
            item.IdDocumento = this.CreateGuid(new string[] { item.Folio.ToString(), item.FechaNuevo.ToString(), item.Creo });
            sqlCommand.Parameters.AddWithValue("@NTDSC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NTDSC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@NTDSC_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@NTDSC_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CTMTV_ID", item.IdMotivo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NTDSC_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@NTDSC_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@NTDSC_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@NTDSC_REF", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@NTDSC_IMPORTE", item.Importe);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@NTDSC_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NTDSC_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSC_DSCNT", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@NTDSC_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@NTDSC_GTOTAL", item.GTotal);

            item.IdNota = this.ExecuteScalar(sqlCommand);
            if (item.IdNota > 0)
                return item.IdNota;
            return 0;
        }

        public int Update(NotaDescuentoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NTDSC SET NTDSC_A = @NTDSC_A, NTDSC_STTS_ID = @NTDSC_STTS_ID, NTDSC_FOLIO = @NTDSC_FOLIO, NTDSC_DRCTR_ID = @NTDSC_DRCTR_ID, NTDSC_CTMTV_ID = @NTDSC_CTMTV_ID, NTDSC_NOM = @NTDSC_NOM, NTDSC_RFC = @NTDSC_RFC, NTDSC_CLV = @NTDSC_CLV, NTDSC_REF = @NTDSC_REF, NTDSC_CNTCT = @NTDSC_CNTCT, NTDSC_IMPORTE = @NTDSC_IMPORTE, NTDSC_OBSRV = @NTDSC_OBSRV, NTDSC_UUID = @NTDSC_UUID, NTDSC_SBTTL = @NTDSC_SBTTL, NTDSC_DSCNT = @NTDSC_DSCNT, NTDSC_TRSIVA = @NTDSC_TRSIVA, NTDSC_GTOTAL = @NTDSC_GTOTAL, NTDSC_USU_M = @NTDSC_USU_M, NTDSC_FM = @NTDSC_FM WHERE (NTDSC_ID = @NTDSC_ID)"
            };

            item.FechaModifica = DateTime.Now;
            sqlCommand.Parameters.AddWithValue("@NTDSC_ID", item.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@NTDSC_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@NTDSC_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NTDSC_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@NTDSC_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CTMTV_ID", item.IdMotivo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@NTDSC_REF", item.Referencia);
            sqlCommand.Parameters.AddWithValue("@NTDSC_IMPORTE", item.Importe);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CNTCT", item.Contacto);
            sqlCommand.Parameters.AddWithValue("@NTDSC_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NTDSC_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NTDSC_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@NTDSC_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSC_DSCNT", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@NTDSC_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@NTDSC_GTOTAL", item.GTotal);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE NTDSC SET NTDSC_A = 0 WHERE NTDSC_ID = @index"
            };
            sqlCommand.Parameters.Add("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public IEnumerable<NotaDescuentoModel> GetList() {
            return this.GetList<NotaDescuentoModel>(new List<IConditional>());
        }

        public NotaDescuentoModel GetById(int index) {
            return this.GetList<NotaDescuentoModel>(new List<IConditional> { new Conditional("NTDSC_ID", index.ToString(), ConditionalTypeEnum.Equal) }).FirstOrDefault();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT NTDSC.* FROM NTDSC LEFT JOIN DRCTR ON DRCTR_ID = NTDSC_DRCTR_ID LEFT JOIN NTDSCS ON NTDSCS_NTDSC_ID = NTDSC_ID @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals).ToList();
        }

        public IEnumerable<NotaDescuentoRelacionDetailModel> GetPartidas(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT NTDSCP_ID, NTDSCP_A, NTDSCP_ALMPT_ID, NTDSCP_NTDSC_ID, NTDSCP_TIPO, NTDSCP_USU_N, NTDSCP_FN, NTDSCP_USU_M, NTDSCP_FM,ALMPT_STTS_ID, 
(CASE ALMPT_STTS_ID WHEN 0 THEN 'Cancelado' WHEN 1 THEN 'Pendiente' WHEN 2 THEN 'Impreso' WHEN 3 THEN 'Ingreso' ELSE 'Notificado' END) STATUS,
ALMPT_FOLIO,ALMPT_SBTTL,ALMPT.ALMPT_TRIVA,ALMPT_TRIVA,(ALMPT_SBTTL + ALMPT_TRIVA) AS TOTAL,ALMPT.ALMPT_FECEMS, ALMPT.ALMPT_FCING,ALMPT.ALMPT_USU_C,ALMPT.ALMPT_CNTCT,ALMPT_OBSRV, ALMPT_USU_N,ALMPT_FN,
DRCTR_CLV,
DRCTR_NOM
FROM NTDSCP 
LEFT JOIN ALMPT ON ALMPT_ID = NTDSCP_ALMPT_ID
LEFT JOIN DRCTR ON ALMPT_DRCTR_ID = DRCTR_ID
WHERE ALMPT_DOC_ID = 30
@condiciones ORDER BY ALMPT_FOLIO DESC"
            };

            if (conditionals.Count > 0) {
                var d = ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "and" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }
            return this.GetMapper<NotaDescuentoRelacionDetailModel>(sqlCommand);
        }

        public IEnumerable<NotaDescuentoDetailModel> GetList1(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT NTDSC.* FROM NTDSC LEFT JOIN DRCTR ON DRCTR_ID = NTDSC_DRCTR_ID @condiciones"
            };

            if (conditionals.Count > 0) {
                var d = ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "where" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }

            var result = this.GetMapper<NotaDescuentoDetailModel>(sqlCommand).ToList();
            var ids = result.Select(it => it.IdNota).ToArray();
            if (ids.Count() == 0) { ids = new int[] { 0 }; }

            var partidas = this.GetPartidas(new List<Conditional> {
                new Conditional("NTDSCP_NTDSC_ID", string.Join(",", ids), ConditionalTypeEnum.In),
                new Conditional("NTDSCP_A", "0", ConditionalTypeEnum.GreaterThan)
                }).ToList();

            if (partidas.Count > 0) {
                for (int i = 0; i < result.Count(); i++) {
                    result[i].Partidas = new System.ComponentModel.BindingList<NotaDescuentoRelacionDetailModel>(partidas.Where(it => it.IdNota == result[i].IdNota).ToList());
                }
            }
            return result;
        }

        public NotaDescuentoDetailModel Save(NotaDescuentoDetailModel model) {
            if (model.IdNota == 0) {
                model.Creo = this.User;
                model.IdNota = this.Insert(model);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model);
            }
            if (model.Partidas.Count() > 0) {
                for (int i = 0; i < model.Partidas.Count(); i++) {
                    if (model.Partidas[i].Id == 0) {
                        model.Partidas[i].IdNota = model.IdNota;
                        //model.Partidas[i] = this.relacionRepository.Save(model.Partidas[i]);
                    } else {
                        model.Partidas[i].IdNota = model.IdNota;
                        //model.Partidas[i] = this.relacionRepository.Save(model.Partidas[i]);
                    }
                }
            }
            return model;
        }

        public bool Cancelar(NotaDescuentoDetailModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NTDSC SET NTDSC_STTS_ID = 0, NTDSC_USU_M = @user, NTDSC_USU_C = @cancela, NTDSC_FM = @date, NTDSC_FCCNCL = @date, NTDSC_CTMTV_ID = @NTDSC_CTMTV_ID WHERE NTDSC_ID = @index"
            };

            sqlCommand.Parameters.AddWithValue("@index", model.IdNota);
            sqlCommand.Parameters.AddWithValue("@cancela", model.Cancela);
            sqlCommand.Parameters.AddWithValue("@date", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@user", this.User);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CTMTV_ID", model.IdMotivo);
            sqlCommand.Parameters.AddWithValue("@NTDSC_CTMTV", model.Motivo);

            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        private int Folio() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT MAX(NTDSC_FOLIO) + 1 FROM NTDSC;"
            };
            int _folio = 0;
            try {
                _folio = this.ExecuteScalar(sqlCommand);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                _folio = -1;
            }
            return _folio;
        }
    }
}
