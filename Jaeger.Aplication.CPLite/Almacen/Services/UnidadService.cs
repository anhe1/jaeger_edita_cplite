﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.DataAccess.FB.Almacen.Repositories;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Almacen.Services {
    public class UnidadService : Aplication.Almacen.Services.UnidadService, Aplication.Almacen.Contracts.IUnidadService {
        public UnidadService() : base(AlmacenEnum.PT) { }

        public override void OnLoad() {
            this.unidadRepository = new SqlFbUnidadRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}