CREATE TABLE RLSUSR
(
  RLSUSR_ID Smallint NOT NULL,
  RLSUSR_A Smallint,
  RLSUSR_M Smallint NOT NULL,
  RLSUSR_CLV Varchar(100),
  RLSUSR_NMBR Varchar(100),
  RLSUSR_FN Timestamp NOT NULL,
  RLSUSR_USR_N Varchar(50),
  RLSUSR_FM Timestamp,
  RLSUSR_USR_M Varchar(50),
  CONSTRAINT PK_RLSUSR PRIMARY KEY (RLSUSR_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice'  where RDB$FIELD_NAME = 'RLSUSR_ID' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RLSUSR_A' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'marca del usuario maestro'  where RDB$FIELD_NAME = 'RLSUSR_M' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de rol'  where RDB$FIELD_NAME = 'RLSUSR_CLV' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del rol'  where RDB$FIELD_NAME = 'RLSUSR_NMBR' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RLSUSR_FN' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'RLSUSR_USR_N' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RLSUSR_FM' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'RLSUSR_USR_M' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'roles de usuarios'
where RDB$RELATION_NAME = 'RLSUSR';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RLSUSR TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_RLSUSR_ID;

SET TERM ^ ;
CREATE TRIGGER RLSUSR_BI FOR RLSUSR ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 260920221718
    Purpose: disparador para el auto numerico
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RLSUSR_ID IS NULL) THEN
    NEW.RLSUSR_ID = GEN_ID(GEN_RLSUSR_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RLSUSR_ID, 0);
    if (tmp < new.RLSUSR_ID) then
      tmp = GEN_ID(GEN_RLSUSR_ID, new.RLSUSR_ID-tmp);
  END
END^
SET TERM ; ^

