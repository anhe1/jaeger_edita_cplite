﻿using System.Drawing;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Services {
    public static class GridNotaDescuentoService {
        /// <summary>
        /// obtener columna de motivo de devoluciones
        /// </summary>
        public static GridViewTextBoxColumn ColClaveMotivo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "CvMotivo",
                    HeaderText = "CvMotivo",
                    Name = "CvMotivo",
                    Width = 275
                };
            }
        }

        public static GridViewComboBoxColumn ColIdMotivo {
            get {
                return new GridViewComboBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdMotivo",
                    HeaderText = "Cv. Motivo",
                    Name = "IdMotivo",
                    Width = 145
                };
            }
        }

        public static GridViewTextBoxColumn ColReferencia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Referencia",
                    HeaderText = "Referencia",
                    Name = "Referencia",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleRight
                };
            }
        }

        public static GridViewTextBoxColumn ColContacto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Contacto",
                    HeaderText = "Contacto",
                    Name = "Contacto",
                    Width = 75,
                    TextAlignment = ContentAlignment.MiddleRight
                };
            }
        }

        public static GridViewTextBoxColumn ColIdDocumento {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "IdDocumento",
                    HeaderText = "IdDocumento",
                    IsVisible = false,
                    Name = "IdDocumento",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 220
                };
            }
        }

        /// <summary>
        /// obtener columnas templete de columnas para cambios de status
        /// </summary>
        public static GridViewDataColumn[] GetColumnsStatus() {
            return new GridViewDataColumn[] { GridCommonService.ColCboIdStatus, ColClaveMotivo, GridCommonService.ColNota, GridCommonService.ColFechaNuevo, GridCommonService.ColCreo };
        }

        public static GridViewDataColumn[] GetNotaDescuentoColumns() {
            return new GridViewDataColumn[] {
                GridCommonService.ColFolio,
                GridCommonService.ColCboIdStatus,
                GridContribuyenteService.ColClave,
                GridContribuyenteService.ColCliente,
                GridCommonService.ColFechaEmision,
                GridCommonService.ColFechaCancela,
                GridNotaDescuentoService.ColIdMotivo,
                GridCommonService.ColGranTotal,
                GridNotaDescuentoService.ColIdDocumento,
                GridCommonService.ColNota,
                GridValeAlmacenService.ColNumeros,
                GridCommonService.ColFechaNuevo,
                GridCommonService.ColCreo };
        }
    }
}
