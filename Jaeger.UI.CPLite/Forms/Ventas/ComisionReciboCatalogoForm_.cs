﻿using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Service;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.UI.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    /// <summary>
    /// formulario de pago de comisiones a vendedor
    /// </summary>
    public partial class ComisionReciboCatalogoForm : RadForm {
        protected IComisionService service;

        private Domain.Base.ValueObjects.UIAction _permisos;

        public ComisionReciboCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void ComisionReciboCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new ComisionService();

            this.ToolBar.Nuevo.Enabled = this._permisos.Agregar;
            this.ToolBar.Cancelar.Enabled = this._permisos.Cancelar;
            this.ToolBar.Editar.Enabled = this._permisos.Editar;
            this.Movimientos.GridData.Columns["Cargo"].HeaderText = "Importe";
            this.Movimientos.GridData.Columns["Abono"].IsVisible = false;
            this.Movimientos.GridData.Columns["Abono"].VisibleInColumnChooser = false;
            this.Movimientos.GridData.Columns["Saldo"].IsVisible = false;
            this.Movimientos.GridData.Columns["Saldo"].VisibleInColumnChooser = false;
            //this.Movimientos.GridComprobantes.Columns["Cargo"].IsVisible = false;

            this.ToolBar.Nuevo.Click += this.ToolBar_Nuevo_Click;
            this.ToolBar.Cancelar.Click += this.ToolBar_Cancelar_Click;
            this.ToolBar.Imprimir.Click += this.ToolBar_Imprimir_Click;
            this.ToolBar.Actualizar.Click += this.ToolBar_Actualizar_Click;
            this.ToolBar.AutoSuma.Click += this.ToolBar_AutoSuma_Click;
            this.ToolBar.Filtro.Click += this.ToolBar_Filtro_Click;
            this.ToolBar.Cerrar.Click += this.ToolBar_Cerrar_Click;

        }

        #region barra de herramientas
        private void ToolBar_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            //this.Movimientos.GridData.DataSource = this.Movimientos.Data;
        }

        private void ToolBar_Nuevo_Click(object sender, EventArgs e) {
            //var _nuevo = new ComisionReciboForm(null);
            //_nuevo.Text = "Recibo de comisión";
            //_nuevo.ShowDialog(this);
        }

        private void ToolBar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBar_Imprimir_Click(object sender, EventArgs e) {
            //var seleccionado = (MovimientoBancarioDetailModel)this.Movimientos.GridData.ReturnRowSelected();
            //if (seleccionado != null) {
            //    var _imprimir = this.service.GetRecibo(seleccionado.Id);
            //    if (_imprimir != null) {
            //        var imprimir = new ReporteForm(new MovimientoBancarioPrinter(_imprimir));
            //        imprimir.Show();
            //    }
            //}
        }

        private void ToolBar_Filtro_Click(object sender, EventArgs e) {
            this.Movimientos.GridData.ActivateFilterRow(((CommandBarToggleButton)sender).ToggleState);
        }

        private void ToolBar_Cancelar_Click(object sender, EventArgs e) {
            if (this.Movimientos.GridData.CurrentRow != null) {
                var seleccionado = this.Movimientos.Current();
                if (seleccionado != null) {
                    if (seleccionado.Status == MovimientoBancarioStatusEnum.Cancelado) { return; }
                    if (RadMessageBox.Show(this, Properties.Resources.Msg_Banco_Movimiento_Cancelar, "Información", MessageBoxButtons.YesNo, RadMessageIcon.Info) == DialogResult.Yes) {
                        seleccionado.Status = MovimientoBancarioStatusEnum.Cancelado;
                        using (var espera = new Waiting1Form(this.Cancelar)) {
                            espera.Text = "Aplicando cambios ...";
                            espera.ShowDialog(this);
                        }
                        if (this.ToolBar.Tag != null) {
                            if ((bool)this.ToolBar.Tag == false) {
                                MessageBox.Show("Error");
                                this.ToolBar.Tag = null;
                            }
                        }
                    }
                }
            }
        }

        private void ToolBar_AutoSuma_Click(object sender, EventArgs e) {
            this.Movimientos.GridData.TelerikGridAutoSum(((CommandBarToggleButton)sender).ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off);
        }

        #endregion

        #region metodos privados
        private void Consultar() {
            this.Movimientos.Data = this.service.GetRecibos(this.ToolBar.GetEjercicio(), this.ToolBar.GetMes());
        }

        private void Cancelar() {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                //  this.ToolBar.Tag = this.service.Cancelar(seleccionado);
            }
        }
        #endregion
    }
}
