﻿using System;
using Jaeger.Aplication.CPLite.Almacen.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class ProductosModeloCatalogoForm : UI.Tienda.Forms.ProductosModeloCatalogoForm {
        public ProductosModeloCatalogoForm(UIMenuElement menuElement) : base() {
            this._permisos = new UIAction(menuElement.Permisos);
            this.Load += this.ProductosModeloCatalogoForm_Load;
        }

        private void ProductosModeloCatalogoForm_Load(object sender, EventArgs e) {
            this._Service = new CatalogoProductoService(AlmacenEnum.PT);
            this.TModelo.Nuevo.Enabled = this._permisos.Agregar;
            this.TModelo.Editar.Enabled = this._permisos.Editar;
            this.TModelo.Remover.Enabled = this._permisos.Remover;
            this.TModelo.Autorizar.Enabled = this._permisos.Autorizar;

            this.TDisponible.Nuevo.Enabled = this._permisos.Agregar;
            this.TDisponible.Editar.Enabled = this._permisos.Editar;
            this.TDisponible.Remover.Enabled = this._permisos.Remover;

            this.TImagen.Nuevo.Enabled = this._permisos.Agregar;
            this.TImagen.Remover.Enabled = this._permisos.Editar;

            this.gModelos.AllowEditRow = this._permisos.Editar;
            this.gVariante.AllowEditRow = this._permisos.Editar;
        }
    }
}
