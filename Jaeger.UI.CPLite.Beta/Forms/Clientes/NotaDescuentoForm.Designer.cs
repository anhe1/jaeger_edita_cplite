﻿namespace Jaeger.UI.CPLite.Forms.Clientes { 
    partial class NotaDescuentoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.FilterDescriptor filterDescriptor1 = new Telerik.WinControls.Data.FilterDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotaDescuentoForm));
            this.gCliente = new Telerik.WinControls.UI.RadGroupBox();
            this.Receptor = new Jaeger.UI.Contribuyentes.Forms.DirectorioControl();
            this.lblMorivo = new Telerik.WinControls.UI.RadLabel();
            this.IdMotivo = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.Referencia = new Telerik.WinControls.UI.RadTextBox();
            this.lblReferencia = new Telerik.WinControls.UI.RadLabel();
            this.lblImporte = new Telerik.WinControls.UI.RadLabel();
            this.Importe = new Telerik.WinControls.UI.RadCalculatorDropDown();
            this.Folio = new Telerik.WinControls.UI.RadTextBox();
            this.lblFolio = new Telerik.WinControls.UI.RadLabel();
            this.Status = new Telerik.WinControls.UI.RadDropDownList();
            this.lblStatus = new Telerik.WinControls.UI.RadLabel();
            this.FechaEmision = new Telerik.WinControls.UI.RadDateTimePicker();
            this.Creo = new Telerik.WinControls.UI.RadTextBox();
            this.Observaciones = new Telerik.WinControls.UI.RadTextBox();
            this.Recibe = new Telerik.WinControls.UI.RadTextBox();
            this.lblFechaEmision = new Telerik.WinControls.UI.RadLabel();
            this.lblNota = new Telerik.WinControls.UI.RadLabel();
            this.lblContacto = new Telerik.WinControls.UI.RadLabel();
            this.lblCreo = new Telerik.WinControls.UI.RadLabel();
            this.TNota = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.TConcepto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.dataGridPartidas = new Telerik.WinControls.UI.RadGridView();
            this.errorVerificar = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gCliente)).BeginInit();
            this.gCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMorivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMotivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMotivo.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMotivo.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReferencia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblImporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorVerificar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // gCliente
            // 
            this.gCliente.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gCliente.Controls.Add(this.Receptor);
            this.gCliente.Controls.Add(this.lblMorivo);
            this.gCliente.Controls.Add(this.IdMotivo);
            this.gCliente.Controls.Add(this.Referencia);
            this.gCliente.Controls.Add(this.lblReferencia);
            this.gCliente.Controls.Add(this.lblImporte);
            this.gCliente.Controls.Add(this.Importe);
            this.gCliente.Controls.Add(this.Folio);
            this.gCliente.Controls.Add(this.lblFolio);
            this.gCliente.Controls.Add(this.Status);
            this.gCliente.Controls.Add(this.lblStatus);
            this.gCliente.Controls.Add(this.FechaEmision);
            this.gCliente.Controls.Add(this.Creo);
            this.gCliente.Controls.Add(this.Observaciones);
            this.gCliente.Controls.Add(this.Recibe);
            this.gCliente.Controls.Add(this.lblFechaEmision);
            this.gCliente.Controls.Add(this.lblNota);
            this.gCliente.Controls.Add(this.lblContacto);
            this.gCliente.Controls.Add(this.lblCreo);
            this.gCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.gCliente.HeaderText = "";
            this.gCliente.Location = new System.Drawing.Point(0, 30);
            this.gCliente.Name = "gCliente";
            this.gCliente.Size = new System.Drawing.Size(810, 115);
            this.gCliente.TabIndex = 4;
            // 
            // Receptor
            // 
            this.Receptor.AllowAddNew = false;
            this.Receptor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Receptor.LDomicilio = "Domicilio:";
            this.Receptor.LNombre = "Receptor:";
            this.Receptor.Location = new System.Drawing.Point(10, 32);
            this.Receptor.Margin = new System.Windows.Forms.Padding(4);
            this.Receptor.Name = "Receptor";
            this.Receptor.ReadOnly = false;
            this.Receptor.Relacion = Jaeger.Domain.Base.ValueObjects.TipoRelacionComericalEnum.None;
            this.Receptor.ShowDomicilio = false;
            this.Receptor.ShowRFC = false;
            this.Receptor.ShowSearch = true;
            this.Receptor.Size = new System.Drawing.Size(609, 20);
            this.Receptor.TabIndex = 379;
            // 
            // lblMorivo
            // 
            this.lblMorivo.Location = new System.Drawing.Point(527, 58);
            this.lblMorivo.Name = "lblMorivo";
            this.lblMorivo.Size = new System.Drawing.Size(44, 18);
            this.lblMorivo.TabIndex = 378;
            this.lblMorivo.Text = "Motivo:";
            // 
            // IdMotivo
            // 
            this.IdMotivo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IdMotivo.AutoSizeDropDownHeight = true;
            this.IdMotivo.AutoSizeDropDownToBestFit = true;
            this.IdMotivo.DisplayMember = "Nombre";
            // 
            // IdMotivo.NestedRadGridView
            // 
            this.IdMotivo.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.IdMotivo.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdMotivo.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.IdMotivo.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.IdMotivo.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.IdMotivo.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.IdMotivo.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.IdMotivo.EditorControl.MasterTemplate.AllowDeleteRow = false;
            this.IdMotivo.EditorControl.MasterTemplate.AllowEditRow = false;
            this.IdMotivo.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn2.FieldName = "Descriptor";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descriptor";
            gridViewTextBoxColumn2.Width = 220;
            this.IdMotivo.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.IdMotivo.EditorControl.MasterTemplate.EnableGrouping = false;
            filterDescriptor1.Operator = Telerik.WinControls.Data.FilterOperator.StartsWith;
            filterDescriptor1.PropertyName = "Descriptor";
            this.IdMotivo.EditorControl.MasterTemplate.FilterDescriptors.AddRange(new Telerik.WinControls.Data.FilterDescriptor[] {
            filterDescriptor1});
            this.IdMotivo.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.IdMotivo.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.IdMotivo.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.IdMotivo.EditorControl.Name = "NestedRadGridView";
            this.IdMotivo.EditorControl.ReadOnly = true;
            this.IdMotivo.EditorControl.ShowGroupPanel = false;
            this.IdMotivo.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.IdMotivo.EditorControl.TabIndex = 0;
            this.IdMotivo.Location = new System.Drawing.Point(583, 57);
            this.IdMotivo.Name = "IdMotivo";
            this.IdMotivo.NullText = "Motivo de la nota";
            this.IdMotivo.Size = new System.Drawing.Size(206, 20);
            this.IdMotivo.TabIndex = 10;
            this.IdMotivo.TabStop = false;
            this.IdMotivo.ValueMember = "Id";
            // 
            // Referencia
            // 
            this.Referencia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Referencia.Location = new System.Drawing.Point(336, 57);
            this.Referencia.MaxLength = 100;
            this.Referencia.Name = "Referencia";
            this.Referencia.Size = new System.Drawing.Size(176, 20);
            this.Referencia.TabIndex = 9;
            // 
            // lblReferencia
            // 
            this.lblReferencia.Location = new System.Drawing.Point(269, 58);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(61, 18);
            this.lblReferencia.TabIndex = 121;
            this.lblReferencia.Text = "Referencia:";
            // 
            // lblImporte
            // 
            this.lblImporte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblImporte.Location = new System.Drawing.Point(643, 85);
            this.lblImporte.Name = "lblImporte";
            this.lblImporte.Size = new System.Drawing.Size(48, 18);
            this.lblImporte.TabIndex = 375;
            this.lblImporte.Text = "Importe:";
            // 
            // Importe
            // 
            this.Importe.Location = new System.Drawing.Point(697, 84);
            this.Importe.Name = "Importe";
            this.Importe.Size = new System.Drawing.Size(92, 20);
            this.Importe.TabIndex = 12;
            this.Importe.TabStop = false;
            this.Importe.Value = "0";
            // 
            // Folio
            // 
            this.Folio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Folio.BackColor = System.Drawing.SystemColors.Window;
            this.Folio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Folio.ForeColor = System.Drawing.Color.Red;
            this.Folio.Location = new System.Drawing.Point(67, 8);
            this.Folio.MaxLength = 100;
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            this.Folio.Size = new System.Drawing.Size(80, 18);
            this.Folio.TabIndex = 0;
            this.Folio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFolio
            // 
            this.lblFolio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFolio.Location = new System.Drawing.Point(31, 8);
            this.lblFolio.Name = "lblFolio";
            this.lblFolio.Size = new System.Drawing.Size(33, 18);
            this.lblFolio.TabIndex = 128;
            this.lblFolio.Text = "Folio:";
            // 
            // Status
            // 
            this.Status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Status.DisplayMember = "Descripcion";
            this.Status.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Status.Location = new System.Drawing.Point(215, 7);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(90, 20);
            this.Status.TabIndex = 1;
            this.Status.ValueMember = "Id";
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.Location = new System.Drawing.Point(173, 8);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 18);
            this.lblStatus.TabIndex = 123;
            this.lblStatus.Text = "Status:";
            // 
            // FechaEmision
            // 
            this.FechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.FechaEmision.Location = new System.Drawing.Point(406, 7);
            this.FechaEmision.Name = "FechaEmision";
            this.FechaEmision.Size = new System.Drawing.Size(199, 20);
            this.FechaEmision.TabIndex = 2;
            this.FechaEmision.TabStop = false;
            this.FechaEmision.Text = "viernes, 30 de julio de 2021";
            this.FechaEmision.Value = new System.DateTime(2021, 7, 30, 0, 15, 36, 617);
            // 
            // Creo
            // 
            this.Creo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Creo.Location = new System.Drawing.Point(707, 7);
            this.Creo.Name = "Creo";
            this.Creo.ReadOnly = true;
            this.Creo.Size = new System.Drawing.Size(82, 20);
            this.Creo.TabIndex = 3;
            this.Creo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Observaciones
            // 
            this.Observaciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Observaciones.Location = new System.Drawing.Point(99, 85);
            this.Observaciones.MaxLength = 100;
            this.Observaciones.Name = "Observaciones";
            this.Observaciones.Size = new System.Drawing.Size(413, 20);
            this.Observaciones.TabIndex = 11;
            // 
            // Recibe
            // 
            this.Recibe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Recibe.Location = new System.Drawing.Point(67, 57);
            this.Recibe.MaxLength = 100;
            this.Recibe.Name = "Recibe";
            this.Recibe.Size = new System.Drawing.Size(196, 20);
            this.Recibe.TabIndex = 8;
            // 
            // lblFechaEmision
            // 
            this.lblFechaEmision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFechaEmision.Location = new System.Drawing.Point(330, 8);
            this.lblFechaEmision.Name = "lblFechaEmision";
            this.lblFechaEmision.Size = new System.Drawing.Size(70, 18);
            this.lblFechaEmision.TabIndex = 120;
            this.lblFechaEmision.Text = "Fec. Emisión:";
            // 
            // lblNota
            // 
            this.lblNota.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNota.Location = new System.Drawing.Point(12, 86);
            this.lblNota.Name = "lblNota";
            this.lblNota.Size = new System.Drawing.Size(81, 18);
            this.lblNota.TabIndex = 118;
            this.lblNota.Text = "Observaciones:";
            // 
            // lblContacto
            // 
            this.lblContacto.Location = new System.Drawing.Point(10, 58);
            this.lblContacto.Name = "lblContacto";
            this.lblContacto.Size = new System.Drawing.Size(54, 18);
            this.lblContacto.TabIndex = 119;
            this.lblContacto.Text = "Contacto:";
            // 
            // lblCreo
            // 
            this.lblCreo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCreo.Location = new System.Drawing.Point(671, 8);
            this.lblCreo.Name = "lblCreo";
            this.lblCreo.Size = new System.Drawing.Size(32, 18);
            this.lblCreo.TabIndex = 121;
            this.lblCreo.Text = "Creó:";
            // 
            // TNota
            // 
            this.TNota.Dock = System.Windows.Forms.DockStyle.Top;
            this.TNota.Etiqueta = "";
            this.TNota.Location = new System.Drawing.Point(0, 0);
            this.TNota.Name = "TNota";
            this.TNota.ReadOnly = false;
            this.TNota.ShowActualizar = false;
            this.TNota.ShowAutorizar = false;
            this.TNota.ShowCerrar = true;
            this.TNota.ShowEditar = false;
            this.TNota.ShowExportarExcel = false;
            this.TNota.ShowFiltro = false;
            this.TNota.ShowGuardar = true;
            this.TNota.ShowHerramientas = false;
            this.TNota.ShowImagen = false;
            this.TNota.ShowImprimir = true;
            this.TNota.ShowNuevo = true;
            this.TNota.ShowRemover = false;
            this.TNota.Size = new System.Drawing.Size(810, 30);
            this.TNota.TabIndex = 0;
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Etiqueta = "Conceptos";
            this.TConcepto.Location = new System.Drawing.Point(0, 145);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ReadOnly = false;
            this.TConcepto.ShowActualizar = false;
            this.TConcepto.ShowAutorizar = false;
            this.TConcepto.ShowCerrar = false;
            this.TConcepto.ShowEditar = false;
            this.TConcepto.ShowExportarExcel = false;
            this.TConcepto.ShowFiltro = false;
            this.TConcepto.ShowGuardar = false;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImagen = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = true;
            this.TConcepto.ShowRemover = true;
            this.TConcepto.Size = new System.Drawing.Size(810, 30);
            this.TConcepto.TabIndex = 1;
            // 
            // dataGridPartidas
            // 
            this.dataGridPartidas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridPartidas.Location = new System.Drawing.Point(0, 175);
            // 
            // 
            // 
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.DataType = typeof(int);
            gridViewTextBoxColumn4.FieldName = "IdPedido";
            gridViewTextBoxColumn4.HeaderText = "# Pedido";
            gridViewTextBoxColumn4.Name = "IdPedido";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 75;
            gridViewComboBoxColumn1.FieldName = "IdStatus";
            gridViewComboBoxColumn1.HeaderText = "Status";
            gridViewComboBoxColumn1.IsVisible = false;
            gridViewComboBoxColumn1.Name = "IdStatus";
            gridViewComboBoxColumn1.Width = 75;
            gridViewTextBoxColumn5.FieldName = "Clave";
            gridViewTextBoxColumn5.HeaderText = "Clave";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Clave";
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn6.FieldName = "Receptor";
            gridViewTextBoxColumn6.HeaderText = "Cliente";
            gridViewTextBoxColumn6.Name = "Receptor";
            gridViewTextBoxColumn6.Width = 220;
            gridViewComboBoxColumn2.DataType = typeof(int);
            gridViewComboBoxColumn2.FieldName = "IdDocumento";
            gridViewComboBoxColumn2.HeaderText = "Documento";
            gridViewComboBoxColumn2.Name = "IdDocumento";
            gridViewComboBoxColumn2.Width = 100;
            gridViewTextBoxColumn7.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn7.FieldName = "FechaEmision";
            gridViewTextBoxColumn7.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn7.HeaderText = "Fec. Emisión";
            gridViewTextBoxColumn7.Name = "FechaEmision";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 75;
            gridViewTextBoxColumn8.DataType = typeof(decimal);
            gridViewTextBoxColumn8.FieldName = "SubTotal";
            gridViewTextBoxColumn8.FormatString = "{0:n2}";
            gridViewTextBoxColumn8.HeaderText = "SubTotal";
            gridViewTextBoxColumn8.Name = "SubTotal";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn8.Width = 85;
            gridViewTextBoxColumn9.DataType = typeof(decimal);
            gridViewTextBoxColumn9.FieldName = "TotalTrasladoIVA";
            gridViewTextBoxColumn9.FormatString = "{0:n2}";
            gridViewTextBoxColumn9.HeaderText = "IVA";
            gridViewTextBoxColumn9.Name = "TotalTrasladoIVA";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn9.Width = 85;
            gridViewTextBoxColumn10.DataType = typeof(decimal);
            gridViewTextBoxColumn10.FieldName = "GTotal";
            gridViewTextBoxColumn10.FormatString = "{0:n2}";
            gridViewTextBoxColumn10.HeaderText = "Total";
            gridViewTextBoxColumn10.Name = "GTotal";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn10.Width = 85;
            gridViewTextBoxColumn11.FieldName = "Nota";
            gridViewTextBoxColumn11.HeaderText = "Observaciones";
            gridViewTextBoxColumn11.Name = "Nota";
            gridViewTextBoxColumn11.Width = 150;
            gridViewTextBoxColumn12.FieldName = "Creo";
            gridViewTextBoxColumn12.HeaderText = "Creó";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "Creo";
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn13.FieldName = "FechaNuevo";
            gridViewTextBoxColumn13.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn13.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "FechaNuevo";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn13.Width = 75;
            this.dataGridPartidas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewComboBoxColumn2,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.dataGridPartidas.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridPartidas.Name = "dataGridPartidas";
            this.dataGridPartidas.ReadOnly = true;
            this.dataGridPartidas.ShowGroupPanel = false;
            this.dataGridPartidas.Size = new System.Drawing.Size(810, 175);
            this.dataGridPartidas.TabIndex = 2;
            // 
            // errorVerificar
            // 
            this.errorVerificar.ContainerControl = this;
            // 
            // NotaDescuentoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 350);
            this.Controls.Add(this.dataGridPartidas);
            this.Controls.Add(this.TConcepto);
            this.Controls.Add(this.gCliente);
            this.Controls.Add(this.TNota);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotaDescuentoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nota de Descuento";
            this.Load += new System.EventHandler(this.NotaDescuentoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gCliente)).EndInit();
            this.gCliente.ResumeLayout(false);
            this.gCliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMorivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMotivo.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMotivo.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdMotivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Referencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReferencia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblImporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Importe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Folio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Creo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Observaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Recibe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFechaEmision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPartidas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorVerificar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGroupBox gCliente;
        private Common.Forms.ToolBarStandarControl TNota;
        public Telerik.WinControls.UI.RadTextBox Folio;
        private Telerik.WinControls.UI.RadLabel lblFolio;
        internal Telerik.WinControls.UI.RadDropDownList Status;
        private Telerik.WinControls.UI.RadLabel lblStatus;
        public Telerik.WinControls.UI.RadDateTimePicker FechaEmision;
        public Telerik.WinControls.UI.RadTextBox Creo;
        public Telerik.WinControls.UI.RadTextBox Observaciones;
        public Telerik.WinControls.UI.RadTextBox Recibe;
        private Telerik.WinControls.UI.RadLabel lblFechaEmision;
        private Telerik.WinControls.UI.RadLabel lblNota;
        private Telerik.WinControls.UI.RadLabel lblContacto;
        private Telerik.WinControls.UI.RadLabel lblCreo;
        private Telerik.WinControls.UI.RadLabel lblImporte;
        private Telerik.WinControls.UI.RadCalculatorDropDown Importe;
        public Telerik.WinControls.UI.RadTextBox Referencia;
        private Telerik.WinControls.UI.RadLabel lblReferencia;
        private Common.Forms.ToolBarStandarControl TConcepto;
        internal Telerik.WinControls.UI.RadLabel lblMorivo;
        internal Telerik.WinControls.UI.RadMultiColumnComboBox IdMotivo;
        private Telerik.WinControls.UI.RadGridView dataGridPartidas;
        private System.Windows.Forms.ErrorProvider errorVerificar;
        public Contribuyentes.Forms.DirectorioControl Receptor;
    }
}
