SET TERM ^ ;
CREATE PROCEDURE CBRNZ_REMISION_COBROS1 (
    INPUT_REMISION_ID DOM_ID DEFAULT 0,
    INPUT_COBRAR$ DOM_MONEDA DEFAULT 0 )
AS
declare variable status_id dom_id;
declare variable cobrado$ dom_moneda;
declare variable feccobro dom_fh;
BEGIN
/* 
Elabora: anhe 22102012
Proposito:
Revisiones:
    12122012: un error, cuando cobrar es cero no es posible hacer comparación y determinar si la remisión esta cobrada o no.
        asi que se agrega a la condición que cobrar sea mayo que cero.
*/                          
    if ( input_remision_id is not null ) then
        begin
            select sum ( rlcnCbrnz.CNTBL_ABONO$ ), max ( cbrnz.cntbl_fecdocs )
            from cntbl rlcnCbrnz, cntbl Cbrnz
            where 
                    rlcnCbrnz.cntbl_poliza = Cbrnz.cntbl_poliza --< relacion partidas -> recibos
                and rlcnCbrnz.cntbl_a = 1
                and rlcnCbrnz.cntbl_doc_id = 2800                --< solo documentos relacionados
                and Cbrnz.cntbl_doc_id = 28  
                and Cbrnz.CNTBL_STTSDCS_ID = 3
                and rlcnCbrnz.CNTBL_REM_ID = :input_remision_id
            into    :cobrado$, :feccobro;
                
                if ( input_cobrar$ is null ) then input_cobrar$ = 0;
                if ( :cobrado$ is null ) then cobrado$ = 0;
                if ( :cobrado$ + 1 >= input_cobrar$ and input_cobrar$ > 0 ) then
                    begin
                    
                        update ctlrms
                            set 
                                ctlrms_sttsdcs_id = 5,
                                ctlrms_cbrd$ = :cobrado$,
                                ctlrms_usu_m = 'sysdba'
                        where ctlrms_id = :input_remision_id;
                        
                        if ( :cobrado$ is not null ) then
                            begin
                                update ctlrms
                                    set 
                                        ctlrms_cbrd$ = :cobrado$
                                where ctlrms_id = :input_remision_id;
                            end
                    end
                else
                    begin
                        if ( :cobrado$ is not null ) then
                            begin
                                update ctlrms
                                    set 
                                        ctlrms_cbrd$ = :cobrado$
                                where ctlrms_id = :input_remision_id;
                            end
                    end
        end
suspend;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_REMISION_COBROS1 TO  SYSDBA;

