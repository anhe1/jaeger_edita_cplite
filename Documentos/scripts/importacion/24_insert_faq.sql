/*
 *** PREGUNTAS FRECUENTES
*/

INSERT INTO TWFAQ (TWFAQ_A, TWFAQ_QSTN, TWFAQ_ANSWR, TWFAQ_USU_N) VALUES ('1', 'Me interesan solo de niño y niña. ¿Se puede?', 'Sí se puede, solo nos indicas cuántas de niño y cuántas de niña en múltiplos de 12,', 'ANHE1');
INSERT INTO TWFAQ (TWFAQ_A, TWFAQ_QSTN, TWFAQ_ANSWR, TWFAQ_USU_N) VALUES ('1', '¿Puedo elegir solo tamaño Mediano y Grande?', 'En los paquetes ya están definidas las cantidades por tamaño. Pero puedes elegir', 'ANHE1');
INSERT INTO TWFAQ (TWFAQ_A, TWFAQ_QSTN, TWFAQ_ANSWR, TWFAQ_USU_N) VALUES ('1', '¿Si alguna bolsa sale dañada qué puedo hacer para no perder el producto?', 'Le tomas foto a la bolsa dañada y en un futuro pedido de la reintegramos.', 'ANHE1');
INSERT INTO TWFAQ (TWFAQ_A, TWFAQ_QSTN, TWFAQ_ANSWR, TWFAQ_USU_N) VALUES ('1', 'Quiero surtidas. ¿Se puede?', 'Todos nuestros paquetes van surtidos para que tengas variedad.', 'ANHE1');
INSERT INTO TWFAQ (TWFAQ_A, TWFAQ_QSTN, TWFAQ_ANSWR, TWFAQ_USU_N) VALUES ('1', '¿Puedo escoger el tipo de Bolsas que quiero?', 'Puedes elegir las Líneas que quieras y cada una de ellas va surtida.', 'ANHE1');
