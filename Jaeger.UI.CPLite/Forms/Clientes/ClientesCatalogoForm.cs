﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class ClientesCatalogoForm : Contribuyentes.Forms.ClientesCatalogoForm {
        #region declaraciones
        protected internal RadMenuItem TContribuyente_SolicitudCredito = new RadMenuItem { Text = "Solicitud de crédito" };
        #endregion

        public ClientesCatalogoForm() {

        }

        public ClientesCatalogoForm(UIMenuElement menuElement) : base(menuElement)  {
            this.Text = "Clientes: Expediente";
            this.GContribuyente.Permisos = new UIAction(menuElement.Permisos);
            this.Load += ClientesCatalogoForm_Load;
        }

        private void ClientesCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new Aplication.CPLite.Services.DirectorioService(this.relacionComericalEnum);
            this.GContribuyente.Herramientas.Items.Add(this.TContribuyente_SolicitudCredito);
            this.TContribuyente_SolicitudCredito.Click += this.TContribuyente_SolicitudCredito_Click;
        }

        public override void TContribuyente_Editar_Click(object sender, EventArgs e) {
            var seleccionado1 = this.GContribuyente.GetCurrent<ContribuyenteDomicilioSingleModel>();
            if (seleccionado1 != null) {
                using (var espera = new Waiting1Form(this.Editar)) {
                    espera.Text = "Consultando información ...";
                    espera.ShowDialog(this);
                }

                if (seleccionado1.Tag != null) {
                    var seleccionado = seleccionado1.Tag as ContribuyenteDetailModel;
                    if (seleccionado != null) {
                        using (var editar = new Clientes.ContribuyenteForm(this.service, seleccionado, this.GContribuyente.Permisos)) {
                            editar.Text = "Edición:";
                            editar.ShowDialog(this);
                        }
                    }
                }
            }
        }

        private void TContribuyente_SolicitudCredito_Click(object sender, EventArgs e) {
            var reporte = new Clientes.ClientesReporteForm("ClienteSolicitudCreditoReporte.rdlc");
            reporte.ShowDialog();
        }
    }
}
