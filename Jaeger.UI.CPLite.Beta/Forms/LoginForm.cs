﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jaeger.UI.CPLite.Forms {
    /// <summary>
    /// login personalizado
    /// </summary>
    internal class LoginForm : UI.Login.Forms.LoginForm {
        public LoginForm(string[] args) : base(args) {
            this.Text = "CP - Beta";
            this.LogoPicture.Image = Properties.Resources.logo_somos_ipo;
            
            this.LogoPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPicture.Padding = new System.Windows.Forms.Padding(20, 20, 20, 20);
            this.LogoPicture.ClientSize = new System.Drawing.Size(120, 120);
            this.LogoPicture.Location = new System.Drawing.Point(90, 50);
            this.lblInformacion.Text = "CP - Beta";
        }
    }
}
