SET TERM ^ ;

CREATE TRIGGER AUTORIZACION_NOTA_DESCUENTO FOR AUTDCS
ACTIVE BEFORE INSERT POSITION 0
AS 
BEGIN 
/* --------------------------------------------------------------------------------------------------------------
    Develop: ANHE1- 251120211703
    Purpose: Autorizacion de status de notas de descuento
-------------------------------------------------------------------------------------------------------------- */
    if ( new.autdcs_doc_id = 35 ) then
        begin
            if ( new.autdcs_ctlcbrnz_id is not null ) then
                begin
                    UPDATE NTDSC set NTDSC_IDSTTS = new.autdcs_sttsaut_id where NTDSC_ID = new.autdcs_ctlcbrnz_id;
                end
        end
END^

SET TERM ; ^ 
