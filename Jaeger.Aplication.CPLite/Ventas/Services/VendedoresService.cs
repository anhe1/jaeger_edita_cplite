﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.Aplication.CPLite.Ventas.Services {
    /// <summary>
    /// Catalogo de vendedores
    /// </summary>
    public class VendedoresService : VendedorService, Contribuyentes.Contracts.IVendedoresService {
        public VendedoresService() : base() { }

        /// <summary>
        /// listado de vendedores
        /// </summary>
        /// <param name="onlyActive">solo registros activos</param>
        public BindingList<Vendedor2DetailModel> GetList(bool onlyActive) {
            var condiciones = new List<IConditional>();
            if (onlyActive) {
                condiciones.Add(new Conditional("DRCTR_A", "1"));
                condiciones.Add(new Conditional("DRCTRR_A", "1"));
            }
            var response = new BindingList<Vendedor2DetailModel>(this.vendedorRepository.GetList<Vendedor2DetailModel>(condiciones).ToList());

            return response;
        }
    }
}
