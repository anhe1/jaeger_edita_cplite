SET TERM ^ ;
CREATE TRIGGER MOVAPT_REM_SUBTOTAL FOR MOVAPT ACTIVE
AFTER INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 12112021 22:00
    Purpose: actualizar el subtotal de la remision relacionda
------------------------------------------------------------------------------------------------------------- */
	if ( new.movapt_doc_id = 26 ) then
        begin
            update ctlrms
                set ctlrms_sbttl$ = ( select sum ( movapt_sbtt$ ) from movapt where movapt_ctlgrmsn_id = new.movapt_ctlgrmsn_id and movapt_a = 1 and movapt_doc_id = 26 )
                where ctlrms_id = new.movapt_ctlgrmsn_id;
        end
END^
SET TERM ; ^
