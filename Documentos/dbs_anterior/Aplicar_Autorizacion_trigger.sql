SET TERM ^ ;
ALTER TRIGGER "autdcs: Aplicar Autorizacion" ACTIVE
BEFORE INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/*
Elabora: anhe 07102012
Proposito: Con este procedimiento aplicamos la autorizacion 
Revisiones:
    06122012: se agrega la autorizacion del documento remision no estaba y se agrega la bandera de autorizacion para el control de status sepa que se trata
    de una autorizacion y la aplique sin tomar en cuenta el control.
    22012013: se agrega la autorizacion de devoluciones de almacen, 
    06092013: se agrega autorizacion para documentos de comisiones
    11092021: se modifica autorizacion del pedido, cuando se cancela se debe actualizar la tabla de pedidos con la clave del usuario y la fecha
*/
    if ( new.autdcs_doc_id = 26 ) then
        begin
            if ( new.autdcs_ctlrms_id is not null ) then
                begin
                    if ( new.autdcs_sttsaut_id = 0 ) then
                        begin
                            update ctlrms set ctlrms_sttsdcs_id = new.autdcs_sttsaut_id, ctlrms_aut_id = 1, CTLRMS_FCCNCL = new.AUTDCS_FN, CTLRMS_USU_M = new.AUTDCS_USU_N
                                where ctlrms_id = new.autdcs_ctlrms_id ;
                        end
                    else 
                        begin
                            update ctlrms set ctlrms_sttsdcs_id = new.autdcs_sttsaut_id , ctlrms_aut_id = 1
                                where ctlrms_id = new.autdcs_ctlrms_id ;
                        end
                end
        end
    if ( new.autdcs_doc_id = 28 ) then
        begin
            if ( new.autdcs_ctlcbrnz_id is not null ) then
                begin
                    update cntbl 
                        set cntbl.cntbl_sttsdcs_id = new.autdcs_sttsaut_id ,
                        cntbl_a = 2
                    where cntbl.cntbl_poliza = new.autdcs_ctlcbrnz_id and cntbl.cntbl_doc_id = 28 ;
                end
        end
        
    if ( new.autdcs_doc_id = 29 ) then
        begin
            if ( new.autdcs_ctlalmpt1_id is not null ) then
                begin
                    update almpt set almpt.almpt_sttsdcs_id = new.autdcs_sttsaut_id 
                        where almpt.almpt_id = new.autdcs_ctlalmpt1_id ;
                end
        end
    if ( new.autdcs_doc_id = 30 ) then
        begin
            if ( new.autdcs_ctlalmpt0_id is not null ) then
                begin
                    update almpt set almpt.almpt_sttsdcs_id = new.autdcs_sttsaut_id 
                        where almpt.almpt_id = new.autdcs_ctlalmpt0_id ;
                end
        end
    /* autorización de los status de los pedidos */
    if ( new.autdcs_doc_id = 33 ) then
        begin
            if ( new.autdcs_ctlalmpt0_id is not null and new.autdcs_ctlaplccn_id > 0 ) then
                begin
                    if (new.autdcs_sttsaut_id = 0) then
                    begin
                        update pddcln set pddcln.pddcln_sttsdcs_id=new.autdcs_sttsaut_id, pddcln.pddcln_usu_cncl=new.autdcs_usu_n, pddcln.pddcln_fccncl=new.autdcs_fn
                            where pddcln.pddcln_id=new.autdcs_ctlalmpt0_id ;
                    end 
                    if (new.autdcs_sttsaut_id > 0) then
                    begin
                        update pddcln set pddcln.pddcln_sttsdcs_id=new.autdcs_sttsaut_id
                            where pddcln.pddcln_id=new.autdcs_ctlalmpt0_id ;
                    end
                end
            else
                begin
                    if ( new.autdcs_ctlalmpt0_id is not null and new.autdcs_ctlaplccn_id = -1 ) then
                        begin
                            update pddcln set pddcln.pddcln_fecaco=new.autdcs_fcemsn
                                where pddcln.pddcln_id=new.autdcs_ctlalmpt0_id ;
                        end
                end
        end 
    
    if ( new.autdcs_doc_id = 34 ) then
        begin
            if ( new.autdcs_ctlalmpt1_id is not null ) then
                begin
                    update almpt set almpt.almpt_sttsdcs_id = new.autdcs_sttsaut_id 
                        where almpt.almpt_id = new.autdcs_ctlalmpt1_id ;
                end
        end
    if ( new.autdcs_doc_id = 36 ) then  -- recibos de comisiones
        begin
            if ( new.autdcs_ctlcbrnz_id is not null ) then
                begin
                    update cntbl 
                        set cntbl.cntbl_sttsdcs_id = new.autdcs_sttsaut_id,
                            cntbl.cntbl_idclass=1 ,
                            CNTBL.cntbl_feccbro=null
                    where cntbl.cntbl_id = new.autdcs_ctlcbrnz_id 
                      and cntbl.cntbl_doc_id = 36 ;
                      if (new.autdcs_sttsaut_id=0) then
                        begin
                            execute procedure comision_rem_pagados ( new.autdcs_ctlcbrnz_id, 0 );
                        end
                end
        end
END^
SET TERM ; ^