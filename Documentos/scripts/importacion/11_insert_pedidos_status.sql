-- IMPORTACIÓN DE AUTORIZACIONES DEL CAMBIO DE STATUS DE PEDIDOS
INSERT INTO PDCLS (PDCLS_STTS_ID, PDCLS_PDCLN_ID, PDCLS_OBSRV, PDCLS_USU_M, PDCLS_FM, PDCLS_TTL, PDCLS_CLMTV) 

SELECT 

a.AUTDCS_STTSAUT_ID   AS PDCLS_STTS_ID,
a.AUTDCS_CTLALMPT0_ID AS PDCLS_PDCLN_ID,
a.AUTDCS_OBSRV        AS PDCLS_OBSRV,
a.AUTDCS_USU_N        AS PDCLS_USU_M,
a.AUTDCS_FN           AS PDCLS_FM,
PDDCLN.PDDCLN_TTL$    AS PDCLS_TTL ,
'00: No definido'     AS PDCLS_CLMTV
FROM AUTDCS a
LEFT JOIN PDDCLN ON a.AUTDCS_CTLALMPT0_ID = PDDCLN.PDDCLN_ID
WHERE AUTDCS_DOC_ID=33 AND a.AUTDCS_CTLALMPT0_ID NOT IN(4866,4936,5218,5872,8710,11715,12141,19171)