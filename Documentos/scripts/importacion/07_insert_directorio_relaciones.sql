/* *******************************************************************************************************************************************
Importacion de datos paso 7
** importar relaciones del directorio a la nueva tabla, es necesario remover la llave  ("DRCTRR_DRCTR_ID" = 1160, "DRCTRR_CTREL_ID" = 1)
********************************************************************************************************************************************* */
INSERT INTO DRCTRR (DRCTRR_DRCTR_ID, DRCTRR_CTREL_ID, DRCTRR_CTDSC_ID, DRCTRR_A, DRCTRR_NOTA, DRCTRR_USR_N, DRCTRR_FN, DRCTRR_USR_M, DRCTRR_FM) 
SELECT 
    RLCDRC.RLCDRC_DRCTR_ID          AS DRCTRR_DRCTR_ID,
    RLCDRC.RLCDRC_LSTSYS_ID         AS DRCTRR_CTREL_ID,
    RLCDRC.RLCDRC_CMSN_ID           AS DRCTRR_CTDSC_ID,
    RLCDRC.RLCDRC_A                 AS DRCTRR_A,
    RLCDRC.RLCDRC_OBSR              AS DRCTRR_NOTA,
    COALESCE(RLCDRC.RLCDRC_USU_N, 'SYSDBA',RLCDRC.RLCDRC_USU_N)     AS DRCTRR_USR_N,
    COALESCE(RLCDRC.RLCDRC_FN, CURRENT_TIMESTAMP, RLCDRC.RLCDRC_FN) AS DRCTRR_FN,
    RLCDRC.RLCDRC_USU_M             AS DRCTRR_USR_M,
    RLCDRC.RLCDRC_FM                AS DRCTRR_FM
FROM
    RLCDRC 
LEFT JOIN DRCTR ON RLCDRC.RLCDRC_DRCTR_ID = DRCTR.DRCTR_ID 
WHERE RLCDRC.RLCDRC_DOC_ID = 1030 AND RLCDRC.RLCDRC_DRCTR_ID > 0 AND RLCDRC.RLCDRC_A > 0
AND (RLCDRC.RLCDRC_LSTSYS_ID IS NOT NULL AND RLCDRC.RLCDRC_LSTSYS_ID <> 0)
ORDER BY RLCDRC.RLCDRC_DRCTR_ID, RLCDRC.RLCDRC_LSTSYS_ID ASC
