﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Tienda.Builder;

namespace Jaeger.Aplication.CPLite.Tienda.Services {
    public class PedidosService : PedidoService, IPedidosService {
        public PedidosService() : base() {

        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.pedidoClienteRepository.GetList<T1>(conditionals);
        }

        public List<PedidoClientePrinter> GetRemisiones(List<PedidoClientePrinter> pedidos) {
            var ints = pedidos.Select(it => it.IdPedido).ToList();
            var _remisiones = this.remisionadoPTRepository.GetList<RemisionDetailModel>(new List<IConditional> { new Conditional("RMSN_PDD_ID", string.Join(",", ints), ConditionalTypeEnum.In) });
            if (_remisiones != null) {
                for (int i = 0; i < pedidos.Count; i++) {
                    pedidos[i].Remisiones = _remisiones.Where(it => it.IdPedido == pedidos[i].IdPedido).ToList();
                }
            }
            return pedidos;
        }

        public static IPedidoQueryBuilder Query() { return new PedidoQueryBuilder(); }
    }
}