﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class ReciboCobroCatalogoForm : UI.Banco.Forms.MovimientosForm {
        protected internal RadMenuItem NuevoRecibo = new RadMenuItem { Text = "Recibo", Name = "NuevoRecibo", ToolTipText = "Nuevo recibo de cobro" };

        public ReciboCobroCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(MovimientoBancarioEfectoEnum.Ingreso) {
            this._Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += ReciboCobroCatalogoForm_Load;
        }

        private void ReciboCobroCatalogoForm_Load(object sender, EventArgs e) {
            this.Icon = base.Icon;
            this.Text = "Cliente: Cobranza";
            this.NuevoRecibo.Click += Nuevo_Click;
            this.TMovimiento.Nuevo.Items.Add(this.NuevoRecibo);

            this.TMovimiento.Nuevo.Enabled = this._Permisos.Agregar;
            this.TMovimiento.Cancelar.Enabled = this._Permisos.Cancelar;
            this.TMovimiento.Editar.Enabled = this._Permisos.Editar;
            this.Movimientos.Cancelar = this._Permisos.Cancelar;
            this.Movimientos.Auditar = this._Permisos.Autorizar;
            this.Movimientos.GridData.AllowEditRow = this._Permisos.Status;
            this.TMovimiento.ExportarExcel.Enabled = this._Permisos.Exportar;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this.Service = new Aplication.CPLite.Banco.Services.BancoService();
        }

        public virtual void Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ReciboCobroForm(this.Service)) {
                nuevo.ShowDialog(this);
            }
        }

        public override void TMovimiento_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                using (var _editar = new ReciboCobroForm(seleccionado, this.Service)) {
                    _editar.ShowDialog(this);
                }
            }
        }

        public override void TMovimiento_IComprobante_Click(object sender, EventArgs e) {
            var seleccionado = this.Movimientos.Current();
            if (seleccionado != null) {
                seleccionado.FormatoImpreso = MovimientoBancarioFormatoImpreso.ReciboCobro;
                var imprimir = new UI.Banco.Forms.ReporteForm(new MovimientoBancarioPrinter(seleccionado));
                imprimir.Show();
            }
        }
    }
}
