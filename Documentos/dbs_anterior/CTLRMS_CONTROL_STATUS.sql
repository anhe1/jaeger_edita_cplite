SET TERM ^ ;
CREATE TRIGGER "ctlrms: Control Status" FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
declare variable subtotal$ dom_moneda;
declare variable dias      dom_id;

BEGIN 
/*
Elabora: anhe 07102012
Proposito:
    26-remision
Revisiones
    26102012: se agrega corrección del subtotal en la remision antes de imprimir. 
            se agrega error_0032 para que el usuario sea forzado a introducir nombre de la persona que recibe.
    07082013: se agrega consulta para calcular la fecha de vencimiento del documento a partir de la fecha de entrega 
        al cliente.
    07022015: se modifco el calculo del subtotal de la remision en el status 2
*/ 
    /* Fecha de Entrega a Cliente */
    
    if ( new.ctlrms_doc_id = 26 and old.ctlrms_sttsdcs_id = 1 and new.ctlrms_sttsdcs_id = 2 and new.ctlrms_sttsdcs_id <> 0 and new.ctlrms_aut_id = 0 ) then
        begin
            select sum ( movapt.MOVAPT_SBTT$ )
            from movapt
            where movapt.movapt_a = 1 and movapt.MOVAPT_CTLGRMSN_ID = new.CTLRMS_ID
            into :subtotal$;
            /*update ctlrms set ctlrms_sbttl$=(select sum(movapt.movapt_sbtt$) from movapt where movapt.movapt_ctlgrmsn_id=new.ctlrms_id and movapt.movapt_a=1) 
                                where ctlrms_id=new.ctlrms_id;
            */
            if ( subtotal$ is null or subtotal$ = 0 ) then
                begin
                    exception error_0031;
                end
            if ( new.ctlrms_sbttl$ = 0 or new.ctlrms_sbttl$ is null) then
                begin 
                    new.ctlrms_sbttl$=subtotal$;
                end
            
        end 
    
    if ( new.ctlrms_doc_id = 26 and new.ctlrms_fcentrg is not null and new.ctlrms_sttsdcs_id = 2 and new.ctlrms_sttsdcs_id <> 0 and new.ctlrms_aut_id = 0 ) then
        begin
            new.ctlrms_sttsdcs_id = 3;
            if ( new.ctlrms_cntt is null or new.ctlrms_cntt = '' ) then
                begin
                    exception error_0032;
                end
                    /*se calcula la fecha de vencimiento a partir de la fecha de entrega al cliente mas los días de credito*/
                    select (case when drctr.drctr_dscrdt is null then 0 else drctr.drctr_dscrdt end)
                    from drctr
                    where drctr.drctr_id = new.ctlrms_drctr_id
                    into :dias;
                    new.ctlrms_fchvnc = new.ctlrms_fcentrg + :dias;
                    if (new.ctlrms_pddclnt_id is not null) then
                        begin
                        /* actualizamos el status del pedido */
                            update pddcln set pddcln_sttsdcs_id=4 where pddcln_id=new.ctlrms_pddclnt_id;
                            
                        end

        end 

    if ( new.ctlrms_doc_id = 26 and new.ctlrms_fcentrg is not null and new.ctlrms_fccbrnz is not null and new.ctlrms_sttsdcs_id <> 0 and old.ctlrms_sttsdcs_id = 3 and new.ctlrms_aut_id = 0 ) then
        begin
            new.ctlrms_sttsdcs_id = 4;
            subtotal$=0;
            select sum ( movapt.MOVAPT_SBTT$ )
            from movapt
            where movapt.movapt_a = 1 and movapt.MOVAPT_CTLGRMSN_ID = new.CTLRMS_ID
            into :subtotal$;
            if ( subtotal$ is null or subtotal$ = 0 ) then
                begin
                    exception error_0031;
                end
            /* actualizamos el subtotal de la remision 11082015 */

                    new.ctlrms_sbttl$=subtotal$;


        end
		
    if ( new.ctlrms_doc_id = 26 and new.ctlrms_aut_id = 1 ) THEN
        begin 
            new.ctlrms_aut_id = 0;
        end
END^
SET TERM ; ^
