﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    /// <summary>
    /// Repositorio para Modelos-Especificaciones
    /// </summary>
    public class SqlFbModeloEspecificacionRepository : Abstractions.RepositoryMaster<ModeloYEspecificacionModel>, ISqlModeloEspecificacionRepository {
        public SqlFbModeloEspecificacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTMDLE WHERE ((CTMDLE_ID = @CTMDLE_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTMDLE_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(ModeloYEspecificacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTMDLE ( CTMDLE_ID, CTMDLE_A, CTMDLE_CTPRD_ID, CTMDLE_CTMDL_ID, CTMDLE_CTESPC_ID, CTMDLE_ATRZD, CTMDLE_ALM_ID, CTMDLE_VIS, CTMDLE_EXT, CTMDLE_MIN, CTMDLE_MAX, CTMDLE_REORD, CTMDLE_USR_N, CTMDLE_FN) 
                                             VALUES(@CTMDLE_ID,@CTMDLE_A,@CTMDLE_CTPRD_ID,@CTMDLE_CTMDL_ID,@CTMDLE_CTESPC_ID,@CTMDLE_ATRZD,@CTMDLE_ALM_ID,@CTMDLE_VIS,@CTMDLE_EXT,@CTMDLE_MIN,@CTMDLE_MAX,@CTMDLE_REORD,@CTMDLE_USR_N,@CTMDLE_FN)"
            };
            sqlCommand.Parameters.AddWithValue("@CTMDLE_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_ATRZD", item.IsAutorizado);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_VIS", item.IdVisibilidad);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_EXT", item.Existencia);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_MIN", item.Minimo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_MAX", item.Maximo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_REORD", item.ReOrden);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_FN", item.FechaNuevo);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(ModeloYEspecificacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTMDLE SET CTMDLE_A = @CTMDLE_A, CTMDLE_CTPRD_ID = @CTMDLE_CTPRD_ID, CTMDLE_CTMDL_ID = @CTMDLE_CTMDL_ID, CTMDLE_CTESPC_ID = @CTMDLE_CTESPC_ID, CTMDLE_ATRZD = @CTMDLE_ATRZD, CTMDLE_ALM_ID = @CTMDLE_ALM_ID, CTMDLE_VIS = @CTMDLE_VIS, CTMDLE_EXT = @CTMDLE_EXT, CTMDLE_MIN = @CTMDLE_MIN, CTMDLE_MAX = @CTMDLE_MAX, CTMDLE_REORD = @CTMDLE_REORD, CTMDLE_USR_M = @CTMDLE_USR_M, CTMDLE_FM = @CTMDLE_FM WHERE ((CTMDLE_ID = @CTMDLE_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@CTMDLE_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_ATRZD", item.IsAutorizado);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_VIS", item.IdVisibilidad);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_EXT", item.Existencia);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_MIN", item.Minimo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_MAX", item.Maximo);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_REORD", item.ReOrden);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@CTMDLE_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public ModeloYEspecificacionModel GetById(int index) {
            return this.GetList<ModeloYEspecificacionModel>(new List<IConditional>() { new Conditional("CTMDLE_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<ModeloYEspecificacionModel> GetList() {
            return this.GetList<ModeloYEspecificacionModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                //CommandText = @"SELECT * FROM CTMDLE @condiciones"
                CommandText = @"SELECT * FROM CTMDLE LEFT JOIN CTMDLX ON CTMDLE.CTMDLE_CTMDL_ID = CTMDLX.CTMDLX_CTMDL_ID AND CTMDLE.CTMDLE_CTESPC_ID = CTMDLX.CTMDLX_CTESPC_ID @wcondiciones"
            };

            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public ModeloYEspecificacionModel Save(ModeloYEspecificacionModel model) {
            if (model.Id == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.Id = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }
    }
}
