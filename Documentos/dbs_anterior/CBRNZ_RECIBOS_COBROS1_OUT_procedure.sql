SET TERM ^ ;
CREATE PROCEDURE CBRNZ_RECIBOS_COBROS1_OUT (
    INPUT_POLIZA_ID Integer,
    DIAS_PROM Integer,
    INPUT_OPTION_UPDATE Integer )
RETURNS (
    RLCNCBRNZ_ID DOM_ID,
    RLCNCBRNZ_REM_ID DOM_ID,
    DRCTR_ID DOM_ID,
    CATPRD_ID DOM_ID,
    CMSDSC_ID DOM_ID,
    DIAS_TRANS DOM_ID,
    CBRNZ_FECPAG DOM_FH,
    CBRNZ_FECCAL DOM_FH,
    REM_FECEMSN DOM_FH,
    REM_FECENTR DOM_FH,
    CTLDSC DOM_STR50,
    DDSCN DOM_STR50,
    DSCNT DOM_MONEDA,
    REM_DSCNT$ DOM_MONEDA,
    SUM_VENTA$ DOM_MONEDA,
    REM_SBTTL$ DOM_MONEDA,
    REM_IMPST DOM_MONEDA,
    REM_IMPST$ DOM_MONEDA,
    REM_COBRA$ DOM_MONEDA,
    REM_CBRIVA$ DOM_MONEDA )
AS
BEGIN
/*
Elabora: anhe 12122012
Proposito: aplicar descuento a las remisiones relacionadas al documento de cobro asi como verificar si es necesario recalcular el descuento
    si la fecha de pago de los mismos se modifico durante la autorización e ingreso a la boveda. Se creo de nuevo todo el procedimiento tomando en
    cuenta que el valor que guardamos como descuento es el total de la remision + iva menos lo que debe ser cobrado. Tambien se agrego al historico del
    recibo el descuento que le tocaba en ese momento de la historia. Tambien se graba en la remisión el descuento y se llama al procedimiento de calculo
    de lo cobrado en las remisiones para que se sume lo cobrado al momento.
Revisiones:
    12122012: 
 */

    if ( INPUT_POLIZA_ID is not null and INPUT_POLIZA_ID <> 0 ) then
        begin
            for
                select rlcncbrnz.cntbl_id, rlcncbrnz.cntbl_rem_id, rem.ctlrms_drctr_id, rem.ctlrms_catprd_id, cbrnz.cntbl_fecdocs, rem.ctlrms_fccal, rem.ctlrms_fcemsn, rem.ctlrms_fcentrg,
                    ( select sum ( ctlrms_sbttl$ ) from ctlrms where ctlrms_sttsdcs_id > 1 and ctlrms_fcemsn >= ( rem.ctlrms_fcemsn - :dias_prom ) and ctlrms_fcemsn <= ( rem.ctlrms_fcemsn ) and ctlrms_drctr_id = rem.ctlrms_drctr_id and ctlrms_id <> rem.ctlrms_id group by ctlrms_drctr_id),
                    rem.ctlrms_impst,
                    rem.ctlrms_sbttl$
                from cntbl rlcncbrnz, cntbl cbrnz, ctlrms rem
                where 
                        cbrnz.cntbl_poliza = :INPUT_POLIZA_ID       --< numero de poliza
                    and rlcnCbrnz.cntbl_poliza = Cbrnz.cntbl_poliza --< relacion partidas -> recibos
                    and rlcnCbrnz.cntbl_rem_id = rem.ctlrms_id      --< relacion con remisiones
                    and rlcnCbrnz.cntbl_doc_id = 2800               --< solo documentos relacionados
                    and Cbrnz.cntbl_doc_id = 28                     --< poliza
                    and rlcnCbrnz.cntbl_a = 1
                into :rlcnCbrnz_id, :rlcnCbrnz_rem_id, :drctr_id, :catprd_id,:cbrnz_fecpag, :cbrnz_fecCal, :rem_fecEmsn, :rem_fecEntr, :sum_Venta$, :rem_impst, :rem_sbttl$
            do
                begin
                    dias_trans = :cbrnz_fecpag - :rem_fecEntr;
                    if ( dias_trans = 0 ) then dias_trans = 1; 
                    if ( dias_prom > 0) then             --< si son diferentes recalculamos el descuento.
                        begin
                            select p.output_cmsdsc_id, p.output_catalogo, p.output_descuent, p.output_factor
                            from cbrnz_descuent2(:drctr_id, :dias_trans, :catprd_id, :rem_fecEmsn, :sum_Venta$) p
                            into :cmsdsc_id, :ctldsc, :ddscn, :dscnt;
                            rem_cobra$ = :rem_sbttl$ * :dscnt;
                            rem_impst$ = :rem_cobra$ * rem_impst;
                            rem_cbriva$ = :rem_cobra$ + rem_impst$;
                            rem_dscnt$ = ( :rem_sbttl$ + ( :rem_sbttl$ * rem_impst) ) - rem_cbriva$; 
                            
                            if ( INPUT_OPTION_UPDATE = 1 ) then 
                                begin
                                    /*update ctlrms
                                        set 
                                            ctlrms_catdsc_id = :cmsdsc_id,
                                            ctlrms_ctldsc = :ctldsc,
                                            ctlrms_dscnt = :dscnt,
                                            ctlrms_total$ = :rem_cbriva$,
                                            --ctlrms_impst$ = :cbrmsn0_$impst,
                                            ctlrms_dscnt$ = :rem_dscnt$ ,
                                            ctlrms_ddscn = :ddscn,
                                            ctlrms_fcultpg = :cbrnz_fecpag,
                                            ctlrms_fccal = current_date
                                            --ctlrms_hstrl = 'si paso'
                                    where ctlrms_id = :rlcnCbrnz_rem_id;
                                    */
                                    UPDATE CNTBL 
                                        set 
                                            cntbl_fctr2 = :dscnt,
                                            cntbl_rfrnc1 = :ctldsc,
                                            cntbl_rfrnc2 = :ddscn,
                                            cntbl_sbtt$ = :rem_sbttl$,
                                            cntbl_total$ =  :rem_cbriva$,
                                            cntbl_dsct$ = :rem_dscnt$,
                                            cntbl_trns_id = :dias_trans
                                    WHERE CNTBL_id = :rlcnCbrnz_id;
                                    
                                    --execute procedure cbrnz_remision_cobros ( :rlcnCbrnz_rem_id , :rem_cbriva$ );
                                end
                        end
                    else
                        begin
                        end
                    suspend;
                end
        end
    else
        begin
            exception ERROR_0018;
        end
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_RECIBOS_COBROS1_OUT TO  SYSDBA;

