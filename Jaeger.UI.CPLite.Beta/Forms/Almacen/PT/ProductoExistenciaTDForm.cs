﻿using System;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    internal class ProductoExistenciaTDForm : UI.Almacen.Forms.ProductoExistenciaTDForm {
        private Jaeger.Aplication.Tienda.Contracts.ICatalogoProductosService _productosCatalogoService;
        public ProductoExistenciaTDForm(UIMenuElement menuElement) : base(){
            this.Load += ProductoExistenciaTDForm_Load;
        }

        private void ProductoExistenciaTDForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Existencias TD";
            this.Service = new Aplication.CPLite.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
            this._productosCatalogoService = new Aplication.CPLite.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
            this.Almacen.DataSource = this._productosCatalogoService.GetAlmacenes();
        }
        

        public override void Cargar() {
            if (this.Almacen.SelectedItem == null)
                return;
            var current = ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            this._DataSource = this.Service.GetList<ProductoModeloExistenciaModel>(CatalogoModeloService.Query().ByIdAlmacen(current.IdAlmacen + 2).Activo().Build()).ToList<ProductoModeloExistenciaModel>();
        }
    }
}
