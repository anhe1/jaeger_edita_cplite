/* *******************************************************************************************************************************************
   Proyecto EDITA-CPLite Versión 3 0 0 (Bolsa de regalo, BETA)
   Proposito: Reemplazar la versión LiteCP escrita en Visual Basic 6, con la modalidad CP para el manejo de la base de datos, esta versión
   incorpora framework telerik 
******************************************************************************************************************************************* */

* Menú de la Aplicación

https://tree.nathanfriend.com/?s=(%27optws!(%27fancy!true~fullPath!false~trailingSlash!false~rootDot!true)~source!(%27source!%27EditaYCP%20LiteY*1.%20t.Administrac901.1OCliIe7b.ExpediIe7.0_q76%25_Nota8DescuIo0GCobranza0GZ7Gq7GRemis90*GPXCobrar0*%241.2OTesorer%C3%ADa0kb.Banco0*kGConcepto7GBeneficiario7GMovimiIo7b.Z7%24*GPXConceptos01.3OAlmac%C3%A9n0_W8%237*J_%237_%23s-Modelo7_Unidade7_Especificacwe76q7_Historial0*_PXSurtir0_MovimiIo7_Vale7_Devolucwe7_%25*_Existencia76Zs01.4OTienda0GAjuste7JJ6Seccwe7.0_Cupone76Banner0_FAQ%22s01.5OVIas06W8Precios06qs06Vendedore7b.W0GComiswe7b.Comis9%20pXRemis90GRecibos8Comis90GZ76VIasKIaKIa%20pXcliIe0_Resumen8vIasY*5.%20t.HerramiIasY*%20OConfigurac90b.Parametros06Emisor06Empresa06Perfil06Usuarios06AvanzadoY*%20OAyuda0b.Acerca%20de%27)~versw!%271%27)*%20%20%20%200Y*_%20%20b.7s0*8%20de%209i%C3%B3nG*b.IentJ6Categor%C3%ADa7K%20pXvendedor0_Estado8cuO%20g.WCat%C3%A1logoXor%20Y%5Cn*ZReporte_*6kGCuIas%20Bancaria7qPedidowion%23Producto%24GEstado8CuIa0%25Remiswado0%01%25%24%23wqk_ZYXWOKJIG98760*

.
└── Edita
    └── CP Lite
        ├── 1. t.Administración
        │   ├── 1.1 g.Clientes
        │   │   ├── b.Expedientes
        │   │   ├── .
        │   │   │   ├── b.Pedidos
        │   │   │   ├── b.Remisionado
        │   │   │   └── b.Nota de Descuento
        │   │   ├── b.Cobranza
        │   │   └── b.Reportes
        │   │       ├── b.Pedidos
        │   │       ├── b.Remisión
        │   │       ├── b.Por Cobrar
        │   │       └── b.Estado de Cuenta
        │   ├── 1.2 g.Tesorería
        │   │   ├── b.Cuentas Bancarias
        │   │   ├── b.Banco
        │   │   │   ├── b.Cuentas Bancarias
        │   │   │   ├── b.Conceptos
        │   │   │   ├── b.Beneficiarios
        │   │   │   └── b.Movimientos
        │   │   └── b.Reportes
        │   │       ├── b.Estado de Cuenta
        │   │       └── b.Por Conceptos
        │   ├── 1.3 g.Almacén
        │   │   ├── b.Catálogo de Productos
        │   │   │   ├── b.Categorías
        │   │   │   ├── b.Productos
        │   │   │   ├── b.Productos-Modelos
        │   │   │   ├── b.Unidades
        │   │   │   └── b.Especificaciones
        │   │   ├── b.Pedidos
        │   │   │   ├── b.Historial
        │   │   │   └── b.Por Surtir
        │   │   ├── b.Movimientos
        │   │   │   ├── b.Vales
        │   │   │   ├── b.Devoluciones
        │   │   │   ├── b.Remisionado
        │   │   │   └── b.Existencias
        │   │   └── b.Reportes
        │   ├── 1.4 g.Tienda
        │   │   ├── b.Ajustes
        │   │   │   ├── b.Categorías
        │   │   │   ├── b.Categorías
        │   │   │   └── b.Secciones
        │   │   └── .
        │   │       ├── b.Cupones
        │   │       ├── b.Banner
        │   │       └── b.FAQ's
        │   └── 1.5 g.Ventas
        │       ├── b.Catálogo de Precios
        │       ├── b.Pedidos
        │       └── b.Vendedores
        │           ├── b.Catálogo
        │           ├── b.Comisiones
        │           ├── b.Comisión por Remisión
        │           ├── b.Recibos de Comisión
        │           └── b.Reportes
        │               ├── b.Ventas por vendedor
        │               ├── b.Estado de cuenta por vendedor
        │               ├── b.Estado de cuenta por cliente
        │               └── b.Resumen de ventas
        └── 5. t.Herramientas
            ├── g.Configuración
            │   └── b.Parametros
            │       ├── b.Emisor
            │       ├── b.Empresa
            │       ├── b.Perfil
            │       ├── b.Usuarios
            │       └── b.Avanzado
            └── g.Ayuda
                └── b.Acerca de


/* **********************************************************************************************************************************
+ Preguntas
********************************************************************************************************************************** */
    Ventas-Catálogo de comisiones
    - ¿Es útil la vigencia del catálogo?
    Esto es porque el catálogo actual contiene vigencia, es decir una vez expirado aparece la leyenda 'Catálogo no asigando' y entonces la comsión es cero (0)
    - ¿Es necesario que a los vendedores se le asigne más de un catálogo de comisiones?
    - ¿Existen tipos de comisiones por vendedor o cliente?

    1  en el pago de comisiones evitar pagar dos veces una comisión de una remisión a un vendedor, es decir que solo las remisiones que no esten relacionadas
    a un recibo de pago de comisiones debe ser utilizadas para un recibo nuevo
    2  podría ser necesario marcar todas los documentos de los cuales ya fueron pagadas comisión con alguna bandera o estatus
    3  talvez sea necesario considerar tener un fomato de pago de comisiones, en el banco se debe reflejar como un movimiento 'Egreso' y
    como un recibo de pago de comisines 
    4  la vista de pago de comisiones (ventas) solo se deben mostar los documentos que esten en un estado de 'Cobrado' para calcular la
    comision correspondiente, el usuario debera poder modificar el monto a pagar por comisión de esta forma el adminsitrador debera poder
    modificar el monto del pago por cada documento y el gerente podra realizar su 'Recibo de pago de comisión'
    5  es necesario un formato impreso deel recibo de comisión, en el banco se utiliza el formato del movimiento o bien colocar una leyenda
    como titulo en el formato impreso
	6  Para lista de precios
	- Solo pueden ser asignados modelos que no tienen variantes
	- Cuando el valor unitario es asignado en el modelo

	7  Para duplicar o clonar una remisión previa
	- la lista de productos o modelos se debe consultar para saber si
		1  Producto disponible
		2  Consulta de precios
		3  Que coincide con el receptor

	8  En el caso de los descuentos a clientes
		- El descuento se puede aplicar a un cliente minorista?
		- Se pueden combinar diferentes productos de mayorista y minorista en una remisión?
	9  Con que documentos se entrega el pedido a un cliente foraneo o cliente minorista en el envío 
	10  Los cupones de descuento se aplican desde el pedido?
*********************************************************************************************************************************** */

