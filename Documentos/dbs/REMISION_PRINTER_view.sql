CREATE VIEW VIEW_REMISION_PRINTER (ID, FOLIO, PEDIDO, VENDEDOR, CLAVE, CLIENTE, RFC, CREDITO, SUBTOTAL, IMPUESTO, TOTAL, CREO, LEYENDA, VENCE, EMISION, RECIBE, DISTRIBUCION, OBSERVACIONES, ENVIO, FISCAL)
AS                          
    select 
        rem.ctlrms_id id, 
        rem.ctlrms_folio folio, 
        rem.ctlrms_pddclnt_id pedido,
        vdd.vwvnddr_nom vendedor,
        dir.drctr_clv clave,
        dir.drctr_nom cliente, 
        dir.drctr_rfc rfc, 
        (case when dir.drctr_dscrdt is null then 0 else dir.drctr_dscrdt end) credito,
        (select sum(MOVAPT.MOVAPT_SBTT$) from MOVAPT where MOVAPT.MOVAPT_CTLGRMSN_ID=rem.CTLRMS_ID and MOVAPT.MOVAPT_A=1) subtotal,
        rem.ctlrms_impst impuesto,
        (rem.ctlrms_sbttl$ * 1.16 ) total,
        rem.ctlrms_usu_n creo,
        ( case when dir.drctr_dscrdt > 0 then 'Cliente con cr�dito' else 'Pago contra entrega' end ) as leyenda,
        ( case when dir.drctr_dscrdt > 0 then rem.ctlrms_fcemsn + dir.drctr_dscrdt else rem.ctlrms_fcemsn end ) as vence,
        rem.ctlrms_fcemsn emision,
        rem.ctlrms_cntt recibe,
        rem.ctlrms_cntt3 distribucion,
        rem.ctlrms_obsrv as observaciones,
        drc.vwdrccn_direc as envio,
        (select first 1 vwdrccn.vwdrccn_direc from vwdrccn where vwdrccn.vwdrccn_drctr_id = dir.drctr_id order by vwdrccn.vwdrcc_sec_id asc ) as fiscal
    from ctlrms rem
    left join drctr dir on rem.ctlrms_drctr_id = dir.drctr_id
    left join vwdrccn drc on rem.ctlrms_drccn_id = drc.vwdrccn_id
    left join vwvnddr vdd on rem.ctlrms_ctlgvddr_id = vdd.vwvnddr_id
    order by rem.ctlrms_id desc;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VIEW_REMISION_PRINTER TO  SYSDBA WITH GRANT OPTION;

