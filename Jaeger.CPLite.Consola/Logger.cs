﻿using System;
using Jaeger.Aplication.Base.Services;
using Jaeger.LockOn.Interfaces;
using Jaeger.LockOn.Services;

namespace Jaeger.CPLite.Consola {
    public class Logger {
        private IRecienteModel _User;
        private ILogin _Service = new LockOn.Services.Login();

        public Logger() {

        }

        public bool Login(string[] args) {
            bool _Success = false;

            using (ICommandService d1 = new CommandService()) {
                this._User = d1.Execute(args);
            }
            this._Service = LoginBuilder.Create().Load().Connect();

            if (this._Service.Execute(this._User)) {

                ConfigService.Synapsis = new Domain.Empresa.Entities.Configuracion {
                    RDS = new Domain.Empresa.Entities.BaseDatos()
                };
                ConfigService.Piloto = new Domain.Base.Profile.KaijuLogger();
                ConfigService.Synapsis = this._Service.Data;
                ConfigService.Piloto = this._Service.Piloto;
                _Success = true;
            }
            if (_Success) {
                Console.WriteLine("Login");
            }
            return _Success;

        }
    }
}
