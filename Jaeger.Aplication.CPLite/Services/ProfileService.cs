﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Aplication.CPLite.Services {
    /// <summary>
    /// servicio de perfiles de sistema, implementacion para version beta
    /// </summary>
    public class ProfileService : Kaiju.Services.ProfileService, Kaiju.Contracts.IProfileToService, Kaiju.Contracts.IProfileService {
        /// <summary>
        /// constructor
        /// </summary>
        public ProfileService() : base() { }

        protected override void OnLoad() {
            base.OnLoad();
            this._ProfileRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUIProfileRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._UserRolRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUserRolRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._RelacionRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUserRolRelacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public override List<UIMenuElement> GetMenus() {
            return this._MenuRepository.GetCPLite().ToList();
        }
    }
}
