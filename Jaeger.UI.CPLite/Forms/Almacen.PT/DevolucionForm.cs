﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using Telerik.WinControls;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class DevolucionForm : UI.Almacen.PT.Forms.DevolucionForm {
        protected List<LPrecioModel> lPrecios;
        protected internal IPrecioCatalogoService sPrecios;

        public DevolucionForm(IValeAlmacenService service) : base(service) {
            this.unidad = new UnidadService();
            this.Load += DevolucionForm_Load;
        }

        private void DevolucionForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén PT: Devolución";
            this.TDevolucion.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente;
            this.TDevolucion.Receptor.Service = new DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
            this.TDevolucion.Receptor.Seleccionado += Receptor_Seleccionado;
            this.sPrecios = new PrecioCatalogoService();
        }

        private void Receptor_Seleccionado(object sender, Domain.Contribuyentes.Entities.ContribuyenteDetailModel e) {
            if (e != null) {
                this.lPrecios = this.sPrecios.GetPrecio(e.IdDirectorio);
            }
        }

        public override void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TDevolucion.Comprobante.IdDirectorio == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            using (var _catalogoProductos = new ProductoModeloBuscarForm(new ProductosCatalogoService(Domain.Base.ValueObjects.AlmacenEnum.PT))) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        public override void TConceptos_Agregar_Producto(object sender, ProductoModeloXModel e) {
            if (this.TDevolucion.Comprobante.Conceptos.Count == 0) {
                this.TDevolucion.Comprobante.Conceptos = new BindingList<ValeAlmacenConceptoModel>();
                this.gConceptos.DataSource = this.TDevolucion.Comprobante.Conceptos;
                this.gConceptoParte.DataSource = this.TDevolucion.Comprobante.Conceptos;
                //this.GridConceptoParte.DataMember = "Parte";
            }
            if (e != null) {
                var nuevo = new ValeAlmacenConceptoModel {
                    IdProducto = e.IdProducto,
                    Identificador = e.Identificador,
                    IdModelo = e.IdModelo,
                    Activo = e.Activo,
                    Catalogo = e.Categoria,
                    Especificacion = e.Especificacion,
                    IdEspecificacion = e.IdEspecificacion,
                    IdUnidad = e.IdUnidad,
                    Marca = e.Marca,
                    Modelo = e.Modelo,
                    Producto = e.Producto,
                    Tamanio = e.Tamanio,
                    TasaIVA = e.TasaIVA,
                    Unidad = e.Unidad,
                    UnidadFactor = e.FactorUnidad,
                    IdTipoMovimiento = this.TDevolucion.Comprobante.IdTipoMovimiento
                };

                if (this.SinDuplicados.IsChecked == true) {
                    //var ed = this.TDevolucion.Comprobante.Conceptos.Where(it => it.Identificador == nuevo.Identificador);
                    //if (ed.Count() > 0) {
                    //    RadMessageBox.Show(this, "El producto o modelo ya se encuentra en la lista actual.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    //    return;
                    //}
                }

                try {
                    var precio = this.lPrecios.Where(it => it.IdEspecificacion == e.IdEspecificacion).FirstOrDefault();
                    if (precio != null) {
                        nuevo.ValorUnitario = precio.Unitario;
                        nuevo.IdPrecio = precio.IdPrecio;
                        nuevo.CostoUnitario = precio.Costo;
                    }
                    this.TDevolucion.Comprobante.Conceptos.Add(nuevo);
                } catch (Exception ex) {
                    RadMessageBox.Show(this, "No existe precio asignado a este producto, informe el error al administrador.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
