﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;
using Jaeger.Aplication.CPLite.Almacen.PT.Services;

namespace Jaeger.UI.CPLite.Builder {
    public class NotaDescuentoGridBuilder : GridViewBuilder, IGridViewBuilder, IDisposable, INotaDescuentoGridBuilder, INotaDescuentoTempletesGridBuilder, INotaDescuentoColumnsGridBuilder {
        #region templetes
        public INotaDescuentoTempletesGridBuilder Templetes() {
            this._Columns.Clear();
            return this;
        }

        public INotaDescuentoTempletesGridBuilder Master() {
            this.Folio().IdStatus().Clave().Cliente().FechaEmision().FechaCancela().IdMotivo().Importe().IdDocumento().Referencia().Contacto().Nota()
                .Numeros().FechaNuevo().Creo();
            return this;
        }

        public INotaDescuentoTempletesGridBuilder Documentos() {
            this.Folio().IdPedido().IdStatus().Clave().Cliente().IdDocumento().FechaEmision().SubTotal().TotalTrasladoIVA().GranTotal().Nota().Creo().FechaNuevo();
            return this;
        }
        #endregion

        #region columnas
        public INotaDescuentoColumnsGridBuilder Folio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "Folio",
                HeaderText = "Folio",
                Name = "Folio",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75,
                FormatString = this.FormarStringFolio
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder IdStatus() {
            var column = new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdStatus",
                HeaderText = "Status",
                Name = "IdStatus",
                Width = 75,
                DataSource = NotaDescuentoService.GetStatus(true),
                DisplayMember = "Descriptor",
                ValueMember = "Id"
            };

            var c0 = column.ConditionalFormattingObjectList;
            c0.Add(this.CanceladoConditionalFormat());
            this._Columns.Add(column);
            return this;
        }

        public INotaDescuentoColumnsGridBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 65
            });
            return this;
        }

        /// <summary>
        /// FieldName = Nombre
        /// </summary>
        public INotaDescuentoColumnsGridBuilder Cliente() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Cliente",
                Name = "Nombre",
                Width = 280
            });
            return this;
        }

        /// <summary>
        /// columna Fecha de Emision
        /// </summary>
        /// <returns></returns>
        public INotaDescuentoColumnsGridBuilder FechaEmision() {
            this._Columns.Add(new GridViewDateTimeColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaEmision",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Emisión",
                Name = "FechaEmision",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder FechaCancela() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaCancela",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Cancela",
                Name = "FechaCancela",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        /// <summary>
        /// obtener columna de motivo de devoluciones
        /// </summary>
        public INotaDescuentoColumnsGridBuilder ClaveMotivo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CvMotivo",
                HeaderText = "CvMotivo",
                Name = "CvMotivo",
                Width = 275
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder IdMotivo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                DataType = typeof(int),
                FieldName = "IdMotivo",
                HeaderText = "Cv. Motivo",
                Name = "IdMotivo",
                Width = 145,
                DataSource = NotaDescuentoService.GetMotivos(),
                DisplayMember = "Descriptor",
                ValueMember = "Id"
            });
            return this;
        }

        /// <summary>
        /// columna de Importe
        /// </summary>
        /// <returns></returns>
        public INotaDescuentoColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = "{0:n2}",
                HeaderText = "Total",
                Name = "Importe",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// columna SubTotal
        /// </summary>
        public INotaDescuentoColumnsGridBuilder SubTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = "{0:n2}",
                HeaderText = "SubTotal",
                Name = "SubTotal",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        /// <summary>
        /// Total del Traslado de IVA
        /// </summary>
        public INotaDescuentoColumnsGridBuilder TotalTrasladoIVA() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TotalTrasladoIVA",
                FormatString = "{0:n2}",
                HeaderText = "IVA",
                Name = "TotalTrasladoIVA",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder GranTotal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "GTotal",
                FormatString = "{0:n2}",
                HeaderText = "Total",
                Name = "Total",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 85
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder IdDocumento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdDocumento",
                HeaderText = "IdDocumento",
                IsVisible = false,
                Name = "IdDocumento",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 220
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder Referencia() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 150,
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder Contacto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Contacto",
                HeaderText = "Contacto",
                Name = "Contacto",
                Width = 195,
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder Nota() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nota",
                HeaderText = "Nota",
                Name = "Nota",
                Width = 200,
                IsVisible = false,
            });
            return this;
        }

        /// <summary>
        /// numero de partidas que contiene
        /// </summary>
        public INotaDescuentoColumnsGridBuilder Numeros() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NPartida",
                HeaderText = "Partidas",
                Name = "NPartida",
                Width = 50,
                TextAlignment = ContentAlignment.MiddleCenter,
                FormatString = "{0:N0}"
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaNuevo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                Width = 75,
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public INotaDescuentoColumnsGridBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }
        #endregion

        public INotaDescuentoColumnsGridBuilder IdPedido() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdPedido",
                HeaderText = "# Pedido",
                Name = "IdPedido",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        #region formatos condiciones
        /// <summary>
        /// formato condicional para registros inactivos
        /// </summary>
        public ExpressionFormattingObject CanceladoConditionalFormat() {
            return new ExpressionFormattingObject("Cancelacion", "IdStatus = 0", true) {
                RowForeColor = Color.SlateGray
            };
        }
        #endregion
    }
}
