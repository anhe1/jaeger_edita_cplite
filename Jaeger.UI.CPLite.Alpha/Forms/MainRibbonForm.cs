﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Builder;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Kaiju.Contracts;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.CPLite.Forms {
    public partial class MainRibbonForm : Telerik.WinControls.UI.RadRibbonForm {
        public MainRibbonForm() {
            InitializeComponent();
        }

        private void MainRibbonForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            RadMessageLocalizationProvider.CurrentProvider = new LocalizationProviderCustomerMessageBox();
            //ConfigService.Synapsis.RDS.Edita = new Domain.DataBase.Entities.DataBaseConfiguracion();
            //ConfigService.Synapsis.RDS.LiteCP.Database = "/dbs/fb/jaeger_ipr981125pn9b.fdb";
            //ConfigService.Synapsis.RDS.LiteCP.Database = "localhost/3050:/dbs/jaeger_BolsaRegalo50.fdb";
            //ConfigService.Synapsis.RDS.LiteCP.HostName = "ipo.mx";
            //ConfigService.Synapsis.RDS.LiteCP.Database = "localhost/3050:/dbs/jaeger_ipr981125pn9b.fdb";
            ConfigService.Synapsis.RDS.LiteCP.Charset = "UTF8";
            ConfigService.Synapsis.RDS.Edita.Database = "jaeger_ipr981125pn9b";

            using (var _espera = new Waiting1Form(this.GetMenus)) {
                _espera.Text = "Esperando respuesta del servidor ...";
                _espera.ShowDialog(this);
            }

            this.Text = ConfigService.Titulo();
            this.BEstado.Items.Add(new ButtonEmpresaBuilder().Usuario(ConfigService.Piloto.Clave).Version(Application.ProductVersion.ToString()).Empresa(ConfigService.Synapsis.Empresa.RFC).Build());
            this.MenuRibbonBar.SetDefaultTab();

            this.MenuRibbonBar.Visible = true;
        }

        #region botones
        private void AcercaDe_Click(object sender, EventArgs e) {
            var acercade_form = new AboutBoxForm();
            acercade_form.ShowDialog(this);
        }
        #endregion

        #region metodos privados
        private void Menu_Main_Click(object sender, EventArgs e) {
            var _item = (RadItem)sender;
            var _menuElement = ConfigService.GeMenuElement(_item.Name);

            if (_menuElement != null) {
                var localAssembly = new object() as Assembly;
                if (_menuElement.Assembly == "Jaeger.UI")
                    localAssembly = typeof(MainRibbonForm).Assembly;
                else
                    localAssembly = this.GetAssembly(_menuElement.Assembly);

                if (localAssembly != null) {
                    try {
                        if (_menuElement.Assembly == "Jaeger.UI") {
                            Type type = localAssembly.GetType(string.Format("{0}.CPLite.{1}", _menuElement.Assembly, _menuElement.Form), true);
                            this.Activar_Form(type, _menuElement);
                        } else {
                            Type type = localAssembly.GetType(string.Format("{0}.{1}", _menuElement.Assembly, _menuElement.Form), true);
                            this.Activar_Form(type, _menuElement);
                        }
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, "Main: " + ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                } else {
                    RadMessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private Assembly GetAssembly(string assembly) {
            try {
                return Assembly.Load(assembly);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void Activar_Form(Type type, UIMenuElement sender) {
            try {
                Form _form = (Form)Activator.CreateInstance(type, sender);
                if (_form.WindowState == FormWindowState.Maximized) {
                    _form.MdiParent = this;
                    _form.WindowState = FormWindowState.Maximized;
                    _form.Show();
                    this.RadDock.ActivateMdiChild(_form);
                } else {
                    _form.StartPosition = FormStartPosition.CenterParent;
                    _form.ShowDialog(this);
                }
            } catch (Exception ex) {
                RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void GetMenus() {
           

            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            MenuPermissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);

            this.MenuRibbonBar.Refresh();
        }
        #endregion
    }
}
