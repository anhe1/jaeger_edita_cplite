/* *******************************************************************************************
Importacion de datos paso 8
importar la informacion de la relacion entre vendedores y clienes (cartera), para este paso
en la tabla temporal debe estar desactivado el indice DRCTRC_VNDR_ID, DRCTRC_DRCTR_ID

ALTER TABLE DRCTRC DROP CONSTRAINT PK_DRCTRC_VNDR_ID
******************************************************************************************* */
ALTER TABLE DRCTRC DROP CONSTRAINT PK_DRCTRC_VNDR_ID;
INSERT INTO DRCTRC (DRCTRC_A, DRCTRC_DRCTR_ID, DRCTRC_VNDR_ID, DRCTRC_USR_M, DRCTRC_FM)
SELECT 
RLCDRC_A        AS DRCTRC_A, 
RLCDRC_DRCTR_ID AS DRCTRC_DRCTR_ID, 
RLCDRC_VNDDR_ID AS DRCTRC_VNDR_ID, 
COALESCE(RLCDRC_USU_M, RLCDRC_USU_N, RLCDRC_USU_M) AS DRCTRC_USR_M, 
COALESCE(RLCDRC_FM, RLCDRC_FN, RLCDRC_FM) AS DRCTRC_FM
FROM rlcdrc
WHERE rlcdrc_doc_id = 1011 AND RLCDRC_VNDDR_ID > 0 AND RLCDRC_A = 1 
ORDER BY RLCDRC_DRCTR_ID, RLCDRC_VNDDR_ID ASC