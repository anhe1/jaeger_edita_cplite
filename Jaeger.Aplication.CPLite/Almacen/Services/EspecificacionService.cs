﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.DataAccess.FB.Almacen.Repositories;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Almacen.Services {
    public class EspecificacionService : Aplication.Almacen.Services.EspecificacionService, IEspecificacionService {
        public EspecificacionService() : base() { }

        public override void OnLoad() {
            this.especificacionRepository = new SqlFbEspecificacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}