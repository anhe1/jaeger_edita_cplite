﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.DataAccess.FB.Contribuyentes.Repositories;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Tienda.Services {
    public class PedidoService : Aplication.Tienda.Services.PedidoService, IPedidoService {
        protected ISqlRemisionadoPTRepository remisionadoPTRepository;

        public PedidoService() : base() { }

        public override void OnLoad() {
            this.pedidoClienteRepository = new SqlFbPedidoClienteRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.partidaRepository = new SqlFbPedidoClientePartidaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.remisionadoPTRepository = new SqlFbRemisionadoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.direccionRepository = new SqlFbDomicilioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.contribuyenteRepository=new SqlFbContribuyenteRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public PedidoClienteDetailModel GetPedido(int index) {
            var _response = this.pedidoClienteRepository.GetPedido(index);
            if (_response != null) {
                _response.Partidas = this.GetPartidas(index);
                _response.Autorizaciones = new BindingList<IPedidoClienteStatusModel>(this.pedidoClienteRepository.GetList<PedidoClienteStatusModel>(new List<IConditional> { new Conditional("PDCLS_PDCLN_ID", index.ToString()) }).ToList<IPedidoClienteStatusModel>());
                var d0 = new BindingList<IPedidoClienteRelacionadoModel>(this.pedidoClienteRepository.GetList<PedidoClienteRelacionadoModel>(new List<IConditional> { new Conditional("PDCLR_PDCLN_ID", index.ToString()) }).ToList<IPedidoClienteRelacionadoModel>());
            }
            return _response;
        }

        public PedidoClienteDetailModel Save(PedidoClienteDetailModel model) {
            model = this.pedidoClienteRepository.Save(model);
            if (model.IdPedido != 0) {
                for (int i = 0; i < model.Partidas.Count; i++) {
                    model.Partidas[i].IdPedido = model.IdPedido;
                    model.Partidas[i].IdDirectorio = model.IdDirectorio;
                    model.Partidas[i] = this.partidaRepository.Save(model.Partidas[i]);
                }
            }
            return model;
        }

        public BindingList<PedidoClientePartidaDetailModel> GetPartidas(int idPedido) {
            return new BindingList<PedidoClientePartidaDetailModel>(this.partidaRepository.GetList(new List<IConditional> { new Conditional("PDCLP_PDCLN_ID", idPedido.ToString()) }).ToList());
        }

        public List<RemisionDetailModel> GetRemisiones(int idPedido) {
            return this.remisionadoPTRepository.GetList<RemisionDetailModel>(new List<IConditional> { new Conditional("RMSN_PDD_ID", idPedido.ToString()) }).ToList();
        }

        public PedidoClientePrinter GetPrinter(int index) {
            var _printer = new PedidoClientePrinter(this.GetPedido(index));
            if (_printer != null) {
                var _embarque = this.GetEmbarque(_printer.IdDireccion);
                _printer.Embarque = "No definido";
                if (_embarque != null) {
                    _printer.Embarque = _embarque.ToString();
                }
            }
            return _printer;
        }
    }
}