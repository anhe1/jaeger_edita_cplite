﻿namespace Jaeger.UI.CPLite.Forms.Clientes {
    partial class NotaDescuentoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotaDescuentoCatalogoForm));
            this.dataGridNota = new Telerik.WinControls.UI.RadGridView();
            this.gridAutorizacion = new Telerik.WinControls.UI.GridViewTemplate();
            this.gridNotaDevolucion = new Telerik.WinControls.UI.GridViewTemplate();
            this.TNota = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNota.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAutorizacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridNotaDevolucion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridNota
            // 
            this.dataGridNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridNota.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.dataGridNota.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridNotaDevolucion,
            this.gridAutorizacion});
            this.dataGridNota.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.dataGridNota.Name = "dataGridNota";
            this.dataGridNota.ShowGroupPanel = false;
            this.dataGridNota.Size = new System.Drawing.Size(1357, 623);
            this.dataGridNota.TabIndex = 2;
            // 
            // gridAutorizacion
            // 
            this.gridAutorizacion.Caption = "Autorización";
            this.gridAutorizacion.ViewDefinition = tableViewDefinition2;
            // 
            // gridNotaDevolucion
            // 
            this.gridNotaDevolucion.Caption = "Nota de devolución";
            this.gridNotaDevolucion.ViewDefinition = tableViewDefinition1;
            // 
            // TNota
            // 
            this.TNota.Dock = System.Windows.Forms.DockStyle.Top;
            this.TNota.Location = new System.Drawing.Point(0, 0);
            this.TNota.Name = "TNota";
            this.TNota.ShowActualizar = true;
            this.TNota.ShowAutosuma = false;
            this.TNota.ShowCancelar = true;
            this.TNota.ShowCerrar = true;
            this.TNota.ShowEditar = true;
            this.TNota.ShowEjercicio = true;
            this.TNota.ShowExportarExcel = false;
            this.TNota.ShowFiltro = true;
            this.TNota.ShowHerramientas = false;
            this.TNota.ShowImprimir = true;
            this.TNota.ShowNuevo = true;
            this.TNota.ShowPeriodo = true;
            this.TNota.Size = new System.Drawing.Size(1357, 30);
            this.TNota.TabIndex = 3;
            this.TNota.Nuevo.Click += this.TNota_Nuevo_Click;
            this.TNota.Editar.Click += this.TNota_Editar_Click;
            this.TNota.Cancelar.Click += this.TNota_Cancelar_Click;
            this.TNota.Imprimir.Click += this.TNota_Imprimir_Click;
            this.TNota.Actualizar.Click += this.TNota_Actualizar_Click;
            this.TNota.Filtro.Click += this.TNota_Filtro_Click;
            this.TNota.Cerrar.Click += this.TNota_Cerrar_Click;
            // 
            // NotaDescuentoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1357, 653);
            this.Controls.Add(this.dataGridNota);
            this.Controls.Add(this.TNota);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NotaDescuentoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CPLite: Notas de Descuento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.NotaDescuentoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNota.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAutorizacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridNotaDevolucion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Jaeger.UI.Common.Forms.ToolBarCommonControl TNota;
        private Telerik.WinControls.UI.RadGridView dataGridNota;
        private Telerik.WinControls.UI.GridViewTemplate gridAutorizacion;
        private Telerik.WinControls.UI.GridViewTemplate gridNotaDevolucion;
    }
}