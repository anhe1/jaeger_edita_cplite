SET TERM ^ ;
CREATE PROCEDURE APLICAR_CNTBL_POLIZA
AS
declare variable domID dom_id;
declare variable poliza dom_id;
begin
/*
este procedimiento asigna el id de relacion de las polizas con las partidas de la poliza esta
es una correcci�n porque se cambio la relaci�n del numero poliza
*/ 
    for
        select  cntbl_id,cntbl_poliza
        from    cntbl
        where   cntbl.cntbl_cntbl_id = 0
            and cntbl_doc_id = 28
            and cntbl_id between 184 and 1005
        order by cntbl_id asc
        into    :domID, :poliza
    do
        begin
            update cntbl set cntbl_cntbl_id = :domID where cntbl_doc_id = 2800 and cntbl_poliza = :poliza;
        end
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE APLICAR_CNTBL_POLIZA TO  SYSDBA;

