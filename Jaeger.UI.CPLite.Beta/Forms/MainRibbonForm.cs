﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI.Localization;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Builder;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Forms {
    public partial class MainRibbonForm : Telerik.WinControls.UI.RadRibbonForm {
        public MainRibbonForm() {
            InitializeComponent();
            this.AllowAero = false;
        }

        private void MainRibbonForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            RadGridLocalizationProvider.CurrentProvider = new LocalizationProviderCustomGrid();
            RadMessageLocalizationProvider.CurrentProvider = new LocalizationProviderCustomerMessageBox();

            using (var _espera = new Waiting1Form(this.Setup)) {
                _espera.Text = "Esperando respuesta del servidor ...";
                _espera.ShowDialog(this);
            }

            this.Text = ConfigService.Titulo();
            this.BEstado.Items.Add(new ButtonEmpresaBuilder().Usuario(ConfigService.Piloto.Clave).Version(Application.ProductVersion.ToString()).Empresa(ConfigService.Synapsis.Empresa.RFC).Build());
            this.MenuRibbonBar.SetDefaultTab();
            this.MenuRibbonBar.Visible = true;
        }

        #region botones
        private void AcercaDe_Click(object sender, EventArgs e) {
            var acercade_form = new AboutBoxForm();
            acercade_form.ShowDialog(this);
        }
        #endregion

        #region metodos privados
        private void Menu_Main_Click(object sender, EventArgs e) {
            var _item = (RadItem)sender;
            var _menuElement = ConfigService.GeMenuElement(_item.Name);

            if (_menuElement != null) {
                var localAssembly = new object() as Assembly;
                if (_menuElement.Assembly == "Jaeger.UI")
                    localAssembly = typeof(MainRibbonForm).Assembly;
                else
                    localAssembly = this.GetAssembly(_menuElement.Assembly);

                if (localAssembly != null) {
                    try {
                        if (_menuElement.Assembly == "Jaeger.UI") {
                            Type type = localAssembly.GetType(string.Format("{0}.CPLite.{1}", _menuElement.Assembly, _menuElement.Form), true);
                            this.Activar_Form(type, _menuElement);
                        } else {
                            Type type = localAssembly.GetType(string.Format("{0}.{1}", _menuElement.Assembly, _menuElement.Form), true);
                            this.Activar_Form(type, _menuElement);
                        }
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, "Main: " + ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                } else {
                    RadMessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private Assembly GetAssembly(string assembly) {
            try {
                return Assembly.Load(assembly);
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private void Activar_Form(Type type, UIMenuElement sender) {
            try {
                Form _form = (Form)Activator.CreateInstance(type, sender);
                if (_form.WindowState == FormWindowState.Maximized) {
                    _form.MdiParent = this;
                    _form.WindowState = FormWindowState.Maximized;
                    _form.Show();
                    this.RadDock.ActivateMdiChild(_form);
                } else {
                    _form.StartPosition = FormStartPosition.CenterParent;
                    _form.ShowDialog(this);
                }
            } catch (Exception ex) {
                RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void Menu_UI_AplicarTema_Click(object sender, EventArgs e) {
            this.Change_Theme(((RadButtonElement)sender).Tag.ToString());
        }

        /// <summary>
        /// cambiar tema
        /// </summary>
        private void Change_Theme(string nameTheme) {
            ThemeResolutionService.ApplicationThemeName = nameTheme;
        }

        private void Setup() {
            // configura el servicio de configuracion que no utilizaremos en la aplicacion
            ConfigService.Synapsis.RDS.CP = null;
            ConfigService.Synapsis.RDS.LiteCP = null;
            var clave = ConfigService.Synapsis.Empresa.Clave;
            var configuracion = new Aplication.CPLite.Services.ConfiguracionService();
            Aplication.CPLite.Services.GeneralService.Configuration = configuracion.Get();
            ConfigService.Synapsis.Empresa = configuracion.GetEmpresa();
            ConfigService.Synapsis.Empresa.Clave = clave;

            // configura el servicio de perfil
            Aplication.Kaiju.Contracts.IProfileToService profile = new Aplication.CPLite.Services.ProfileService();
            ConfigService.Piloto = profile.CreateProfile(ConfigService.Piloto);
            ConfigService.Menus = profile.CreateMenus(ConfigService.Piloto.Id).ToList();

            var MenuPermissions = new UIMenuItemPermission(UIPermissionEnum.Invisible);
            MenuPermissions.Load(ConfigService.Menus);
            UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);

            this.MenuRibbonBar.Refresh();
        }
        #endregion
    }
}
