﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbPrecioRepository : Abstractions.RepositoryMaster<PrecioModel>, ISqlPrecioRepository {
        protected ISqlPrecioPartidaRepository partidaRepository;

        public SqlFbPrecioRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.partidaRepository = new SqlFbPrecioPartidaRepository(configuracion, user);
            this.User = user;
        }

        #region CRUD
        public int Insert(PrecioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTPRC (CTPRC_ID, CTPRC_A, CTPRC_SEC_ID, CTPRC_STTSDCS_ID, CTPRC_NOM, CTPRC_FECINI, CTPRC_FECFIN, CTPRC_OBSRV, CTPRC_FN, CTPRC_USU_N) VALUES (@CTPRC_ID, @CTPRC_A, @CTPRC_SEC_ID, @CTPRC_STTSDCS_ID, @CTPRC_NOM, @CTPRC_FECINI, @CTPRC_FECFIN, @CTPRC_OBSRV, @CTPRC_FN, @CTPRC_USU_N) RETURNING CTPRC_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@CTPRC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTPRC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRC_SEC_ID", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@CTPRC_STTSDCS_ID", item.IdPrioridad);
            sqlCommand.Parameters.AddWithValue("@CTPRC_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTPRC_FECINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@CTPRC_FECFIN", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@CTPRC_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CTPRC_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTPRC_USU_N", item.Creo);
            item.IdPrecio = this.ExecuteScalar(sqlCommand);
            if (item.IdPrecio > 0)
                return item.IdPrecio;
            return 0;
        }

        public int Update(PrecioModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTPRC SET CTPRC_A = @CTPRC_A, CTPRC_SEC_ID = @CTPRC_SEC_ID, CTPRC_STTSDCS_ID = @CTPRC_STTSDCS_ID, CTPRC_NOM = @CTPRC_NOM, CTPRC_FECINI = @CTPRC_FECINI, CTPRC_FECFIN = @CTPRC_FECFIN, CTPRC_OBSRV = @CTPRC_OBSRV, CTPRC_FM = @CTPRC_FM, CTPRC_USU_M = @CTPRC_USU_M WHERE ((CTPRC_ID = @CTPRC_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@CTPRC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTPRC_SEC_ID", item.Secuencia);
            sqlCommand.Parameters.AddWithValue("@CTPRC_STTSDCS_ID", item.IdPrioridad);
            sqlCommand.Parameters.AddWithValue("@CTPRC_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTPRC_FECINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@CTPRC_FECFIN", item.FechaFin);
            sqlCommand.Parameters.AddWithValue("@CTPRC_OBSRV", item.Nota);
            sqlCommand.Parameters.AddWithValue("@CTPRC_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTPRC_USU_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTPRC WHERE ((CTPRC_ID = @CTPRC_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTPRC_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PrecioModel GetById(int index) {
            return this.GetList(new List<IConditional>() { new Conditional("CTPRC_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<PrecioModel> GetList() {
            return this.GetList(new List<IConditional>());
        }
        #endregion

        public PrecioDetailModel Save(PrecioDetailModel model) {
            if (model.IdPrecio == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdPrecio = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
            }
            return model;
        }

        public IEnumerable<PrecioDetailModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CTPRC @wcondiciones"
            };
            return this.GetMapper<PrecioDetailModel>(sqlCommand, conditionals);
        }

        /// <summary>
        /// obtener listado de precios
        /// </summary>
        public IEnumerable<LPrecioModel> GetList(int idCliente, int idCatalogo = 0) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM LPRECIO(@idCatalogo, @idCliente)"
            };
            sqlCommand.Parameters.AddWithValue("@idCatalogo", idCatalogo);
            sqlCommand.Parameters.AddWithValue("@idCliente", idCliente);
            return this.GetMapper<LPrecioModel>(sqlCommand);
        }
    }
}
