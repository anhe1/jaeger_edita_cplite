SET TERM ^ ;
CREATE TRIGGER CTLRMS_IVA_TRASLADO FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 12112021 22:00
    Purpose: actualizar el importe del IVA traslado, solo cuando el subtotal es modificado
------------------------------------------------------------------------------------------------------------- */
    if ( new.ctlrms_doc_id = 26 and old.ctlrms_sbttl$ <> new.ctlrms_sbttl$) then
        begin
            new.ctlrms_impst$ = new.ctlrms_sbttl$ * new.ctlrms_impst ;
        end
END^
SET TERM ; ^
