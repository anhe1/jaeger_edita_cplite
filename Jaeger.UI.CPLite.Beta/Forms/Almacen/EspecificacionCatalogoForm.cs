﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class EspecificacionCatalogoForm : UI.Almacen.Forms.EspecificacionCatalogoForm {
        public EspecificacionCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += EspecificacionCatalogoForm_Load;
        }

        private void EspecificacionCatalogoForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.CPLite.Almacen.Services.EspecificacionService();
        }
    }
}
