﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.CPLite.Empresa.Entities;
using Jaeger.Domain.CPLite.Empresa.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Empresa {
    public partial class AutorizarForm : RadForm {
        private AutorizacionModel autorizacion = new AutorizacionModel();
        protected IStatusService statusService;
        protected IAutorizacionService autorizacionService;

        public void OnAuthorized(AutorizacionModel e) {
            if (this.Authorized != null) { 
                this.Authorized(this, e);
            }
        }

        public event EventHandler<AutorizacionModel> Authorized;

        //public AutorizarForm(int idDocumento, int indice) {
        //    InitializeComponent();
        //    this.autorizacion.IdDocumento = idDocumento;
        //    if (idDocumento == 26)
        //        this.autorizacion.IdRemision = indice;
        //    if(idDocumento == 33)
        //        this.autorizacion.AUTDCS_CTLALMPT0_ID = indice;
        //}

        public AutorizarForm(AutorizacionModel autorizacion) {
            InitializeComponent();
            this.autorizacion = autorizacion;
        }

        private void AutorizarForm_Load(object sender, EventArgs e) {
            this.autorizacionService = new AutorizacionService();
            this.statusService = new StatusService();

            IdStatusAutorizado.DataSource = this.statusService.GetStatus((DocumentoTipoEnum)this.autorizacion.IdDocumento);
            IdStatusAutorizado.DisplayMember = "Descripcion";
            IdStatusAutorizado.ValueMember = "Id";
            
            IdAfectacion.DataSource = this.statusService.GetAfectaciones();
            IdAfectacion.DisplayMember = "Descripcion";
            IdAfectacion.ValueMember = "Id";

            this.IdAfectacion.DataBindings.Add("SelectedValue", this.autorizacion,"IdAfectacion", true, DataSourceUpdateMode.OnPropertyChanged);
            this.IdStatusAutorizado.DataBindings.Add("SelectedValue", this.autorizacion, "IdStatusAutorizado", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Importe.DataBindings.Add("Value", this.autorizacion, "Total", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Nota.DataBindings.Add("Text", this.autorizacion, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void ToolBar_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBar_Guardar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Autorizando ...";
                espera.ShowDialog(this);
            }

            if (this.autorizacion.IdAutoDoc > 0) {
                this.OnAuthorized(this.autorizacion);
                this.ToolBar.Cerrar.PerformClick();
            } else {
                this.OnActivated(null);
            }
        }

        private void Guardar() {
            this.autorizacion.Creo = ConfigService.Piloto.Clave;
            this.autorizacion = this.autorizacionService.Save(this.autorizacion);
        }
    }
}
