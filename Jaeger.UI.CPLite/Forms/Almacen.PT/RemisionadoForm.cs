﻿using System;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class RemisionadoForm : UI.Almacen.PT.Forms.RemisionadoForm {
        public RemisionadoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = false;
            this.Load += RemisionadoForm_Load;
        }

        private void RemisionadoForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén PT: Remisionado";
            this.TRemision.Nuevo.Enabled = true;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._Service = new Aplication.CPLite.Services.RemisionadoService();
        }

        #region barra de herramientas
        public override void TRemision_Nuevo_Click(object sender, EventArgs e) {
            var _nuevo = new RemisionFiscalForm(this._Service, this.ShowValues) { MdiParent = ParentForm };
            _nuevo.Show();
        }

        public override void TRemision_Editar_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var _nuevo = new UI.Almacen.PT.Forms.RemisionFiscalForm(this._Service, seleccionado, this.ShowValues) { MdiParent = ParentForm };
                _nuevo.TRemision.Receptor.Service = new Aplication.CPLite.Services.DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
                _nuevo.Show();
            }
        }

        public override void TImprimir_Comprobante_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (!string.IsNullOrEmpty(seleccionado.IdDocumento)) {

                using (var espera = new Waiting1Form(this.GetPrinter)) {
                    espera.Text = "Espera un momento ...";
                    espera.ShowDialog(this);
                }

                if (seleccionado.Tag != null) {
                    var printer = (RemisionDetailPrinter)(seleccionado.Tag);
                    var reporte = new ReporteForm(printer);
                    reporte.Show();
                }
            }
        }

        public override void ContextMenuClonar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Clonar)) {
                espera.ShowDialog(this);
            }
            var seleccionado = this.TRemision.GetCurrent<RemisionSingleModel>();
            if (seleccionado != null) {
                var _nuevo = new RemisionFiscalForm(this._Service, (RemisionDetailModel)seleccionado.Tag, true) { MdiParent = ParentForm };
                _nuevo.Show();
            }
        }
        #endregion
    }
}
