﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Almacen.Services {
    /// <summary>
    /// servicio de categorias de productos para tienda web
    /// </summary>
    public class CategoriasService : Aplication.Almacen.Services.CategoriasService, ICategoriasService {
        public CategoriasService() : base() {
            this.categoriasRepository = new DataAccess.FB.CPLite.Repositories.SqlFbCategoriaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
