﻿using System.Windows.Forms;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class ReporteVentasForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CategoricalAxis categoricalAxis1 = new Telerik.WinControls.UI.CategoricalAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.BarSeries barSeries1 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries2 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries3 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries4 = new Telerik.WinControls.UI.BarSeries();
            Telerik.WinControls.UI.BarSeries barSeries5 = new Telerik.WinControls.UI.BarSeries();
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReporteVentasForm));
            this.TResumen = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.VendedoresChart = new Telerik.WinControls.UI.RadChartView();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.VendedoresPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.RemisionadoPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.TRemisionado = new Jaeger.UI.Common.Forms.GridStandarControl();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.VendedoresChart)).BeginInit();
            this.VendedoresChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.VendedoresPage.SuspendLayout();
            this.RemisionadoPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TResumen
            // 
            this.TResumen.Dock = System.Windows.Forms.DockStyle.Top;
            this.TResumen.Location = new System.Drawing.Point(0, 0);
            this.TResumen.Name = "TResumen";
            this.TResumen.ShowActualizar = true;
            this.TResumen.ShowAutosuma = false;
            this.TResumen.ShowCancelar = false;
            this.TResumen.ShowCerrar = true;
            this.TResumen.ShowEditar = false;
            this.TResumen.ShowEjercicio = true;
            this.TResumen.ShowExportarExcel = true;
            this.TResumen.ShowFiltro = false;
            this.TResumen.ShowHerramientas = true;
            this.TResumen.ShowImprimir = false;
            this.TResumen.ShowItem = false;
            this.TResumen.ShowNuevo = false;
            this.TResumen.ShowPeriodo = true;
            this.TResumen.Size = new System.Drawing.Size(1254, 30);
            this.TResumen.TabIndex = 0;
            // 
            // VendedoresChart
            // 
            this.VendedoresChart.AreaDesign = cartesianArea1;
            categoricalAxis1.IsPrimary = true;
            linearAxis1.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis1.IsPrimary = true;
            linearAxis1.LabelFormat = "{0:N0}";
            linearAxis1.TickOrigin = null;
            this.VendedoresChart.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            categoricalAxis1,
            linearAxis1});
            this.VendedoresChart.Controls.Add(this.radLabel1);
            this.VendedoresChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VendedoresChart.Location = new System.Drawing.Point(0, 0);
            this.VendedoresChart.Name = "VendedoresChart";
            barSeries1.HorizontalAxis = categoricalAxis1;
            barSeries1.LegendTitle = "Remisionado";
            barSeries1.VerticalAxis = linearAxis1;
            barSeries2.HorizontalAxis = categoricalAxis1;
            barSeries2.LegendTitle = "Descuento";
            barSeries2.VerticalAxis = linearAxis1;
            barSeries3.HorizontalAxis = categoricalAxis1;
            barSeries3.LegendTitle = "Por Cobrar";
            barSeries3.VerticalAxis = linearAxis1;
            barSeries4.HorizontalAxis = categoricalAxis1;
            barSeries4.LegendTitle = "Cobrado";
            barSeries4.VerticalAxis = linearAxis1;
            barSeries5.HorizontalAxis = categoricalAxis1;
            barSeries5.LegendTitle = "Saldo";
            barSeries5.VerticalAxis = linearAxis1;
            this.VendedoresChart.Series.AddRange(new Telerik.WinControls.UI.ChartSeries[] {
            barSeries1,
            barSeries2,
            barSeries3,
            barSeries4,
            barSeries5});
            this.VendedoresChart.ShowGrid = false;
            this.VendedoresChart.ShowLegend = true;
            this.VendedoresChart.ShowPanZoom = true;
            this.VendedoresChart.ShowToolTip = true;
            this.VendedoresChart.ShowTrackBall = true;
            this.VendedoresChart.Size = new System.Drawing.Size(1233, 494);
            this.VendedoresChart.TabIndex = 0;
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.VendedoresPage);
            this.radPageView1.Controls.Add(this.RemisionadoPage);
            this.radPageView1.DefaultPage = this.VendedoresPage;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 30);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.VendedoresPage;
            this.radPageView1.Size = new System.Drawing.Size(1254, 542);
            this.radPageView1.TabIndex = 0;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // VendedoresPage
            // 
            this.VendedoresPage.Controls.Add(this.VendedoresChart);
            this.VendedoresPage.ItemSize = new System.Drawing.SizeF(76F, 28F);
            this.VendedoresPage.Location = new System.Drawing.Point(10, 37);
            this.VendedoresPage.Name = "VendedoresPage";
            this.VendedoresPage.Size = new System.Drawing.Size(1233, 494);
            this.VendedoresPage.Text = "Vendedores";
            // 
            // RemisionadoPage
            // 
            this.RemisionadoPage.Controls.Add(this.TRemisionado);
            this.RemisionadoPage.ItemSize = new System.Drawing.SizeF(81F, 28F);
            this.RemisionadoPage.Location = new System.Drawing.Point(10, 37);
            this.RemisionadoPage.Name = "RemisionadoPage";
            this.RemisionadoPage.Size = new System.Drawing.Size(1233, 494);
            this.RemisionadoPage.Text = "Remisionado";
            // 
            // TRemisionado
            // 
            this.TRemisionado.Caption = "";
            this.TRemisionado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TRemisionado.Location = new System.Drawing.Point(0, 0);
            this.TRemisionado.Name = "TRemisionado";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TRemisionado.Permisos = uiAction1;
            this.TRemisionado.ShowActualizar = false;
            this.TRemisionado.ShowAutorizar = false;
            this.TRemisionado.ShowAutosuma = true;
            this.TRemisionado.ShowCerrar = false;
            this.TRemisionado.ShowEditar = false;
            this.TRemisionado.ShowExportarExcel = false;
            this.TRemisionado.ShowFiltro = true;
            this.TRemisionado.ShowHerramientas = false;
            this.TRemisionado.ShowImagen = false;
            this.TRemisionado.ShowImprimir = false;
            this.TRemisionado.ShowNuevo = false;
            this.TRemisionado.ShowRemover = false;
            this.TRemisionado.ShowSeleccionMultiple = false;
            this.TRemisionado.Size = new System.Drawing.Size(1233, 494);
            this.TRemisionado.TabIndex = 1;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(46, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(113, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Ventas por Vendedor";
            // 
            // ReporteVentasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 572);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.TResumen);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReporteVentasForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Ventas: Por vendedor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReporteVentasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VendedoresChart)).EndInit();
            this.VendedoresChart.ResumeLayout(false);
            this.VendedoresChart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.VendedoresPage.ResumeLayout(false);
            this.RemisionadoPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarCommonControl TResumen;
        private Telerik.WinControls.UI.RadChartView VendedoresChart;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage VendedoresPage;
        private Telerik.WinControls.UI.RadPageViewPage RemisionadoPage;
        private Common.Forms.GridStandarControl TRemisionado;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}