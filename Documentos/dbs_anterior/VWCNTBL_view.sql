CREATE VIEW VWCNTBL (VWCNTBL_ID, VWCNTBL_A, VWCNTBL_DOC_ID, VWCNTBL_POLIZA, VWCNTBL_CNTBL_ID, VWCNTBL_REM_ID, VWCNTBL_RFRNC2, VWCNTBL_RFRNC1, VWCNTBL_ABONO$, VWCNTBL_OBSRV, VWCNTBL_USU_N, VWCNTBL_FN)
AS  
select 
cntbl_id	vwcntbl_id,
cntbl_a	vwcntbl_a,
cntbl_doc_id	vwcntbl_doc_id,
cntbl_poliza	vwcntbl_poliza,
cntbl_cntbl_id	vwcntbl_cntbl_id,
cntbl_rem_id	vwcntbl_rem_id,
cntbl_rfrnc2	vwcntbl_rfrnc2,
cntbl_rfrnc1	vwcntbl_rfrnc1,
cntbl_abono$	vwcntbl_abono$,
cntbl_obsrv	vwcntbl_obsrv,
cntbl_usu_n	vwcntbl_usu_n,
cntbl_fn 	vwcntbl_fn 
from cntbl  
where cntbl_doc_id = 2800
order by cntbl_id asc;

UPDATE RDB$RELATIONS set
  RDB$DESCRIPTION = 'Bancos: catalogo de formas de pago'
  where RDB$RELATION_NAME = 'VWCNTBL';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWCNTBL TO  SYSDBA WITH GRANT OPTION;

