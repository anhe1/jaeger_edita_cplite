SET TERM ^ ;
CREATE PROCEDURE COMISION_CALCULAR (
    COBRADO Numeric(18,4),
    PORCOBRAR Numeric(18,4),
    DIASTRANSCURRIDOS Integer,
    IDVENDEDOR Integer )
RETURNS (
    DESCRIPCION Varchar(120),
    COMISIONDE Varchar(20),
    MULTADEL Varchar(20),
    COMISIONPAGAR Numeric(18,4) )
AS
DECLARE VARIABLE descuento numeric(18, 4);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: anhe 21112021 0157
    Purpose: procedimiento para el calculo de pago de comision
------------------------------------------------------------------------------------------------------------- */
    descuento = 1 - (:cobrado / :porCobrar);
    if (descuento < 0) then
        begin
            descuento = 0;
        end 
    select comision.cmsdsc_nom, r.tbrngs_nom, multa.tbrngs_nom, ((r.tbrngs_factor * :cobrado) * multa.tbrngs_factor) 
    from cmsdsc comision
    left join rlcdrc vnddr on comision.CMSDSC_ID = vnddr.rlcdrc_cmsn_id and vnddr.RLCDRC_A > 0 and vnddr.rlcdrc_doc_id = 1016 and vnddr.rlcdrc_vnddr_id = :idVendedor
    left join tbrngs r on comision.cmsdsc_id = r.tbrngs_cmsdsc_id and r.tbrngs_a > 0 and r.tbrngs_doc_id = 1037
    left join tbrngs multa on comision.cmsdsc_id = multa.tbrngs_cmsdsc_id and multa.tbrngs_a > 0 and multa.tbrngs_doc_id = 1038
    where comision.cmsdsc_doc_id = 1016 and comision.cmsdsc_a = 1 and comision.cmsdsc_sec_id is not null and comision.cmsdsc_id = vnddr.rlcdrc_cmsn_id
    and (:descuento >= r.tbrngs_rng1 and :descuento <= r.tbrngs_rng2 )
    and (:diasTranscurridos >= multa.tbrngs_rng1 and :diasTranscurridos <= multa.tbrngs_rng2)
    into :descripcion, :comisionde, :multadel, :comisionPagar;
    suspend;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE COMISION_CALCULAR TO  SYSDBA;

