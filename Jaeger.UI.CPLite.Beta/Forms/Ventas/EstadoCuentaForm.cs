﻿using System;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class EstadoCuentaForm : RadForm {
        public EstadoCuentaForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void EstadoCuentaForm_Load(object sender, EventArgs e) {
            this.TReporte.Cerrar.Click += TRemision_Cerrar_Click;
        }

        private void ExportarExcel_Click(object sender, EventArgs e) {
            var exportar = new TelerikGridExportForm(this.TReporte.GridData);
            exportar.ShowDialog(this);
        }

        private void TRemision_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
