﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class DevolucionCatalogoForm : UI.Almacen.PT.Forms.ValeAlmacenCatalogoForm {
        public DevolucionCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += DevolucionCatalogoForm_Load;
        }

        private void DevolucionCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new ValeAlmacenService();
            this.TVale.Almacen.DataSource = this.Service.GetAlmacenes();
        }

        public override void TVale_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new DevolucionForm(this.Service) { MdiParent = this.ParentForm };
            nuevo.Show();
        }

        public override void Consultar() {
            if (this.TVale.Almacen.SelectedItem == null)
                return;
            var current = ((GridViewDataRowInfo)this.TVale.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            var d0 = ValeAlmacenService.Query().ByIdAlmacen(current.IdAlmacen).ByTipoAlmacen(Domain.Base.ValueObjects.AlmacenEnum.PT).ByType(Domain.Almacen.PT.ValueObjects.TipoComprobanteEnum.Devolucion).WithYear(this.TVale.GetEjercicio()).WithMonth(this.TVale.GetPeriodo()).Build();
            this.vales = new BindingList<ValeAlmacenSingleModel>(this.Service.GetList<ValeAlmacenSingleModel>(d0).ToList());
        }
    }
}
