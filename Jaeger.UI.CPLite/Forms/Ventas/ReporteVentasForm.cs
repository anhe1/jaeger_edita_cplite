﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.Charting;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Almacen.PT.Services;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class ReporteVentasForm : RadForm {
        private List<RemisionSingleModel> remisiones;
        protected IRemisionadoService service2;

        public ReporteVentasForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ReporteVentasForm_Load(object sender, EventArgs e) {
            this.service2 = new Aplication.CPLite.Services.RemisionadoService();
            this.gridRemisiones.GridCommon();
            //this.gridRemisiones.Columns.AddRange(UI.Almacen.PT.Services.GridRemisionService.GetColumns(true));

            this.TResumen.Actualizar.Click += this.TResumen_Actualizar_Click;
            this.TResumen.ExportarExcel.Click += this.TResumen_ExportarExcel_Click;
            this.TResumen.Cerrar.Click += this.TResumen_Cerrar_Click;
        }

        #region barra de herramientas
        private void TResumen_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            //var IdStatus = this.gridRemisiones.Columns["IdStatus"] as GridViewComboBoxColumn;
            //IdStatus.DataSource = RemisionadoService.GetStatus();
            //IdStatus.DisplayMember = "Descripcion";
            //IdStatus.ValueMember = "Id";

            this.gridRemisiones.DataSource = this.remisiones;
            // limpiar chart
            this.chart1.Series[0].DataPoints.Clear();
            this.chart1.Series[1].DataPoints.Clear();
            this.chart1.Series[2].DataPoints.Clear();
            this.chart1.Series[3].DataPoints.Clear();

            var _data = this.remisiones.OrderBy(it => it.Vendedor).Select(it => it.Vendedor).Distinct().ToList();

            foreach (var item in _data) {
                var _sublista = this.remisiones.Where(it => it.Vendedor == item).Where(it => it.IdStatus != 0).ToList();
                this.chart1.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = item, Value = double.Parse(_sublista.Sum(it => it.Total).ToString()) });
                this.chart1.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = item, Value = double.Parse(_sublista.Sum(it => it.PorCobrar).ToString()) });
                this.chart1.Series[2].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = item, Value = double.Parse(_sublista.Sum(it => it.Acumulado).ToString()) });
                this.chart1.Series[3].DataPoints.Add(new CategoricalDataPoint { Category = item, Label = item, Value = double.Parse(_sublista.Sum(it => it.Saldo).ToString()) });
            }
        }

        private void TResumen_ExportarExcel_Click(object sender, EventArgs e) {
            var _exportar = new TelerikGridExportForm(this.gridRemisiones);
            _exportar.ShowDialog(this);
        }

        private void TResumen_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        private void Consultar() {
            var d0 = RemisionadoService.Create().WithYear(this.TResumen.GetEjercicio()).WithMonth(this.TResumen.GetMes()).Build();
            this.remisiones = this.service2.GetList<RemisionSingleModel>(d0).ToList();
        }
    }
}
