﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Banco {
    public class ConceptoCatalogoForm : UI.Banco.Forms.ConceptoCatalogoForm {
        public ConceptoCatalogoForm(UIMenuElement menuElement) : base(menuElement) { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._Service = new Aplication.CPLite.Services.BancoConceptoService();
        }
    }
}
