-- --------------------------------------------------------
-- Host:                         grupoedita.cix9t3wpbegp.us-west-2.rds.amazonaws.com
-- Versión del servidor:         10.4.30-MariaDB-log - Source distribution
-- SO del servidor:              Linux
-- HeidiSQL Versión:             12.6.0.6783
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla jaeger_ipr981125pn9b.ALMPT
CREATE TABLE IF NOT EXISTS `ALMPT` (
  `ALMPT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `ALMPT_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `ALMPT_VER` varchar(3) DEFAULT NULL COMMENT 'version del documento',
  `ALMPT_CTALM_ID` int(11) DEFAULT NULL COMMENT 'indice del almacen',
  `ALMPT_TPALM_ID` int(11) DEFAULT NULL COMMENT 'indice del tipo de almacen',
  `ALMPT_CTEFC_ID` smallint(6) DEFAULT NULL COMMENT 'efecto del comprobante (1 = ingreso, 2 = egreso) ',
  `ALMPT_DOC_ID` int(11) DEFAULT NULL COMMENT 'indice del tipo de documento',
  `ALMPT_STTS_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de status',
  `ALMPT_CTREL_ID` smallint(6) DEFAULT NULL COMMENT 'indice del catalogo de relacion con otros comprobantes',
  `ALMPT_CTPRD_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de productos',
  `ALMPT_PDCLN_ID` int(11) DEFAULT NULL COMMENT 'indice del pedido de control interno del cliente',
  `ALMPT_NTDSC_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de  las notas de descuento, este campo lo utilice para marcar las devoluciones que ya fueron asignadas a una nota de descuento',
  `ALMPT_CTDP_ID` int(11) DEFAULT NULL COMMENT 'indice del depratamento emisor',
  `ALMPT_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice del directorio',
  `ALMPT_VNDDR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el catalogo de vendedores',
  `ALMPT_CVDEV` smallint(6) DEFAULT NULL COMMENT 'clave del tipo de devolucion',
  `ALMPT_FOLIO` int(11) DEFAULT NULL COMMENT 'folio de control interno',
  `ALMPT_SERIE` varchar(10) DEFAULT NULL COMMENT 'serie de control interno',
  `ALMPT_FECEMS` date DEFAULT NULL COMMENT 'fecha de emision del comprobante',
  `ALMPT_CLV` varchar(10) DEFAULT NULL COMMENT 'clave de control interno',
  `ALMPT_NOM` varchar(255) DEFAULT NULL COMMENT 'nombre o descripcion de receptor',
  `ALMPT_CNTCT` varchar(100) DEFAULT NULL COMMENT 'nombre del contacto',
  `ALMPT_RFRNC` varchar(100) DEFAULT NULL COMMENT 'referencia',
  `ALMPT_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'subtotal',
  `ALMPT_DSCT` decimal(18,4) DEFAULT NULL COMMENT 'importe total del descuento aplicado ',
  `ALMPT_TRIVA` decimal(18,4) DEFAULT NULL COMMENT 'total del impuesto traslado iva',
  `ALMPT_GTOTAL` decimal(18,4) DEFAULT NULL COMMENT 'gran total subtotal + trasladoiva',
  `ALMPT_FACIVA` decimal(18,4) DEFAULT NULL COMMENT 'factor del iva pactado',
  `ALMPT_OBSRV` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `ALMPT_VNDDR` varchar(10) DEFAULT NULL COMMENT 'clave del vendedor asociado',
  `ALMPT_FCING` date DEFAULT NULL COMMENT 'fecha de ingreso',
  `ALMPT_USU_C` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que cancela el comprobante',
  `ALMPT_FCCNCL` date DEFAULT NULL COMMENT 'fecha de cancelacion del comprobante',
  `ALMPT_CVCAN` smallint(6) DEFAULT NULL,
  `ALMPT_CLNTA` varchar(100) DEFAULT NULL,
  `ALMPT_CLMTV` varchar(100) DEFAULT NULL,
  `ALMPT_USU_N` varchar(10) DEFAULT NULL COMMENT 'usuario creador del registro',
  `ALMPT_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del comprobante',
  `ALMPT_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `ALMPT_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `ALMPT_URL_PDF` varchar(1000) DEFAULT NULL,
  `ALMPT_UUID` varchar(36) DEFAULT NULL COMMENT 'id de documento',
  `ALMPT_ANIO` int(11) DEFAULT year(`ALMPT_FECEMS`) COMMENT 'ejercicio',
  `ALMPT_MES` int(11) DEFAULT month(`ALMPT_FECEMS`) COMMENT 'periodo',
  PRIMARY KEY (`ALMPT_ID`),
  KEY `ALMPT_DRCTR_ID` (`ALMPT_DRCTR_ID`),
  KEY `ALMPT_CTDP_ID` (`ALMPT_CTDP_ID`),
  KEY `ALMPT_CTREL_ID` (`ALMPT_CTREL_ID`),
  KEY `ALMPT_NTDSC_ID` (`ALMPT_NTDSC_ID`),
  KEY `ALMPT_PDCLN_ID` (`ALMPT_PDCLN_ID`),
  KEY `ALMPT_CTPRD_ID` (`ALMPT_CTPRD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='almacen de producto terminado, vales y devoluciones';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.ALMPTR
CREATE TABLE IF NOT EXISTS `ALMPTR` (
  `ALMPTR_ALMPT_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de documentos de almacen (almmp)',
  `ALMPTR_DOC_ID` int(11) NOT NULL COMMENT 'tipo de documento',
  `ALMPTR_CTREL_ID` int(11) NOT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
  `ALMPTR_CTREL` varchar(60) DEFAULT NULL COMMENT 'descripcion de la clave de relacion con otros comprobantes',
  `ALMPTR_UUID` varchar(36) NOT NULL DEFAULT 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' COMMENT 'id del documento',
  `ALMPTR_DRCTR_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice del directorio',
  `ALMPTR_FOLIO` int(11) NOT NULL COMMENT 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',
  `ALMPTR_SERIE` varchar(50) DEFAULT NULL COMMENT 'serie para control interno del contribuyente. este atributo acepta una cadena de caracteres',
  `ALMPTR_FECEMS` timestamp NULL DEFAULT NULL COMMENT 'fecha de emision del comprobante',
  `ALMPTR_CLV` varchar(10) DEFAULT NULL COMMENT 'clave de control del directorio',
  `ALMPTR_RFCR` varchar(14) DEFAULT NULL,
  `ALMPTR_NOM` varchar(255) DEFAULT NULL COMMENT 'nombre o razon social del receptor',
  `ALMPTR_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `ALMPTR_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha y hora de expedicion del comprobante.',
  PRIMARY KEY (`ALMPTR_ALMPT_ID`,`ALMPTR_DOC_ID`,`ALMPTR_CTREL_ID`,`ALMPTR_UUID`,`ALMPTR_DRCTR_ID`,`ALMPTR_FOLIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='almacen de producto terminado: relacion entre documentos (vales y devoluciones)';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.ALMPTS
CREATE TABLE IF NOT EXISTS `ALMPTS` (
  `ALMPTS_ALMPT_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de remisiones',
  `ALMPTS_STTS_ID` int(11) NOT NULL COMMENT 'indice del status anterior',
  `ALMPTS_STTSB_ID` int(11) NOT NULL COMMENT 'indice del status autorizado',
  `ALMPTS_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla del directorio',
  `ALMPTS_CTMTV_ID` int(11) NOT NULL COMMENT 'indice de la clave de motivos',
  `ALMPTS_CTMTV` varchar(100) DEFAULT NULL COMMENT 'clave del motivo del cambio de status',
  `ALMPTS_NOTA` varchar(100) DEFAULT NULL COMMENT 'obervaciones',
  `ALMPTS_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que autoriza el cambio',
  `ALMPTS_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  KEY `ALMPTS_DRCTR_ID` (`ALMPTS_DRCTR_ID`),
  KEY `ALMPTS_ALMPT_ID` (`ALMPTS_ALMPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='almacen de producto terminado, tabla de registro de cambios de status';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.ARDPT
CREATE TABLE IF NOT EXISTS `ARDPT` (
  `ARDPT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `ARDPT_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `ARDPT_DOC_ID` smallint(6) DEFAULT NULL COMMENT 'para definir si es un area o departamento (1= area, 2= departamento)',
  `ARDPT_ARDPT_ID` int(11) DEFAULT NULL COMMENT 'para indicar la relacion entre departamento y area)',
  `ARDPT_SEC_ID` int(11) DEFAULT NULL,
  `ARDPT_NOM` varchar(100) DEFAULT NULL COMMENT 'nombre o descripcion del departamento o area',
  `ARDPT_RES` varchar(100) DEFAULT NULL COMMENT 'nombre del resposable',
  `ARDPT_OBSRV` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `ARDPT_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `ARDPT_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `ARDPT_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `ARDPT_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro',
  PRIMARY KEY (`ARDPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='empresa: catalogo: areas y departamentos (eliminar)';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCAUX
CREATE TABLE IF NOT EXISTS `BNCAUX` (
  `BNCAUX_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del movimiento',
  `BNCAUX_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCAUX_SBID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de relacion con la tabla de movimientos',
  `BNCAUX_DESC` varchar(256) NOT NULL COMMENT 'descripcion del auxiliar',
  `BNCAUX_NOM` varchar(256) NOT NULL COMMENT 'nombre del archivo',
  `BNCAUX_TP` varchar(32) DEFAULT NULL COMMENT 'descripcion del movimiento',
  `BNCAUX_URL` text DEFAULT NULL COMMENT 'url de descarga para el archivo',
  `BNCAUX_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `BNCAUX_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  PRIMARY KEY (`BNCAUX_ID`) USING BTREE,
  KEY `BNCAUX_SBID` (`BNCAUX_SBID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: documentos relacionados a movmientos bancarios';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCCMP
CREATE TABLE IF NOT EXISTS `BNCCMP` (
  `BNCCMP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
  `BNCCMP_A` smallint(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCCMP_SUBID` int(11) NOT NULL COMMENT 'indice de relacion con el movimiento bancario',
  `BNCCMP_NOIDEN` varchar(11) DEFAULT NULL COMMENT 'identificador de la prepoliza',
  `BNCCMP_VER` varchar(3) DEFAULT NULL COMMENT 'registro activo',
  `BNCCMP_EFECTO` varchar(10) NOT NULL COMMENT 'efecto del comprobante',
  `BNCCMP_SBTP` int(11) NOT NULL COMMENT 'subtipo del comprobante, emitido, recibido, nomina, etc',
  `BNCCMP_TIPO` varchar(10) NOT NULL COMMENT 'tipo de comprobante',
  `BNCCMP_IDCOM` int(11) NOT NULL COMMENT 'indice del comprobante fiscal',
  `BNCCMP_FOLIO` varchar(22) DEFAULT NULL COMMENT 'folio del comprobante',
  `BNCCMP_SERIE` varchar(25) DEFAULT NULL COMMENT 'serie del comprobante',
  `BNCCMP_RFCE` varchar(15) NOT NULL COMMENT 'rfc emisor del comprobante',
  `BNCCMP_EMISOR` varchar(255) NOT NULL COMMENT 'nombre del emisor del comprobante',
  `BNCCMP_RFCR` varchar(15) NOT NULL COMMENT 'rfc del receptor del comprobante',
  `BNCCMP_RECEPTOR` varchar(255) NOT NULL COMMENT 'nombre del receptor del comprobante',
  `BNCCMP_FECEMS` datetime NOT NULL COMMENT 'fecha de emision del comprobante',
  `BNCCMP_MTDPG` varchar(3) DEFAULT NULL COMMENT 'clave del metodo de pago',
  `BNCCMP_FRMPG` varchar(2) DEFAULT NULL COMMENT 'clave de la forma de pago',
  `BNCCMP_MONEDA` varchar(3) DEFAULT NULL COMMENT 'clave de moneda',
  `BNCCMP_UUID` varchar(36) DEFAULT NULL COMMENT 'folio fiscal (uuid)',
  `BNCCMP_PAR` int(11) DEFAULT NULL COMMENT 'numero de partida',
  `BNCCMP_TOTAL` decimal(11,4) NOT NULL COMMENT 'total del comprobante',
  `BNCCMP_XCBR` decimal(11,4) NOT NULL COMMENT 'importe por cobrar una vez aplicado el descuento',
  `BNCCMP_CARGO` decimal(11,4) DEFAULT NULL COMMENT 'cargo',
  `BNCCMP_ABONO` decimal(11,4) DEFAULT NULL COMMENT 'abono',
  `BNCCMP_CBRD` decimal(11,4) DEFAULT NULL COMMENT 'acumulado del comprobante',
  `BNCCMP_ESTADO` varchar(10) DEFAULT NULL COMMENT 'ultimo estado del comprobante',
  `BNCCMP_NOTA` varchar(255) DEFAULT NULL,
  `BNCCMP_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `BNCCMP_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCCMP_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCCMP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCCMP_ID`) USING BTREE,
  KEY `BNCCMP_SUBID` (`BNCCMP_SUBID`) USING BTREE,
  KEY `BNCCMP_IDCOM` (`BNCCMP_IDCOM`) USING BTREE,
  KEY `BNCCMP_NOIDEN` (`BNCCMP_NOIDEN`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: comprobantes relacionados a movmientos bancarios';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCCNP
CREATE TABLE IF NOT EXISTS `BNCCNP` (
  `BNCCNP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del concepto',
  `BNCCNP_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCCNP_R` smallint(6) NOT NULL DEFAULT 0 COMMENT 'registro de solo lectura',
  `BNCCNP_REQ` int(1) NOT NULL DEFAULT 0 COMMENT 'requiere comprobante',
  `BNCCNP_AFS` int(1) NOT NULL DEFAULT 0 COMMENT 'afectactacion de saldos de comprobantes',
  `BNCCNP_IDSBT` int(11) NOT NULL DEFAULT 0 COMMENT 'sub tipo de comprobante',
  `BNCCNP_STTS_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice del status del comporbante',
  `BNCCNP_IDOP` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de tipo de operacion',
  `BNCCNP_IDTP` int(11) NOT NULL DEFAULT 1 COMMENT 'indice de tipo o efecto',
  `BNCCNP_IDTPCMB` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de tipo de comprobante',
  `BNCCNP_FRMT_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'nombre del formato impreso',
  `BNCCNP_IVA` decimal(14,4) NOT NULL DEFAULT 0.0000 COMMENT 'porcentaje del iva aplicable a la operacion',
  `BNCCNP_TPCMP` varchar(40) DEFAULT NULL COMMENT 'tipos de comprobante aceptado por el movimiento',
  `BNCCNP_STATUS` varchar(40) DEFAULT NULL COMMENT 'status de los comprobantes',
  `BNCCNP_RLCCMP` varchar(40) DEFAULT NULL COMMENT 'lista de indice de relacion entre comprobantes',
  `BNCCNP_CLASS` varchar(3) DEFAULT NULL COMMENT 'clasificacion',
  `BNCCNP_DESC` varchar(40) NOT NULL COMMENT 'descipcion del concepto',
  `BNCCNP_NOM` varchar(40) NOT NULL COMMENT 'nombre, concepto o descripcion',
  `BNCCNP_CNTA` varchar(40) DEFAULT NULL COMMENT 'cuenta contable',
  `BNCCNP_RLCDIR` varchar(20) DEFAULT NULL COMMENT 'objetos de relacion del directorio',
  `BNCCNP_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `BNCCNP_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario creador del registro',
  `BNCCNP_FM` datetime DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `BNCCNP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCCNP_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: conceptos bancarios';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCCTA
CREATE TABLE IF NOT EXISTS `BNCCTA` (
  `BNCCTA_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id principal de la tabla',
  `BNCCTA_A` smallint(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCCTA_R` smallint(6) NOT NULL DEFAULT 0 COMMENT 'registro de solo lectura',
  `BNCCTA_E` smallint(1) NOT NULL DEFAULT 0 COMMENT 'indicar si el banco es extranjero',
  `BNCCTA_DCRT` smallint(2) NOT NULL DEFAULT 1 COMMENT 'dia del corte',
  `BNCCTA_NOCHQ` int(11) DEFAULT NULL COMMENT 'numero de cheque',
  `BNCCTA_SLDINI` decimal(11,4) NOT NULL DEFAULT 0.0000 COMMENT 'saldo inicial',
  `BNCCTA_CLVB` varchar(3) DEFAULT NULL COMMENT 'clave del banco segun catalogo del sat',
  `BNCCTA_CLV` varchar(3) DEFAULT NULL COMMENT 'clave de la moneda a utilizar segun catalgo sat',
  `BNCCTA_RFCB` varchar(14) DEFAULT NULL COMMENT 'rfc del beneficiario de la cuenta de banco',
  `BNCCTA_RFC` varchar(14) DEFAULT NULL COMMENT 'rfc del banco',
  `BNCCTA_NUMCLI` varchar(14) DEFAULT NULL COMMENT 'numero de identificación del cliente, numero de contrato',
  `BNCCTA_CLABE` varchar(18) DEFAULT NULL COMMENT 'cuenta clabe',
  `BNCCTA_NUMCTA` varchar(20) DEFAULT NULL COMMENT 'numero de cuenta',
  `BNCCTA_SCRSL` varchar(20) DEFAULT NULL COMMENT 'sucursal bancaria',
  `BNCCTA_FUNC` varchar(20) DEFAULT NULL COMMENT 'nombre del asesor o funcionario',
  `BNCCTA_PLAZA` varchar(20) DEFAULT NULL COMMENT 'numero de plaza',
  `BNCCTA_TEL` varchar(20) DEFAULT NULL COMMENT 'telefono de contacto',
  `BNCCTA_ALIAS` varchar(20) DEFAULT NULL COMMENT 'alias de la cuenta',
  `BNCCTA_CNTA` varchar(40) DEFAULT NULL COMMENT 'numero de cuenta contable',
  `BNCCTA_BANCO` varchar(64) DEFAULT NULL COMMENT 'nombre del banco',
  `BNCCTA_BENEF` varchar(256) DEFAULT NULL COMMENT 'nombre del beneficiario de la cuenta de banco',
  `BNCCTA_FECAPE` datetime DEFAULT NULL COMMENT 'fecha de apertura',
  `BNCCTA_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `BNCCTA_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCCTA_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCCTA_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCCTA_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: registro de cuentas bancarias';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCFRM
CREATE TABLE IF NOT EXISTS `BNCFRM` (
  `BNCFRM_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `BNCFRM_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCFRM_AFS` int(1) NOT NULL DEFAULT 1 COMMENT 'afectar saldo de la cuenta',
  `BNCFRM_CLV` varchar(3) NOT NULL COMMENT 'clave de forma de pago',
  `BNCFRM_CLVS` varchar(3) NOT NULL COMMENT 'clave sat',
  `BNCFRM_NOM` varchar(64) NOT NULL COMMENT 'descripcion de la forma de pago',
  `BNCFRM_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `BNCFRM_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCFRM_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCFRM_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`BNCFRM_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: formas de pago';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCMOV
CREATE TABLE IF NOT EXISTS `BNCMOV` (
  `BNCMOV_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice del movimiento',
  `BNCMOV_A` int(1) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `BNCMOV_VER` varchar(3) NOT NULL DEFAULT '3.0' COMMENT 'version del documento',
  `BNCMOV_STTS_ID` int(11) NOT NULL DEFAULT 1 COMMENT 'status del movimiento',
  `BNCMOV_TIPO_ID` int(11) NOT NULL DEFAULT 1 COMMENT 'tipo de movimiento',
  `BNCMOV_OPER_ID` int(11) NOT NULL DEFAULT 1 COMMENT 'tipo de operacion',
  `BNCMOV_CNCP_ID` int(11) NOT NULL COMMENT 'indice del concepto de movimiento',
  `BNCMOV_FRMT_ID` int(11) NOT NULL COMMENT 'indice del concepto de movimiento',
  `BNCMOV_ASCTA` int(1) NOT NULL COMMENT 'indice del concepto de movimiento',
  `BNCMOV_ASCOM` int(1) NOT NULL COMMENT 'indice del concepto de movimiento',
  `BNCMOV_DRCTR_ID` int(11) NOT NULL COMMENT 'indice del directorio',
  `BNCMOV_CTAE_ID` int(11) NOT NULL COMMENT 'indice de la cuenta de banco a utilizar',
  `BNCMOV_CTAR_ID` int(11) NOT NULL COMMENT 'indice de la cuenta del banco receptor',
  `BNCMOV_POR` int(1) NOT NULL COMMENT 'bandera solo para indicar si el gasto es por justificar',
  `BNCMOV_NOIDEN` varchar(11) DEFAULT NULL COMMENT 'identificador de la prepoliza',
  `BNCMOV_NUMCTAP` varchar(20) DEFAULT NULL COMMENT 'numero de cuenta',
  `BNCMOV_CLVBP` varchar(3) DEFAULT NULL COMMENT 'clave del banco segun catalogo del sat',
  `BNCMOV_FECEMS` datetime DEFAULT NULL COMMENT 'fecha de la prepoliza',
  `BNCMOV_FECDOC` datetime DEFAULT NULL COMMENT 'fecha del documento',
  `BNCMOV_FCVNC` datetime DEFAULT NULL COMMENT 'fecha de vencimiento',
  `BNCMOV_FCCBR` datetime DEFAULT NULL COMMENT 'fecha de cobro o pago',
  `BNCMOV_FCLIB` datetime DEFAULT NULL COMMENT 'fecha de liberación (transmición al banco)',
  `BNCMOV_FCCNCL` datetime DEFAULT NULL COMMENT 'fecha de cancelacion',
  `BNCMOV_BENF` varchar(256) DEFAULT NULL COMMENT 'nombre deel beneficiario',
  `BNCMOV_RFCR` varchar(15) DEFAULT NULL COMMENT 'registro federal de causante del receptor',
  `BNCMOV_CLVBT` varchar(3) DEFAULT NULL COMMENT 'clave del banco segun catalogo del sat',
  `BNCMOV_BANCOT` varchar(255) DEFAULT NULL COMMENT 'numero de sucursal',
  `BNCMOV_NUMCTAT` varchar(20) DEFAULT NULL COMMENT 'numero de cuenta',
  `BNCMOV_CLABET` varchar(18) DEFAULT NULL COMMENT 'clave bancaria estandarizada (clabe)',
  `BNCMOV_SCRSLT` varchar(20) DEFAULT NULL COMMENT 'numero de sucursal',
  `BNCMOV_CLVFRM` varchar(2) NOT NULL COMMENT 'clave forma de pago',
  `BNCMOV_FRMPG` varchar(64) DEFAULT NULL COMMENT 'descripcion de la forma de pago o cobro',
  `BNCMOV_NODOCTO` varchar(20) DEFAULT NULL COMMENT 'numero de documento',
  `BNCMOV_DESC` varchar(256) NOT NULL COMMENT 'descripcion del movimiento',
  `BNCMOV_REF` varchar(24) DEFAULT NULL COMMENT 'referencia alfanumerica',
  `BNCMOV_NUMAUTO` varchar(24) DEFAULT NULL COMMENT 'numero de autorizacion',
  `BNCMOV_NUMOPE` varchar(20) DEFAULT NULL COMMENT 'numero de operacion',
  `BNCMOV_REFNUM` varchar(20) DEFAULT NULL COMMENT 'referencia numerica',
  `BNCMOV_TPCMB` decimal(4,2) NOT NULL COMMENT 'tipo de cambio',
  `BNCMOV_CARGO` decimal(11,4) DEFAULT NULL COMMENT 'cargo / retiro',
  `BNCMOV_ABONO` decimal(11,4) DEFAULT NULL COMMENT 'abono / deposito',
  `BNCMOV_CLVMND` varchar(3) DEFAULT NULL COMMENT 'clave sat de moneda',
  `BNCMOV_NOTA` varchar(64) DEFAULT NULL COMMENT 'observaciones',
  `BNCMOV_USR_C` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que cancela el comprobante',
  `BNCMOV_USR_A` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que cancela',
  `BNCMOV_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `BNCMOV_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCMOV_FM` datetime DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `BNCMOV_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `BNCMOV_JSON` text DEFAULT NULL COMMENT 'json',
  PRIMARY KEY (`BNCMOV_ID`) USING BTREE,
  KEY `BNCMOV_OPER_ID` (`BNCMOV_OPER_ID`) USING BTREE,
  KEY `BNCMOV_CNCP_ID` (`BNCMOV_CNCP_ID`) USING BTREE,
  KEY `BNCMOV_FRMT_ID` (`BNCMOV_FRMT_ID`) USING BTREE,
  KEY `BNCMOV_DRCTR_ID` (`BNCMOV_DRCTR_ID`) USING BTREE,
  KEY `BNCMOV_CTAE_ID` (`BNCMOV_CTAE_ID`) USING BTREE,
  KEY `BNCMOV_CTAR_ID` (`BNCMOV_CTAR_ID`) USING BTREE,
  KEY `BNCMOV_STTS_ID` (`BNCMOV_STTS_ID`) USING BTREE,
  KEY `BNCMOV_NOIDEN` (`BNCMOV_NOIDEN`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: movimientos bancarios';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.BNCSLD
CREATE TABLE IF NOT EXISTS `BNCSLD` (
  `BNCSLD_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice',
  `BNCSLD_SBID` int(11) NOT NULL COMMENT 'indice de relacion de la cuenta de bancos.',
  `BNCSLD_ANIO` int(2) NOT NULL COMMENT 'ejecicio',
  `BNCSLD_MES` int(2) NOT NULL COMMENT 'periodo',
  `BNCSLD_INICIAL` decimal(14,4) NOT NULL COMMENT 'saldo inicial de la cuenta',
  `BNCSLD_FINAL` decimal(14,4) DEFAULT NULL COMMENT 'saldo final de la cuenta',
  `BNCSLD_FS` datetime NOT NULL COMMENT 'fecha inicial del saldo',
  `BNCSLD_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del nuevo registro',
  `BNCSLD_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `BNCSLD_NOTA` varchar(255) DEFAULT NULL COMMENT 'nota',
  PRIMARY KEY (`BNCSLD_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='bancos: saldos del control de bancos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CMSND
CREATE TABLE IF NOT EXISTS `CMSND` (
  `CMSND_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CMSND_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `CMSND_CMSNT_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de comisiones (cmsnt_id)',
  `CMSND_RNG1` decimal(18,4) DEFAULT NULL COMMENT 'rango inicial del descuento',
  `CMSND_RNG2` decimal(18,4) DEFAULT NULL COMMENT 'rango final del descuento',
  `CMSND_FACTOR` decimal(18,4) DEFAULT NULL COMMENT 'factor del la comision',
  `CMSND_NOM` varchar(20) DEFAULT NULL COMMENT 'descripcion de la comision',
  `CMSND_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `CMSND_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CMSND_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CMSND_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`CMSND_ID`),
  KEY `CMSND_CMSNT_ID` (`CMSND_CMSNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='comisiones: catalogo rangos de descuento a cliente';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CMSNM
CREATE TABLE IF NOT EXISTS `CMSNM` (
  `CMSNM_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CMSNM_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CMSNM_CMSNT_ID` int(11) DEFAULT NULL,
  `CMSNM_RNG1` decimal(18,4) DEFAULT NULL,
  `CMSNM_RNG2` decimal(18,4) DEFAULT NULL,
  `CMSNM_FACTOR` decimal(18,4) DEFAULT NULL,
  `CMSNM_NOM` varchar(20) DEFAULT NULL COMMENT 'nombre o descripcion',
  `CMSNM_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `CMSNM_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CMSNM_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CMSNM_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`CMSNM_ID`),
  KEY `CMSNM_CMSNT_ID` (`CMSNM_CMSNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='comisiones: multas a la cartera vencida';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CMSNT
CREATE TABLE IF NOT EXISTS `CMSNT` (
  `CMSNT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CMSNT_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CMSNT_SEC_ID` int(11) DEFAULT NULL COMMENT 'indice de secuencia',
  `CMSNT_VMIN` decimal(18,4) DEFAULT NULL COMMENT 'monto de venta minima',
  `CMSNT_VMAX` decimal(18,4) DEFAULT NULL COMMENT 'monto de venta maxima',
  `CMSNT_NOM` varchar(64) DEFAULT NULL COMMENT 'descripcion de comision',
  `CMSNT_NOTA` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `CMSNT_FCHINI` timestamp NULL DEFAULT NULL COMMENT 'fecha de inicio de vigencia',
  `CMSNT_FCHVIG` timestamp NULL DEFAULT NULL COMMENT 'fecha final de vigencia',
  `CMSNT_FN` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `CMSNT_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que creo el registro',
  `CMSNT_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CMSNT_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`CMSNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='comisiones/ventas: catalogo de comisiones';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CONF
CREATE TABLE IF NOT EXISTS `CONF` (
  `CONF_KEY` varchar(25) NOT NULL DEFAULT 'default' COMMENT 'llave',
  `CONF_GRP` int(11) NOT NULL DEFAULT 0 COMMENT 'grupo',
  `CONF_DATA` varchar(500) DEFAULT NULL COMMENT 'configuracion o data',
  `CONF_NOTA` varchar(50) DEFAULT NULL COMMENT 'nota o comentario',
  PRIMARY KEY (`CONF_KEY`,`CONF_GRP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de configuraciones';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTCAT
CREATE TABLE IF NOT EXISTS `CTCAT` (
  `CTCAT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la categoria',
  `CTCAT_CTCAT_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con categorias',
  `CTCAT_ALM_ID` smallint(1) DEFAULT 1 COMMENT 'tipo de almacen (mp=1, pt = 2, dp = 3, tw = 4)',
  `CTCAT_A` smallint(1) DEFAULT 1 COMMENT 'registro activo',
  `CTCAT_CLV` varchar(128) DEFAULT NULL COMMENT 'clave',
  `CTCAT_NOM` varchar(128) DEFAULT NULL COMMENT 'nombre o descripcion de la categoria',
  `CTCAT_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CTCAT_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  PRIMARY KEY (`CTCAT_ID`) USING BTREE,
  KEY `CTCAT_CTCAT_ID` (`CTCAT_CTCAT_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='categorias de bienes, productos y servicios (bps) ';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTCLS
CREATE TABLE IF NOT EXISTS `CTCLS` (
  `CTCLS_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla',
  `CTCLS_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `CTCLS_ALM_ID` smallint(6) NOT NULL COMMENT 'tipo de almacen',
  `CTCLS_SBID` int(11) DEFAULT NULL COMMENT 'indice de relacion en si mismo',
  `CTCLS_SEC` int(11) DEFAULT NULL COMMENT 'secuencia',
  `CTCLS_CLASS1` varchar(128) DEFAULT NULL COMMENT 'sub clasifiacacion',
  `CTCLS_CLASS2` varchar(128) DEFAULT NULL COMMENT 'nombre de la categoria',
  `CTCLS_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CTCLS_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de sistema',
  `CTCLS_URL` varchar(255) DEFAULT NULL COMMENT 'url de la imagen asociada',
  PRIMARY KEY (`CTCLS_ID`),
  KEY `CTCLS_SBID` (`CTCLS_SBID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='categorias / clasifiaciones';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTDPT
CREATE TABLE IF NOT EXISTS `CTDPT` (
  `CTDPT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTDPT_A` int(11) DEFAULT NULL COMMENT 'registro activo',
  `CTDPT_CTCAT_ID` int(11) DEFAULT NULL COMMENT 'indice de clasificacion',
  `CTDPT_CTAREA_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de areas',
  `CTDPT_CNTBL_ID` int(11) DEFAULT NULL COMMENT 'cuenta contable',
  `CTDPT_NOM` varchar(40) DEFAULT NULL COMMENT 'nombre o descripcion del departamento',
  `CTDPT_RESP` varchar(50) DEFAULT NULL COMMENT 'nombre del responsable',
  `CTDPT_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `CTDPT_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  PRIMARY KEY (`CTDPT_ID`) USING BTREE,
  KEY `CTDPT_CTCTBL` (`CTDPT_CNTBL_ID`) USING BTREE,
  KEY `CTDPT_CLS_ID` (`CTDPT_CTCAT_ID`) USING BTREE,
  KEY `CTDPT_CTAREA_ID` (`CTDPT_CTAREA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='empresa: catalogo de departamentos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTESPC
CREATE TABLE IF NOT EXISTS `CTESPC` (
  `CTESPC_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTESPC_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTESPC_NOM` varchar(50) DEFAULT NULL COMMENT 'nombre o descripcion',
  `CTESPC_NOTA` varchar(50) DEFAULT NULL COMMENT 'observaciones',
  `CTESPC_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `CTESPC_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CTESPC_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CTESPC_USR_M` varchar(10) DEFAULT NULL COMMENT 'ultima clave del usuario que modifica el registro',
  PRIMARY KEY (`CTESPC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de especificaciones (tamanio) ';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDL
CREATE TABLE IF NOT EXISTS `CTMDL` (
  `CTMDL_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice principal de la tabla',
  `CTMDL_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTMDL_CTCLS_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de categorias',
  `CTMDL_CTPRD_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de la tabla de productos',
  `CTMDL_CTPRC_ID` int(11) DEFAULT 0 COMMENT 'indice del catalogo de precios',
  `CTMDL_ALM_ID` smallint(6) NOT NULL DEFAULT 0 COMMENT 'indice del almacen mp = 1, pt = 2, tw = 3',
  `CTMDL_ATRZD` smallint(6) NOT NULL DEFAULT 0 COMMENT 'precio autorizado',
  `CTMDL_TIPO` smallint(6) NOT NULL DEFAULT 1 COMMENT 'alm:|mp,pt,tw| desc: tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). es utilizado en cfdi para hacer la distincion de producto o servicio.',
  `CTMDL_VIS` smallint(6) NOT NULL DEFAULT 0 COMMENT 'para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)',
  `CTMDL_CLV` varchar(128) DEFAULT NULL COMMENT 'clave del modelo (url)',
  `CTMDL_DSCRC` varchar(1000) DEFAULT NULL COMMENT 'descripcion corta',
  `CTMDL_MRC` varchar(128) DEFAULT NULL COMMENT 'marca o fabricante',
  `CTMDL_ESPC` varchar(128) DEFAULT NULL COMMENT 'especifcaciones',
  `CTMDL_UNDD` varchar(20) DEFAULT NULL COMMENT 'unidad personalizada',
  `CTMDL_UNTR` decimal(11,4) DEFAULT NULL COMMENT 'precio unitario',
  `CTMDL_TRSIVAF` varchar(6) DEFAULT NULL COMMENT 'factor del iva traslado',
  `CTMDL_TRSIVA` decimal(14,4) DEFAULT NULL COMMENT 'importe del impuesto del iva trasladado',
  `CTMDL_UNDDZ` int(11) DEFAULT NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad utiliza en alto o calibre eje z',
  `CTMDL_UNDDXY` int(11) DEFAULT NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad utilizada en largo y ancho',
  `CTMDL_UNDDA` int(11) DEFAULT NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad de almacenamiento en el el almacen',
  `CTMDL_LARGO` decimal(14,4) DEFAULT NULL COMMENT 'largo',
  `CTMDL_ANCHO` decimal(14,4) DEFAULT NULL COMMENT 'ancho',
  `CTMDL_ALTO` decimal(14,4) DEFAULT NULL COMMENT 'alto',
  `CTMDL_PESO` decimal(14,4) DEFAULT NULL COMMENT 'peso',
  `CTMDL_CDG` varchar(12) DEFAULT NULL COMMENT 'codigo de barras',
  `CTMDL_SKU` varchar(100) DEFAULT NULL COMMENT 'obtener o establecer el numero de parte, identificador del producto o del servicio, la clave de producto o servicio, sku o equivalente, propia de la operacion del emisor (en cfdi: noidentificacion) (publicacion)',
  `CTMDL_MIN` decimal(11,4) DEFAULT NULL COMMENT 'stock minimo del almacen',
  `CTMDL_MAX` decimal(11,4) DEFAULT NULL COMMENT 'stock maximo del almacen',
  `CTMDL_REORD` decimal(11,4) DEFAULT NULL COMMENT 'punto de reorden',
  `CTMDL_EXT` decimal(11,4) DEFAULT NULL COMMENT 'cantidad de existencia',
  `CTMDL_CTAPRE` varchar(20) DEFAULT NULL COMMENT 'numero de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar los datos de identificacion del certificado de participacion inmobiliaria no amortizable.',
  `CTMDL_NUMREQ` varchar(50) DEFAULT NULL COMMENT 'numero de requerimiento de aduana',
  `CTMDL_CLVUND` varchar(3) DEFAULT NULL COMMENT 'clave de unidad sat',
  `CTMDL_CLVPRDS` varchar(8) DEFAULT NULL COMMENT 'clave del producto o del servicio sat',
  `CTMDL_CLVOBJ` varchar(2) DEFAULT NULL COMMENT 'clave si la operacion es objeto o no de impuesto sat',
  `CTMDL_ETQTS` varchar(254) DEFAULT NULL COMMENT 'etiquetas de busqueda (mover a publicacion)',
  `CTMDL_DSCR` varchar(8000) DEFAULT NULL COMMENT 'descripcion larga del producto (publicacion)',
  `CTMDL_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que crea el registro',
  `CTMDL_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `CTMDL_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `CTMDL_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CTMDL_SEC` int(11) DEFAULT NULL,
  `CTMDL_DIS` smallint(6) NOT NULL,
  `CTMDL_RETIVAF` varchar(5) DEFAULT NULL,
  `CTMDL_RETIEPS` decimal(14,4) DEFAULT NULL,
  `CTMDL_RETISR` decimal(14,4) DEFAULT NULL,
  `CTMDL_RETIEPSF` varchar(6) DEFAULT NULL,
  `CTMDL_RETISRF` varchar(5) DEFAULT NULL,
  `CTMDL_TRSIEPSF` varchar(6) DEFAULT NULL,
  `CTMDL_UNTR1` decimal(11,4) DEFAULT NULL COMMENT '(eliminar)',
  `CTMDL_TRSIEPS` decimal(14,4) DEFAULT NULL,
  `CTMDL_UNTR2` decimal(11,4) DEFAULT NULL COMMENT '(eliminar)',
  `CTMDL_RETIVA` decimal(14,4) DEFAULT NULL,
  `CTMDL_CANT1` smallint(6) DEFAULT NULL COMMENT '(eliminar)',
  `CTMDL_CANT2` smallint(6) DEFAULT NULL COMMENT '(eliminar)',
  PRIMARY KEY (`CTMDL_ID`),
  KEY `CTMDL_CTPRD_ID` (`CTMDL_CTPRD_ID`),
  KEY `CTMDL_CTCLS_ID` (`CTMDL_CTCLS_ID`),
  KEY `CTMDL_CTPRC_ID` (`CTMDL_CTPRC_ID`),
  KEY `CTMDL_ATRZD` (`CTMDL_ATRZD`)
) ENGINE=InnoDB AUTO_INCREMENT=2414 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos de productos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDLE
CREATE TABLE IF NOT EXISTS `CTMDLE` (
  `CTMDLE_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTMDLE_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTMDLE_CTPRD_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de productos',
  `CTMDLE_CTMDL_ID` int(11) NOT NULL COMMENT 'indice de relacion de la tabla de modelos',
  `CTMDLE_CTESPC_ID` int(11) NOT NULL COMMENT 'indice de relacion de la tabla de ctespc_id',
  `CTMDLE_ATRZD` smallint(6) NOT NULL COMMENT 'autorizacion',
  `CTMDLE_ALM_ID` smallint(6) NOT NULL COMMENT 'indice de tipo de almacen',
  `CTMDLE_VIS` smallint(6) NOT NULL COMMENT 'bandera para el registro visible',
  `CTMDLE_EXT` decimal(11,4) DEFAULT NULL COMMENT 'existencia',
  `CTMDLE_MIN` decimal(11,4) DEFAULT NULL COMMENT 'minimo',
  `CTMDLE_MAX` decimal(11,4) DEFAULT NULL COMMENT 'maximo',
  `CTMDLE_REORD` decimal(11,4) DEFAULT NULL COMMENT 'reorden',
  `CTMDLE_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CTMDLE_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `CTMDLE_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `CTMDLE_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CTMDLE_DIS` smallint(6) NOT NULL,
  PRIMARY KEY (`CTMDLE_ID`),
  KEY `CTMDLE_CTPRD_ID` (`CTMDLE_CTPRD_ID`),
  KEY `CTMDLE_CTMDL_ID` (`CTMDLE_CTMDL_ID`),
  KEY `CTMDLE_CTESPC_ID` (`CTMDLE_CTESPC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2321 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos: tabla de relacion modelos vs especificaciones (tamanios)';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDLF
CREATE TABLE IF NOT EXISTS `CTMDLF` (
  `CTMDLF_CTMDL_ID` int(11) NOT NULL COMMENT 'indice de la tabla de modelos',
  `CTMDLF_CVUND` varchar(3) DEFAULT NULL COMMENT 'clave de unidad SAT',
  `CTMDLF_CVPRD` varchar(8) DEFAULT NULL COMMENT 'clave del producto o del servicio sat',
  `CTMDLF_CVOBJ` varchar(2) DEFAULT NULL COMMENT 'clave si la operacion es objeto o no de impuesto sat',
  `CTMDLF_FM` date DEFAULT current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
  `CTMDLF_USR_FM` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`CTMDLF_CTMDL_ID`),
  KEY `CTMDLF_CTMDL_ID` (`CTMDLF_CTMDL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos: datos fiscales relacionados al modelo';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDLI
CREATE TABLE IF NOT EXISTS `CTMDLI` (
  `CTMDLI_CTMDL_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de la tabla de modelos',
  `CTMDLI_IMPST` varchar(10) NOT NULL DEFAULT 'IVA' COMMENT 'impuesto (IVA, IEPS, ISR)',
  `CTMDLI_VALOR` decimal(20,6) NOT NULL DEFAULT 0.160000 COMMENT 'valor de la tasa o cuota',
  `CTMDLI_FACTOR` smallint(6) NOT NULL DEFAULT 0 COMMENT 'factor del impuesto (Tasa, Cuota)',
  `CTMDLI_TR` smallint(6) NOT NULL DEFAULT 1 COMMENT 'traslado = 1, retencion = 2',
  `CTMDLI_FM` date DEFAULT current_timestamp(),
  `CTMDLI_USR_M` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`CTMDLI_CTMDL_ID`,`CTMDLI_IMPST`,`CTMDLI_VALOR`,`CTMDLI_TR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos: lista de impuestos aplicables';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDLM
CREATE TABLE IF NOT EXISTS `CTMDLM` (
  `CTMDLM_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTMDLM_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTMDLM_CTMDL_ID` int(11) DEFAULT NULL COMMENT 'indice del modelo',
  `CTMDLM_CTPRD_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de productos',
  `CTMDLM_URL` varchar(8000) DEFAULT NULL COMMENT 'url amazon',
  PRIMARY KEY (`CTMDLM_ID`),
  KEY `CTMDLM_CTMDL_ID` (`CTMDLM_CTMDL_ID`),
  KEY `CTMDLM_CTPRD_ID` (`CTMDLM_CTPRD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos: imagenes relacionadas al modelo';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDLP
CREATE TABLE IF NOT EXISTS `CTMDLP` (
  `CTMDLP_CTMDL_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `CTMDLP_VIS` smallint(6) NOT NULL COMMENT 'para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)',
  `CTMDLP_SEC` int(11) DEFAULT NULL COMMENT 'secuencia de aparicion',
  `CTMDLP_LARGO` decimal(14,4) DEFAULT NULL COMMENT 'largo',
  `CTMDLP_ANCHO` decimal(14,4) DEFAULT NULL COMMENT 'ancho',
  `CTMDLP_ALTO` decimal(14,4) DEFAULT NULL COMMENT 'alto o calibre',
  `CTMDLP_PESO` decimal(14,4) DEFAULT NULL COMMENT 'peso',
  `CTMDLP_SKU` varchar(100) DEFAULT NULL COMMENT 'obtener o establecer el numero de parte, identificador del producto o del servicio, la clave de producto o servicio, sku o equivalente, propia de la operacion del emisor (en cfdi: noidentificacion)',
  `CTMDLP_DSCRC` varchar(1000) DEFAULT NULL COMMENT 'descripcion corta',
  `CTMDLP_DSCR` varchar(8000) DEFAULT NULL COMMENT 'descripcion larga de producto',
  `CTMDLP_ETQTS` varchar(254) DEFAULT NULL COMMENT 'etiquetas de busqueda',
  `CTMDLP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `CTMDLP_FM` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de ultima modificacion',
  PRIMARY KEY (`CTMDLP_CTMDL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos, publicacion para la tienda web';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTMDLR
CREATE TABLE IF NOT EXISTS `CTMDLR` (
  `CTMDLR_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTMDLR_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTMDLR_RATING` smallint(6) DEFAULT NULL COMMENT 'calificacion',
  `CTMDLR_CTMDL_ID` int(11) DEFAULT NULL COMMENT 'indice del modelo',
  `CTMDLR_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice del directorio de clientes',
  `CTMDLR_NOTA` varchar(1000) DEFAULT NULL COMMENT 'comentario del cliente',
  `CTMDLR_URL` varchar(8000) DEFAULT NULL COMMENT 'url amazon',
  `CTMDLR_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  PRIMARY KEY (`CTMDLR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de modelos: comentarios de los clientes de la tienda web';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTPRC
CREATE TABLE IF NOT EXISTS `CTPRC` (
  `CTPRC_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTPRC_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTPRC_SEC_ID` smallint(6) DEFAULT NULL,
  `CTPRC_STTS_ID` smallint(6) DEFAULT NULL COMMENT 'status de la lista',
  `CTPRC_NOM` varchar(50) DEFAULT NULL COMMENT 'nombre o descripcion de la lista',
  `CTPRC_FECINI` date DEFAULT NULL COMMENT 'fecha de inicio de la vigencia',
  `CTPRC_FECFIN` date DEFAULT NULL COMMENT 'fecha final de la vigencia',
  `CTPRC_OBSRV` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `CTPRC_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `CTPRC_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `CTPRC_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `CTPRC_USU_M` varchar(10) DEFAULT NULL COMMENT 'ultima clave del usuario que modifica el registro',
  PRIMARY KEY (`CTPRC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de precios';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTPRCP
CREATE TABLE IF NOT EXISTS `CTPRCP` (
  `CTPRCP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTPRCP_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTPRCP_CTPRC_ID` int(11) NOT NULL COMMENT 'indice de relacion con el catalogo de lista de precios',
  `CTPRCP_CTESPC_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de especificaciones',
  `CTPRCP_UNITC` decimal(18,4) DEFAULT NULL,
  `CTPRCP_UNIT` decimal(18,4) DEFAULT NULL COMMENT 'valor unitario',
  `CTPRCP_OBSRV` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `CTPRCP_FN` timestamp NULL DEFAULT NULL COMMENT 'fecha de creacion del registro',
  `CTPRCP_USU_N` varchar(10) DEFAULT current_timestamp() COMMENT 'clave del usuario creador del registro',
  `CTPRCP_FM` timestamp NULL DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  `CTPRCP_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifca el registro',
  PRIMARY KEY (`CTPRCP_ID`),
  KEY `CTPRCP_CTPRC_ID` (`CTPRCP_CTPRC_ID`),
  KEY `CTPRCP_CTESPC_ID` (`CTPRCP_CTESPC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de precios: tabla de partidas de la lista de precios';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTPRD
CREATE TABLE IF NOT EXISTS `CTPRD` (
  `CTPRD_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTPRD_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTPRD_ALM_ID` smallint(6) NOT NULL COMMENT 'indice del almacen',
  `CTPRD_SEC` int(11) DEFAULT NULL,
  `CTPRD_TIPO` smallint(6) NOT NULL,
  `CTPRD_CTCLS_ID` smallint(6) NOT NULL COMMENT 'indice del catalogo de categorias',
  `CTPRV_CNVR` decimal(14,4) DEFAULT NULL COMMENT 'factor de conversion a la unidad de almacen',
  `CTPRD_CNVR` decimal(14,4) DEFAULT NULL COMMENT 'clave de producto para formar url',
  `CTPRD_CLV` varchar(128) DEFAULT NULL COMMENT 'clave de producto para formar url',
  `CTPRD_NOM` varchar(128) DEFAULT NULL COMMENT 'nombre o descripcion del producto',
  `CTPRD_USR_N` varchar(20) DEFAULT NULL COMMENT 'clave de usuario que crea el registro',
  `CTPRD_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `CTPRD_URL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CTPRD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2208 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de productos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTPRDM
CREATE TABLE IF NOT EXISTS `CTPRDM` (
  `CTPRDM_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `CTPRDM_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTPRDM_CTPRD_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de productos',
  `CTPRDM_URL` varchar(8000) DEFAULT NULL COMMENT 'url amazon',
  PRIMARY KEY (`CTPRDM_ID`),
  KEY `CTPRDM_CTPRD_ID` (`CTPRDM_CTPRD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='catalogo de productos: tabla de imagenes relacionadas a productos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.CTUND
CREATE TABLE IF NOT EXISTS `CTUND` (
  `CTUND_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `CTUND_A` int(11) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `CTUND_ALM_ID` int(11) DEFAULT NULL COMMENT 'indice del almacen al que pertenece el catalogo',
  `CTUND_NOM` varchar(32) NOT NULL COMMENT 'nombre de la unidad',
  `CTUND_CLVUND` varchar(3) DEFAULT NULL COMMENT 'clave sat',
  `CTUND_FAC` decimal(14,6) DEFAULT NULL COMMENT 'factor de la unidad',
  `CTUND_FN` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `CTUND_USR_N` varchar(10) NOT NULL COMMENT 'clave del usuario que creo el registro',
  `CTUND_FM` datetime DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
  `CTUND_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`CTUND_ID`) USING BTREE,
  KEY `CTUND_ALM_ID` (`CTUND_ALM_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='almacen: catalogo de unidades';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCCN
CREATE TABLE IF NOT EXISTS `DRCCN` (
  `DRCCN_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `DRCCN_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `DRCCN_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion del directorio',
  `DRCCN_CTDRC_ID` int(11) DEFAULT NULL COMMENT 'alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=envio)',
  `DRCCN_AUTO_ID` int(11) DEFAULT NULL COMMENT 'domicilio autorizado',
  `DRCCN_DFLT` int(11) DEFAULT NULL COMMENT 'domicilio por defecto (1 = default)',
  `DRCCN_TTL` varchar(50) DEFAULT NULL COMMENT 'titulo del domicilio, utilizado en tienda web',
  `DRCCN_CDGPS` varchar(50) DEFAULT NULL COMMENT 'codigo de pais',
  `DRCCN_CP` varchar(50) DEFAULT NULL COMMENT 'codigo postal',
  `DRCCN_TP` varchar(10) DEFAULT NULL COMMENT 'alias de direccion (1=fiscal, 2=particular, 3=sucursal, 4=envio)',
  `DRCCN_ASNH` varchar(64) DEFAULT NULL COMMENT 'asentamiento humano',
  `DRCCN_ASNTMNT_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de asentamiento humano',
  `DRCCN_EXTR` varchar(64) DEFAULT NULL COMMENT 'numero exterior (antes drccn_nmrextr)',
  `DRCCN_INTR` varchar(64) DEFAULT NULL COMMENT 'numero interior (antes drccn_nmrintr)',
  `DRCCN_CLL` varchar(124) DEFAULT NULL COMMENT 'calle',
  `DRCCN_CLN` varchar(124) DEFAULT NULL COMMENT 'colonia',
  `DRCCN_DLG` varchar(124) DEFAULT NULL COMMENT 'delegacion / municipio',
  `DRCCN_CDD` varchar(100) DEFAULT NULL COMMENT 'ciudad',
  `DRCCN_STD` varchar(124) DEFAULT NULL COMMENT 'estado (antes drccn_estd)',
  `DRCCN_PS` varchar(124) DEFAULT NULL COMMENT 'pais',
  `DRCCN_LCLDD` varchar(124) DEFAULT NULL COMMENT 'localidad',
  `DRCCN_RFRNC` varchar(124) DEFAULT NULL COMMENT 'referencia',
  `DRCCN_TLFNS` varchar(124) DEFAULT NULL COMMENT 'telefono de contacto (antes drccn_tlfn)',
  `DRCCN_DSP` varchar(124) DEFAULT NULL COMMENT 'descripcion de la ubicacion',
  `DRCCN_REQ` varchar(124) DEFAULT NULL COMMENT 'horario y requisito de acceso',
  `DRCCN_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro (antes drccn_usu_n)',
  `DRCCN_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `DRCCN_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `DRCCN_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `DRCCN_SYNC_ID` int(11) DEFAULT NULL COMMENT 'sincronizacion',
  PRIMARY KEY (`DRCCN_ID`),
  KEY `DRCCN_DRCTR_ID` (`DRCCN_DRCTR_ID`),
  KEY `DRCCN_CTDRC_ID` (`DRCCN_CTDRC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: direcciones de los contactos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTR
CREATE TABLE IF NOT EXISTS `DRCTR` (
  `DRCTR_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `DRCTR_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `DRCTR_EXTRNJR` smallint(6) DEFAULT NULL COMMENT 'bandera para identificar la nacionalidad (0 => nacional, 1 => extranjero)',
  `DRCTR_RFCV` smallint(6) DEFAULT NULL COMMENT 'bandera para indicar si el registro ya fue validado (nombre, rfc y domicilio fiscal)',
  `DRCTR_RFC` varchar(14) DEFAULT NULL COMMENT 'registro federal de contribuyentes',
  `DRCTR_CURP` varchar(20) DEFAULT NULL COMMENT 'clave unica de registro de poblacion',
  `DRCTR_CLV` varchar(10) DEFAULT NULL COMMENT 'clave de sistema',
  `DRCTR_NOM` varchar(255) DEFAULT NULL COMMENT 'nombre o razon social del cliente o proveedor',
  `DRCTR_NOMC` varchar(255) DEFAULT NULL COMMENT 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.',
  `DRCTR_WEB` varchar(64) DEFAULT NULL COMMENT 'url del sitio web',
  `DRCTR_NTS` varchar(100) DEFAULT NULL COMMENT 'observaciones (antes drctr_obsr)',
  `DRCTR_CRD` decimal(18,4) DEFAULT NULL COMMENT 'lmite de credito (drctr_lmtcrdt$)',
  `DRCTR_SCRD` decimal(18,4) DEFAULT NULL COMMENT 'sobre credito (antes drctr_sbrcrdt$)',
  `DRCTR_PCTD` decimal(18,4) DEFAULT NULL COMMENT 'factor pactado del descuento al cliente (drctr_factor)',
  `DRCTR_IVA` decimal(18,4) DEFAULT NULL COMMENT 'factor iva factura (drctr_ffctr)',
  `DRCTR_DSP` int(11) DEFAULT NULL COMMENT 'dias pactados de entrega (antes drctr_dspct)',
  `DRCTR_DSCRD` int(11) DEFAULT NULL COMMENT 'dias de credito (antes drctr_dscrdt)',
  `DRCTR_CMSN_ID` int(11) DEFAULT NULL COMMENT 'id del catalogo de comisiones en el caso de ser vendedor',
  `DRCTR_DSCNT_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de descuentos (antes drctr_ctlgdscnt_id)',
  `DRCTR_CTPRC_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de precios (antes drctr_prcs_id)',
  `DRCTR_PDFC` varchar(255) DEFAULT NULL COMMENT 'constancia de situacion fiscal',
  `DRCTR_CTRG_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de tipos de regimenes fiscales, no_definido = 0, persona_fisica = 1, persona_moral = 2 (antes drctr_ctlgrgf_id)',
  `DRCTR_RGFSC` varchar(5) DEFAULT NULL COMMENT 'clave de regimen fiscal segun catalogo sat',
  `DRCTR_RGF` varchar(10) DEFAULT NULL COMMENT 'regimen fiscal (fisica o moral)',
  `DRCTR_NMREG` varchar(40) DEFAULT NULL COMMENT 'numero de registro de identidad fiscal',
  `DRCTR_RESFIS` varchar(20) DEFAULT NULL COMMENT 'clave del pais de residencia para efectos fiscales del receptor del comprobante',
  `DRCTR_USOCFDI` varchar(5) DEFAULT NULL COMMENT 'clave de uso de cfdi',
  `DRCTR_CLL` varchar(124) DEFAULT NULL COMMENT 'direccion fiscal: calle o nombre de la vialidad',
  `DRCTR_EXTR` varchar(64) DEFAULT NULL COMMENT 'direccion fiscal: numero exterior (antes drctr_nmrextr)',
  `DRCTR_INTR` varchar(64) DEFAULT NULL COMMENT 'direccion fiscal: numero interior (antes drctr_nmrintr)',
  `DRCTR_DOMFIS` varchar(5) DEFAULT NULL COMMENT 'direccion fiscal: codigo postal del domicilio fiscal del receptor del comprobante',
  `DRCTR_CP` varchar(50) DEFAULT NULL COMMENT 'direccion fiscal: codigo postal',
  `DRCTR_CLN` varchar(124) DEFAULT NULL COMMENT 'direccion fiscal: colonia',
  `DRCTR_STD` varchar(124) DEFAULT NULL COMMENT 'direccion fiscal: entidad federativa, estado (antes drctr_estd)',
  `DRCTR_DLG` varchar(124) DEFAULT NULL COMMENT 'direccion fiscal: delegacion / municipio',
  `DRCTR_CDD` varchar(100) DEFAULT NULL COMMENT 'direccion fiscal: ciudad',
  `DRCTR_TEL` varchar(64) DEFAULT NULL COMMENT 'telefono de contacto (antes drctr_tlfn)',
  `DRCTR_MAIL` varchar(100) DEFAULT NULL COMMENT 'correo electronico del contacto',
  `DRCTR_RFRNC` varchar(124) DEFAULT NULL COMMENT 'direccion fiscal: referencia',
  `DRCTR_LCLDD` varchar(124) DEFAULT NULL COMMENT 'direccion fiscal: localidad',
  `DRCTR_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `DRCTR_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `DRCTR_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `DRCTR_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `DRCTR_RLCN` varchar(64) DEFAULT NULL COMMENT 'relacion comercial con la empresa (27=cliente,163=proveedor)(eliminar)',
  `DRCTR_RGSTRPTRNL` varchar(12) DEFAULT NULL COMMENT 'registro patronal (por eliminar pasa a configuracion)',
  `DRCTR_PSW` varchar(64) DEFAULT NULL COMMENT 'contrasenia (eliminar)',
  `DRCTR_PRNCPL` smallint(6) DEFAULT NULL COMMENT 'por eliminar',
  `DRCTR_CVROLL` varchar(32) DEFAULT NULL COMMENT 'por eliminar',
  `DRCTR_USR` smallint(6) DEFAULT NULL COMMENT 'por eliminar',
  `DRCTR_APIKEY` varchar(32) DEFAULT NULL COMMENT 'por eliminar',
  `DRCTR_DOC_ID` int(11) DEFAULT NULL COMMENT '(eliminar)',
  `DRCTR_USR_ID` smallint(6) DEFAULT NULL COMMENT '(eliminar)',
  `DRCTR_WEB_ID` int(11) DEFAULT NULL COMMENT '(eliminar)',
  `DRCTR_SYNC_ID` int(11) DEFAULT NULL COMMENT '(eliminar)',
  `DRCTR_FILE` longblob DEFAULT NULL COMMENT '(eliminar)',
  PRIMARY KEY (`DRCTR_ID`) USING BTREE,
  KEY `DRCTR_RFC` (`DRCTR_RFC`) USING BTREE,
  KEY `DRCTR_CMSN_ID` (`DRCTR_CMSN_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: directorio de la aplicacion';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTRB
CREATE TABLE IF NOT EXISTS `DRCTRB` (
  `DRCTRB_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `DRCTRB_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `DRCTRB_VRFCD` smallint(6) NOT NULL COMMENT 'para marcar que la cuenta de banco esta verificada',
  `DRCTRB_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con el directorio',
  `DRCTRB_CRGMX` decimal(14,4) DEFAULT NULL COMMENT 'cargo maximo',
  `DRCTRB_CLV` varchar(4) DEFAULT NULL COMMENT 'para expresar el banco emisor del cheque, de acuerdo al catalogo publicado en la pagina de internet del sat',
  `DRCTRB_REFNUM` varchar(7) DEFAULT NULL COMMENT 'referencia numerica',
  `DRCTRB_MND` varchar(10) DEFAULT NULL COMMENT 'opcion tipo moneda adicional',
  `DRCTRB_RFC` varchar(14) DEFAULT NULL COMMENT 'registro federal de contribuyentes',
  `DRCTRB_SCRSL` varchar(20) DEFAULT NULL COMMENT 'sucursal',
  `DRCTRB_TIPO` varchar(20) DEFAULT NULL COMMENT 'tipo de cuenta',
  `DRCTRB_REFALF` varchar(20) DEFAULT NULL COMMENT 'referencia alfanumerica',
  `DRCTRB_NMCTA` varchar(50) DEFAULT NULL COMMENT 'para expresar el numero de cuenta',
  `DRCTRB_CNTCLB` varchar(50) DEFAULT NULL COMMENT 'cuenta clabe',
  `DRCTRB_NMCRT` varchar(64) DEFAULT NULL COMMENT 'nombre corto definido en el listado del sat',
  `DRCTRB_PRAPLL` varchar(125) DEFAULT NULL COMMENT 'primer apellido',
  `DRCTRB_SGAPLL` varchar(125) DEFAULT NULL COMMENT 'segundo apellido',
  `DRCTRB_ALIAS` varchar(255) DEFAULT NULL COMMENT 'alias de la cuenta',
  `DRCTRB_BANCO` varchar(255) DEFAULT NULL COMMENT 'nombre o razon social',
  `DRCTRB_BNFCR` varchar(300) DEFAULT NULL COMMENT 'nombre del beneficiario',
  `DRCTRB_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `DRCTRB_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `DRCTRB_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `DRCTRB_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`DRCTRB_ID`),
  KEY `DRCTRB_RFC` (`DRCTRB_RFC`),
  KEY `DRCTRB_DRCTR_ID` (`DRCTRB_DRCTR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: cuentas bancarias relacionadas';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTRC
CREATE TABLE IF NOT EXISTS `DRCTRC` (
  `DRCTRC_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `DRCTRC_VNDR_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de vendedores',
  `DRCTRC_DRCTR_ID` int(11) NOT NULL COMMENT 'indice del directorio de clientes',
  `DRCTRC_NOTA` varchar(100) DEFAULT NULL,
  `DRCTRC_FM` timestamp NULL DEFAULT current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
  `DRCTRC_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`DRCTRC_VNDR_ID`,`DRCTRC_DRCTR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: cartera de clientes, relacion entre clientes y vendedores';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTRF
CREATE TABLE IF NOT EXISTS `DRCTRF` (
  `DRCTRF_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla del directorio (clientes)',
  `DRCTRF_FACTOR` decimal(18,4) NOT NULL COMMENT 'valor del factor anterior',
  `DRCTRF_FM` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de modificacion del registro',
  `DRCTRF_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`DRCTRF_DRCTR_ID`,`DRCTRF_FACTOR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: historial de cambios del factor pactado al cliente';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTRL
CREATE TABLE IF NOT EXISTS `DRCTRL` (
  `DRCTRL_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `DRCTRL_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `DRCTRL_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla del directorio',
  `DRCTRL_TTEL` varchar(16) DEFAULT NULL COMMENT 'tipo de telefono',
  `DRCTRL_TMAIL` varchar(16) DEFAULT NULL COMMENT 'tipo de correo laboral, personal',
  `DRCTRL_SUG` varchar(32) DEFAULT NULL COMMENT 'trato sugerido',
  `DRCTRL_NOM` varchar(128) DEFAULT NULL COMMENT 'nombre del contacto',
  `DRCTRL_PST` varchar(128) DEFAULT NULL COMMENT 'puesto',
  `DRCTRL_TEL` varchar(64) DEFAULT NULL COMMENT 'telefono de contacto',
  `DRCTRL_MAIL` varchar(64) DEFAULT NULL COMMENT 'correo electronico',
  `DRCTRL_NOTA` varchar(128) DEFAULT NULL COMMENT 'detalles del contacto',
  `DRCTRL_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `DRCTRL_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `DRCTRL_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `DRCTRL_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`DRCTRL_ID`),
  KEY `DRCTRL_DRCTR_ID` (`DRCTRL_DRCTR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: lista de contactos relacionados';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTRM
CREATE TABLE IF NOT EXISTS `DRCTRM` (
  `DRCTRM_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `DRCTRM_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `DRCTRM_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de del directorio (clientes)',
  `DRCTRM_URL` varchar(1000) DEFAULT NULL COMMENT 'direccion url amazon s3',
  `DRCTRM_NOTA` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `DRCTRM_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `DRCTRM_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  PRIMARY KEY (`DRCTRM_ID`),
  KEY `DRCTRM_DRCTR_ID` (`DRCTRM_DRCTR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: documentos relacionados al directorio';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.DRCTRR
CREATE TABLE IF NOT EXISTS `DRCTRR` (
  `DRCTRR_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con el directorio',
  `DRCTRR_CTREL_ID` smallint(6) NOT NULL COMMENT 'indice del tipo de relacion comercial',
  `DRCTRR_A` smallint(6) DEFAULT 1 COMMENT 'registro activo',
  `DRCTRR_CTCMS_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con tabla de comisiones en caso de ser una relacion con vendedor',
  `DRCTRR_CTDSC_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de descuentos en caso de ser cliente',
  `DRCTRR_CTPRC_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de precios en caso de que la relacion sea cliente',
  `DRCTRR_NOTA` varchar(100) DEFAULT NULL COMMENT 'nota de la relacion',
  `DRCTRR_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `DRCTRR_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `DRCTRR_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `DRCTRR_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`DRCTRR_DRCTR_ID`,`DRCTRR_CTREL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='directorio: relacion comercial del directorio';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.MVADP
CREATE TABLE IF NOT EXISTS `MVADP` (
  `MVADP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `MVADP_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `MVADP_DOC_ID` int(11) DEFAULT NULL COMMENT 'indice de tipos de documento (26 = remisiones)',
  `MVADP_CTALM_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de almacenes',
  `MVADP_ALMPT_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla almpt',
  `MVADP_RMSN_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de remisiones',
  `MVADP_CTEFC_ID` smallint(6) DEFAULT NULL COMMENT 'efecto del comprobante (1 = ingreso, 2 = egreso, 3 = traslado)',
  `MVADP_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el directorio, receptor (idcliente)',
  `MVADP_PDCLN_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el numero de pedido',
  `MVADP_CTDPT_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)',
  `MVADP_CTPRD_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de productos',
  `MVADP_CTMDL_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de productos',
  `MVADP_CTUND_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de unidades',
  `MVADP_UNDF` decimal(18,4) DEFAULT NULL COMMENT 'factor de unidad',
  `MVADP_UNDN` varchar(50) DEFAULT NULL COMMENT 'nombre de la unidad utilizada',
  `MVADP_CANTE` decimal(18,4) DEFAULT NULL COMMENT 'cantidad entrante',
  `MVADP_CANTS` decimal(18,4) DEFAULT NULL COMMENT 'cantidad salida, generalmente el cantidad en una remision al cliente',
  `MVADP_NOMR` varchar(128) DEFAULT NULL COMMENT 'nombre o descripcion del receptor (cliente)',
  `MVADP_PRDN` varchar(255) DEFAULT NULL COMMENT 'nombre del producto o descripcion',
  `MVADP_COM` varchar(200) DEFAULT NULL COMMENT 'nombre o descripcion del componente',
  `MVADP_UNTC` decimal(18,4) DEFAULT NULL COMMENT 'costo unitario del producto / modelo',
  `MVADP_UNDC` decimal(18,4) DEFAULT NULL COMMENT 'costo unitario por la unidad',
  `MVADP_UNTR` decimal(18,4) DEFAULT NULL COMMENT 'valor unitario por pieza',
  `MVADP_UNTR2` decimal(18,4) DEFAULT NULL COMMENT 'unitario de la unidad (valor unitario por el factor de la unidad)',
  `MVADP_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'sub total',
  `MVADP_DESC` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento aplicado al producto',
  `MVADP_IMPRT` decimal(18,4) DEFAULT NULL COMMENT 'importe = subtotal - descuento',
  `MVADP_TSIVA` decimal(18,4) DEFAULT NULL COMMENT 'tasa del iva aplicable',
  `MVADP_TRIVA` decimal(18,4) DEFAULT NULL COMMENT 'importe del impuesto traslado iva',
  `MVADP_TOTAL` decimal(18,4) DEFAULT NULL COMMENT 'total = importe + importe del iva',
  `MVADP_SKU` varchar(50) DEFAULT NULL COMMENT 'identificador: idorden + idproducto + idmodelo',
  `MVADP_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que creo el registro',
  `MVADP_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `MVADP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro',
  `MVADP_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`MVADP_ID`),
  KEY `MVADP_DOC_ID` (`MVADP_DOC_ID`),
  KEY `MVADP_CTALM_ID` (`MVADP_CTALM_ID`),
  KEY `MVADP_ALMPT_ID` (`MVADP_ALMPT_ID`),
  KEY `MVADP_RMSN_ID` (`MVADP_RMSN_ID`),
  KEY `MVADP_CTEFC_ID` (`MVADP_CTEFC_ID`),
  KEY `MVADP_DRCTR_ID` (`MVADP_DRCTR_ID`),
  KEY `MVADP_PDCLN_ID` (`MVADP_PDCLN_ID`),
  KEY `MVADP_CTDPT_ID` (`MVADP_CTDPT_ID`),
  KEY `MVADP_CTPRD_ID` (`MVADP_CTPRD_ID`),
  KEY `MVADP_CTMDL_ID` (`MVADP_CTMDL_ID`),
  KEY `MVADP_CTUND_ID` (`MVADP_CTUND_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='almacen de deparamento: movimientos del las remisiones de departamento';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.MVAPT
CREATE TABLE IF NOT EXISTS `MVAPT` (
  `MVAPT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `MVAPT_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `MVAPT_DOC_ID` int(11) DEFAULT NULL COMMENT 'indice de tipos de movimiento',
  `MVAPT_CTALM_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de almacenes',
  `MVAPT_ALMPT_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla almpt',
  `MVAPT_RMSN_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de remisiones',
  `MVAPT_CTEFC_ID` smallint(6) DEFAULT NULL COMMENT 'efecto del comprobante (1 = ingreso, 2 = egreso, 3 = traslado)',
  `MVAPT_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el directorio, receptor (idcliente)',
  `MVAPT_PDCLN_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el numero de pedido',
  `MVAPT_CTDPT_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)',
  `MVAPT_CTPRD_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de productos',
  `MVAPT_CTMDL_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de modelos',
  `MVAPT_CTPRC_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de precios',
  `MVAPT_CTESPC_ID` int(11) DEFAULT NULL,
  `MVAPT_CTUND_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de unidades',
  `MVAPT_UNDF` decimal(18,4) DEFAULT NULL COMMENT 'factor de la unidad',
  `MVAPT_UNDN` varchar(50) DEFAULT NULL COMMENT 'nombre de la unidad utilizada',
  `MVAPT_CANTE` decimal(18,4) DEFAULT NULL COMMENT 'cantidad entrante',
  `MVAPT_CANTS` decimal(18,4) DEFAULT NULL COMMENT 'cantidad salida, generalmente el cantidad en una remision al cliente',
  `MVAPT_CTCLS` varchar(128) DEFAULT NULL COMMENT 'nombre del catalogo',
  `MVAPT_PRDN` varchar(128) DEFAULT NULL,
  `MVAPT_MDLN` varchar(128) DEFAULT NULL COMMENT 'nombre del modelo',
  `MVAPT_MRC` varchar(128) DEFAULT NULL COMMENT 'nombre de la marca del producto o modelo',
  `MVAPT_ESPC` varchar(128) DEFAULT NULL COMMENT 'especificacion',
  `MVAPT_ESPN` varchar(50) DEFAULT NULL COMMENT 'nombre de la descripcion',
  `MVAPT_UNTC` decimal(18,4) DEFAULT NULL COMMENT 'costo unitario del producto / modelo',
  `MVAPT_UNDC` decimal(18,4) DEFAULT NULL COMMENT 'costo unitario por la unidad',
  `MVAPT_UNTR` decimal(18,4) DEFAULT NULL COMMENT 'valor unitario por pieza',
  `MVAPT_UNTR2` decimal(18,4) DEFAULT NULL COMMENT 'unitario de la unidad (valor unitario por el factor de la unidad)',
  `MVAPT_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'sub total',
  `MVAPT_DESC` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento aplicado al producto',
  `MVAPT_IMPRT` decimal(18,4) DEFAULT NULL COMMENT 'importe = subtotal - descuento',
  `MVAPT_TSIVA` decimal(18,4) DEFAULT NULL COMMENT 'tasa del iva aplicable',
  `MVAPT_TRIVA` decimal(18,4) DEFAULT NULL COMMENT 'importe del impuesto traslado iva',
  `MVAPT_TOTAL` decimal(18,4) DEFAULT NULL COMMENT 'total = importe + importe del iva',
  `MVAPT_SKU` varchar(20) DEFAULT NULL COMMENT 'identificador: idproducto + idmodelo + idespecificacion (tamanio)',
  `MVAPT_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que creo el registro',
  `MVAPT_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `MVAPT_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro',
  `MVAPT_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`MVAPT_ID`),
  KEY `MVAPT_CTALM_ID` (`MVAPT_CTALM_ID`),
  KEY `MVAPT_ALMPT_ID` (`MVAPT_ALMPT_ID`),
  KEY `MVAPT_RMSN_ID` (`MVAPT_RMSN_ID`),
  KEY `MVAPT_CTEFC_ID` (`MVAPT_CTEFC_ID`),
  KEY `MVAPT_DRCTR_ID` (`MVAPT_DRCTR_ID`),
  KEY `MVAPT_PDCLN_ID` (`MVAPT_PDCLN_ID`),
  KEY `MVAPT_CTDPT_ID` (`MVAPT_CTDPT_ID`),
  KEY `MVAPT_CTPRD_ID` (`MVAPT_CTPRD_ID`),
  KEY `MVAPT_CTMDL_ID` (`MVAPT_CTMDL_ID`),
  KEY `MVAPT_CTPRC_ID` (`MVAPT_CTPRC_ID`),
  KEY `MVAPT_CTESPC_ID` (`MVAPT_CTESPC_ID`),
  KEY `MVAPT_CTUND_ID` (`MVAPT_CTUND_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='almacen producto terminado: registro de movientos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.NTDSC
CREATE TABLE IF NOT EXISTS `NTDSC` (
  `NTDSC_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `NTDSC_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `NTDSC_STTS_ID` smallint(6) NOT NULL COMMENT 'indice del catalogo de status',
  `NTDSC_FOLIO` int(11) NOT NULL COMMENT 'folio de control interno',
  `NTDSC_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con el directorio',
  `NTDSC_CTMTV_ID` char(1) DEFAULT NULL COMMENT 'indice de relacion con la tabla de movitos de la nota de descuento',
  `NTDSC_NOM` varchar(255) DEFAULT NULL COMMENT 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.',
  `NTDSC_RFC` varchar(14) DEFAULT NULL COMMENT 'registro federal de contribuyentes',
  `NTDSC_CLV` varchar(10) DEFAULT NULL COMMENT 'clave del sistema',
  `NTDSC_REF` varchar(100) DEFAULT NULL COMMENT 'referencia',
  `NTDSC_CNTCT` varchar(100) DEFAULT NULL COMMENT 'nombre del contacto',
  `NTDSC_IMPORTE` decimal(18,4) DEFAULT NULL COMMENT 'importe de la nota',
  `NTDSC_OBSRV` varchar(255) DEFAULT NULL COMMENT 'observaciones',
  `NTDSC_UUID` varchar(36) DEFAULT NULL COMMENT 'id del documento',
  `NTDSC_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'sub total del comprobante',
  `NTDSC_DSCNT` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento',
  `NTDSC_TRSIVA` decimal(18,4) DEFAULT NULL COMMENT 'total del impuesto traslado iva',
  `NTDSC_GTOTAL` decimal(18,4) DEFAULT NULL COMMENT 'gran total (subtotal - descuento + traslado de iva)',
  `NTDSC_CTMTV` varchar(100) DEFAULT NULL,
  `NTDSC_USU_C` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que cancela el documento',
  `NTDSC_FCCNCL` date DEFAULT NULL COMMENT 'fecha de cancelacion',
  `NTDSC_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `NTDSC_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro y fecha de emision',
  `NTDSC_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que crea el registro',
  `NTDSC_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`NTDSC_ID`),
  KEY `NTDSC_STTS_ID` (`NTDSC_STTS_ID`),
  KEY `NTDSC_DRCTR_ID` (`NTDSC_DRCTR_ID`),
  KEY `NTDSC_CTMTV_ID` (`NTDSC_CTMTV_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='cobranza: notas de descuento';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.NTDSCP
CREATE TABLE IF NOT EXISTS `NTDSCP` (
  `NTDSCP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `NTDSCP_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `NTDSCP_DOC_ID` int(11) NOT NULL COMMENT 'indice del tipo de documento',
  `NTDSCP_ALMPT_ID` int(11) NOT NULL COMMENT 'indice de relacion del documento de almacen',
  `NTDSCP_NTDSC_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de notas de descuento (ntdsc)',
  `NTDSCP_PDCLN_ID` int(11) DEFAULT NULL COMMENT 'indice del pedido de control interno del cliente',
  `NTDSCP_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el directorio',
  `NTDSCP_TIPO` varchar(25) DEFAULT NULL COMMENT 'tipo de documento devolucion',
  `NTDSCP_FOLIO` int(11) DEFAULT NULL COMMENT 'folio de control interno',
  `NTDSCP_SERIE` varchar(10) DEFAULT NULL COMMENT 'serie',
  `NTDSCP_CLV` varchar(10) DEFAULT NULL COMMENT 'clave de sistema',
  `NTDSCP_NOM` varchar(255) DEFAULT NULL COMMENT 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.',
  `NTDSCP_RFC` varchar(14) DEFAULT NULL COMMENT 'registro federal de contribuyentes',
  `NTDSCP_FECEMS` date DEFAULT NULL COMMENT 'fecha de emision del comprobante',
  `NTDSCP_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'subtotal del comprobante',
  `NTDSCP_DSCNT` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento',
  `NTDSCP_TRSIVA` decimal(18,4) DEFAULT NULL COMMENT 'total del impuesto trasladado iva',
  `NTDSCP_GTOTAL` decimal(18,4) DEFAULT NULL COMMENT 'gran total (subtotal - descuento + traslado de iva)',
  `NTDSCP_UUID` varchar(36) DEFAULT NULL COMMENT 'id de documento',
  `NTDSCP_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que crea el registro',
  `NTDSCP_FN` timestamp NULL DEFAULT NULL COMMENT 'fecha de creacion del registro',
  `NTDSCP_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `NTDSCP_FM` date DEFAULT NULL COMMENT 'fecha de la ultima modificacion del registro',
  PRIMARY KEY (`NTDSCP_ID`) USING BTREE,
  KEY `NTDSCP_DOC_ID` (`NTDSCP_DOC_ID`) USING BTREE,
  KEY `NTDSCP_ALMPT_ID` (`NTDSCP_ALMPT_ID`) USING BTREE,
  KEY `NTDSCP_NTDSC_ID` (`NTDSCP_NTDSC_ID`) USING BTREE,
  KEY `NTDSCP_PDCLN_ID` (`NTDSCP_PDCLN_ID`) USING BTREE,
  KEY `NTDSCP_DRCTR_ID` (`NTDSCP_DRCTR_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='nota de descuento: relacion de documentos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.NTDSCS
CREATE TABLE IF NOT EXISTS `NTDSCS` (
  `NTDSCS_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `NTDSCS_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `NTDSCS_NTDSC_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con tabla de notas de descuento',
  `NTDSCS_NTDSC_STTS_ID` int(11) DEFAULT NULL COMMENT 'indice del status anterior',
  `NTDSCS_NTDSC_STTSA_ID` int(11) DEFAULT NULL COMMENT 'indice del status autorizado',
  `NTDSCS_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla del directorio',
  `NTDSCS_CVMTV` varchar(100) DEFAULT NULL COMMENT 'clave del motivo del cambio de status',
  `NTDSCS_NOTA` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `NTDSCS_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que autoriza el cambio',
  `NTDSCS_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  PRIMARY KEY (`NTDSCS_ID`),
  KEY `NTDSCS_NTDSC_ID` (`NTDSCS_NTDSC_ID`),
  KEY `NTDSCS_NTDSC_STTS_ID` (`NTDSCS_NTDSC_STTS_ID`),
  KEY `NTDSCS_NTDSC_STTSA_ID` (`NTDSCS_NTDSC_STTSA_ID`),
  KEY `NTDSCS_DRCTR_ID` (`NTDSCS_DRCTR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='cobranza: tabla de registro de cambios de status para notas de descuento';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.PDCLM
CREATE TABLE IF NOT EXISTS `PDCLM` (
  `PDCLM_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `PDCLM_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `PDCLM_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla del directorio (clientes)',
  `PDCLM_PDCLN_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de pedidos',
  `PDCLM_URL` varchar(1000) DEFAULT NULL COMMENT 'direccion url amazon s3',
  `PDCLM_NOTA` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `PDCLM_FN` timestamp NULL DEFAULT NULL COMMENT 'fecha de creacion del registro',
  `PDCLM_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  PRIMARY KEY (`PDCLM_ID`),
  KEY `PDCLM_DRCTR_ID` (`PDCLM_DRCTR_ID`),
  KEY `PDCLM_PDCLN_ID` (`PDCLM_PDCLN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='pedidos, tienda web: tabla de documentos relacionados a la tabla de pedidos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.PDCLN
CREATE TABLE IF NOT EXISTS `PDCLN` (
  `PDCLN_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice dela tabla (numero de pedido)',
  `PDCLN_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `PDCLN_STTS_ID` smallint(6) DEFAULT NULL COMMENT 'status del pedido',
  `PDCLN_ORGN_ID` smallint(6) DEFAULT NULL COMMENT 'origen del pedido (local = 0, vendedor = 1, tienda web = 2)',
  `PDCLN_FRMPG_ID` int(11) DEFAULT NULL COMMENT 'forma de pago (deposito = 1, mercadopago = 2, paypal = 3)',
  `PDCLN_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla del directorio (clientes)',
  `PDCLN_DRCCN_ID` int(11) DEFAULT NULL COMMENT 'indice de la direccion del envio (tabla de direcciones)',
  `PDCLN_VNDR_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de vendedores',
  `PDCLN_RQIRFCTR` smallint(6) DEFAULT NULL COMMENT 'requiere factura = 1',
  `PDCLN_ENVCST` decimal(18,4) DEFAULT NULL COMMENT 'importe del costo de envio',
  `PDCLN_AUT_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de autorizaciones',
  `PDCLN_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'subtotal',
  `PDCLN_DSCNT` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento',
  `PDCLN_IVA` decimal(18,4) DEFAULT NULL COMMENT 'importe del iva al 16',
  `PDCLN_TTL` decimal(18,4) DEFAULT NULL COMMENT 'gran total',
  `PDCLN_MTDENV` varchar(60) DEFAULT NULL COMMENT 'metodo de envio',
  `PDCLN_RCB` varchar(100) DEFAULT NULL COMMENT 'nombre de la persona que recibe',
  `PDCLN_OBSRV` varchar(255) DEFAULT NULL COMMENT 'observaciones',
  `PDCLN_FCPED` timestamp NULL DEFAULT current_timestamp(),
  `PDCLN_FCREQ` timestamp NULL DEFAULT NULL COMMENT 'fecha requerida para el pedido',
  `PDCLN_FCHAUT` timestamp NULL DEFAULT NULL COMMENT 'fecha de autorizacion',
  `PDCLN_FCENTR` timestamp NULL DEFAULT NULL COMMENT 'fecha de entrega al cliente',
  `PDCLN_URL_PDF` varchar(1000) DEFAULT NULL COMMENT 'url para archivo adjunto',
  `PDCLN_SYNC_ID` int(11) DEFAULT NULL,
  `PDCLN_USU_AUT` varchar(10) DEFAULT NULL,
  `PDCLN_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `PDCLN_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `PDCLN_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro',
  `PDCLN_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `PDCLN_ANIO` smallint(6) DEFAULT year(`PDCLN_FCPED`) COMMENT 'ejercicio',
  `PDCLN_MES` smallint(6) DEFAULT month(`PDCLN_FCPED`) COMMENT 'periodo',
  `PDCLN_MTVCL` varchar(50) DEFAULT NULL,
  `PDCLN_FCCNCL` timestamp NULL DEFAULT NULL COMMENT 'fecha de cancelacion del pedido',
  `PDCLN_USU_CNCL` varchar(10) DEFAULT NULL,
  `PDCLN_CLNTA` varchar(100) DEFAULT NULL,
  `PDCLN_CLMTV` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`PDCLN_ID`),
  KEY `PDCLN_STTS_ID` (`PDCLN_STTS_ID`),
  KEY `PDCLN_DRCTR_ID` (`PDCLN_DRCTR_ID`),
  KEY `PDCLN_DRCCN_ID` (`PDCLN_DRCCN_ID`),
  KEY `PDCLN_VNDR_ID` (`PDCLN_VNDR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='pedidos de cliente';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.PDCLP
CREATE TABLE IF NOT EXISTS `PDCLP` (
  `PDCLP_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `PDCLP_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `PDCLP_DOC_ID` int(11) DEFAULT NULL COMMENT 'indice del tipo de documento',
  `PDCLP_PDCLN_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de pedidos',
  `PDCLP_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con el directorio, receptor (idcliente)',
  `PDCLP_CTDPT_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)',
  `PDCLP_CTALM_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de almacenes',
  `PDCLP_CTPRCP_ID` int(11) DEFAULT NULL,
  `PDCLP_CTPRD_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de productos',
  `PDCLP_CTMDL_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de modelos',
  `PDCLP_CTPRC_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de precios',
  `PDCLP_CTESPC_ID` int(11) DEFAULT NULL,
  `PDCLP_CTUND_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de unidades',
  `PDCLP_UNDF` decimal(18,4) DEFAULT NULL,
  `PDCLP_UNDN` varchar(50) DEFAULT NULL,
  `PDCLP_CNTD` decimal(18,4) DEFAULT NULL COMMENT 'cantidad salida, generalmente el cantidad del pedido al cliente',
  `PDCLP_CTCLS` varchar(128) DEFAULT NULL COMMENT 'nombre del catalogo',
  `PDCLP_PRDN` varchar(128) DEFAULT NULL,
  `PDCLP_MDLN` varchar(128) DEFAULT NULL COMMENT 'nombre del modelo',
  `PDCLP_MRC` varchar(128) DEFAULT NULL COMMENT 'nombre de la marca del producto o modelo',
  `PDCLP_ESPC` varchar(128) DEFAULT NULL COMMENT 'especificacion',
  `PDCLP_ESPN` varchar(50) DEFAULT NULL COMMENT 'nombre de la descripcion',
  `PDCLP_UNTC` decimal(18,4) DEFAULT NULL COMMENT 'costo unitario por la unidad',
  `PDCLP_UNDC` decimal(18,4) DEFAULT NULL COMMENT 'costo unitario por la unidad',
  `PDCLP_UNTR` decimal(18,4) DEFAULT NULL COMMENT 'valor unitario por pieza',
  `PDCLP_UNTR2` decimal(18,4) DEFAULT NULL COMMENT 'unitario de la unidad (valor unitario por el factor de la unidad)',
  `PDCLP_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'subtotal',
  `PDCLP_DESC` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento aplicado al producto',
  `PDCLP_IMPRT` decimal(18,4) DEFAULT NULL COMMENT 'importe = subtotal - descuento',
  `PDCLP_TSIVA` decimal(18,4) DEFAULT NULL COMMENT 'tasa del iva aplicable',
  `PDCLP_TRIVA` decimal(18,4) DEFAULT NULL COMMENT 'importe del impuesto traslado iva',
  `PDCLP_TOTAL` decimal(18,4) DEFAULT NULL COMMENT 'total = importe + importe del iva',
  `PDCLP_SKU` varchar(20) DEFAULT NULL COMMENT 'identificador: idproducto + idmodelo + idespecificacion (tamanio)',
  `PDCLP_NOTA` varchar(50) DEFAULT NULL COMMENT 'observaciones',
  `PDCLP_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que creo el registro',
  `PDCLP_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `PDCLP_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del utlimo usuario que modifica el registro',
  `PDCLP_FM` date DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`PDCLP_ID`),
  KEY `PDCLP_PDCLN_ID` (`PDCLP_PDCLN_ID`),
  KEY `PDCLP_DRCTR_ID` (`PDCLP_DRCTR_ID`),
  KEY `PDCLP_CTDPT_ID` (`PDCLP_CTDPT_ID`),
  KEY `PDCLP_CTALM_ID` (`PDCLP_CTALM_ID`),
  KEY `PDCLP_CTPRCP_ID` (`PDCLP_CTPRCP_ID`),
  KEY `PDCLP_CTPRD_ID` (`PDCLP_CTPRD_ID`),
  KEY `PDCLP_CTMDL_ID` (`PDCLP_CTMDL_ID`),
  KEY `PDCLP_CTPRC_ID` (`PDCLP_CTPRC_ID`),
  KEY `PDCLP_CTESPC_ID` (`PDCLP_CTESPC_ID`),
  KEY `PDCLP_CTUND_ID` (`PDCLP_CTUND_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='pedido del cliente: partidas';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.PDCLR
CREATE TABLE IF NOT EXISTS `PDCLR` (
  `PDCLR_PDCLN_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de pedidos',
  `PDCLR_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion del directorio',
  `PDCLR_CTREL_ID` int(11) NOT NULL COMMENT 'indice de la tabla de tipos de relacion',
  `PDCLR_RELN` varchar(60) DEFAULT NULL COMMENT 'descripcion de la relacion del comprobante',
  `PDCLR_FOLIO` int(11) DEFAULT NULL COMMENT 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',
  `PDCLR_NOMR` varchar(255) DEFAULT NULL COMMENT 'nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razon social del contribuyente, inscrito en el rfc, del receptor del comprobante.',
  `PDCLR_FECEMS` date DEFAULT NULL COMMENT 'fecha y hora de expedicion del comprobante.',
  `PDCLR_TOTAL` decimal(18,4) DEFAULT NULL COMMENT 'importe total del comprobante',
  `PDCLR_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `PDCLR_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  PRIMARY KEY (`PDCLR_PDCLN_ID`,`PDCLR_DRCTR_ID`,`PDCLR_CTREL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='pedidos de cliente, relacion entre pedidos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.PDCLS
CREATE TABLE IF NOT EXISTS `PDCLS` (
  `PDCLS_PDCLN_ID` int(11) NOT NULL COMMENT 'indice del pedido asociado',
  `PDCLS_STTS_ID` smallint(6) NOT NULL COMMENT 'indice del status',
  `PDCLS_CLMTV_ID` smallint(6) NOT NULL COMMENT 'indice de la clave de motivo de cambio de status',
  `PDCLS_DRCTR_ID` smallint(6) NOT NULL COMMENT 'indice del directorio',
  `PDCLS_CLMTV` varchar(80) DEFAULT NULL COMMENT 'descripcion de la clave de motivo de cancelacion',
  `PDCLS_OBSRV` varchar(255) DEFAULT NULL COMMENT 'observaciones',
  `PDCLS_TTL` decimal(18,4) DEFAULT NULL COMMENT 'total del pedido',
  `PDCLS_URL_PDF` varchar(1000) DEFAULT NULL COMMENT 'url del archivo relacionado al pedido',
  `PDCLS_USU_M` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que modifica el registro',
  `PDCLS_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  PRIMARY KEY (`PDCLS_PDCLN_ID`,`PDCLS_STTS_ID`,`PDCLS_CLMTV_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='pedidos cliente, registro de cambios de status';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RLSUSR
CREATE TABLE IF NOT EXISTS `RLSUSR` (
  `RLSUSR_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice',
  `RLSUSR_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `RLSUSR_M` smallint(6) NOT NULL COMMENT 'marca del usuario maestro',
  `RLSUSR_CLV` varchar(100) DEFAULT NULL COMMENT 'clave de rol',
  `RLSUSR_NMBR` varchar(100) DEFAULT NULL COMMENT 'nombre o descripcion del rol',
  `RLSUSR_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `RLSUSR_USR_N` varchar(50) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `RLSUSR_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `RLSUSR_USR_M` varchar(50) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`RLSUSR_ID`) USING BTREE,
  KEY `RLSUSR_ID` (`RLSUSR_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='usuarios: roles de usuario';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RLUSRL
CREATE TABLE IF NOT EXISTS `RLUSRL` (
  `RLUSRL_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `RLUSRL_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `RLUSRL_USR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion del usuario',
  `RLUSRL_RLSUSR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion del rol',
  PRIMARY KEY (`RLUSRL_ID`) USING BTREE,
  KEY `RLUSRL_ID` (`RLUSRL_ID`) USING BTREE,
  KEY `RLUSRL_USR_ID` (`RLUSRL_USR_ID`) USING BTREE,
  KEY `RLUSRL_RLSUSR_ID` (`RLUSRL_RLSUSR_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='usuarios: relaciones de roles de usuario';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSDP
CREATE TABLE IF NOT EXISTS `RMSDP` (
  `RMSDP_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `RMSDP_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `RMSDP_CTDEP_ID` int(11) DEFAULT NULL,
  `RMSDP_CTDOC_ID` smallint(6) NOT NULL COMMENT 'indice del catalogo de documentos (generalmente para la remision a cliente es 26)',
  `RMSDP_DECI` smallint(6) NOT NULL COMMENT 'numero de decimales',
  `RMSDP_STTS_ID` int(11) DEFAULT NULL COMMENT 'indice del status de la remision',
  `RMSDP_CTSR_ID` int(11) DEFAULT NULL,
  `RMSDP_CTCLS_ID` int(11) DEFAULT NULL,
  `RMSDP_CTENV_ID` int(11) DEFAULT NULL,
  `RMSDP_DRCTR_ID` int(11) DEFAULT NULL,
  `RMSDP_DRCCN_ID` int(11) DEFAULT NULL,
  `RMSDP_VNDDR_ID` int(11) DEFAULT NULL,
  `RMSDP_GUIA_ID` int(11) DEFAULT NULL,
  `RMSDP_CTREL_ID` int(11) DEFAULT NULL,
  `RMSDP_VER` varchar(3) DEFAULT NULL,
  `RMSDP_FOLIO` int(11) DEFAULT NULL,
  `RMSDP_SERIE` varchar(10) DEFAULT NULL,
  `RMSDP_RFCE` varchar(14) DEFAULT NULL,
  `RMSDP_CLVR` varchar(10) DEFAULT NULL,
  `RMSDP_RFCR` varchar(14) DEFAULT NULL,
  `RMSDP_NOMR` varchar(255) DEFAULT NULL,
  `RMSDP_CNTCT` varchar(255) DEFAULT NULL,
  `RMSDP_FECEMS` timestamp NULL DEFAULT NULL,
  `RMSDP_FECENT` timestamp NULL DEFAULT NULL,
  `RMSDP_TPCMB` decimal(18,4) DEFAULT NULL,
  `RMSDP_SBTTL` decimal(18,4) DEFAULT NULL,
  `RMSDP_DSCNT` decimal(18,4) DEFAULT NULL,
  `RMSDP_TRSIVA` decimal(18,4) DEFAULT NULL,
  `RMSDP_GTOTAL` decimal(18,4) DEFAULT NULL,
  `RMSDP_MONEDA` varchar(3) DEFAULT NULL,
  `RMSDP_DRCCN` varchar(400) DEFAULT NULL,
  `RMSDP_NOTA` varchar(255) DEFAULT NULL,
  `RMSDP_VNDDR` varchar(10) DEFAULT NULL,
  `RMSDP_UUID` varchar(36) DEFAULT NULL,
  `RMSDP_FCCNCL` date DEFAULT NULL,
  `RMSDP_USR_C` varchar(10) DEFAULT NULL,
  `RMSDP_URL_PDF` varchar(100) DEFAULT NULL,
  `RMSDP_FN` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RMSDP_USR_N` varchar(10) DEFAULT NULL,
  `RMSDP_FM` timestamp NULL DEFAULT NULL,
  `RMSDP_USR_M` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado de productos entre departamentos';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSN
CREATE TABLE IF NOT EXISTS `RMSN` (
  `RMSN_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `RMSN_A` smallint(6) NOT NULL COMMENT 'registro activo (por eliminar)',
  `RMSN_CTDOC_ID` smallint(6) NOT NULL COMMENT 'indice del catalogo de documentos (generalmente para la remision a cliente es 26)',
  `RMSN_DECI` smallint(6) NOT NULL COMMENT 'numero de decimales',
  `RMSN_VER` varchar(3) DEFAULT NULL COMMENT 'version del documento',
  `RMSN_FOLIO` int(11) DEFAULT NULL COMMENT 'folio de control interno',
  `RMSN_CTSR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de series y folios',
  `RMSN_SERIE` varchar(10) DEFAULT NULL COMMENT 'serie de control interno del documento en modo texto',
  `RMSN_STTS_ID` int(11) DEFAULT NULL COMMENT 'indice del status del documento',
  `RMSN_PDD_ID` int(11) DEFAULT NULL COMMENT 'indice o numero de pedido',
  `RMSN_CTCLS_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de productos',
  `RMSN_CTENV_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de metodo de envio',
  `RMSN_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice del directorio de clientes',
  `RMSN_DRCCN_ID` int(11) DEFAULT NULL COMMENT 'indice del domicilio ',
  `RMSN_VNDDR_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de vendedores',
  `RMSN_GUIA_ID` int(11) DEFAULT NULL COMMENT 'numero de guia o referencia del metodo de envio',
  `RMSN_CTREL_ID` int(11) DEFAULT NULL COMMENT 'indice o clave de relacion con otros comprobantes',
  `RMSN_RFCE` varchar(14) DEFAULT NULL COMMENT 'registro federal de contribuyentes (emisor)',
  `RMSN_RFCR` varchar(14) DEFAULT NULL COMMENT 'registro federal de contribuyentes (receptor)',
  `RMSN_NOMR` varchar(255) DEFAULT NULL COMMENT 'nombre o razon social del receptor',
  `RMSN_CLVR` varchar(10) DEFAULT NULL COMMENT 'clave de control interno del cliente',
  `RMSN_CNTCT` varchar(255) DEFAULT NULL COMMENT 'nombre del contacto',
  `RMSN_UUID` varchar(36) DEFAULT NULL COMMENT 'id del documento',
  `RMSN_FECEMS` timestamp NULL DEFAULT NULL COMMENT 'fecha de emision del comprante',
  `RMSN_FECENT` timestamp NULL DEFAULT NULL COMMENT 'fecha de entrega',
  `RMSN_FECUPC` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de pago del comprobante',
  `RMSN_FECCBR` timestamp NULL DEFAULT NULL COMMENT 'fecha de recepcion de cobranza',
  `RMSN_FCCNCL` date DEFAULT NULL COMMENT 'fecha de cancelacion del comprobante',
  `RMSN_FECVNC` date DEFAULT NULL COMMENT 'fecha de vencimiento del pagare',
  `RMSN_TPCMB` decimal(18,4) DEFAULT NULL COMMENT 'tipo de cambio',
  `RMSN_SBTTL` decimal(18,4) DEFAULT NULL COMMENT 'sub total del comprobante',
  `RMSN_DSCNT` decimal(18,4) DEFAULT NULL COMMENT 'importe del descuento',
  `RMSN_TRSIVA` decimal(18,4) DEFAULT NULL COMMENT 'total del impuesto  traslado iva',
  `RMSN_GTOTAL` decimal(18,4) DEFAULT NULL COMMENT 'gran total (subtotal - descuento + traslado de iva)',
  `RMSN_FACIVA` decimal(18,4) DEFAULT NULL COMMENT 'factor del iva pactado con el cliente',
  `RMSN_IVAPAC` decimal(18,4) DEFAULT NULL COMMENT 'importe del iva pactado (subtotal * ivapac)',
  `RMSN_TOTAL` decimal(18,4) DEFAULT NULL COMMENT 'total del comprobante',
  `RMSN_FACPAC` decimal(18,4) DEFAULT NULL COMMENT 'factor pactado',
  `RMSN_XCBPAC` decimal(18,4) DEFAULT NULL COMMENT 'por cobrar pactado',
  `RMSN_DESC` varchar(100) DEFAULT NULL COMMENT 'descripcion del tipo de descuento aplicado',
  `RMSN_FACDES` decimal(18,4) DEFAULT NULL COMMENT 'descripcion del tipo de descuento aplicado',
  `RMSN_DESCT` decimal(18,4) DEFAULT NULL COMMENT 'importe total del descuento (total x facdes)',
  `RMSN_XCBRR` decimal(18,4) DEFAULT NULL COMMENT 'importe por cobrar del comprobante',
  `RMSN_CBRD` decimal(18,4) DEFAULT NULL COMMENT 'acumulado de la cobranza del comprobante',
  `RMSN_MONEDA` varchar(3) DEFAULT NULL COMMENT 'clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra mxn. conforme con la especificacion iso 4217.',
  `RMSN_MTDPG` varchar(3) DEFAULT NULL COMMENT 'clave del metodo de pago que aplica para este comprobante fiscal digital por internet, conforme al articulo 29-a fraccion vii incisos a y b del cff.',
  `RMSN_FRMPG` varchar(2) DEFAULT NULL COMMENT 'clave de la forma de pago de los bienes o servicios amparados por el comprobante, si no se conoce la forma de pago este atributo se debe omitir.',
  `RMSN_NOTA` varchar(255) DEFAULT NULL COMMENT 'observaciones',
  `RMSN_VNDDR` varchar(10) DEFAULT NULL COMMENT 'clave del vendedor asociada',
  `RMSN_USR_C` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que cancela el comprobante',
  `RMSN_CVCAN` smallint(6) DEFAULT NULL COMMENT 'clave de cancelacion',
  `RMSN_CLMTV` varchar(100) DEFAULT NULL COMMENT 'clave de cancelacion en modo texto {00:descripcion}',
  `RMSN_CLNTA` varchar(100) DEFAULT NULL COMMENT 'nota de la cancelacion',
  `RMSN_DRCCN` varchar(400) DEFAULT NULL COMMENT 'direccion del envio en modo texto',
  `RMSN_CNDNS` varchar(30) DEFAULT NULL,
  `RMSN_URL_PDF` varchar(100) DEFAULT NULL COMMENT '/remision/{0:36}.pdf',
  `RMSN_FN` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `RMSN_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que crea el registro',
  `RMSN_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `RMSN_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica',
  `RMSN_MES` smallint(6) DEFAULT NULL,
  `RMSN_ANIO` smallint(6) DEFAULT NULL,
  `RMSN_SALDO` decimal(18,4) GENERATED ALWAYS AS (`RMSN_XCBRR` - `RMSN_CBRD`) VIRTUAL,
  PRIMARY KEY (`RMSN_ID`),
  KEY `RMSN_CTDOC_ID` (`RMSN_CTDOC_ID`),
  KEY `RMSN_CTSR_ID` (`RMSN_CTSR_ID`),
  KEY `RMSN_STTS_ID` (`RMSN_STTS_ID`),
  KEY `RMSN_PDD_ID` (`RMSN_PDD_ID`),
  KEY `RMSN_CTCLS_ID` (`RMSN_CTCLS_ID`),
  KEY `RMSN_CTENV_ID` (`RMSN_CTENV_ID`),
  KEY `RMSN_DRCTR_ID` (`RMSN_DRCTR_ID`),
  KEY `RMSN_DRCCN_ID` (`RMSN_DRCCN_ID`),
  KEY `RMSN_VNDDR_ID` (`RMSN_VNDDR_ID`),
  KEY `RMSN_GUIA_ID` (`RMSN_GUIA_ID`),
  KEY `RMSN_CTREL_ID` (`RMSN_CTREL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado al cliente: remisiones del almacen de producto terminado, remision a cliente';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSNA
CREATE TABLE IF NOT EXISTS `RMSNA` (
  `RMSNA_RMSN_ID` int(11) NOT NULL COMMENT 'indice de la tabla',
  `RMSNA_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice del directorio',
  `RMSNA_FECUPC` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de pago del comprobante',
  `RMSNA_FACPAC` decimal(18,4) DEFAULT NULL COMMENT 'factor pactado',
  `RMSNA_XCBPAC` decimal(18,4) DEFAULT NULL COMMENT 'por cobrar pactado',
  `RMSNA_DESC` varchar(100) DEFAULT NULL COMMENT 'descripcion del tipo de descuento aplciado',
  `RMSNA_FACDES` decimal(18,4) DEFAULT NULL COMMENT 'factor del descuento aplciado',
  `RMSNA_DESCT` decimal(18,4) DEFAULT NULL COMMENT 'importe total del descuento (total x facdes)',
  `RMSNA_XCBRR` decimal(18,4) DEFAULT NULL COMMENT 'importe por cobrar del comprobante',
  `RMSNA_CBRD` decimal(18,4) DEFAULT NULL COMMENT 'acumulado de la cobranza del comprobante',
  `RMSNA_NOTA` varchar(255) DEFAULT NULL COMMENT 'observaciones',
  `RMSNA_FM` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'ultima fecha de modificacion del registro',
  `RMSNA_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica',
  KEY `RMSNA_RMSN_ID` (`RMSNA_RMSN_ID`),
  KEY `RMSNA_DRCTR_ID` (`RMSNA_DRCTR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado al cliente: remisiones del almacen de producto terminado, remision a cliente';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSNC
CREATE TABLE IF NOT EXISTS `RMSNC` (
  `RMSNC_RMSN_ID` int(11) NOT NULL COMMENT 'indice de la tabla de remision',
  `RMSNC_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `RMSNC_CTCMS_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de comisiones',
  `RMSNC_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice del directorio',
  `RMSNC_VNDR_ID` int(11) DEFAULT NULL COMMENT 'indice de la tabla de vendedores',
  `RMSNC_CTDIS_ID` int(11) DEFAULT NULL COMMENT '0 < --no esta agregada a ningun recibo\r\n1 < --la comsion autorizada y esta agregada a un recibo pero sin aplicar\r\n2 < --comision pagada y ya esta autorizada en un recibo, es decir ya esta pagada la comision',
  `RMSNC_DESC` varchar(150) DEFAULT NULL COMMENT 'descripcion',
  `RMSNC_FCPAC` decimal(18,4) DEFAULT NULL COMMENT 'factor pactado',
  `RMSNC_FCACT` decimal(18,4) DEFAULT NULL COMMENT 'factor actualizado',
  `RMSNC_IMPR` decimal(18,4) DEFAULT NULL COMMENT 'importe calculado de la comision',
  `RMSNC_ACUML` decimal(18,4) DEFAULT NULL COMMENT 'importe pagado o acumulado',
  `RMSNC_VNDR` varchar(10) DEFAULT NULL COMMENT 'clave del vendedor',
  `RMSNC_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ultima fecha de moficacion del registro',
  `RMSNC_USR_N` varchar(10) DEFAULT NULL COMMENT 'ultima clave del usuario que mofica el registro',
  KEY `RMSNC_RMSN_ID` (`RMSNC_RMSN_ID`),
  KEY `RMSNC_CTCMS_ID` (`RMSNC_CTCMS_ID`),
  KEY `RMSNC_DRCTR_ID` (`RMSNC_DRCTR_ID`),
  KEY `RMSNC_VNDR_ID` (`RMSNC_VNDR_ID`),
  KEY `RMSNC_CTDIS_ID` (`RMSNC_CTDIS_ID`),
  KEY `RMSNC_A` (`RMSNC_A`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado al cliente: comisiones a vendedores\r\n';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSND
CREATE TABLE IF NOT EXISTS `RMSND` (
  `RMSND_RMSN_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice de la tabla de remisiones',
  `RMSND_A` smallint(6) DEFAULT 1 COMMENT 'registro activo',
  `RMSND_DESC` varchar(60) DEFAULT NULL COMMENT 'descripcion del descuento',
  `RMSND_FCTR` decimal(18,4) DEFAULT 0.0000 COMMENT 'factor o porcentaje del descuento',
  `RMSND_SBTTL` decimal(18,4) DEFAULT 0.0000 COMMENT 'importe del descuento',
  `RMSND_USR_N` varchar(10) DEFAULT NULL,
  `RMSND_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`RMSND_RMSN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado al cliente: tabla de descuentos ';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSNG
CREATE TABLE IF NOT EXISTS `RMSNG` (
  `RMSNG_RMSN_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de remisiones (rmsn_id)',
  `RMSNG_CTPRV_ID` int(11) DEFAULT NULL COMMENT 'indice del proveedor de envio',
  `RMSNG_CTPRV` varchar(128) DEFAULT NULL COMMENT 'nombre o descripcion del proveedor',
  `RMSNG_CTENV_ID` int(11) DEFAULT NULL COMMENT 'indice de la clave de tipo de envio ocurre, flete, recoleccion, flete por cobrar',
  `RMSNG_GUIA_ID` varchar(128) DEFAULT NULL COMMENT 'numero de guia o referencia del proveedor para rastreo',
  `RMSNG_ESPC` varchar(255) DEFAULT NULL COMMENT 'especificaciones del paquete',
  `RMSNG_FECENT` date DEFAULT NULL COMMENT 'fecha de entrega',
  `RMSNG_FECREP` date DEFAULT NULL COMMENT 'fecha de recepcion',
  `RMSNG_NOTA` varchar(255) DEFAULT NULL,
  `RMSNG_URL_PDF` varchar(255) DEFAULT NULL,
  `RMSNG_FM` date DEFAULT current_timestamp(),
  `RMSNG_USR_M` varchar(50) DEFAULT NULL COMMENT 'fecha de actualizacion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado: información de la guia de envio al cliente';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSNR
CREATE TABLE IF NOT EXISTS `RMSNR` (
  `RMSNR_RMSN_ID` int(11) NOT NULL COMMENT 'indice de relacion con la tabla de remisiones',
  `RMSNR_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion del directorio',
  `RMSNR_CTREL_ID` int(11) NOT NULL COMMENT 'indice de la tabla de tipos de relacion',
  `RMSNR_RELN` varchar(60) DEFAULT NULL COMMENT 'descripcion de la relacion del comprobante',
  `RMSNR_UUID` varchar(36) DEFAULT NULL,
  `RMSNR_SERIE` varchar(25) DEFAULT NULL COMMENT 'serie para control interno del contribuyente. este atributo acepta una cadena de caracteres',
  `RMSNR_FOLIO` int(11) DEFAULT NULL COMMENT 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',
  `RMSNR_NOMR` varchar(255) DEFAULT NULL COMMENT 'nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razon social del contribuyente, inscrito en el rfc, del receptor del comprobante.',
  `RMSNR_FECEMS` date DEFAULT NULL COMMENT 'fecha y hora de expedicion del comprobante.',
  `RMSNR_TOTAL` decimal(18,4) DEFAULT NULL COMMENT 'importe total del comprobante',
  `RMSNR_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `RMSNR_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  KEY `RMSNR_RMSN_ID` (`RMSNR_RMSN_ID`),
  KEY `RMSNR_DRCTR_ID` (`RMSNR_DRCTR_ID`),
  KEY `RMSNR_CTREL_ID` (`RMSNR_CTREL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='comprobantes relacionados remisiones';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.RMSNS
CREATE TABLE IF NOT EXISTS `RMSNS` (
  `RMSNS_ID` int(11) NOT NULL COMMENT 'indice incremental',
  `RMSNS_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `RMSNS_RMSN_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla de remisiones',
  `RMSNS_RMSN_STTS_ID` int(11) DEFAULT NULL COMMENT 'indice del status anterior',
  `RMSNS_RMSN_STTSA_ID` int(11) DEFAULT NULL COMMENT 'indice del status autorizado',
  `RMSNS_DRCTR_ID` int(11) DEFAULT NULL COMMENT 'indice de relacion con la tabla del directorio',
  `RMSNS_CVMTV` varchar(100) DEFAULT NULL COMMENT 'clave del motivo del cambio de status',
  `RMSNS_NOTA` varchar(100) DEFAULT NULL COMMENT 'observaciones',
  `RMSNS_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario que autoriza el cambio',
  `RMSNS_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='remisionado al cliente: tabla de registro de cambios de status';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TVDR
CREATE TABLE IF NOT EXISTS `TVDR` (
  `TVDR_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `TVDR_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `TVDR_DRCTR_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice del directorio',
  `TVDR_CTCMS_ID` int(11) NOT NULL DEFAULT 0 COMMENT 'indice del catalogo de comisiones',
  `TVDR_RLSUSR_ID` int(11) NOT NULL DEFAULT -1 COMMENT 'indice de la tabla de roles de usuario',
  `TNDR_CLV` varchar(10) DEFAULT NULL COMMENT 'clave de control interno',
  `TVDR_NOM` varchar(255) DEFAULT NULL COMMENT 'nombre completo del vendedor',
  `TVDR_EMAIL` varchar(255) DEFAULT NULL COMMENT 'correo electronico',
  `TVDR_PSW` varchar(255) DEFAULT NULL COMMENT 'password',
  `TVDR_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del usuario',
  `TVDR_FM` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ultima fecha de modificacion',
  PRIMARY KEY (`TVDR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='vendedores: tabla de vendedores';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TWBNR
CREATE TABLE IF NOT EXISTS `TWBNR` (
  `TWBNR_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `TWBNR_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `TWBNR_CTMDL_ID` int(11) DEFAULT NULL COMMENT 'indice del catalogo de modelos',
  `TWBNR_URL_IMG` varchar(255) DEFAULT NULL COMMENT 'url de la ubicacion de la imagen',
  `TWBNR_TXTE` varchar(255) DEFAULT NULL COMMENT 'texto adicional',
  PRIMARY KEY (`TWBNR_ID`),
  KEY `TWBNR_CTMDL_ID` (`TWBNR_CTMDL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tienda: imagenes para el carrusel';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TWCPN
CREATE TABLE IF NOT EXISTS `TWCPN` (
  `TWCPN_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `TWCPN_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `TWCPN_DESC` varchar(255) DEFAULT NULL COMMENT 'descripcion',
  `TWCPN_CLV` varchar(16) DEFAULT NULL COMMENT 'clave del cupon',
  `TWCPN_NOM` varchar(32) DEFAULT NULL COMMENT 'nombre del cupon',
  `TWCPN_VGINI` date DEFAULT NULL COMMENT 'fecha de inicio de vigencia',
  `TWCPN_VGTER` date DEFAULT NULL COMMENT 'fecha termino de vigencia',
  `TWCPN_DESCNT` decimal(14,4) DEFAULT NULL COMMENT 'descuento del cupon',
  `TWCPN_UMAX` smallint(6) DEFAULT NULL COMMENT 'usus maximos',
  `TWCPN_UACT` smallint(6) DEFAULT NULL COMMENT 'minimo de compra',
  `TWCPN_MIN` decimal(14,4) DEFAULT NULL COMMENT 'minimo de compra',
  `TWCPN_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  `TWCPN_USR_N` varchar(255) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `TWCPN_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `TWCPN_USR_M` varchar(255) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`TWCPN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tienda: cupones de descuento';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TWFAQ
CREATE TABLE IF NOT EXISTS `TWFAQ` (
  `TWFAQ_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `TWFAQ_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `TWFAQ_QSTN` varchar(255) DEFAULT NULL COMMENT 'pregunta',
  `TWFAQ_ANSWR` varchar(8000) DEFAULT NULL COMMENT 'respuesta',
  `TWFAQ_USU_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `TWFAQ_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion del registro',
  PRIMARY KEY (`TWFAQ_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tienda: preguntas frecuentes';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TWLSTDS
CREATE TABLE IF NOT EXISTS `TWLSTDS` (
  `TWLSTDS_A` smallint(6) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `TWLSTDS_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de relacion con el directorio de clientes',
  `TWLSTDS_CTMDL_ID` int(11) NOT NULL COMMENT 'indice del catalogo de modelos',
  `TWLSTDS_FN` timestamp NULL DEFAULT current_timestamp() COMMENT 'fecha de creacion',
  KEY `TWLSTDS_DRCTR_ID` (`TWLSTDS_DRCTR_ID`),
  KEY `TWLSTDS_CTMDL_ID` (`TWLSTDS_CTMDL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tienda: lista de deseos de tienda web';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TWMTDPG
CREATE TABLE IF NOT EXISTS `TWMTDPG` (
  `TWMTDPG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `TWMTDPG_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `TWMTDPG_PDCLN_ID` int(11) NOT NULL COMMENT 'indice de la tabla de pedido de cliente',
  `TWMTDPG_DRCTR_ID` int(11) NOT NULL COMMENT 'indice de la tabla del directorio',
  `TWMTDPG_MTD` varchar(20) DEFAULT NULL COMMENT 'metodo de pago: depisito, mercado pago, paypal',
  `TWMTDPG_TRNSC_ID` varchar(255) DEFAULT NULL COMMENT 'identificador de la transaccion',
  `TWMTDPG_STTS` varchar(20) DEFAULT NULL COMMENT 'status de la transaccion',
  `TWMTDPG_PRFRNCID` varchar(255) DEFAULT NULL COMMENT 'identificador de reprefencia (mercado pago)',
  `TWMTDPG_PYRID` varchar(255) DEFAULT NULL COMMENT 'identifciador del comprador',
  `TWMTDPG_PYRNM` varchar(255) DEFAULT NULL COMMENT 'nombre del comprador',
  `TWMTDPG_TTL` decimal(14,4) DEFAULT NULL COMMENT 'total de la compra',
  `TWMTDPG_FILE` varchar(32) DEFAULT NULL,
  `TWMTDPG_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion',
  PRIMARY KEY (`TWMTDPG_ID`),
  KEY `TWMTDPG_PDCLN_ID` (`TWMTDPG_PDCLN_ID`),
  KEY `TWMTDPG_DRCTR_ID` (`TWMTDPG_DRCTR_ID`),
  KEY `TWMTDPG_TRNSC_ID` (`TWMTDPG_TRNSC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tienda: metodos de pago';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.TWUSR
CREATE TABLE IF NOT EXISTS `TWUSR` (
  `TWUSR_DRCTR_ID` int(11) NOT NULL COMMENT 'indice del directorio',
  `TWUSR_TIP_ID` smallint(6) NOT NULL COMMENT 'tipo de acceso 0 = ninguno, 1 = tienda distribuidores, 2 = tienda minorista, 3 = ambos',
  `TWUSR_RLSUSR_ID` int(11) NOT NULL COMMENT '3 = administrador, 2 = vendedor, 1 = cliente',
  `TWUSR_A` smallint(6) NOT NULL COMMENT 'registro activo',
  `TWUSR_KEY` varchar(64) DEFAULT NULL COMMENT 'llave o password',
  `TWUSR_MAIL` varchar(255) DEFAULT NULL COMMENT 'correo electronico',
  `TWUSR_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  `TWUSR_FM` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'ultima fecha de moficiacion del registro',
  `TWUSR_NOM` varchar(255) DEFAULT NULL,
  KEY `TWUSR_DRCTR_ID` (`TWUSR_DRCTR_ID`),
  KEY `TWUSR_TIP_ID` (`TWUSR_TIP_ID`),
  KEY `TWUSR_RLSUSR_ID` (`TWUSR_RLSUSR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tienda: tabla de accesos a la tienda web';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b.UIPERFIL
CREATE TABLE IF NOT EXISTS `UIPERFIL` (
  `UIPERFIL_ID` int(11) NOT NULL COMMENT 'indice',
  `UIPERFIL_A` smallint(6) NOT NULL,
  `UIPERFIL_UIMENU_ID` int(11) DEFAULT NULL COMMENT 'indice del menu de opciones',
  `UIPERFIL_RLSUSR_ID` int(11) DEFAULT NULL COMMENT 'indice del rol del usuario',
  `UIPERFIL_ACTION` varchar(10) DEFAULT NULL COMMENT 'acciones permitidas',
  `UIPERFIL_KEY` varchar(25) DEFAULT NULL COMMENT 'llave del menu',
  `UIPERFIL_FN` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha de creacion del registro',
  `UIPERFIL_USR_N` varchar(10) DEFAULT NULL COMMENT 'clave del usuario creador del registro',
  `UIPERFIL_FM` timestamp NULL DEFAULT NULL COMMENT 'ultima fecha de modificacion del registr',
  `UIPERFIL_USR_M` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifico el registr',
  PRIMARY KEY (`UIPERFIL_ID`) USING BTREE,
  KEY `UIPERFIL_ID` (`UIPERFIL_ID`) USING BTREE,
  KEY `UIPERFIL_UIMENU_ID` (`UIPERFIL_UIMENU_ID`) USING BTREE,
  KEY `UIPERFIL_RLSUSR_ID` (`UIPERFIL_RLSUSR_ID`) USING BTREE,
  KEY `UIPERFIL_KEY` (`UIPERFIL_KEY`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='permisos del perfil de usuario';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b._rlsusr
CREATE TABLE IF NOT EXISTS `_rlsusr` (
  `_rlsusr_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id de roll',
  `_rlsusr_a` tinyint(3) NOT NULL DEFAULT 1 COMMENT 'activo',
  `_rlsusr_m` int(11) unsigned NOT NULL DEFAULT 0 COMMENT 'maestro',
  `_rlsusr_clv` varchar(100) NOT NULL COMMENT 'clave',
  `_rlsusr_nmbr` varchar(100) NOT NULL COMMENT 'nombre',
  `_rlsusr_usr_n` varchar(50) DEFAULT NULL COMMENT 'usuario nuevo',
  `_rlsusr_usr_m` varchar(50) DEFAULT NULL COMMENT 'usuario modifica',
  `_rlsusr_fn` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'fecha nuevo',
  `_rlsusr_fm` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'fecha modifica',
  `_rlsusr_json` text DEFAULT NULL,
  PRIMARY KEY (`_rlsusr_id`) USING BTREE,
  UNIQUE KEY `unique` (`_rlsusr_clv`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='roles de usuario';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b._rlusrl
CREATE TABLE IF NOT EXISTS `_rlusrl` (
  `_rlusrl_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla',
  `_rlusrl_a` smallint(6) NOT NULL COMMENT 'registro activo',
  `_rlusrl_usr_id` int(11) DEFAULT NULL COMMENT 'indice de relacion del usuario',
  `_rlusrl_rlsusr_id` int(11) DEFAULT NULL COMMENT 'indice de relacion del rol',
  PRIMARY KEY (`_rlusrl_id`) USING BTREE,
  KEY `_rlusrl_id` (`_rlusrl_id`) USING BTREE,
  KEY `_rlusrl_usr_id` (`_rlusrl_usr_id`) USING BTREE,
  KEY `_rlusrl_rlsusr_id` (`_rlusrl_rlsusr_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='perfiles: tabla de relaciones entre usuarios y perfiles';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla jaeger_ipr981125pn9b._uiperfil
CREATE TABLE IF NOT EXISTS `_uiperfil` (
  `_uiperfil_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'indice de la tabla de perfiles',
  `_uiperfil_a` int(11) NOT NULL DEFAULT 1 COMMENT 'registro activo',
  `_uiperfil_uimenu_id` int(11) NOT NULL COMMENT 'indice de la ',
  `_uiperfil_rlsusr_id` int(11) NOT NULL COMMENT 'indice del rol',
  `_uiperfil_key` varchar(25) NOT NULL COMMENT 'llave del menu',
  `_uiperfil_action` varchar(10) NOT NULL,
  `_uiperfil_fn` datetime NOT NULL COMMENT 'fecha de creacion del registro',
  `_uiperfil_usr_n` varchar(10) NOT NULL COMMENT 'clave del usuario creador del registro',
  `_uiperfil_fm` datetime DEFAULT NULL COMMENT 'ultima fecha de modificacion del registro',
  `_uiperfil_usr_m` varchar(10) DEFAULT NULL COMMENT 'clave del ultimo usuario que modifica el registro',
  PRIMARY KEY (`_uiperfil_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='perfiles: opciones del menu por perfil';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para vista jaeger_ipr981125pn9b.VWCATE
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `VWCATE` (
	`VWCATE_ID` INT(11) NOT NULL COMMENT 'id de la tabla',
	`VWCATE_CATE_ID` INT(11) NOT NULL COMMENT 'id de la tabla',
	`VWCATE_A` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`VWCATE_ALM_ID` SMALLINT(6) NOT NULL COMMENT 'tipo de almacen',
	`VWCATE_SEC` INT(11) NULL COMMENT 'secuencia',
	`VWCATE_TIPO` VARCHAR(258) NULL COLLATE 'utf8_general_ci',
	`VWCATE_NOM` VARCHAR(258) NULL COLLATE 'utf8_general_ci',
	`VWCATE_CLASE` VARCHAR(128) NULL COLLATE 'utf8_general_ci',
	`VWCATE_URL` VARCHAR(255) NULL COMMENT 'url de la imagen asociada' COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Volcando estructura para vista jaeger_ipr981125pn9b.VWXMDL
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `VWXMDL` (
	`VWXMDL_CTMDL_ID` INT(11) NOT NULL COMMENT 'indice principal de la tabla',
	`VWXMDL_CTMDL_A` SMALLINT(6) NOT NULL COMMENT 'registro activo',
	`VWXMDL_CTCLS_ID` INT(11) NULL COMMENT 'id de la tabla',
	`VWXMDL_CTCLS_SEC` INT(11) NULL COMMENT 'secuencia',
	`VWXMDL_CTPRD_ID` INT(11) NOT NULL COMMENT 'indice de la tabla de productos',
	`VWXMDL_TIPO` SMALLINT(6) NULL,
	`VWXMDL_CTPRD_SEC` INT(11) NULL,
	`VWXMDL_CTMDL_SEC` INT(11) NULL,
	`VWXMDL_CAT` VARCHAR(258) NULL COLLATE 'utf8_general_ci',
	`VWXMDL_CLASE` VARCHAR(128) NULL COLLATE 'utf8_general_ci',
	`VWXMDL_PROD` VARCHAR(128) NULL COMMENT 'nombre o descripcion del producto' COLLATE 'utf8_general_ci',
	`VWXMDL_CLV` VARCHAR(128) NULL COMMENT 'clave del modelo (url)' COLLATE 'utf8_general_ci',
	`VWXMDL_MODELO` VARCHAR(1000) NULL COMMENT 'descripcion corta' COLLATE 'utf8_general_ci',
	`VWXMDL_MRC` VARCHAR(128) NULL COMMENT 'marca o fabricante' COLLATE 'utf8_general_ci',
	`VWXMDL_ESPC` VARCHAR(128) NULL COMMENT 'especifcaciones' COLLATE 'utf8_general_ci',
	`VWXMDL_SKUP` VARCHAR(100) NULL COMMENT 'obtener o establecer el numero de parte, identificador del producto o del servicio, la clave de producto o servicio, sku o equivalente, propia de la operacion del emisor (en cfdi: noidentificacion) (publicacion)' COLLATE 'utf8_general_ci',
	`VWXMDL_VIS` SMALLINT(6) NOT NULL COMMENT 'para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)',
	`VWXMDL_CLVUND` VARCHAR(3) NULL COMMENT 'clave de unidad sat' COLLATE 'utf8_general_ci',
	`VWXMDL_CLVPRDS` VARCHAR(8) NULL COMMENT 'clave del producto o del servicio sat' COLLATE 'utf8_general_ci',
	`VWXMDL_CLVOBJ` VARCHAR(2) NULL COMMENT 'clave si la operacion es objeto o no de impuesto sat' COLLATE 'utf8_general_ci',
	`VWXMDL_UNDDP` VARCHAR(20) NULL COMMENT 'unidad personalizada' COLLATE 'utf8_general_ci',
	`VWXMDL_UNDD_ID` INT(11) NULL COMMENT 'alm:|mp,pt| desc: indice de la unidad de almacenamiento en el el almacen',
	`VWXMDL_UNTR` DECIMAL(11,4) NULL COMMENT 'precio unitario',
	`VWXMDL_UNDD` VARCHAR(32) NULL COMMENT 'nombre de la unidad' COLLATE 'utf8_general_ci',
	`VWXMDL_FUNDD` DECIMAL(14,6) NULL COMMENT 'factor de la unidad',
	`VWXMDL_ATRZD` SMALLINT(6) NOT NULL COMMENT 'precio autorizado',
	`VWXMDL_TRSIVA` DECIMAL(14,4) NULL COMMENT 'importe del impuesto del iva trasladado',
	`VWXMDL_TRSIVAF` VARCHAR(6) NULL COMMENT 'factor del iva traslado' COLLATE 'utf8_general_ci',
	`VWXMDL_DSCR` VARCHAR(8000) NULL COMMENT 'descripcion larga del producto (publicacion)' COLLATE 'utf8_general_ci',
	`VWXMDL_DIS` SMALLINT(6) NOT NULL,
	`VWXMDL_SKU` VARCHAR(65) NOT NULL COLLATE 'utf8mb4_general_ci',
	`VWXMDL_SEARCH` TEXT NULL COLLATE 'utf8_general_ci',
	`VWCATE_URL` VARCHAR(255) NULL COMMENT 'url de la imagen asociada' COLLATE 'utf8_general_ci',
	`VWCATE_CTPRD_URL` VARCHAR(255) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `VWCATE`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VWCATE` AS select `C2`.`CTCLS_ID` AS `VWCATE_ID`,`C1`.`CTCLS_ID` AS `VWCATE_CATE_ID`,`C2`.`CTCLS_A` AS `VWCATE_A`,`C1`.`CTCLS_ALM_ID` AS `VWCATE_ALM_ID`,`C2`.`CTCLS_SEC` AS `VWCATE_SEC`,concat(if(`C1`.`CTCLS_CLASS1` = '/','',if(`C1`.`CTCLS_CLASS1` = '/',`C2`.`CTCLS_CLASS1`,concat('/',`C1`.`CTCLS_CLASS1`))),'/',if(`C2`.`CTCLS_CLASS1` = '',`C1`.`CTCLS_CLASS1`,`C2`.`CTCLS_CLASS1`)) AS `VWCATE_TIPO`,concat(if(`C1`.`CTCLS_CLASS2` = '/','',if(`C1`.`CTCLS_CLASS2` = '/',`C2`.`CTCLS_CLASS2`,concat('/',`C1`.`CTCLS_CLASS2`))),'/',if(`C2`.`CTCLS_CLASS2` = '',`C1`.`CTCLS_CLASS2`,`C2`.`CTCLS_CLASS2`)) AS `VWCATE_NOM`,if(`C2`.`CTCLS_CLASS2` = '',`C1`.`CTCLS_CLASS2`,`C2`.`CTCLS_CLASS2`) AS `VWCATE_CLASE`,`C2`.`CTCLS_URL` AS `VWCATE_URL` from (`CTCLS` `C1` join `CTCLS` `C2` on(`C2`.`CTCLS_SBID` = `C1`.`CTCLS_ID`)) order by `C2`.`CTCLS_ID`,concat(if(`C1`.`CTCLS_CLASS2` = '/','',if(`C1`.`CTCLS_CLASS2` = '/',`C2`.`CTCLS_CLASS2`,concat('/',`C1`.`CTCLS_CLASS2`))),'/',if(`C2`.`CTCLS_CLASS2` = '',`C1`.`CTCLS_CLASS2`,`C2`.`CTCLS_CLASS2`));

-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `VWXMDL`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `VWXMDL` AS select `CTMDL`.`CTMDL_ID` AS `VWXMDL_CTMDL_ID`,`CTMDL`.`CTMDL_A` AS `VWXMDL_CTMDL_A`,`VWCATE`.`VWCATE_ID` AS `VWXMDL_CTCLS_ID`,`VWCATE`.`VWCATE_SEC` AS `VWXMDL_CTCLS_SEC`,`CTMDL`.`CTMDL_CTPRD_ID` AS `VWXMDL_CTPRD_ID`,`CTPRD`.`CTPRD_TIPO` AS `VWXMDL_TIPO`,`CTPRD`.`CTPRD_SEC` AS `VWXMDL_CTPRD_SEC`,`CTMDL`.`CTMDL_SEC` AS `VWXMDL_CTMDL_SEC`,`VWCATE`.`VWCATE_NOM` AS `VWXMDL_CAT`,`VWCATE`.`VWCATE_CLASE` AS `VWXMDL_CLASE`,`CTPRD`.`CTPRD_NOM` AS `VWXMDL_PROD`,`CTMDL`.`CTMDL_CLV` AS `VWXMDL_CLV`,`CTMDL`.`CTMDL_DSCRC` AS `VWXMDL_MODELO`,`CTMDL`.`CTMDL_MRC` AS `VWXMDL_MRC`,`CTMDL`.`CTMDL_ESPC` AS `VWXMDL_ESPC`,`CTMDL`.`CTMDL_SKU` AS `VWXMDL_SKUP`,`CTMDL`.`CTMDL_VIS` AS `VWXMDL_VIS`,`CTMDL`.`CTMDL_CLVUND` AS `VWXMDL_CLVUND`,`CTMDL`.`CTMDL_CLVPRDS` AS `VWXMDL_CLVPRDS`,`CTMDL`.`CTMDL_CLVOBJ` AS `VWXMDL_CLVOBJ`,`CTMDL`.`CTMDL_UNDD` AS `VWXMDL_UNDDP`,`CTMDL`.`CTMDL_UNDDA` AS `VWXMDL_UNDD_ID`,`CTMDL`.`CTMDL_UNTR` AS `VWXMDL_UNTR`,`CTUND`.`CTUND_NOM` AS `VWXMDL_UNDD`,`CTUND`.`CTUND_FAC` AS `VWXMDL_FUNDD`,`CTMDL`.`CTMDL_ATRZD` AS `VWXMDL_ATRZD`,`CTMDL`.`CTMDL_TRSIVA` AS `VWXMDL_TRSIVA`,`CTMDL`.`CTMDL_TRSIVAF` AS `VWXMDL_TRSIVAF`,`CTMDL`.`CTMDL_DSCR` AS `VWXMDL_DSCR`,`CTMDL`.`CTMDL_DIS` AS `VWXMDL_DIS`,concat(case `CTMDL`.`CTMDL_ALM_ID` when 1 then 'MP-' when 2 then 'PT-' when 3 then 'TW-' else 'NA-' end,concat(substr('00000',1,5 - char_length(`CTMDL`.`CTMDL_CTPRD_ID`)),`CTMDL`.`CTMDL_CTPRD_ID`,substr('-0000',1,5 - char_length(`CTMDL`.`CTMDL_ID`)),`CTMDL`.`CTMDL_ID`)) AS `VWXMDL_SKU`,lcase(concat(if(`VWCATE`.`VWCATE_NOM` is null,'',replace(replace(replace(replace(replace(`VWCATE`.`VWCATE_NOM`,'Á','A'),'É','E'),'Í','I'),'Ó','O'),'Ú','U')),',',replace(replace(replace(replace(replace(`CTPRD`.`CTPRD_NOM`,'Á','A'),'É','E'),'Í','I'),'Ó','O'),'Ú','U'),',',replace(replace(replace(replace(replace(`CTMDL`.`CTMDL_DSCRC`,'Á','A'),'É','E'),'Í','I'),'Ó','O'),'Ú','U'),',',case `CTMDL`.`CTMDL_ALM_ID` when 1 then 'MP-' when 2 then 'PT-' when 3 then 'TW-' else 'NA-' end,substr('00000',1,5 - char_length(`CTMDL`.`CTMDL_CTPRD_ID`)),`CTMDL`.`CTMDL_CTPRD_ID`,substr('-0000',1,5 - char_length(`CTMDL`.`CTMDL_ID`)),`CTMDL`.`CTMDL_ID`)) AS `VWXMDL_SEARCH`,`VWCATE`.`VWCATE_URL` AS `VWCATE_URL`,`CTPRD`.`CTPRD_URL` AS `VWCATE_CTPRD_URL` from (((`CTMDL` left join `CTPRD` on(`CTMDL`.`CTMDL_CTPRD_ID` = `CTPRD`.`CTPRD_ID`)) left join `VWCATE` on(`CTPRD`.`CTPRD_CTCLS_ID` = `VWCATE`.`VWCATE_ID`)) left join `CTUND` on(`CTMDL`.`CTMDL_UNDDA` = `CTUND`.`CTUND_ID`)) order by `VWCATE`.`VWCATE_SEC`,`CTPRD`.`CTPRD_SEC`,`CTMDL`.`CTMDL_SEC`,`CTPRD`.`CTPRD_ID`,`CTMDL`.`CTMDL_ID`;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
