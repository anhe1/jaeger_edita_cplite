﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.Charting;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Almacen.PT.Services;
using Jaeger.UI.Almacen.PT.Builder;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class ReporteVentasForm : RadForm {
        private List<RemisionSingleModel> _DataSource;
        protected IRemisionadoService _Service;

        public ReporteVentasForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void ReporteVentasForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.CPLite.Almacen.PT.Services.RemisionadoService();
            using (IRemisionadoGridBuilder builder = new RemisionadoGridBuilder()) {
                this.TRemisionado.GridData.Columns.AddRange(builder.Templetes().Remisionado().Build());
            }
            this.TResumen.Actualizar.Click += this.TResumen_Actualizar_Click;
            this.TResumen.ExportarExcel.Click += this.TResumen_ExportarExcel_Click;
            this.TResumen.Cerrar.Click += this.TResumen_Cerrar_Click;
        }

        #region barra de herramientas
        private void TResumen_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            this.TRemisionado.GridData.DataSource = this._DataSource;
            // limpiar chart
            this.VendedoresChart.Series[0].DataPoints.Clear();
            this.VendedoresChart.Series[0].ShowLabels = true;
            this.VendedoresChart.Series[0].LabelFormat = "{0:N0}";
            this.VendedoresChart.Series[1].DataPoints.Clear();
            this.VendedoresChart.Series[1].ShowLabels = true;
            this.VendedoresChart.Series[1].LabelFormat = "{0:N0}";
            this.VendedoresChart.Series[2].DataPoints.Clear();
            this.VendedoresChart.Series[2].ShowLabels = true;
            this.VendedoresChart.Series[2].LabelFormat = "{0:N0}";
            this.VendedoresChart.Series[3].DataPoints.Clear();
            this.VendedoresChart.Series[3].ShowLabels = true;
            this.VendedoresChart.Series[3].LabelFormat = "{0:N0}";
            this.VendedoresChart.Series[4].DataPoints.Clear();
            this.VendedoresChart.Series[4].ShowLabels = true;
            this.VendedoresChart.Series[4].LabelFormat = "{0:N0}";

            var vendedores = this._DataSource.OrderBy(it => it.Vendedor).Select(it => it.Vendedor).Distinct().ToList();

            foreach (var vendedor in vendedores) {
                var remisionado = this._DataSource.Where(it => it.Vendedor == vendedor).Where(it => it.IdStatus != 0).ToList();
                var total = double.Parse(remisionado.Sum(it => it.Total).ToString());
                var descuento = double.Parse(remisionado.Sum(it => it.DescuentoTotal).ToString());
                var porcobrar = double.Parse(remisionado.Sum(it => it.PorCobrar).ToString());
                var cobrado = double.Parse(remisionado.Sum(it => it.Acumulado).ToString());
                var saldo = double.Parse(remisionado.Sum(it => it.Saldo).ToString());
                this.VendedoresChart.Series[0].DataPoints.Add(new CategoricalDataPoint { Category = vendedor, Label = total, Value = total });
                this.VendedoresChart.Series[1].DataPoints.Add(new CategoricalDataPoint { Category = vendedor, Label = descuento, Value = descuento });
                this.VendedoresChart.Series[2].DataPoints.Add(new CategoricalDataPoint { Category = vendedor, Label = porcobrar, Value = porcobrar });
                this.VendedoresChart.Series[3].DataPoints.Add(new CategoricalDataPoint { Category = vendedor, Label = cobrado, Value = cobrado });
                this.VendedoresChart.Series[4].DataPoints.Add(new CategoricalDataPoint { Category = vendedor, Label = saldo, Value = saldo });
            }
        }

        private void TResumen_ExportarExcel_Click(object sender, EventArgs e) {
            var _exportar = new TelerikGridExportForm(this.TRemisionado.GridData);
            _exportar.ShowDialog(this);
        }

        private void TResumen_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            var query = RemisionadoService.Query().WithYear(this.TResumen.GetEjercicio()).WithMonth(this.TResumen.GetMes()).WithIdStatus(new int[] { 2, 3, 4 }).Build();
            this._DataSource = this._Service.GetList<RemisionSingleModel>(query).ToList();
        }
        #endregion
    }
}