﻿namespace Jaeger.UI.CPLite.Forms.Tesoreria {
    public class BeneficiarioCatalogoForms : Banco.Forms.BeneficiarioCatalogoForms {
        public BeneficiarioCatalogoForms() {
            this.service = new Aplication.CPLite.Banco.Services.BancoService();
            this.Load += this.BeneficiarioCatalogoForms_Load;
        }

        public BeneficiarioCatalogoForms(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.service = new Aplication.CPLite.Banco.Services.BancoService();
            this.Load += this.BeneficiarioCatalogoForms_Load;
        }

        private void BeneficiarioCatalogoForms_Load(object sender, System.EventArgs e) { }
    }
}
