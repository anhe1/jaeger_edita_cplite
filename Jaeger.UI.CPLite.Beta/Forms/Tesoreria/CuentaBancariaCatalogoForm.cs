﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Tesoreria {
    public class CuentaBancariaCatalogoForm : UI.Banco.Forms.CuentaBancariaCatalogoForm {
        public CuentaBancariaCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.CuentaBancariaCatalogoForm_Load;
        }

        private void CuentaBancariaCatalogoForm_Load(object sender, EventArgs e) {
            
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this.TCuentaB._Service = new Aplication.CPLite.Banco.Services.BancoCuentaService();
        }
    }
}
