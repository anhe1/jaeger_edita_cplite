﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Util.Services;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Almacen.PT");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(PedidoClientePrinter)) {
                this.CrearPedido();
            } else if (this.CurrentObject.GetType() == typeof(RemisionDetailPrinter)) {
                this.CrearRemision();
            } else if (this.CurrentObject.GetType() == typeof(List<RemisionDetailPrinter>)) {
                this.ListaRemision();
            } else if (this.CurrentObject.GetType() == typeof(ValeAlmacenPrinter)) {
                this.CrearVale();
            } else if (this.CurrentObject.GetType() == typeof(List<ValeAlmacenPrinter>)) {
                this.ListaVale();
            } else if (this.CurrentObject.GetType() == typeof(DevolucionDetailPrinterModel)) {
                this.CrearDevolucion();
            } else if (this.CurrentObject.GetType() == typeof(List<PedidoClientePrinter>)) {
                this.CrearPedidoL();
            }
        }

        private void CrearRemision() {
            var current = (RemisionDetailPrinter)this.CurrentObject;
            if (current.ShowPagare) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionPagareV201.rdlc");
            } else if (current.IsValuesVisible) {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionImportesV20.rdlc");
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.Almacen.PT.Reports.RemisionSimpleV20.rdlc");
            }

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Remision");
            var d = DbConvert.ConvertToDataTable<RemisionDetailPrinter > (new List<RemisionDetailPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearPedido() {
            var current = (PedidoClientePrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Almacen.Reports.PedidoClienteReporte.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Pedido");
            var d = DbConvert.ConvertToDataTable<PedidoClientePrinter>(new List<PedidoClientePrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Partidas));
            this.Finalizar();
        }

        private void ListaRemision() {
            var current = (List<RemisionDetailPrinter>)this.CurrentObject;
            //this.LoadDefinition = File.Open(@"D:\bitbucket\jaeger_edita_cplite\Jaeger.Domain.CPLite\AlmacenPT\Reports\RemisionFiscalLReporte.rdlc", FileMode.Open, FileAccess.Read);
            this.LoadDefinition = File.Open(@"Jaeger.Domain.CPLite.Almacen.PT.Reports.RemisionFiscalLReporte.rdlc", FileMode.Open, FileAccess.Read);
            this.Procesar();
            this.SetDisplayName("Remisiones");
            this.SetDataSource("Remision", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }

        private void ListaVale() {
            var current = (List<ValeAlmacenPrinter>)this.CurrentObject;
            //this.LoadDefinition = File.Open(@"D:\bitbucket\jaeger_edita_desktop\Jaeger.Domain.AlmacenPT\Reports\ValeAlmacen15LVReporte.rdlc", FileMode.Open, FileAccess.Read);
            this.LoadDefinition = File.Open(@"Jaeger.Domain.CPLite.Almacen.PT.ReportsValeAlmacen15LVReporte.rdlc", FileMode.Open, FileAccess.Read);
            this.Procesar();
            this.SetDisplayName("Vales");
            this.SetDataSource("Comprobante", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }

        private void CrearVale() {
            var current = (ValeAlmacenPrinter)this.CurrentObject;
            if (current.Documento != null) {
                //this.LoadDefinition = File.Open(@"D:\bitbucket\jaeger_edita_desktop\Jaeger.Domain.AlmacenPT\Reports\" + current.Documento.FormatoImpreso, FileMode.Open, FileAccess.Read);
                this.LoadDefinition = File.Open(@"Jaeger.Domain.CPLite.Almacen.PT.Reports." + current.Documento.FormatoImpreso, FileMode.Open, FileAccess.Read);
            } else {
                this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Almacen.PT.Reports.ValeAlmacenv15.rdlc");
            }
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Vale Almacén");
            var d = DbConvert.ConvertToDataTable<ValeAlmacenPrinter>(new List<ValeAlmacenPrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearDevolucion() {
            var current = (DevolucionDetailPrinterModel)this.CurrentObject;
            //this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Almacen.Reports.ValeAlmacenv15.rdlc");
            //this.LoadDefinition = File.Open(@"D:\bitbucket\jaeger_edita_desktop\Jaeger.Domain.AlmacenPT\Reports\ValeAlmacen15Reporte.rdlc", FileMode.Open, FileAccess.Read);
            this.LoadDefinition = File.Open(@"Jaeger.Domain.CPLite.Almacen.PT.Reports.ValeAlmacen15Reporte.rdlc", FileMode.Open, FileAccess.Read);
            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Vale Almacén");
            var d = DbConvert.ConvertToDataTable<DevolucionDetailPrinterModel>(new List<DevolucionDetailPrinterModel>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.Finalizar();
        }

        private void CrearPedidoL() {
            var current = (List<PedidoClientePrinter>)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Tienda.Reports.PedidoClienteL2Reporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Pedidos");
            this.SetDataSource("Pedido", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }
    }
}
