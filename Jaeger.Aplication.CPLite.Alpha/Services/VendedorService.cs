﻿using Jaeger.Aplication.Contribuyentes.Contracts;

namespace Jaeger.Aplication.CPLite.Alpha.Services {
    /// <summary>
    /// Vendedor
    /// </summary>
    public class VendedorService : Contribuyentes.Services.VendedorService, IVendedorService {

        /// <summary>
        /// constructor
        /// </summary>
        public VendedorService() : base() { }
    }
}
