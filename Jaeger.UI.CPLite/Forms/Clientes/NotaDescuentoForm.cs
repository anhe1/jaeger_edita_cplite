﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class NotaDescuentoForm : RadForm {
        protected internal INotaDescuentoService Service;
        protected internal NotaDescuentoDetailModel Comprobante;

        public NotaDescuentoForm() {
            InitializeComponent();
        }

        public NotaDescuentoForm(NotaDescuentoDetailModel model) {
            InitializeComponent();
            this.Comprobante = model;
        }

        private void NotaDescuentoForm_Load(object sender, EventArgs e) {
            this.Service = new NotaDescuentoService();
            this.Receptor.Service = new Aplication.CPLite.Services.DirectorioService();
            this.dataGridPartidas.TelerikGridCommon();

            this.TNota.Nuevo.Click += this.TNota_Nuevo_Click;
            this.TNota.Guardar.Click += this.TNota_Guardar_Click;
            this.TNota.Actualizar.Click += this.TNota_Actualizar_Click;
            this.TNota.Cerrar.Click += this.TNota_Cerrar_Click;

            this.Status.DataSource = this.Service.GetStatus(false);
            this.Status.DisplayMember = "Descripcion";
            this.Status.ValueMember = "Id";

            this.IdMotivo.DataSource = this.Service.GetMotivos();
            this.IdMotivo.DisplayMember = "Descriptor";
            this.IdMotivo.ValueMember = "Id";

            this.TNota_Actualizar_Click(sender, e);
        }

        #region barra de herramientas
        public virtual void TNota_Nuevo_Click(object sender, EventArgs e) {
            this.Comprobante = null;
            this.CreateBinding();
        }

        public virtual void TNota_Actualizar_Click(object sender, EventArgs eventArgs) {
            if (this.Comprobante == null) {
                this.Comprobante = this.Service.GetNew();
            } else {
                using (var espera = new Waiting1Form(this.Cargar)) {
                    espera.Text = "Consultando ...";
                    espera.ShowDialog(this);
                }
            }

            if (this.Comprobante.IsEditable) {
                this.Receptor.Init();
            }

            this.CreateBinding();
        }

        public virtual void TNota_Guardar_Click(object sender, EventArgs e) {
            if (this.Comprobante.Partidas.Count == 0) {
                if (RadMessageBox.Show(this, "No se relacionaron devoluciones para esta nota. ¿Es correcto?", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;
            }

            using (var espera = new Common.Forms.Waiting2Form(this.Verificar2)) {
                espera.ShowDialog(this);
            }

            if (this.Tag != null) {
                var respues = (string)this.Tag;
                RadMessageBox.Show(this, respues, "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                return;
            }

            if (this.Verificar()) {
                using (var espera = new Common.Forms.Waiting2Form(this.Guardar)) {
                    espera.ShowDialog(this);
                    this.Close();
                }
            } else {
                RadMessageBox.Show(this, "Existen algunos errores de captura, verifique y vuelva a intentar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        public virtual void TNota_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region barra de herramientas conceptos
        public virtual void TConcepto_Nuevo_Click(object sender, EventArgs e) {
            //var relacion = new DocumentoAlmacenBuscarForm(DocumentoAlmacenTipoEnum.Devolucion);
            //relacion.Selected += TConcepto_Agregar;
            //relacion.ShowDialog(this);
        }

        public virtual void TConcepto_Agregar(object sender, ValeAlmacenDetailModel e) {
            if (e != null) {
                var agregar = new NotaDescuentoRelacionDetailModel {
                    Activo = true,
                    Receptor = e.Receptor,
                    Clave = e.ReceptorRFC,
                    RFC = "",
                    TotalTrasladoIVA = e.TotalTrasladoIVA,
                    SubTotal = e.SubTotal,
                    IdDocumento = e.IdDocumento,
                    IdTipo = e.IdTipoComprobante,
                    Descuento = e.TotalDescuento,
                    GTotal = e.Total,
                    IdComprobante = e.IdComprobante,
                    //IdPedido = e.IdPedido,
                    Folio = e.Folio,
                    FechaEmision = e.FechaEmision,
                    Serie = e.Serie,
                    IdDirectorio = e.IdDirectorio,
                    FechaNuevo = DateTime.Now
                };
                this.Comprobante.Partidas.Add(agregar);
            }
        }

        public virtual void TConcepto_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridPartidas.CurrentRow != null) {
                var _seleccionado = this.dataGridPartidas.CurrentRow.DataBoundItem as NotaDescuentoRelacionDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Id == 0) {
                        this.dataGridPartidas.Rows.Remove(this.dataGridPartidas.CurrentRow);
                    } else {
                        _seleccionado.Activo = false;
                        this.dataGridPartidas.CurrentRow.IsVisible = false;
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        public virtual void CreateBinding() {

            this.Receptor.DataBindings.Clear();
            this.Receptor.DataBindings.Add("Text", this.Comprobante, "Nombre", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Receptor.Enabled = this.Comprobante.IsEditable;

            this.Receptor.IdDirectorio.DataBindings.Clear();
            this.Receptor.IdDirectorio.DataBindings.Add("Value", this.Comprobante, "IdDirectorio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.Clave.DataBindings.Clear();
            this.Receptor.Clave.DataBindings.Add("Text", this.Comprobante, "Clave", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Receptor.RFC.DataBindings.Clear();
            this.Receptor.RFC.DataBindings.Add("Text", this.Comprobante, "RFC", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Folio.DataBindings.Clear();
            this.Folio.DataBindings.Add("Text", this.Comprobante, "Folio", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Status.SetEditable(this.Comprobante.IsEditable);
            this.Status.DataBindings.Add("SelectedValue", this.Comprobante, "IdStatus", true, DataSourceUpdateMode.OnPropertyChanged);

            this.FechaEmision.DataBindings.Clear();
            this.FechaEmision.SetEditable(this.Comprobante.IsEditable);
            this.FechaEmision.DataBindings.Add("Value", this.Comprobante, "FechaNuevo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Recibe.DataBindings.Clear();
            this.Recibe.DataBindings.Add("Text", this.Comprobante, "Contacto", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Recibe.ReadOnly = !this.Comprobante.IsEditable;

            this.Referencia.DataBindings.Clear();
            this.Referencia.DataBindings.Add("Text", this.Comprobante, "Referencia", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Referencia.ReadOnly = !this.Comprobante.IsEditable;

            this.Observaciones.DataBindings.Clear();
            this.Observaciones.DataBindings.Add("Text", this.Comprobante, "Nota", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Observaciones.ReadOnly = !this.Comprobante.IsEditable;

            this.Creo.DataBindings.Clear();
            this.Creo.DataBindings.Add("Text", this.Comprobante, "Creo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.Importe.DataBindings.Clear();
            this.Importe.DataBindings.Add("Value", this.Comprobante, "GTotal", true, DataSourceUpdateMode.OnPropertyChanged);
            this.Importe.ReadOnly = !this.Comprobante.IsEditable;

            this.IdMotivo.SetEditable(this.Comprobante.IsEditable);
            this.IdMotivo.DataBindings.Clear();
            this.IdMotivo.DataBindings.Add("SelectedValue", this.Comprobante, "IdMotivo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dataGridPartidas.DataSource = this.Comprobante.Partidas;

            this.TNota.Guardar.Enabled = this.Comprobante.IsEditable;
            this.TConcepto.Enabled = this.Comprobante.IsEditable;
        }

        public virtual void Guardar() {
            this.Comprobante = this.Service.Save(this.Comprobante);
        }

        public virtual void Cargar() {
            this.Comprobante = this.Service.GetById(this.Comprobante.IdNota);
        }

        public virtual bool Verificar() {
            this.errorVerificar.Clear();
            if (this.Comprobante.GTotal <= 0) {
                this.errorVerificar.SetError(this.Importe, "El importe de la nota no es válido");
                return false;
            }

            if (this.Comprobante.IdDirectorio == 0) {
                this.errorVerificar.SetError(this.Receptor, "Es necesario seleccionar un beneficiario.");
                return false;
            }
            return true;
        }

        public virtual void Verificar2() {
            this.Tag = this.Service.Verificar(this.Comprobante);
        }
        #endregion
    }
}
