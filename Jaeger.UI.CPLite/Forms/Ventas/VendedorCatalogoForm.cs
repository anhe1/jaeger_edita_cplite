﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class VendedorCatalogoForm : RadForm {
        protected IVendedoresService service;
        protected IComisionService comision;
        private BindingList<Vendedor2DetailModel> vendedores;
        private BindingList<ComisionDetailModel> catalogoComision;
        private readonly RadMenuItem _registroInactivo = new RadMenuItem { Text = "Mostrar registros inactivos", CheckOnClick = true };
        private readonly RadMenuItem _accesoWeb = new RadMenuItem { Text = "Acceso Web" };
        protected internal Domain.Base.ValueObjects.UIAction _permisos;

        public VendedorCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void VendedorCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new VendedoresService();
            this.comision = new ComisionService();
            this.dataGridVendedores.TelerikGridCommon();
            this.gridClientes.TelerikTemplateCommon();
            this.dataGridVendedores.RowSourceNeeded += GridRemisiones_RowSourceNeeded;
            this.gridClientes.HierarchyDataProvider = new GridViewEventDataProvider(this.gridClientes);
            this.TVendedor.Herramientas.Items.Add(_registroInactivo);
            this.TVendedor.Herramientas.Items.Add(_accesoWeb);
            this._accesoWeb.Click += TVendedor_Acceso_Click;
            //this.TVendedor.Nuevo.Enabled = this._permisos.Agregar;
            //this.TVendedor.Editar.Enabled = this._permisos.Editar;
            //this.TVendedor.Remover.Enabled = this._permisos.Remover;
            //this.TCliente.Enabled = this._permisos.Editar;
        }

        #region barra de herramientas vendedor
        private void TVendedor_Agregar_Click(object sender, EventArgs e) {
            var editar = new VendedorForm(this._permisos);
            editar.ShowDialog(this);
        }

        private void TVendedor_Editar_Click(object sender, EventArgs e) {
            if (this.dataGridVendedores.CurrentRow != null) {
                using (var espera = new Waiting1Form(this.GetClientes)) {
                    espera.Text = "Consultando autorizaciones ...";
                    espera.ShowDialog(this);
                }
                var _seleccionado = this.dataGridVendedores.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                var editar = new VendedorForm(_seleccionado, this._permisos);
                editar.ShowDialog(this);
            }
        }

        private void TVendedor_Remover_Click(object sender, EventArgs e) {
            if (this.dataGridVendedores.CurrentRow != null) {
                var _seleccionado = this.dataGridVendedores.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                if (_seleccionado != null) {
                    if (RadMessageBox.Show(this, "¿Esta seguro de remover al vendedor seleccionado? Esta acción no se puede revertir.", "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {

                    }
                }
            }
        }

        private void TVendedor_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            var IdTamanio = this.dataGridVendedores.Columns["IdComision"] as GridViewComboBoxColumn;
            IdTamanio.DataSource = this.catalogoComision;
            IdTamanio.DisplayMember = "Descripcion";
            IdTamanio.ValueMember = "IdComision";

            this.dataGridVendedores.DataSource = this.vendedores;
        }

        private void TVendedor_Filtro_Click(object sender, EventArgs e) {
            this.dataGridVendedores.ShowFilteringRow = this.TVendedor.Filtro.ToggleState != ToggleState.On;
            if (this.dataGridVendedores.ShowFilteringRow == false) {
                this.dataGridVendedores.FilterDescriptors.Clear();
            }
        }

        private void TVendedor_Acceso_Click(object sender, EventArgs e) {
            if (this.dataGridVendedores.CurrentRow != null) {
                var edicion = this.dataGridVendedores.CurrentRow.DataBoundItem as Vendedor2DetailModel;
                if (edicion != null) {
                    var editar = new VendedorAccesoForm(edicion);
                    editar.ShowDialog(this);
                }
            }
        }

        private void TVendedor_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region acciones del grid
        public virtual void GridRemisiones_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            var _rowData = e.ParentRow.DataBoundItem as Vendedor2DetailModel;
            if (_rowData != null) {
                if (e.Template.Caption == this.gridClientes.Caption) {
                    using (var espera = new Waiting1Form(this.GetClientes)) {
                        espera.Text = "Consultando autorizaciones ...";
                        espera.ShowDialog(this);
                    }

                    foreach (var item in _rowData.Clientes) {
                        var _row = e.Template.Rows.NewRow();
                        _row.Cells["Activo"].Value = item.Activo;
                        _row.Cells["Clave"].Value = item.Clave;
                        _row.Cells["Nombre"].Value = item.Nombre;
                        _row.Cells["Nota"].Value = item.Nota;
                        _row.Cells["Creo"].Value = item.Modifica;
                        _row.Cells["FechaNuevo"].Value = item.FechaModifica;
                        e.SourceCollection.Add(_row);
                    }
                }
            }
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.vendedores = this.service.GetList(!_registroInactivo.IsChecked);
            this.catalogoComision = this.comision.GetComisiones(true);
        }

        private void GetClientes() {
            var _seleccionado = this.dataGridVendedores.CurrentRow.DataBoundItem as Vendedor2DetailModel;
            if (_seleccionado != null) {
                var d1 = this.service.GetById(_seleccionado.IdVendedor);
                if (d1 != null) {
                    _seleccionado.Clientes = d1.Clientes;
                }
            }
        }
        #endregion
    }
}
