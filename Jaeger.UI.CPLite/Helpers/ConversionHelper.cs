﻿using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.CPLite.Helpers {
    internal class ConversionHelper {
        public static Domain.Banco.Entities.MovimientoBancarioComprobanteDetailModel ConverTo(Domain.Almacen.PT.Contracts.IRemisionSingleModel item) {
            var movimiento = new Domain.Banco.Entities.MovimientoBancarioComprobanteDetailModel() {
                Activo = true,
                TipoDocumento = Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision,
                ClaveFormaPago = item.ClaveFormaPago,
                ClaveMetodoPago = item.ClaveMetodoPago,
                ClaveMoneda = item.ClaveMoneda,
                EmisorNombre = item.EmisorRFC,
                EmisorRFC = item.EmisorRFC,
                Estado = item.Estado,
                FechaEmision = item.FechaEmision,
                FechaNuevo = item.FechaNuevo,
                IdComprobante = item.IdRemision,
                IdDocumento = item.IdDocumento,
                //NumParcialidad = item.NumParcialidad,
                ReceptorNombre = item.ReceptorNombre,
                ReceptorRFC = item.ReceptorRFC,
                Serie = item.Serie,
                Version = item.Version,
                Total = item.Total,
                SubTipoComprobante = CFDISubTipoEnum.Emitido,
                TipoComprobanteText = "Ingreso",
                Folio = item.Folio.ToString()
            };
            return movimiento;
        }
    }
}
