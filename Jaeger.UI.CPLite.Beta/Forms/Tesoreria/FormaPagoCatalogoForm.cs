﻿using System;
using Jaeger.Aplication.CPLite.Banco.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Tesoreria {
    public class FormaPagoCatalogoForm : UI.Banco.Forms.FormaPagoCatalogoForm {
        public FormaPagoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Service = new BancoFormaPagoService();
            this.Load += this.FormaPagoCatalogoForm_Load;
        }

        private void FormaPagoCatalogoForm_Load(object sender, EventArgs e) {
        }

        //public override void OnLoad() {
        //    base.OnLoad();  
        //}
    }
}
