﻿using Jaeger.Domain.Almacen.Builder;

namespace Jaeger.Aplication.CPLite.Almacen.Services {
    public class CatalogoModeloService : Aplication.Almacen.Services.CatalogoModeloService, Aplication.Almacen.Contracts.ICatalogoModeloService {
        #region metodos estaticos
        /// <summary>
        /// consulta para productos - modelos
        /// </summary>
        public new static IModelosQueryBuilder Query() {
            return new  ModelosQueryBuilder();
        }
        #endregion
    }
}
