﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Services {
    public static class GridValeAlmacenService {
        /// <summary>
        /// obtener columnas para el grid de vales de almacen
        /// </summary>
        public static GridViewDataColumn[] GetValeAlmacenColumns() {
            return new List<GridViewDataColumn>() {
                GridCommonService.ColFolio,
                GridCommonService.ColIdPedido,
                ColIdTipo,
                GridCommonService.ColCboIdStatus,
                ColIdDepartamento,
                ColClave,
                ColRepcetor,
                GridCommonService.ColFechaEmision,
                ColContacto,
                ColNota,
                GridCommonService.ColFechaCancela,
                GridCommonService.ColCancela,
                ColClaveCancelacion,
                ColNotaCancelacion,
                ColPDF,
                GridCommonService.ColCreo,
                GridCommonService.ColFechaNuevo
            }.ToArray();
        }

        /// <summary>
        /// obtener columnas para el grid de devoluciones del almacen
        /// </summary>
        public static GridViewDataColumn[] GetDevolucionColumns() {
            return new List<GridViewDataColumn>() {
                GridCommonService.ColFolio,
                GridCommonService.ColIdPedido,
                GridValeAlmacenService.ColIdTipo,
                GridCommonService.ColCboIdStatus,
                GridValeAlmacenService.ColIdDepartamento,
                GridValeAlmacenService.ColClave,
                GridValeAlmacenService.ColRepcetor,
                GridCommonService.ColFechaEmision,
                GridValeAlmacenService.ColContacto,
                GridValeAlmacenService.ColNota,
                GridCommonService.ColFechaCancela,
                GridCommonService.ColCancela,
                GridValeAlmacenService.ColPDF,
                GridCommonService.ColSubTotal,
                GridCommonService.ColTotalTrasladoIVA,
                GridCommonService.ColTotal,
                GridCommonService.ColCreo,
                GridCommonService.ColFechaNuevo,
                GridValeAlmacenService.ColNumeros
            }.ToArray();
        }

        /// <summary>
        /// obtener columnas para el grid de devoluciones del almacen
        /// </summary>
        public static GridViewDataColumn[] GetDevolucionColumnsView() {
            return new List<GridViewDataColumn>() {
                GridCommonService.ColFolio,
                GridCommonService.ColIdPedido,
                GridValeAlmacenService.ColClave,
                GridValeAlmacenService.ColRepcetor,
                GridCommonService.ColFechaEmision,
                GridCommonService.ColTotal,
                GridCommonService.ColCreo,
                GridCommonService.ColFechaNuevo,
            }.ToArray();
        }

        /// <summary>
        /// solo lo hice para pruebas
        /// </summary>
        public static GridViewTextBoxColumn ColNumeros {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NPartida",
                    HeaderText = "Partidas",
                    Name = "NPartida",
                    Width = 50,
                    TextAlignment = ContentAlignment.MiddleCenter,
                    FormatString = "{0:N0}"
                };
            }
        }
        #region columnas para vales de almacen y devoluciones
        public static GridViewComboBoxColumn ColIdTipo {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "IdTipo",
                    HeaderText = "Documento",
                    Name = "IdTipo",
                    Width = 120
                };
            }
        }

        public static GridViewComboBoxColumn ColIdDepartamento {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "IdDepartamento",
                    HeaderText = "Departamento",
                    Name = "IdDepartamento",
                    Width = 120
                };
            }
        }

        public static GridViewTextBoxColumn ColClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorClave",
                    HeaderText = "Clave",
                    Name = "ReceptorClave",
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColRepcetor {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Receptor",
                    HeaderText = "Cliente",
                    Name = "Receptor",
                    Width = 220
                };
            }
        }

        public static GridViewDateTimeColumn ColFechaIngreso {
            get {
                return new GridViewDateTimeColumn {
                    FieldName = "FechaIngreso",
                    Format = DateTimePickerFormat.Custom,
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Ingreso",
                    Name = "FechaIngreso",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColContacto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Contacto",
                    HeaderText = "Contacto",
                    Name = "Contacto",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColPDF {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "UrlFilePDF",
                    HeaderText = "PDF",
                    Name = "UrlFilePDF",
                    TextAlignment = ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Observaciones",
                    Name = "Nota",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveCancelacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveCancelacion",
                    HeaderText = "Cv. Cancelacion",
                    Name = "ClaveCancelacion",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColNotaCancelacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NotaCancelacion",
                    HeaderText = "Nota Cancela",
                    Name = "NotaCancelacion",
                    TextAlignment = ContentAlignment.MiddleLeft,
                    Width = 150,
                    IsVisible = false
                };
            }
        }
        #endregion
    }
}
