﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class RemisionadoPorCobrarForm : RemisionadoForm {
        public RemisionadoPorCobrarForm(UIMenuElement menuElement, IRemisionadoService service) : base(menuElement) {
            this._Service = service;
            this.TRemision.ShowPeriodo = false;
            this.TRemision.ShowEjercicio = false;
            this.TRemision.ShowEditar = false;
            this.TRemision.ShowNuevo = false;
            this.Load += RemisionadoPorCobrarForm_Load;
        }

        private void RemisionadoPorCobrarForm_Load(object sender, System.EventArgs e) {
            this.Text = "Cliente: Remisionado por Cobrar";
        }

        public override void Actualizar() {
            this._DataSource = new BindingList<RemisionSingleModel>(
                this._Service.GetList<RemisionSingleModel>(
                    Aplication.Almacen.PT.Services.RemisionadoService.Query().WithIdStatus(Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Build()).ToList()
                );
        }
    }
}
