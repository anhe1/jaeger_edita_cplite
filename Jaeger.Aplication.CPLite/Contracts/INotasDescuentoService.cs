﻿using System.ComponentModel;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Ventas.Entities;
using System.Collections.Generic;

namespace Jaeger.Aplication.CPLite.Contracts {
    public interface INotasDescuentoService : INotaDescuentoService {
        BindingList<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new();
    }
}
