﻿namespace Jaeger.Aplication.CPLite.Banco.Builder {
    /// <summary>
    /// configuracion de parametros de la seccion bancos
    /// </summary>
    public class ConfigurationBuilder : Aplication.Banco.Builder.ConfigurationBuilder, Aplication.Banco.Builder.IConfigurationBuilder, Aplication.Banco.Builder.IConfiguracionBuild {
        public ConfigurationBuilder() : base() {
            // aqui sobre escribimos el indice del recibo de cobro por las diferencias
            this._IConfiguration.IdReciboCobro = 1;
        }
    }
}
