﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.DataAccess.FB.Contribuyentes.Repositories;

namespace Jaeger.Aplication.CPLite.Ventas.Services {
    /// <summary>
    /// Vendedor
    /// </summary>
    public class VendedorService : Contribuyentes.Services.VendedorService, IVendedorService {
        /// <summary>
        /// constructor
        /// </summary>
        public VendedorService() : base() { }

        protected override void OnLoad() {
            this.vendedorRepository = new SqlFbVendedorRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.carteraRepository = new SqlFbVendedorCarteraRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener vendedor
        /// </summary>
        public override IVendedor2DetailModel GetById(int index) {
            var response = this.vendedorRepository.GetList<Vendedor2DetailModel>(new List<IConditional> {
                new Conditional("DRCTRR_DRCTR_ID", index.ToString())
            }).FirstOrDefault();

            if (response != null) {
                response.Clientes = new BindingList<IContribuyenteVendedorModel>(this.carteraRepository.GetClientes<ContribuyenteVendedorModel>(new List<IConditional> { new Conditional("DRCTRC_VNDR_ID", index.ToString()) }).ToList<IContribuyenteVendedorModel>());
            }
            return response;
        }

        /// <summary>
        /// almacenar
        /// </summary>
        public override IVendedor2DetailModel Saveable(IVendedor2DetailModel vendedor) {
            if (this.vendedorRepository.Saveable(vendedor)) {
                // clientes relacionados
                for (int i = 0; i < vendedor.Clientes.Count; i++) {
                    if (vendedor.Clientes[i].SetModified)
                        this.carteraRepository.Salveable(vendedor.Clientes[i]);
                }
            }
            return vendedor;
        }
    }
}
