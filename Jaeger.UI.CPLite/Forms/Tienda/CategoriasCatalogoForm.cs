﻿using System;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    public class CategoriasCatalogoForm : UI.Tienda.Forms.CategoriasCatalogoForm { 
        public CategoriasCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += this.CategoriasCatalogoForm_Load;
        }

        private void CategoriasCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new CategoriaService();
        }
    }
}
