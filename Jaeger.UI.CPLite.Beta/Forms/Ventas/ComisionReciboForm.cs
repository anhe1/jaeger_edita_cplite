﻿using System;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Ventas.Services;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public class ComisionReciboForm : UI.Banco.Forms.MovimientoBancarioForm {
        protected IComisionService comisionService;

        public ComisionReciboForm() : base() {
            this.Load += ReciboCobroForm_Load;
        }

        public ComisionReciboForm(Aplication.Banco.IBancoService service, MovimientoBancarioDetailModel movimiento) : base(service) {
            this.Text = "Recibo de Comisión";
            this.Load += ReciboCobroForm_Load;
            this.TMovimiento.Guardar.Click += Guardar_Click;
            this.comisionService = new ComisionService();
        }

        private void ReciboCobroForm_Load(object sender, EventArgs e) {
            this.Text = "Recibo de Comisión";
            this.TMovimiento.ShowHerramientas = false;
            this.TMovimiento.Guardar.Click += Guardar_Click;
            this.comisionService = new ComisionService();
        }

        protected override void TMovimiento_Imprimir_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento != null) {
                var imprimir = new ReporteForm(new MovimientoBancarioPrinter(this.TMovimiento.Movimiento));
                imprimir.Show();
            }
        }

        private void Guardar_Click(object sender, EventArgs e) {
            if (this.TMovimiento.Movimiento.Id > 0 && this.TMovimiento.Movimiento.Status != MovimientoBancarioStatusEnum.Cancelado) {
                var _indices = this.TMovimiento.Movimiento.Comprobantes.Select(it => it.IdComprobante).ToArray();
                // actualizar bandera de registro de comisiones RMSNC_CTDIS
                this.comisionService.Update(_indices, 1);
            }
        }

        protected override void TComprobante_Buscar_Click(object sender, EventArgs e) {
            if (this.ConceptoMovimiento.SelectedItem != null) {
                this._CurrentTipoMovimiento = ((GridViewDataRowInfo)this.ConceptoMovimiento.SelectedItem).DataBoundItem as MovimientoConceptoDetailModel;
                var buscar = new ComprobantesBuscarForm(this._CurrentTipoMovimiento, this.TMovimiento.Movimiento.IdDirectorio);
                buscar.AgregarComprobante += Buscar_AgregarComprobante;
                buscar.ShowDialog(this);
            } else {
                RadMessageBox.Show(this, "No selecciono tipo de movimiento", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void Buscar_AgregarComprobante(object sender, MovimientoBancarioComprobanteDetailModel e) {
            if (e != null) {
                if (e.Tag != null) {
                    var _comisionPorPagar = (decimal)e.Tag;
                    if (_comisionPorPagar <= 0) {
                        RadMessageBox.Show("Este documento no tiene asignado una comisión por pagar.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }
                if (this.TMovimiento.Movimiento.Agregar(e) == true) {
                    RadMessageBox.Show("Properties.Resources.Msg_Banco_Comprobante_Duplicado", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                } else {
                    if (e.Tag != null) {
                        var _comisionPorPagar = (decimal)e.Tag;
                        e.Abono = _comisionPorPagar;
                    }
                }
            }
        }

        protected override void GetTiposMovimiento() {
            this._TiposMovimiento = new System.ComponentModel.BindingList<MovimientoConceptoDetailModel>(
                this.TMovimiento.Service.GetList<MovimientoConceptoDetailModel>(Aplication.Banco.BancoConceptoService.Query().Efecto(MovimientoBancarioEfectoEnum.Egreso).Build()
                ).ToList());
        }
    }
}
