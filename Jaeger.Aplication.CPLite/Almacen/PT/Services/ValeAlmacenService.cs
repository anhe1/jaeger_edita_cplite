﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Almacen.PT.Services {
    public class ValeAlmacenService : Aplication.Almacen.PT.Services.ValeAlmacenService, Aplication.Almacen.Contracts.IAlmacenService, IValeAlmacenService {

        public ValeAlmacenService() {
            this.almacenRepository = new SqlFbValeAlmacenRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.relacionRepository = new SqlFbValeRelacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.statusRepository = new SqlFbValeStatusRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.movimientoAlmacen = new SqlFbMovimientoAlmacenRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
