﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Aplication.Contribuyentes.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class ClienteBuscarForm : RadForm {
        protected Aplication.Contribuyentes.Contracts.IDirectorioService service;
        public BindingList<ContribuyenteDetailModel> datos;
        protected BindingList<ItemSelectedModel> relationType;

        public event EventHandler<ContribuyenteDetailModel> Selected;
        public void OnSelected(ContribuyenteDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public ClienteBuscarForm() {
            InitializeComponent();
            this.relationType = null;
        }

        public ClienteBuscarForm(BindingList<ItemSelectedModel> relationType) {
            InitializeComponent();
            this.relationType = relationType;
        }

        private void ClienteBuscarForm_Load(object sender, EventArgs e) {
            this.dataGridDirectorio.Standard();
            this.service = new DirectorioToService();
            this.ToolBar.Descripcion.TextChanged += Buscar_TextChanged;
        }

        private void Buscar_TextChanged(object sender, EventArgs e) {
            if (this.ToolBar.Descripcion.Text.Length > 4) {
                this.ToolBar.Buscar.PerformClick();
            }
        }

        private void TCliente_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.dataGridDirectorio.DataSource = this.datos;
            this.dataGridDirectorio.Select();
        }

        private void TCliente_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void dataGridDirectorio_DoubleClick(object sender, EventArgs e) {
            if (this.dataGridDirectorio.CurrentRow != null) {
                var seleccionado = this.dataGridDirectorio.CurrentRow.DataBoundItem as ContribuyenteDetailModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                    this.ToolBar.Cerrar.PerformClick();
                }
            }
        }

        private void Consultar() {
            var _busqueda = this.ToolBar.Descripcion.Text.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u").ToLower();
            if (_busqueda.Length > 0) {
                //if (this.relationType != null) {
                //    this.datos = new BindingList<ContribuyenteDetailModel>(this.service.GetBeneficiarios<ContribuyenteDetailModel>(this.relationType, _busqueda));
                //} else {
                //this.datos = new BindingList<ContribuyenteDetailModel>(this.service.GetList(_busqueda, true));
                //}
            }
        }
    }
}
