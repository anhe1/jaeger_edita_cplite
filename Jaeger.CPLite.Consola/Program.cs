﻿using Jaeger.Aplication.Base.Services;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.Domain.Almacen.Builder;

namespace Jaeger.CPLite.Consola {
    internal class Program {
        static void Main(string[] args) {
            var programa = new Logger();
            if (programa.Login(args)) {
                ConfigService.Synapsis.RDS.LiteCP.Database = "localhost/3050:/dbs/jaeger_ipr981125pn9b.fdb";
                ConfigService.Synapsis.RDS.LiteCP.Charset = "UTF8";
                var test = new SqlFbMovimientoAlmacenRepository(ConfigService.Synapsis.RDS.LiteCP, "TEST");
                IMovimientoAlmacenQueryBuilder queryBuilder = new MovimientoAlmacenQueryBuilder();
                test.Existencia(queryBuilder.Documento(4).IsCancel(false).Build());
            }
        }
    }

    internal class TestingAlmacen {
        public TestingAlmacen() { }
    }
}
