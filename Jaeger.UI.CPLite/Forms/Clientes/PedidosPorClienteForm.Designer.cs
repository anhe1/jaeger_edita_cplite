﻿namespace Jaeger.UI.CPLite.Forms.Clientes {
    partial class PedidosPorClienteForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.TPedido = new Jaeger.UI.Tienda.Forms.PedidosGridControl();
            this.Nombre = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TPedido
            // 
            this.TPedido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TPedido.Location = new System.Drawing.Point(0, 0);
            this.TPedido.Name = "TPedido";
            this.TPedido.PDF = null;
            this.TPedido.ShowActualizar = true;
            this.TPedido.ShowAutosuma = false;
            this.TPedido.ShowCancelar = false;
            this.TPedido.ShowCerrar = true;
            this.TPedido.ShowEditar = true;
            this.TPedido.ShowEjercicio = true;
            this.TPedido.ShowExportarExcel = false;
            this.TPedido.ShowFiltro = true;
            this.TPedido.ShowHerramientas = false;
            this.TPedido.ShowImprimir = false;
            this.TPedido.ShowItem = false;
            this.TPedido.ShowNuevo = true;
            this.TPedido.ShowPeriodo = true;
            this.TPedido.ShowSeleccionMultiple = true;
            this.TPedido.Size = new System.Drawing.Size(800, 450);
            this.TPedido.TabIndex = 0;
            // 
            // Nombre
            // 
            this.Nombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Nombre.AutoSizeDropDownColumnMode = Telerik.WinControls.UI.BestFitColumnMode.DisplayedCells;
            this.Nombre.AutoSizeDropDownHeight = true;
            this.Nombre.AutoSizeDropDownToBestFit = true;
            this.Nombre.DisplayMember = "Nombre";
            // 
            // Nombre.NestedRadGridView
            // 
            this.Nombre.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nombre.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nombre.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nombre.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nombre.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nombre.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdCliente";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdCliente";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Denominación o Razón Social";
            gridViewTextBoxColumn2.Name = "Nombre";
            gridViewTextBoxColumn2.Width = 250;
            gridViewTextBoxColumn3.FieldName = "RFC";
            gridViewTextBoxColumn3.HeaderText = "RFC";
            gridViewTextBoxColumn3.Name = "RFC";
            gridViewTextBoxColumn3.Width = 105;
            this.Nombre.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Nombre.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nombre.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nombre.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Nombre.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Nombre.EditorControl.Name = "NestedRadGridView";
            this.Nombre.EditorControl.ReadOnly = true;
            this.Nombre.EditorControl.ShowGroupPanel = false;
            this.Nombre.EditorControl.Size = new System.Drawing.Size(380, 150);
            this.Nombre.EditorControl.TabIndex = 0;
            this.Nombre.Location = new System.Drawing.Point(601, 0);
            this.Nombre.Name = "Nombre";
            this.Nombre.NullText = "Denominación o Razon Social ";
            this.Nombre.Size = new System.Drawing.Size(210, 20);
            this.Nombre.TabIndex = 13;
            this.Nombre.TabStop = false;
            this.Nombre.ValueMember = "IdCliente";
            // 
            // PedidosPorClienteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.TPedido);
            this.Name = "PedidosPorClienteForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "PedidosPorClienteForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PedidosPorClienteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.Tienda.Forms.PedidosGridControl TPedido;
        public Telerik.WinControls.UI.RadMultiColumnComboBox Nombre;
    }
}