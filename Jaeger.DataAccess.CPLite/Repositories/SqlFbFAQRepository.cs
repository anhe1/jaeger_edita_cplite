﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    /// <summary>
    /// repositorio: tienda web seccion preguntas frecuentes
    /// </summary>
    public class SqlFbFAQRepository : Abstractions.RepositoryMaster<PreguntaFrecuenteModel>, ISqlFAQRepository {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="configuracion">configuracion de la base de datos</param>
        public SqlFbFAQRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE FROM TWFAQ WHERE TWFAQ_ID = @TWFAQ_ID"
            };
            sqlCommand.Parameters.AddWithValue("@TWFAQ_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PreguntaFrecuenteModel GetById(int index) {
            return this.GetList<PreguntaFrecuenteModel>(new List<IConditional> { new Conditional("TWFAQ_ID", index.ToString()) }).FirstOrDefault();
        }

        public int Insert(PreguntaFrecuenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO TWFAQ ( TWFAQ_ID, TWFAQ_A, TWFAQ_QSTN, TWFAQ_ANSWR, TWFAQ_USU_N, TWFAQ_FN)
                                           VALUES (@TWFAQ_ID,@TWFAQ_A,@TWFAQ_QSTN,@TWFAQ_ANSWR,@TWFAQ_USU_N,@TWFAQ_FN) RETURNING TWFAQ_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@TWFAQ_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_QSTN", item.Pregunta);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_ANSWR", item.Respuesta);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_FN", item.FechaNuevo);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(PreguntaFrecuenteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE TWFAQ SET TWFAQ_A = @TWFAQ_A, TWFAQ_QSTN = @TWFAQ_QSTN, TWFAQ_ANSWR = @TWFAQ_ANSWR, TWFAQ_USU_N = @TWFAQ_USU_N, TWFAQ_FN = @TWFAQ_FN WHERE TWFAQ_ID = @TWFAQ_ID"
            };

            sqlCommand.Parameters.AddWithValue("@TWFAQ_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_QSTN", item.Pregunta);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_ANSWR", item.Respuesta);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@TWFAQ_FN", item.FechaNuevo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<PreguntaFrecuenteModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM TWFAQ"
            };
            return this.GetMapper<PreguntaFrecuenteModel>(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM TWFAQ @condiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public PreguntaFrecuenteModel Save(PreguntaFrecuenteModel model) {
            model.Creo = this.User;
            model.FechaNuevo = DateTime.Now;
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}

