SET TERM ^ ;
CREATE PROCEDURE FECHA_ULTIMOPAGO_REMISIONES (
    START_ID DOM_ID DEFAULT 0,
    END_ID DOM_ID DEFAULT 0,
    STATUS_ID DOM_ID DEFAULT 0 )
AS
declare variable id    dom_id;
declare variable fecha dom_fh;
declare variable fecha2 dom_fh;
declare variable dias  dom_id;
begin
/*  */ 

    for 
        select ctlrms.ctlrms_id, ctlrms.ctlrms_fcemsn, drctr.drctr_dscrdt
        from ctlrms, drctr
        where ctlrms.ctlrms_drctr_id = drctr.drctr_id and ctlrms.ctlrms_fcultpg is null
        and ctlrms.ctlrms_sttsdcs_id = :STATUS_ID
        and ctlrms.ctlrms_id between :start_id and :end_id
        into :id, :fecha, :dias
    do
        begin
            fecha2 =null;
            select max ( cbrnz.cntbl_fecdocs )
            from cntbl rlcncbrnz, cntbl cbrnz
            where   rlcncbrnz.cntbl_a = 1
                and rlcncbrnz.cntbl_doc_id = 2800                --< solo documentos relacionados
                and cbrnz.cntbl_doc_id = 28  
                and cbrnz.cntbl_sttsdcs_id = 3
                and rlcncbrnz.cntbl_cntbl_id = cbrnz.cntbl_id
                and rlcncbrnz.cntbl_rem_id = :id
            into :fecha2;
            if (:fecha2 is not null or fecha2 <> 0 ) then
                begin
                    update ctlrms set ctlrms_fcultpg = :fecha2 where ctlrms_id = :id;
                end
        end

end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE FECHA_ULTIMOPAGO_REMISIONES TO  SYSDBA;

