﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.Pivot.Core;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Ventas.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class ResumenForm : RadForm {
        #region declaraciones
        protected internal IVentasService _Service;
        protected List<VentasResumenModel> _DataSource;
        #endregion

        public ResumenForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void RemisionResumenForm_Load(object sender, EventArgs e) {
            this.TResumen.TPivot.ShowItem = true;
            this.TResumen.TPivot.HostItem.HostedItem = this.Resumen.MultiColumnComboBoxElement;
            this.TResumen.TPivot.HostItem.Size = new System.Drawing.Size(100, 20);
            this.TResumen.TPivot.HostItem.MinSize = new System.Drawing.Size(100, 20);
            this.Resumen.EditorControl.ShowRowHeaderColumn = false;
            this.Resumen.EditorControl.AllowAutoSizeColumns = true;
            this.Resumen.MultiColumnComboBoxElement.MultiColumnPopupForm.MinimumSize = this.TResumen.TPivot.HostItem.MinSize;
            this.Resumen.MultiColumnComboBoxElement.MultiColumnPopupForm.Size = this.TResumen.TPivot.HostItem.MinSize;
            
            ///configuracion de la tabla pivote
            this.TResumen.PivotGrid.AggregateDescriptions.Add(new PropertyAggregateDescription {
                CustomName = "Cantidad",
                PropertyName = "Cantidad",
                StringFormat = "#,###0",
                StringFormatSelector = null,
                TotalFormat = null,
                AggregateFunction = new SumAggregateFunction()
            });

            this.TResumen.PivotGrid.RowGroupDescriptions.Add(new PropertyGroupDescription() {
                CustomName = "Catálogo",
                GroupComparer = new GroupNameComparer(),
                GroupFilter = null,
                PropertyName = "Catalogo",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending
            });

            this.TResumen.PivotGrid.RowGroupDescriptions.Add(new PropertyGroupDescription() {
                CustomName = "Modelo",
                GroupComparer = new GroupNameComparer(),
                GroupFilter = null,
                PropertyName = "Modelo",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending
            });

            this.TResumen.PivotGrid.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() {
                CustomName = "Año",
                GroupComparer = new GroupNameComparer(),
                GroupFilter = null,
                PropertyName = "FechaPedido",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending,
                Step = DateTimeStep.Year
            });

            this.TResumen.PivotGrid.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() {
                CustomName = "Mes",
                GroupComparer = new GroupNameComparer(),
                GroupFilter = null,
                PropertyName = "FechaPedido",
                ShowGroupsWithNoData = false,
                SortOrder = SortOrder.Ascending,
                Step = DateTimeStep.Month
            });

            this.TResumen.PivotGrid.PivotGridElement.RowHeadersLayout = PivotLayout.Compact;

            this._Service = new VentasService();
            this.Resumen.DisplayMember = "Descriptor";
            this.Resumen.ValueMember = "Id";
            this.Resumen.DataSource = VentasService.GetResumenTipo();
            this.TResumen.TPivot.Actualizar.Click += TResumen_Actualizar_Click;
            this.TResumen.TPivot.Cerrar.Click += Cerrar_Click;
        }

        private void TResumen_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TResumen.PivotGrid.DataSource = this._DataSource;
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            bool isRemision;
            if (this.Resumen.SelectedValue != null) {
                isRemision = (int)this.Resumen.SelectedValue == 2;
            } else {
                isRemision = this.Resumen.Text.ToLower().Contains("remis");
            }
            if (isRemision) {
                this._DataSource = this._Service.GetList<VentasResumenModel>(VentasService.Query().WithRemisionado().Year(this.TResumen.TPivot.GetEjercicio()).Build()).ToList();
            } else {
                this._DataSource = this._Service.GetList<VentasResumenModel>(VentasService.Query().WithPedido().Year(TResumen.TPivot.GetEjercicio()).Build()).ToList();
            }
        }
    }
}
