SET TERM ^ ;
CREATE PROCEDURE COMSN_COMISION0 (
    IN_CMSN DOM_ID DEFAULT 0,
    IN_DIAS DOM_ID DEFAULT 0,
    IN_FECH DOM_FH,
    IN_DESC DOM_MONEDA DEFAULT 0,
    IN_VENT DOM_MONEDA DEFAULT 0 )
RETURNS (
    OUT_CTLG DOM_STR50,
    OUT_CMSN DOM_STR50,
    OUT_INTG DOM_STR50,
    OUT_FCT1 DOM_MONEDA,
    OUT_FCT2 DOM_MONEDA )
AS
BEGIN
/*
Elabora: Anhe.13092012
Proposito: devuelve los calculos de la comisi�n aplicando las tablas correspondientes de las tablas de descuentoa al cliente y las multas al vendedor
por la cobranza.
Revisiones:
    07112012: se modifica la consulta para el calculo de la comisi�n otorgada al vendedor. tambien se identifican partes
        de la consulta como referencia para futuras modificaciones.
*/ 
    if ( :in_cmsn is null ) then in_cmsn = 0;
    
    select first 1 cms.cmsdsc_nom, tbl1.tbrngs_nom, tbl2.tbrngs_nom, tbl1.tbrngs_factor, tbl2.tbrngs_factor
    from cmsdsc cms, tbrngs tbl1, tbrngs tbl2
    where   cms.cmsdsc_doc_id = 1016                                    --< catalogos de comision
        and tbl1.tbrngs_doc_id = 1037                                   --< rangos de descuentos en comisiones
        and tbl2.tbrngs_doc_id = 1038                                   --< rangos de dias de cobranza para multa de comisi�n
        and tbl1.tbrngs_cmsdsc_id = cms.cmsdsc_id
        and tbl2.tbrngs_cmsdsc_id = cms.cmsdsc_id
        and cms.cmsdsc_a = 1
        and tbl1.tbrngs_a = 1
        and tbl2.tbrngs_a = 1
        and cms.cmsdsc_id = :IN_CMSN                                    -- <-Aqui catalogo a usar
        and :in_desc between tbl1.tbrngs_rng1 and tbl1.tbrngs_rng2      -- <-Aqui rango del descuento
        and :in_dias between tbl2.tbrngs_rng1 and tbl2.tbrngs_rng2      -- <-Aqui dias transcurridos para la cobranza
        and :in_fech between cms.cmsdsc_fchini and cms.cmsdsc_fchvig    -- <-Aqui la vigencia del catalogo
        and :in_vent between cms.cmsdsc_opc_mnd0$ and cms.cmsdsc_opc_mnd1$  --< monto de la venta al cliente
    order by cms.cmsdsc_fchini                                          -- <- siempre ordenado por la fecha de la vigencia
    into
        :out_ctlg, 
        :out_cmsn,
        :out_intg,
        :out_fct1,
        :out_fct2;
        
    if ( out_ctlg is null ) then
        begin
            select first 1 cms.cmsdsc_nom, tbl1.tbrngs_nom, tbl2.tbrngs_nom, tbl1.tbrngs_factor, tbl2.tbrngs_factor
            from cmsdsc cms, tbrngs tbl1, tbrngs tbl2
            where   cms.cmsdsc_doc_id = 1016                                    --< catalogos de comision
                and tbl1.tbrngs_doc_id = 1037                                   --< rangos de descuentos en comisiones
                and tbl2.tbrngs_doc_id = 1038                                   --< rangos de dias de cobranza para multa de comisi�n
                and tbl1.tbrngs_cmsdsc_id = cms.cmsdsc_id
                and tbl2.tbrngs_cmsdsc_id = cms.cmsdsc_id
                and cms.cmsdsc_sec_id = 1                                       -- <-Aqui catalogo a usar
                and cms.cmsdsc_a = 1
                and tbl1.tbrngs_a = 1
                and tbl2.tbrngs_a = 1
                and :in_desc between tbl1.tbrngs_rng1 and tbl1.tbrngs_rng2      -- <-Aqui rango del descuento
                and :in_dias between tbl2.tbrngs_rng1 and tbl2.tbrngs_rng2      -- <-Aqui dias transcurridos para la cobranza
                --and '10/05/2012' between cms.cmsdsc_fchini and cms.cmsdsc_fchvig    -- <-Aqui la vigencia del catalogo
                --and 100 between cms.cmsdsc_opc_mnd0$ and cms.cmsdsc_opc_mnd1$
            order by cms.cmsdsc_fchini                                          -- <- siempre ordenado por la fecha de la vigencia
                into
                    :out_ctlg, 
                    :out_cmsn,
                    :out_intg,
                    :out_fct1,
                    :out_fct2;
        end
    if ( out_ctlg is null ) then
        begin
            out_ctlg = 'N/C';
            out_cmsn = 'N/C';
            out_intg = 0;
            out_fct1 = 0;
            out_fct2 = 0;
        end
    suspend ;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE COMSN_COMISION0 TO  SYSDBA;

