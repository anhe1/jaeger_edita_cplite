SET TERM ^ ;
CREATE PROCEDURE GET_HEX_UUID
RETURNS (
    REAL_UUID Char(16) CHARACTER SET OCTETS,
    HEX_UUID Varchar(32) )
AS
declare variable i integer;
declare variable c integer;
BEGIN
real_uuid = GEN_UUID();
hex_uuid = '';
i = 0;
while (i < 16) do
begin
c = ascii_val(substring(real_uuid from i+1 for 1));
if (c < 0) then c = 256 + c;
hex_uuid = hex_uuid
|| substring('0123456789abcdef' from bin_shr(c, 4) + 1 for 1)
|| substring('0123456789abcdef' from bin_and(c, 15) + 1 for 1);
i = i + 1;
end
suspend;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE GET_HEX_UUID TO  SYSDBA;

