/* **************************************************************************************************************
-- IMPORTACIÓN DE AUTORIZACIONES DE CAMBIO DE STATUS DE REMISIONES
 ************************************************************************************************************** */
 INSERT INTO RMSNS (RMSNS_ID, RMSNS_A, RMSNS_RMSN_STTSA_ID, RMSNS_RMSN_ID, RMSNS_NOTA, RMSNS_USR_N, RMSNS_FN, RMSNS_DRCTR_ID)
SELECT 
a.AUTDCS_ID           AS RMSNS_ID, 
a.AUTDCS_A            AS RMSNS_A,
a.AUTDCS_STTSAUT_ID   AS RMSNS_RMSN_STTSA_ID, 
a.AUTDCS_CTLRMS_ID    AS RMSNS_RMSN_ID, 
a.AUTDCS_OBSRV        AS RMSNS_NOTA, 
a.AUTDCS_USU_N        AS RMSNS_USR_N, 
a.AUTDCS_FN           AS RMSNS_FN, 
CTLRMS.CTLRMS_DRCTR_ID AS RMSNS_DRCTR_ID
FROM AUTDCS a
LEFT JOIN CTLRMS ON a.AUTDCS_CTLRMS_ID = CTLRMS.CTLRMS_ID
WHERE AUTDCS_DOC_ID=26 --< REMISIONES