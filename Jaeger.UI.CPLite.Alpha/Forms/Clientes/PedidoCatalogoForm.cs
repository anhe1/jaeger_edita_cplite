﻿using System;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.Tienda.Services;
using Jaeger.Aplication.Contribuyentes.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class PedidoCatalogoForm : UI.Tienda.Forms.PedidoCatalogoForm {
        protected internal IVendedoresService vendedorService;
        protected internal RadMenuItem menuContextualAutorizar = new RadMenuItem { Text = "Autorizar" };

        public PedidoCatalogoForm(UIMenuElement menuElement) : base() {
            this.TPedido.Permisos = new UIAction(menuElement.Permisos);
            this.Load += this.PedidoCatalogoForm_Load;
        }

        public virtual void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Cliente: Pedidos";
            this.TPedido.Service = new PedidosService();    
            this.vendedorService = new VendedoresService();
            this.TPedido.menuContextual.Items.Add(this.menuContextualAutorizar);
            this.TPedido.Nuevo.Enabled = this.TPedido.Permisos.Agregar;
            this.TPedido.Cancelar.Enabled = this.TPedido.Permisos.Cancelar;
            this.TPedido.Editar.Enabled = this.TPedido.Permisos.Editar;
            this.TPedido.ShowExportarExcel = this.TPedido.Permisos.Exportar;
            this.menuContextualAutorizar.Enabled = this.TPedido.Permisos.Autorizar;
        }

        public override void Vendedores() {
            this.TPedido.Vendedores = this.vendedorService.GetList(false).ToList();
        }
    }
}
