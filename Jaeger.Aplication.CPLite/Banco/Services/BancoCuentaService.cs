﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.Banco.Repositories;

namespace Jaeger.Aplication.CPLite.Banco.Services {
    /// <summary>
    /// servicio para cuentas bancarias
    /// </summary>
    public class BancoCuentaService : Jaeger.Aplication.Banco.BancoCuentaService, Aplication.Banco.IBancoCuentaService {
        public BancoCuentaService() { }

        public override void Onload() {
            this.cuentaBancariaRepository = new SqlFbCuentaBancariaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.cuentaSaldoRepository = new SqlFbCuentaSaldoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
