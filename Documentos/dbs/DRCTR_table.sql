CREATE TABLE DRCTR
(
  DRCTR_ID Integer DEFAULT 0 NOT NULL,
  DRCTR_A Smallint DEFAULT 1 NOT NULL,
  DRCTR_DOC_ID Integer DEFAULT 0,
  DRCTR_USR_ID Smallint,
  DRCTR_WEB_ID Integer DEFAULT 0,
  DRCTR_RGF_ID Integer DEFAULT 0,
  DRCTR_SYNC_ID Integer DEFAULT 0,
  DRCTR_PRCS_ID Integer DEFAULT 0,
  DRCTR_DSCNT_ID Integer DEFAULT 0,
  DRCTR_FACTOR Numeric(18,4) DEFAULT 0,
  DRCTR_FACIVA Numeric(18,4) DEFAULT 0,
  DRCTR_DSCRDT Integer DEFAULT 0,
  DRCTR_DSPCT Integer DEFAULT 0,
  DRCTR_SBRCRDT Numeric(18,4) DEFAULT 0,
  DRCTR_LMTCRDT Numeric(18,4) DEFAULT 0,
  DRCTR_USOCFDI Varchar(3),
  DRCTR_CLV Varchar(10),
  DRCTR_RESFIS Varchar(20),
  DRCTR_NMREG Varchar(40),
  DRCTR_CURP DOM_CURP COLLATE UTF8,
  DRCTR_RFC DOM_RFC COLLATE UTF8,
  DRCTR_TLFN Varchar(100),
  DRCTR_MAIL Varchar(100),
  DRCTR_WEB Varchar(100),
  DRCTR_OBSR Varchar(100),
  DRCTR_PSW Varchar(100),
  DRCTR_NOM Varchar(255),
  DRCTR_CDD Varchar(100),
  DRCTR_FN Timestamp DEFAULT current_timestamp   NOT NULL,
  DRCTR_USU_N Varchar(10),
  DRCTR_FM Timestamp,
  DRCTR_USU_M Varchar(10),
  DRCTR_FILE Blob sub_type 0,
  PRIMARY KEY (DRCTR_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'DRCTR_ID' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'DRCTR_A' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de precios'  where RDB$FIELD_NAME = 'DRCTR_PRCS_ID' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de descuentos'  where RDB$FIELD_NAME = 'DRCTR_DSCNT_ID' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor pactado del descuento al cliente'  where RDB$FIELD_NAME = 'DRCTR_FACTOR' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor iva factura'  where RDB$FIELD_NAME = 'DRCTR_FACIVA' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de credito'  where RDB$FIELD_NAME = 'DRCTR_DSCRDT' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias pactados de entrega'  where RDB$FIELD_NAME = 'DRCTR_DSPCT' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sobre credito'  where RDB$FIELD_NAME = 'DRCTR_SBRCRDT' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'lmite de credito'  where RDB$FIELD_NAME = 'DRCTR_LMTCRDT' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de uso de CFDI'  where RDB$FIELD_NAME = 'DRCTR_USOCFDI' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de sistema'  where RDB$FIELD_NAME = 'DRCTR_CLV' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del pais de residencia para efectos fiscales del receptor del comprobante'  where RDB$FIELD_NAME = 'DRCTR_RESFIS' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de registro de identidad fiscal'  where RDB$FIELD_NAME = 'DRCTR_NMREG' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave unica de registro de poblacion'  where RDB$FIELD_NAME = 'DRCTR_CURP' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes'  where RDB$FIELD_NAME = 'DRCTR_RFC' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'telefono de contacto'  where RDB$FIELD_NAME = 'DRCTR_TLFN' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'correo electronico de contacto'  where RDB$FIELD_NAME = 'DRCTR_MAIL' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'url del sitio web'  where RDB$FIELD_NAME = 'DRCTR_WEB' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'DRCTR_OBSR' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'contrasenia'  where RDB$FIELD_NAME = 'DRCTR_PSW' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o razon social del cliente o proveedor'  where RDB$FIELD_NAME = 'DRCTR_NOM' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ciudad'  where RDB$FIELD_NAME = 'DRCTR_CDD' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'DRCTR_FN' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'DRCTR_USU_N' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion'  where RDB$FIELD_NAME = 'DRCTR_FM' and RDB$RELATION_NAME = 'DRCTR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'DRCTR_USU_M' and RDB$RELATION_NAME = 'DRCTR';
CREATE INDEX IDX_DRCTR_PRCS_ID ON DRCTR (DRCTR_PRCS_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Directorio'
where RDB$RELATION_NAME = 'DRCTR';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON DRCTR TO  SYSDBA WITH GRANT OPTION;



CREATE GENERATOR GEN_DRCTR_ID;

SET TERM !! ;
CREATE TRIGGER DRCTR_BI FOR DRCTR
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1-12/10/2021 22:30
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.DRCTR_ID IS NULL) THEN
    NEW.DRCTR_ID = GEN_ID(GEN_DRCTR_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_DRCTR_ID, 0);
    if (tmp < new.DRCTR_ID) then
      tmp = GEN_ID(GEN_DRCTR_ID, new.DRCTR_ID-tmp);
  END
END!!
SET TERM ; !!
