﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbPedidoClienteRepository : Abstractions.RepositoryMaster<PedidoClienteModel>, ISqlPedidoClienteRepository {
        public SqlFbPedidoClienteRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM PDCLN WHERE ((PDCLN_ID = @PDCLN_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@PDCLN_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(PedidoClienteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO PDCLN ( PDCLN_ID, PDCLN_A, PDCLN_STTS_ID, PDCLN_ORGN_ID, PDCLN_FRMPG_ID, PDCLN_DRCTR_ID, PDCLN_DRCCN_ID, PDCLN_VNDR_ID, PDCLN_RQIRFCTR, PDCLN_AUT_ID, PDCLN_ENVCST, PDCLN_IVA, PDCLN_SBTTL, PDCLN_TTL, PDCLN_MTDENV, PDCLN_RCB, PDCLN_OBSRV, PDCLN_FCPED, PDCLN_FCREQ, PDCLN_FCHAUT, PDCLN_FCENTR, PDCLN_SYNC_ID, PDCLN_USU_AUT, PDCLN_USU_N, PDCLN_FN) 
                                           VALUES (@PDCLN_ID,@PDCLN_A,@PDCLN_STTS_ID,@PDCLN_ORGN_ID,@PDCLN_FRMPG_ID,@PDCLN_DRCTR_ID,@PDCLN_DRCCN_ID,@PDCLN_VNDR_ID,@PDCLN_RQIRFCTR,@PDCLN_AUT_ID,@PDCLN_ENVCST,@PDCLN_IVA,@PDCLN_SBTTL,@PDCLN_TTL,@PDCLN_MTDENV,@PDCLN_RCB,@PDCLN_OBSRV,@PDCLN_FCPED,@PDCLN_FCREQ,@PDCLN_FCHAUT,@PDCLN_FCENTR,@PDCLN_SYNC_ID,@PDCLN_USU_AUT,@PDCLN_USU_N,@PDCLN_FN) RETURNING PDCLN_ID"
            };

            sqlCommand.Parameters.AddWithValue("@PDCLN_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@PDCLN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@PDCLN_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@PDCLN_ORGN_ID", item.IdOrigen);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FRMPG_ID", item.IdFormaPago);
            sqlCommand.Parameters.AddWithValue("@PDCLN_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PDCLN_DRCCN_ID", item.IdDireccion);
            sqlCommand.Parameters.AddWithValue("@PDCLN_VNDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@PDCLN_RQIRFCTR", item.ReqFactura);
            sqlCommand.Parameters.AddWithValue("@PDCLN_AUT_ID", item.IdAutorizacion);
            sqlCommand.Parameters.AddWithValue("@PDCLN_ENVCST", item.TotalEnvio);
            sqlCommand.Parameters.AddWithValue("@PDCLN_IVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@PDCLN_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@PDCLN_TTL", item.Total);
            sqlCommand.Parameters.AddWithValue("@PDCLN_MTDENV", item.MetodoEnvio);
            sqlCommand.Parameters.AddWithValue("@PDCLN_RCB", item.Recibe);
            sqlCommand.Parameters.AddWithValue("@PDCLN_OBSRV", item.Nota);
            //sqlCommand.Parameters.AddWithValue("@PDCLN_CLMTV", item.ClaveCancelacion);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCPED", item.FechaPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCREQ", item.FechaRequerida);
            //sqlCommand.Parameters.AddWithValue("@PDCLN_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCHAUT", item.FechaAutoriza);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCENTR", item.FechaEntrega);
            sqlCommand.Parameters.AddWithValue("@PDCLN_SYNC_ID", item.PDCLN_SYNC_ID);
            //sqlCommand.Parameters.AddWithValue("@PDCLN_USU_CNCL", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@PDCLN_USU_AUT", item.Autoriza);
            sqlCommand.Parameters.AddWithValue("@PDCLN_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FN", item.FechaNuevo);
            item.IdPedido = this.ExecuteScalar(sqlCommand);
            if (item.IdPedido > 0)
                return item.IdPedido;
            return 0;
        }

        public int Update(PedidoClienteModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE PDCLN SET PDCLN_A = @PDCLN_A, PDCLN_STTS_ID = @PDCLN_STTS_ID, PDCLN_ORGN_ID = @PDCLN_ORGN_ID, PDCLN_FRMPG_ID = @PDCLN_FRMPG_ID, PDCLN_DRCTR_ID = @PDCLN_DRCTR_ID, PDCLN_DRCCN_ID = @PDCLN_DRCCN_ID, PDCLN_VNDR_ID = @PDCLN_VNDR_ID, PDCLN_RQIRFCTR = @PDCLN_RQIRFCTR, PDCLN_AUT_ID = @PDCLN_AUT_ID, PDCLN_ENVCST = @PDCLN_ENVCST, PDCLN_IVA = @PDCLN_IVA, PDCLN_SBTTL = @PDCLN_SBTTL, PDCLN_TTL = @PDCLN_TTL, PDCLN_MTDENV = @PDCLN_MTDENV, PDCLN_RCB = @PDCLN_RCB, PDCLN_OBSRV = @PDCLN_OBSRV, PDCLN_FCPED = @PDCLN_FCPED, PDCLN_FCREQ = @PDCLN_FCREQ, PDCLN_FCHAUT = @PDCLN_FCHAUT, PDCLN_FCENTR = @PDCLN_FCENTR, PDCLN_SYNC_ID = @PDCLN_SYNC_ID, PDCLN_USU_AUT = @PDCLN_USU_AUT, PDCLN_USU_M = @PDCLN_USU_M, PDCLN_FM = @PDCLN_FM WHERE ((PDCLN_ID = @PDCLN_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLN_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@PDCLN_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@PDCLN_ORGN_ID", item.IdOrigen);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FRMPG_ID", item.IdFormaPago);
            sqlCommand.Parameters.AddWithValue("@PDCLN_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PDCLN_DRCCN_ID", item.IdDireccion);
            sqlCommand.Parameters.AddWithValue("@PDCLN_VNDR_ID", item.IdVendedor);
            sqlCommand.Parameters.AddWithValue("@PDCLN_RQIRFCTR", item.ReqFactura);
            sqlCommand.Parameters.AddWithValue("@PDCLN_AUT_ID", item.IdAutorizacion);
            sqlCommand.Parameters.AddWithValue("@PDCLN_ENVCST", item.TotalEnvio);
            sqlCommand.Parameters.AddWithValue("@PDCLN_IVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@PDCLN_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@PDCLN_TTL", item.Total);
            sqlCommand.Parameters.AddWithValue("@PDCLN_MTDENV", item.MetodoEnvio);
            sqlCommand.Parameters.AddWithValue("@PDCLN_RCB", item.Recibe);
            sqlCommand.Parameters.AddWithValue("@PDCLN_OBSRV", item.Nota);
            //sqlCommand.Parameters.AddWithValue("@PDCLN_CLMTV", item.ClaveCancelacion);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCPED", item.FechaPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCREQ", item.FechaRequerida);
            //sqlCommand.Parameters.AddWithValue("@PDCLN_FCCNCL", item.FechaCancela);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCHAUT", item.FechaAutoriza);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FCENTR", item.FechaEntrega);
            sqlCommand.Parameters.AddWithValue("@PDCLN_SYNC_ID", item.PDCLN_SYNC_ID);
            //sqlCommand.Parameters.AddWithValue("@PDCLN_USU_CNCL", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@PDCLN_USU_AUT", item.Autoriza);
            sqlCommand.Parameters.AddWithValue("@PDCLN_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FM", item.FechaModifica);

            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<PedidoClienteModel> GetList() {
            return this.GetList(new List<IConditional>());
        }

        public PedidoClienteModel GetById(int index) {
            return this.GetList(new List<IConditional> { new Conditional("PDCLN_ID", index.ToString()) }).FirstOrDefault();
        }

        public PedidoClienteDetailModel GetPedido(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PDCLN.*, DRCTR.DRCTR_CLV, DRCTR.DRCTR_NOM FROM PDCLN LEFT JOIN DRCTR ON PDCLN.PDCLN_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };

            sqlCommand = FireBird.Services.ExpressionTool.Where(sqlCommand, new List<IConditional> { new Conditional("PDCLN_ID", index.ToString()) });
            return this.GetMapper<PedidoClienteDetailModel>(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<PedidoClienteDetailModel> GetList(List<IConditional> condiciones) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PDCLN.*, DRCTR.DRCTR_CLV, DRCTR.DRCTR_NOM FROM PDCLN LEFT JOIN DRCTR ON PDCLN.PDCLN_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };

            return this.GetMapper<PedidoClienteDetailModel>(sqlCommand, condiciones);
        }

        public IEnumerable<PedidoClientePrinter> GetListE(string sql, List<IConditional> condiciones) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PDCLN.*, DRCTR.DRCTR_CLV, DRCTR.DRCTR_NOM FROM PDCLN LEFT JOIN DRCTR ON PDCLN.PDCLN_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };
            return this.GetMapper<PedidoClientePrinter>(sqlCommand, condiciones);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PDCLN.*, DRCTR.DRCTR_CLV, DRCTR.DRCTR_NOM FROM PDCLN LEFT JOIN DRCTR ON PDCLN.PDCLN_DRCTR_ID = DRCTR.DRCTR_ID @wcondiciones"
            };
            if (typeof(T1) == typeof(PedidoReceptorModel)) {
                sqlCommand.CommandText = @"SELECT DRCTR.DRCTR_ID, DRCTR.DRCTR_RFC, DRCTR.DRCTR_NOM FROM DRCTR @wcondiciones";
            } else if (typeof(T1) == typeof(PedidoClienteDetailModel)) {
                sqlCommand.CommandText = @"SELECT PDCLN.*, PDCLS.*, DRCTR.DRCTR_CLV, DRCTR.DRCTR_NOM 
                                            FROM PDCLN 
                                            LEFT JOIN PDCLS ON PDCLS.PDCLS_PDCLN_ID = PDCLN.PDCLN_ID AND PDCLS.PDCLS_STTS_ID = 0
                                            LEFT JOIN DRCTR ON PDCLN.PDCLN_DRCTR_ID = DRCTR.DRCTR_ID  @wcondiciones";
            } else if (typeof(T1) == typeof(PedidoClienteStatusModel)) {
                sqlCommand.CommandText = @"SELECT PDCLS.* FROM PDCLS  @wcondiciones";
            } else if (typeof(T1) == typeof(PedidoClienteRelacionadoModel)) {
                sqlCommand.CommandText = @"SELECT PDCLR.* FROM PDCLR  @wcondiciones";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public PedidoClienteDetailModel Save(PedidoClienteDetailModel model) {
            if (model.IdPedido == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdPedido = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }

        /// <summary>
        /// registro del cambio de status en la tabla de registros
        /// </summary>
        public bool Cancelar(IPedidoClienteStatusDetailModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO PDCLS (PDCLS_PDCLN_ID, PDCLS_STTS_ID, PDCLS_DRCTR_ID, PDCLS_CLMTV_ID, PDCLS_CLMTV, PDCLS_OBSRV, PDCLS_TTL, PDCLS_URL_PDF, PDCLS_USU_M, PDCLS_FM)
                                                     VALUES(@PDCLS_PDCLN_ID,@PDCLS_STTS_ID,@PDCLS_DRCTR_ID,@PDCLS_CLMTV_ID,@PDCLS_CLMTV,@PDCLS_OBSRV,@PDCLS_TTL,@PDCLS_URL_PDF,@PDCLS_USU_M,@PDCLS_FM) MATCHING (PDCLS_PDCLN_ID, PDCLS_STTS_ID, PDCLS_CLMTV_ID, PDCLS_DRCTR_ID)"
            };

            sqlCommand.Parameters.AddWithValue("@PDCLS_PDCLN_ID", model.IdPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLS_STTS_ID", model.IdStatus);
            sqlCommand.Parameters.AddWithValue("@PDCLS_DRCTR_ID", model.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PDCLS_CLMTV_ID", model.IdCvMotivo);
            sqlCommand.Parameters.AddWithValue("@PDCLS_CLMTV", model.CvMotivo);
            sqlCommand.Parameters.AddWithValue("@PDCLS_TTL", model.Total);
            sqlCommand.Parameters.AddWithValue("@PDCLS_URL_PDF", model.UrlFilePDF);
            sqlCommand.Parameters.AddWithValue("@PDCLS_FM", model.FechaStatus);
            sqlCommand.Parameters.AddWithValue("@PDCLS_USU_M", model.Cancela);
            sqlCommand.Parameters.AddWithValue("@PDCLS_OBSRV", model.Nota);

            if (this.ExecuteTransaction(sqlCommand) > 0) {
                return this.Status(model);
            }
            return false;
        }

        /// <summary>
        /// cambios de status
        /// </summary>
        public bool Status(IPedidoClienteStatusDetailModel model) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE PDCLN SET PDCLN_STTS_ID = @PDCLN_STTS_ID, PDCLN_FM = @PDCLN_FM, PDCLN_USU_M = @PDCLN_USU_M WHERE PDCLN_ID = @PDCLN_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@PDCLN_ID", model.IdPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLN_STTS_ID", model.IdStatus);
            sqlCommand.Parameters.AddWithValue("@PDCLN_FM", model.FechaStatus);
            sqlCommand.Parameters.AddWithValue("@PDCLN_USU_M", model.Cancela);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public bool Relacion(IPedidoClienteStatusDetailModel model1) {
            foreach (var model in model1.Relaciones) {
                var sqlCommand = new FbCommand {
                    CommandText = @"UPDATE OR INSERT INTO PDCLR (PDCLR_PDCLN_ID, PDCLR_DRCTR_ID, PDCLR_CTREL_ID, PDCLR_RELN, PDCLR_FOLIO, PDCLR_NOMR, PDCLR_FECEMS, PDCLR_TOTAL, PDCLR_USR_N, PDCLR_FN)
                                                     VALUES(@PDCLR_PDCLN_ID,@PDCLR_DRCTR_ID,@PDCLR_CTREL_ID,@PDCLR_RELN,@PDCLR_FOLIO,@PDCLR_NOMR,@PDCLR_FECEMS,@PDCLR_TOTAL,@PDCLR_USR_N,@PDCLR_FN) MATCHING (PDCLR_PDCLN_ID, PDCLR_DRCTR_ID, PDCLR_CTREL_ID)"
                };

                sqlCommand.Parameters.AddWithValue("@PDCLR_PDCLN_ID", model.IdPedido);
                sqlCommand.Parameters.AddWithValue("@PDCLR_DRCTR_ID", model.IdDirectorio);
                sqlCommand.Parameters.AddWithValue("@PDCLR_CTREL_ID", model.IdRelacion);
                sqlCommand.Parameters.AddWithValue("@PDCLR_RELN", model.Relacion);
                sqlCommand.Parameters.AddWithValue("@PDCLR_FOLIO", model.Folio);
                sqlCommand.Parameters.AddWithValue("@PDCLR_NOMR", model.Cliente);
                sqlCommand.Parameters.AddWithValue("@PDCLR_FECEMS", model.FechaEmision);
                sqlCommand.Parameters.AddWithValue("@PDCLR_TOTAL", model.GTotal);
                sqlCommand.Parameters.AddWithValue("@PDCLR_USR_N", model.Creo);
                sqlCommand.Parameters.AddWithValue("@PDCLR_FN", model.FechaNuevo);

                var d0 = this.ExecuteTransaction(sqlCommand) > 0;
            }
            return true;
        }
    }
}

