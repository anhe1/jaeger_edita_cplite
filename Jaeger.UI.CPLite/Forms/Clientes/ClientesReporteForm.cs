﻿using System;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Util.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class ClientesReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.Contribuyentes");

        public ClientesReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(string)) {
                this.CrearSolicitud();
            }
        }

        private void CrearSolicitud() {
            var d1 = (string)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream(string.Format("Jaeger.Domain.Contribuyentes.Reportes.{0}", d1));
            this.ImagenQR = QRCodeExtension.GetQRBase64(new string[] { "" });
            this.Procesar();
            this.SetDisplayName("Solicitud de Crédito");
            this.Finalizar();
        }
    }
}
