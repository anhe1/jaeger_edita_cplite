﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class PedidosPorClienteForm : RadForm {
        protected internal BackgroundWorker _ClientesLoad = new BackgroundWorker();

        public PedidosPorClienteForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void PedidosPorClienteForm_Load(object sender, EventArgs e) {
            this.TPedido.Enabled = false;
            this.TPedido.ItemLbl.Text = "Cliente: ";
            this.TPedido.ShowItem = true;
            this.TPedido.ItemHost.HostedItem = this.Nombre.MultiColumnComboBoxElement;
            this.TPedido.Consultar += TPedido_Consultar;
            this.TPedido.Cerrar.Click += this.TPedido_Cerrar_Click;
            this.TPedido.Service = new PedidosService();
            this.TPedido.ShowImprimir = true;
            this._ClientesLoad.DoWork += LoadClientes_DoWork;
            this._ClientesLoad.RunWorkerCompleted += LoadClientes_RunWorkerCompleted;
            this._ClientesLoad.RunWorkerAsync();
            this.Nombre.EditorControl.AllowSearchRow = true;
            this.Nombre.AutoFilter = true;
            this.TPedido.CreateView();
        }

        private void TPedido_Consultar() {
            if (this.Nombre.SelectedItem != null) {
                var seleccionado = ((GridViewDataRowInfo)this.Nombre.SelectedItem).DataBoundItem as IPedidoReceptorModel;
                if (seleccionado != null) {
                    this.TPedido._DataSource = new BindingList<PedidoClienteDetailModel>(
                        this.TPedido.Service.GetList<PedidoClienteDetailModel>(
                            PedidosService.Query().Year(this.TPedido.GetEjercicio()).Month(TPedido.GetPeriodo()).IdCliente(seleccionado.IdDirectorio).Build()).ToList());
                }
            }
        }

        private void TPedido_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void LoadClientes_DoWork(object sender, DoWorkEventArgs e) {
            this.Nombre.DataSource = this.TPedido.Service.GetList<PedidoReceptorModel>(PedidosService.Query().Clientes().Build()).ToList();
        }

        private void LoadClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Nombre.EditorControl.FilterDescriptors.Add(new FilterDescriptor("Nombre", FilterOperator.Contains, ""));
            this.TPedido.Enabled = true;
        }
    }
}
