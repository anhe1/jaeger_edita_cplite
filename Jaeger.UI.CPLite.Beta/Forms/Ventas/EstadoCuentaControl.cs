﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    internal class EstadoCuentaControl : Common.Forms.GridCommonControl {
        #region declaraciones
        public Aplication.Almacen.PT.Contracts.IRemisionadoService _Remisionado;
        protected IVendedoresService _Vendedores;
        public IDirectorioService _Directorio;
        private List<RemisionSingleModel> DataSource;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Vendedores;
        #endregion

        public EstadoCuentaControl() : base() {
            this.Vendedores = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.ItemHost.HostedItem = this.Vendedores.MultiColumnComboBoxElement;
            this.Load += EstadoCuentaControl_Load;
        }

        private void EstadoCuentaControl_Load(object sender, EventArgs e) {
            this.ShowItem = true;
            this.ShowNuevo = false;
            this.ShowEditar = false;
            this.ShowImprimir = true;
            this.ShowPeriodo = false;
            this.ItemLbl.Text = "Vendedor: ";
            this._Directorio = new Aplication.CPLite.Services.DirectorioService();
            this._Remisionado = new Aplication.CPLite.Almacen.PT.Services.RemisionadoService();
            this._Vendedores = new Aplication.CPLite.Ventas.Services.VendedoresService();

            this.Vendedores.AutoFilter = true;
            FilterDescriptor descriptor = new FilterDescriptor(this.Vendedores.DisplayMember, FilterOperator.StartsWith, string.Empty);
            this.Vendedores.EditorControl.FilterDescriptors.Add(descriptor);
            this.Vendedores.AutoFilter = true;
            this.Vendedores.EditorControl.AllowRowResize = false;
            this.Vendedores.EditorControl.AllowSearchRow = true;
            this.Vendedores.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.Vendedores.EditorControl.GridViewElement.ShowRowErrors = false;
            this.Vendedores.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Vendedores.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Vendedores.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Vendedores.EditorControl.Name = "NestedRadGridView";
            this.Vendedores.EditorControl.ReadOnly = true;
            this.Vendedores.EditorControl.ShowGroupPanel = false;
            this.Vendedores.EditorControl.Size = new System.Drawing.Size(380, 150);
            this.Vendedores.EditorControl.TabIndex = 0;
            this.Vendedores.Columns.Add(new GridViewTextBoxColumn { FieldName = "IdVendedor", HeaderText = "Id", IsVisible = false, DataType = typeof(int) });
            this.Vendedores.Columns.Add(new GridViewTextBoxColumn { FieldName = "Clave", HeaderText = "Clave", Width = 75 });
            this.Vendedores.Columns.Add(new GridViewTextBoxColumn { FieldName = "Nombre", HeaderText = "Vendedor", Width = 200 });
            this.Vendedores.DisplayMember = "Nombre";
            this.Vendedores.ValueMember = "IdVendedor";
            this.Vendedores.DataSource = this._Vendedores.GetList(true);

            using (IRemisionadoGridBuilder builder = new RemisionadoGridBuilder()) {
                this.GridData.Columns.AddRange(builder.Templetes().Remisionado().Build());
            }

            this.Actualizar.Click += TRemision_Actualizar_Click;
            this.Imprimir.Click += TReporte_Imprimir_Click;
        }

        private void TReporte_Imprimir_Click(object sender, EventArgs e) {
            if (this.DataSource != null) {
                var seleccion = this.GridData.ChildRows.Select(x => x.DataBoundItem as RemisionSingleModel);
                if (seleccion.Count() > 0) {
                    var _vendedor = seleccion.Select(it => it.Vendedor).FirstOrDefault();
                    var _reporte = new ReporteForm(new EdoCuentaVendedorPrinter() { Vendedor = _vendedor, Conceptos = seleccion.ToList() });
                    _reporte.Show();
                }
            }
        }

        private void TRemision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.DataSource;
        }

        private void Consulta() {
            var selectedValue = this.Vendedores.SelectedItem as GridViewRowInfo;
            if (selectedValue != null) {
                var seleccionado = selectedValue.DataBoundItem as Vendedor2DetailModel;
                var query = Aplication.Almacen.PT.Services.RemisionadoService.Query().WithYear(this.GetEjercicio()).IdVendedor(seleccionado.IdVendedor).WithIdStatus(Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Build();
                this.DataSource = this._Remisionado.GetList<RemisionSingleModel>(query).ToList();
            }
        }
    }
}
