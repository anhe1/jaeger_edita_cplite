﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Almacen.Contracts;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Almacen.Services {
    /// <summary>
    /// servicio de categorias o clasifiacion general
    /// </summary>
    public class ClasificacionService : Aplication.Almacen.Services.ClasificacionService, IClasificacionService {
        public ClasificacionService() : base() {
            this.categoriaRepository = new DataAccess.FB.Almacen.Repositories.SqlFbClasificacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
