SET TERM ^ ;
CREATE PROCEDURE CBRNZ_RECIBOS_COBROS (
    INPUT_RECIBO_ID Integer,
    INPUT_OPTION_UPDATE Integer )
AS
declare variable facturas_id dom_id;
declare variable remision_id dom_id;
declare variable remdirec_id dom_id;
declare variable catgprod_id dom_id;
declare variable catgdesc_id dom_id;
declare variable diasprom dom_id;
declare variable diastran dom_id;
declare variable estatus_id dom_id;
declare variable ventprom dom_moneda;
declare variable factor dom_moneda;
declare variable subTotal$ dom_moneda;
declare variable total$ dom_moneda;
declare variable cobrado$ dom_moneda;
declare variable observ dom_str50;
declare variable descnom dom_str50;
declare variable descCat dom_str50;
declare variable fechaemsn dom_fh;
declare variable fechapago dom_fh;
declare variable fechacalc dom_fh;
declare variable fechaentr dom_fh;
declare variable dias_trans dom_id;
BEGIN
/*
Elabora: anhe 25092012
Proposito: aplicar descuento a las remisiones relacionadas al documento de cobro asi como verificar si es necesario recalcular el descuento
si la fecha de pago de los mismos se modifico durante la autorización e ingreso a la boveda.
Revisiones:
    18102012: se modifico la consulta de los recibos para obtener la fecha de pago autorizada
 */
    diasprom = 30;
    if ( input_Recibo_id is not null and input_Recibo_id <> 0 ) then
        begin
            for
                select rlcnCbrnz.cntbl_rem_id, Cbrnz.cntbl_fecdocs, rem.ctlrms_fccal,
                    ( select sum ( ctlrms_sbttl$ ) from ctlrms where ctlrms_fcemsn > ( Cbrnz.cntbl_fecdocs - :diasprom ) and ctlrms_drctr_id = rem.ctlrms_drctr_id group by ctlrms_drctr_id ),
                    rem.CTLRMS_FCEMSN,
                    rem.CTLRMS_FCENTRG,
                    rem.ctlrms_sbttl$
                from cntbl rlcnCbrnz, cntbl Cbrnz, ctlrms rem
                where 
                        rlcnCbrnz.cntbl_folio = :input_recibo_id   --< id del recibo o poliza
                        --cbrnz.cntbl_folio = :input_recibo_id
                    and rlcnCbrnz.cntbl_poliza = Cbrnz.cntbl_poliza --< relacion partidas -> recibos
                    and rlcnCbrnz.cntbl_rem_id = rem.ctlrms_id      --< relacion con remisiones
                    and rlcnCbrnz.cntbl_doc_id = 2800               --< solo documentos relacionados
                    and Cbrnz.cntbl_doc_id = 28                     --< poliza
                into
                    :remision_id, :fechapago, :fechacalc, :ventprom, :fechaemsn, :fechaentr, :subtotal$
            do
                begin
                    if ( :fechapago <> :fechacalc or diasprom = 0) then             --< si son diferentes recalculamos el descuento.
                        begin
                            catgdesc_id = 0;
                            observ = '';
                            dias_trans = ( :fechapago - :fechaentr );
                            if ( dias_trans < 1 ) then dias_trans = 1;
                            if ( ventprom is null ) then ventprom = 0;
                            /*localizar catalogo de descuentos */
                            select p.out_cmsdsc_id, p.out_observ
                            from cbrnz_catdesc0(:remdirec_id,:catgprod_id, :fechaemsn) p
                                into :catgdesc_id, :observ;
                            /* calcular descuento a aplicar */
                            select p.out_catalogo, p.out_descuento, p.out_factor
                            from cbrnz_descuent1 (:ventprom, :dias_trans, :catgdesc_id) p
                                into :descCat, :descnom, :factor;
                            
                            update ctlrms
                                set 
                                    ctlrms_catdsc_id = :catgdesc_id,
                                    ctlrms_ctldsc = :descCat,
                                    ctlrms_dscnt = :factor,
                                    ctlrms_total$ = :subtotal$ * :factor,
                                    --ctlrms_impst$ = :cbrmsn0_$impst,
                                    --ctlrms_dscnt$ = :subtotal$ - ctlrms_total$ ,
                                    ctlrms_ddscn = :descnom,
                                    ctlrms_fcultpg = :fechapago,
                                    ctlrms_fccal = current_date
                                    --ctlrms_hstrl = 'si paso'
                            where ctlrms_id = :remision_id;
                            
                            execute procedure cbrnz_remision_cobros ( :remision_id , :subtotal$ * :factor );
                        end
                    else
                        begin
                            select ctlrms_total$
                            from ctlrms
                            where ctlrms_id = :remision_id
                            into :subtotal$;
                            
                            update ctlrms
                                set 
                                    ctlrms_fcultpg = :fechapago
                            where ctlrms_id = :remision_id;
                            
                            execute procedure cbrnz_remision_cobros ( :remision_id , :subtotal$ );
                        end
                end
        end
    else
        begin
            exception ERROR_0018;
        end
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_RECIBOS_COBROS TO  SYSDBA;

