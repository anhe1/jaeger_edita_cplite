﻿namespace Jaeger.UI.CPLite.Forms.Tesoreria {
    public class CuentasBancariaCatalogoForm : UI.Banco.Forms.CuentaBancariaCatalogoForm {
        public CuentasBancariaCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.TCuentaB._Service = new Aplication.CPLite.Banco.Services.BancoCuentaService();
            this.Load += this.CuentasBancariaCatalogoForm_Load;
        }

        private void CuentasBancariaCatalogoForm_Load(object sender, System.EventArgs e) {
            
        }
    }
}
