﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.FB.Contribuyentes.Repositories;

namespace Jaeger.Aplication.CPLite.Services {
    public class ContribuyenteService : Contribuyentes.Services.ContribuyenteService, Contribuyentes.Contracts.IContribuyenteService {
        public ContribuyenteService() : base() {
        }

        protected override void OnLoad() {
            this.contribuyenteRepository = new SqlFbContribuyenteRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.relacionComercialRepository = new SqlFbRelacionComercialRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.domicilioRepository = new SqlFbDomicilioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.cuentaBancariaRepository = new SqlFbContribuyenteCuentaBancariaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.carteraRepository = new SqlFbVendedorCarteraRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.contactoRepository = new SqlFbContactoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        /// <summary>
        /// obtener objeto complejo del contribuyente por su indice
        /// </summary>
        public override IContribuyenteDetailModel GetById(int index) {
            var response = this.contribuyenteRepository.GetById<ContribuyenteDetailModel>(index);
            if (response != null) {
                Contribuyentes.Builder.IRelacionComercialBuilder r0 = new Contribuyentes.Builder.RelacionComercialBuilder();
                response.Domicilios = this.GetDomicilios(index);
                response.Relaciones = this.GetRelacion(index);
                response.Vendedores = this.GetVendedor(index);
                response.CuentasBancarias = this.GetCuentaBancarias(index);
                response.Contactos = this.GetContactos(index, true);
                //response.Relaciones = new BindingList<IRelacionComercialDetailModel>(this.GetRelacionesComerciales(response.Relaciones));
                response.Relaciones = new BindingList<IRelacionComercialDetailModel>(r0.Get(response.Relaciones));
            }
            return response;
        }

        public override BindingList<IDomicilioFiscalDetailModel> GetDomicilios(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("DRCCN_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("DRCCN_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }
            if (onlyActive) condiciones.Add(new Conditional("DRCCN_A", "1"));
            return new BindingList<IDomicilioFiscalDetailModel>(this.domicilioRepository.GetList<DomicilioFiscalDetailModel>(condiciones).ToList<IDomicilioFiscalDetailModel>());
        }

        public override BindingList<IContribuyenteVendedorModel> GetVendedor(object index, bool onlyActive = true) {
            var condiciones = new List<IConditional>();

            if (index.GetType() == typeof(int)) {
                condiciones.Add(new Conditional("DRCTRC_DRCTR_ID", index.ToString()));
            } else if (index.GetType() == typeof(int[])) {
                var real = (int[])index;
                condiciones.Add(new Conditional("DRCTRC_DRCTR_ID", string.Join(",", real), ConditionalTypeEnum.In));
            }

            if (onlyActive)
                condiciones.Add(new Conditional("DRCTRC_A", "1"));

            return new BindingList<IContribuyenteVendedorModel>(this.carteraRepository.GetVendedores<ContribuyenteVendedorModel>(condiciones).ToList<IContribuyenteVendedorModel>());
        }
        
        #region metodos estaticos
        public new void Crear() {
            throw new NotImplementedException();
        }
        #endregion
    }
}
