﻿using Jaeger.UI.Common.Services;
using System.Drawing;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Services {
    public static class GridPedidosService {
        /// <summary>
        /// Columnas para el templete de cancelaciones
        /// </summary>
        public static GridViewDataColumn[] GetCancelacion() {
            return new GridViewDataColumn[] { GridCommonService.ColCboIdStatus, GridCommonService.ColFechaCancela, GridCommonService.ColCancela, ClaveCancelacion, NotaCancelacion };
        }

        /// <summary>
        /// columnas para el grid de pedidos
        /// </summary>
        /// <param name="valores">Verdadero con importes</param>
        public static GridViewDataColumn[] GetPedido(bool valores) {
            var colIdPedido = GridCommonService.ColIdPedido;
            colIdPedido.IsPinned = true;
            var claveCancelacion = ClaveCancelacion;
            claveCancelacion.IsVisible = false;
            var notaCancelacion = NotaCancelacion;
            notaCancelacion.IsVisible = false;
            notaCancelacion.HeaderText = "Cancelación: Nota";
            var colStatus = GridCommonService.ColCboIdStatus;
            colStatus.IsPinned = true;

            if (valores) {
                return new GridViewDataColumn[] { 
                    colIdPedido, colStatus, ColClienteClave, ColCliente, ColRecibe, ColMetodoEnvio, GridTelerikCommon.ColIdVendedor, GridTelerikCommon.ColFechaPedido, ColFechaAcuerdo, GridTelerikCommon.ColFechaEntrega, GridTelerikCommon.ColFechaCancela, 
                    GridTelerikCommon.ColCancela, claveCancelacion, notaCancelacion, GridTelerikCommon.ColSubTotal, GridTelerikCommon.ColTotalDescuento, GridTelerikCommon.ColTotalEnvio, GridTelerikCommon.ColTotalTrasladoIVA, 
                    GridTelerikCommon.ColTotal, ColReqFactura, ColNota, GridCommonService.ColCreo, GridCommonService.ColFechaNuevo };
            }
            return new GridViewDataColumn[] {
                    colIdPedido, colStatus, ColClienteClave, ColCliente, ColRecibe, ColMetodoEnvio, GridTelerikCommon.ColIdVendedor, GridTelerikCommon.ColFechaPedido, ColFechaAcuerdo, GridTelerikCommon.ColFechaEntrega, GridTelerikCommon.ColFechaCancela,
                    GridTelerikCommon.ColCancela, claveCancelacion, notaCancelacion, ColReqFactura, ColNota, GridCommonService.ColCreo, GridCommonService.ColFechaNuevo };
        }

        public static GridViewDataColumn[] GetConceptos(bool valores) {
            if (valores) {
                return new GridViewDataColumn[] { 
                    GridTelerikCommon.ColCantidad, GridTelerikCommon.ColUnidad, GridPedidosService.Descriptor, GridPedidosService.Unitario, GridTelerikCommon.ColSubTotal, Descuento, Importe, TasaIVA, ColTrasladoIVA, ColTotal, ColIdentificador, GridCommonService.ColFechaNuevo };
            }
            return new GridViewDataColumn[] { GridTelerikCommon.ColCantidad, GridTelerikCommon.ColUnidad, GridPedidosService.Descriptor, ColIdentificador, GridCommonService.ColFechaNuevo };
        }

        #region pedidos

        public static GridViewTextBoxColumn ColClienteClave {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClienteClave",
                    HeaderText = "Clave",
                    IsPinned = true,
                    Name = "ClienteClave",
                    PinPosition = PinnedColumnPosition.Left,
                    Width = 65
                };
            }
        }

        public static GridViewTextBoxColumn ColCliente {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Cliente",
                    HeaderText = "Cliente",
                    IsPinned = true,
                    Name = "Cliente",
                    PinPosition = PinnedColumnPosition.Left,
                    Width = 280
                };
            }
        }

        public static GridViewTextBoxColumn ColRecibe {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Recibe",
                    HeaderText = "Recibe",
                    IsVisible = false,
                    Name = "Recibe",
                    Width = 100
                };
            }
        }

        public static GridViewTextBoxColumn ColMetodoEnvio {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "MetodoEnvio",
                    HeaderText = "Método de envío",
                    Name = "MetodoEnvio",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColFechaAcuerdo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(System.DateTime),
                    FieldName = "FechaAcuerdo",
                    FormatString = "{0:dd MMM yy}",
                    HeaderText = "Fec. Prioridad",
                    Name = "FechaAcuerdo",
                    TextAlignment = ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        public static GridViewComboBoxColumn ClaveCancelacion {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "ClaveCancelacion",
                    HeaderText = "Clave de cancelación",
                    Name = "ClaveCancelacion",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn NotaCancelacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NotaCancelacion",
                    HeaderText = "Observaciones",
                    Name = "NotaCancelacion",
                    ReadOnly = true,
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColTrasladoIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TrasladoIVA",
                    FormatString = "{0:n}",
                    HeaderText = "% IVA",
                    Name = "TrasladoIVA",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Total",
                    FormatString = "{0:n}",
                    HeaderText = "Total",
                    Name = "Total",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 85
                };
            }
        }

        public static GridViewCheckBoxColumn ColReqFactura {
            get {
                return new GridViewCheckBoxColumn {
                    FieldName = "ReqFactura",
                    HeaderText = "Facturar",
                    Name = "ReqFactura"
                };
            }
        }

        public static GridViewTextBoxColumn ColNota {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Nota",
                    HeaderText = "Observaciones",
                    Name = "Nota",
                    Width = 270
                };
            }
        }
        #endregion

        #region conceptos
        public static GridViewTextBoxColumn Cantidad {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Cantidad",
                    FormatString = "{0:N2}",
                    HeaderText = "Cantidad",
                    Name = "Cantidad",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 65
                };
            }
        }


        private static GridViewTextBoxColumn Catalogo() {
            return new GridViewTextBoxColumn {
                FieldName = "Catalogo",
                HeaderText = "Catálogo",
                Name = "Catalogo",
                Width = 100
            };
        }

        private static GridViewTextBoxColumn Producto() {
            return new GridViewTextBoxColumn {
                FieldName = "Producto",
                HeaderText = "Producto",
                Name = "Producto",
                Width = 200
            };
        }

        private static GridViewTextBoxColumn Marca() {
            return new GridViewTextBoxColumn {
                FieldName = "Marca",
                HeaderText = "Marca",
                Name = "Marca",
                Width = 150
            };
        }

        private static GridViewTextBoxColumn Modelo() {
            return new GridViewTextBoxColumn {
                FieldName = "Modelo",
                HeaderText = "Modelo",
                Name = "Modelo",
                Width = 150
            };
        }

        private static GridViewTextBoxColumn Especificacion() {
            return new GridViewTextBoxColumn {
                FieldName = "Especificacion",
                HeaderText = "Especificación",
                Name = "Especificacion",
                Width = 100
            };
        }

        private static GridViewTextBoxColumn Tamanio() {
            return new GridViewTextBoxColumn {
                FieldName = "Tamanio",
                HeaderText = "Variante",
                Name = "Tamanio",
                Width = 115
            };
        }

        public static GridViewTextBoxColumn Descriptor {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Descriptor",
                    HeaderText = "Descripción",
                    Name = "Descriptor",
                    Width = 510,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn Unitario {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Unitario",
                    FormatString = "{0:N2}",
                    HeaderText = "Unitario",
                    Name = "Unitario",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        private static GridViewTextBoxColumn Descuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Descuento",
                    FormatString = "{0:N2}",
                    HeaderText = "Descuento",
                    Name = "Descuento",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn Importe {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Importe",
                    FormatString = "{0:N2}",
                    HeaderText = "Importe",
                    Name = "Importe",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn TasaIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TasaIVA",
                    FormatString = "{0:N2}",
                    HeaderText = "% IVA",
                    Name = "TasaIVA",
                    TextAlignment = ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        private static GridViewTextBoxColumn ColIdentificador {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Identificador",
                    HeaderText = "Identificador",
                    Name = "Identificador",
                    Width = 115
                };
            }
        }
        #endregion
    }
}
