﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class ComisionReciboCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.Movimientos = new UI.Banco.Forms.MovimientosGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutosuma = true;
            this.ToolBar.ShowCancelar = true;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowEjercicio = true;
            this.ToolBar.ShowExportarExcel = false;
            this.ToolBar.ShowFiltro = true;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = true;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowPeriodo = true;
            this.ToolBar.Size = new System.Drawing.Size(1001, 30);
            this.ToolBar.TabIndex = 3;
            // 
            // Movimientos
            // 
            this.Movimientos.Auditar = false;
            this.Movimientos.Cancelar = false;
            this.Movimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Movimientos.Location = new System.Drawing.Point(0, 30);
            this.Movimientos.Name = "Movimientos";
            this.Movimientos.Size = new System.Drawing.Size(1001, 523);
            this.Movimientos.TabIndex = 4;
            // 
            // ComisionReciboCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 553);
            this.Controls.Add(this.Movimientos);
            this.Controls.Add(this.ToolBar);
            this.Name = "ComisionReciboCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Comisión Recibos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComisionReciboCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public Common.Forms.ToolBarCommonControl ToolBar;
        private UI.Banco.Forms.MovimientosGridControl Movimientos;
    }
}