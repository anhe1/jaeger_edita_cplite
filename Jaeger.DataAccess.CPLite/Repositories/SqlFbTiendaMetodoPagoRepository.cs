﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbTiendaMetodoPagoRepository : Abstractions.RepositoryMaster<TiendaMetodoPagoModel>, ISqlTiendaMetodoPagoRepository {
        public SqlFbTiendaMetodoPagoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public int Insert(TiendaMetodoPagoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERt INTO TWMTDPG (TWMTDPG_ID, TWMTDPG_A, TWMTDPG_PDCLN_ID, TWMTDPG_DRCTR_ID, TWMTDPG_MTD, TWMTDPG_TRNSC_ID, TWMTDPG_STTS, TWMTDPG_PRFRNCID, TWMTDPG_PYRID, TWMTDPG_PYRNM, TWMTDPG_TTL, TWMTDPG_FILE, TWMTDPG_FN)" +
                                           "VALUES(@TWMTDPG_ID,@TWMTDPG_A,@TWMTDPG_PDCLN_ID,@TWMTDPG_DRCTR_ID,@TWMTDPG_MTD,@TWMTDPG_TRNSC_ID,@TWMTDPG_STTS,@TWMTDPG_PRFRNCID,@TWMTDPG_PYRID,@TWMTDPG_PYRNM,@TWMTDPG_TTL,@TWMTDPG_FILE,@TWMTDPG_FN) RETURNING TWMTDPG_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@TWMTDPG_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_MTD", item.Metodo);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_TRNSC_ID", item.IdTransaccion);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_STTS", item.Status);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PRFRNCID", item.IdPreferencia);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PYRID", item.IdComprador);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PYRNM", item.Comprador);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_TTL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_FILE", item);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_FN", item.FechaNuevo);
            throw new NotImplementedException();
        }

        public int Update(TiendaMetodoPagoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE TWMTDPG SET TWMTDPG_A, TWMTDPG_PDCLN_ID, TWMTDPG_DRCTR_ID, TWMTDPG_MTD, TWMTDPG_TRNSC_ID, TWMTDPG_STTS, TWMTDPG_PRFRNCID, TWMTDPG_PYRID, TWMTDPG_PYRNM, TWMTDPG_TTL, TWMTDPG_FILE, TWMTDPG_FN WHERE TWMTDPG_ID = @TWMTDPG_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_DRCTR_ID", item.IdCliente);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_MTD", item.Metodo);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_TRNSC_ID", item.IdTransaccion);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_STTS", item.Status);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PRFRNCID", item.IdPreferencia);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PYRID", item.IdComprador);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_PYRNM", item.Comprador);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_TTL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_FILE", item);
            sqlCommand.Parameters.AddWithValue("@TWMTDPG_FN", item.FechaNuevo);
            throw new NotImplementedException();
        }

        public TiendaMetodoPagoModel GetById(int index) {
            throw new NotImplementedException();
        }

        public IEnumerable<TiendaMetodoPagoModel> GetList() {
            throw new NotImplementedException();
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT TWMTDPG.* FROM TWMTDPG @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
