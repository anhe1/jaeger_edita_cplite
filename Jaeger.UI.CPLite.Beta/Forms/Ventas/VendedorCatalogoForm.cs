﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    internal class VendedorCatalogoForm : UI.Ventas.Forms.VendedorCatalogoForm {
        public VendedorCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += VendedorCatalogoForm_Load;
        }

        private void VendedorCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Ventas: Vendedores";
            this.service = new Aplication.CPLite.Ventas.Services.VendedoresService();
            this.comision = new Aplication.CPLite.Ventas.Services.ComisionService();
        }
    }
}
