﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.Aplication.CPLite.Almacen.PT.Services {
    public class NotasDescuentoService : NotaDescuentoService, INotasDescuentoService {
        public BindingList<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var response = this.notaDescuentoRepository.GetList<T1>(conditionals).ToList();
            if (response != null) {
                var ids = response.Select(it => (it as NotaDescuentoDetailModel).IdNota).ToArray();
                if (ids.Count() == 0) { ids = new int[] { 0 }; }
                var p = new List<Conditional> {
                    new Conditional("NTDSCP_NTDSC_ID", string.Join(",", ids), ConditionalTypeEnum.In),
                    new Conditional("NTDSCP_A", "0", ConditionalTypeEnum.GreaterThan)
                };

                var partidas = this.relacionRepository.GetList<NotaDescuentoRelacionDetailModel>(p).ToList();
                if (partidas.Count > 0) {
                    for (int i = 0; i < response.Count(); i++) {
                        (response[i] as NotaDescuentoDetailModel).Partidas = new BindingList<NotaDescuentoRelacionDetailModel>(partidas.Where(it => it.IdNota == (response[i] as NotaDescuentoDetailModel).IdNota).ToList());
                    }
                }

                var auto = this.statusRepository.GetList<NotaDescuentoStatusModel>(new List<IConditional> {
                    new Conditional("NTDSCS_A", "1"),
                    new Conditional("NTDSCS_NTDSC_ID", string.Join(",", ids), ConditionalTypeEnum.In)
                }).ToList();

                if (auto.Count > 0) {
                    for (int i = 0; i < response.Count; i++) {
                        (response[i] as NotaDescuentoDetailModel).Autorizaciones = new BindingList<NotaDescuentoStatusModel>(auto.Where(it => it.IdNota == (response[i] as NotaDescuentoDetailModel).IdNota).ToList());
                    }
                }
            }

            return new BindingList<T1>(response);
        }
    }
}
