﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Jaeger.Aplication.Contribuyentes.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class ContribuyenteSearchControl : UserControl {
        protected BackgroundWorker consultar = new BackgroundWorker();
        private List<ContribuyenteDetailModel> _DataSource;
        private string _Nombre;
        private BindingList<ItemSelectedModel> _RelacionComercial;

        public ContribuyenteSearchControl() {
            InitializeComponent();
            //this.gridResult.Columns.AddRange(GridContribuyenteService.GetBusqueda());
        }

        public event EventHandler<ContribuyenteDetailModel> Selected;
        public void OnSelected(ContribuyenteDetailModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }


        private void ContribuyenteSearchControl_Load(object sender, EventArgs e) {
            this.consultar = new BackgroundWorker();
            this.consultar.DoWork += Consultar_DoWork;
            this.consultar.RunWorkerCompleted += Consultar_RunWorkerCompleted;
        }

        public Aplication.Contribuyentes.Contracts.IDirectorioService Service { get; set; }

        public BindingList<ItemSelectedModel> RelacionComerical {
            get { return this._RelacionComercial; }
            set { this._RelacionComercial = value; }
        }

        public string Buscar {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public virtual void Search() {
            if (this.Service == null) {
                this.Service = new DirectorioToService();
            }
            this.Espera.Visible = true;
            if (!this.consultar.IsBusy) {
                this.Espera.StartWaiting();
                this.consultar.RunWorkerAsync();
            }
        }

        public virtual void Search(string descripcion) {
            this.Buscar = descripcion;
            this.Search();
        }

        public virtual ContribuyenteDetailModel GetCurrent() {
            if (this.gridResult.CurrentRow != null) {
                var seleccionado = this.gridResult.CurrentRow.DataBoundItem as ContribuyenteDetailModel;
                if (seleccionado != null) {
                    return seleccionado;
                }
            }
            return null;
        }

        #region acciones del grid
        private void GridResult_DoubleClick(object sender, EventArgs e) {
            if (this.gridResult.CurrentRow != null) {
                var seleccionado = this.gridResult.CurrentRow.DataBoundItem as ContribuyenteDetailModel;
                if (seleccionado != null) {
                    this.OnSelected(seleccionado);
                }
            }
        }

        private void GridResult_Resize(object sender, EventArgs e) {
            var w = this.gridResult.Width / 2;
            this.Espera.Left = w - (this.Espera.Width / 2);
            var h = this.gridResult.Height / 2;
            this.Espera.Top = h - (this.Espera.Height / 2);
        }
        #endregion

        private void Consultar_DoWork(object sender, DoWorkEventArgs e) {
            this.gridResult.Enabled = false;
            //this.datos = this.Service.GetList<ContribuyenteDetailModel>(this.RelacionComerical, this.Buscar);
        }

        private void Consultar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Espera.Visible = false;
            this.Espera.StopWaiting();
            this.gridResult.DataSource = this._DataSource;
            this.gridResult.Enabled = true;
        }
    }
}
