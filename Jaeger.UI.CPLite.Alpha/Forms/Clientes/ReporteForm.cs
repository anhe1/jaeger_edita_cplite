﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.CPLite.Contable.Entities;
using Jaeger.Util.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources _recursos = new EmbeddedResources("Jaeger.Domain.CPLite");
        private readonly string _dominio = "Jaeger.Domain.CPLite.Contable.Reports.";
        public ReporteForm() : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
        }

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(NotaDescuentoPrinter)) {
                this.NotaDescuentoPrinter();
            } else if (this.CurrentObject.GetType() == typeof(MovimientoBancarioPrinter)) {
                this.ReciboCobroPrinter();
            } else if (this.CurrentObject.GetType() == typeof(RemisionDetailReportePrinter)) {
                this.ReporteRemisionPrinter();
            }
        }

        private void NotaDescuentoPrinter() {
            var current = (NotaDescuentoPrinter)this.CurrentObject;
            this.LoadDefinition = this._recursos.GetStream("Jaeger.Domain.CPLite.Contable.Reports.NotaDescuentoReporte20.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Reporte");
            //this.SetParameter("Domicilio", "");
            var d = DbConvert.ConvertToDataTable<NotaDescuentoDetailModel>(new List<NotaDescuentoDetailModel>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Partidas));
            this.Finalizar();
        }

        private void ReciboCobroPrinter() {
            var current = (MovimientoBancarioPrinter)this.CurrentObject;
                if (current.FormatoImpreso == MovimientoBancarioFormatoImpreso.ReciboCobro) {
                    this.LoadDefinition = this.GetResourceStream("ReciboCobro31.rdlc");
                }
                else if (current.FormatoImpreso == MovimientoBancarioFormatoImpreso.ReciboPago) {
                    this.LoadDefinition = this.GetResourceStream("BancosMovimientoReporte31.rdlc");
                }
                else if (current.FormatoImpreso == MovimientoBancarioFormatoImpreso.ReciboComision) {
                    this.LoadDefinition = this.GetResourceStream("BancosMovimientoReporte31.rdlc");
                }
                this.SetDataSource("MovimientoBancarioComprobantes", DbConvert.ConvertToDataTable(current.Comprobantes));
            

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("MovimientoBancario");
            var d = DbConvert.ConvertToDataTable<MovimientoBancarioPrinter>(new List<MovimientoBancarioPrinter>() { current });
            this.SetDataSource("MovimientoBancario", d);
            this.Finalizar();
        }

        private void ReporteRemisionPrinter() {
            var current = (RemisionDetailReportePrinter)this.CurrentObject;
            this.LoadDefinition = this.GetResourceStream("RemisionPorCobrarReporte.rdlc");
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Conceptos));
            this.SetParameter("Domicilio", "");
            this.ImagenQR = QRCodeExtension.GetQRBase64(new string[] { "" });
            this.Procesar();
            this.SetDisplayName("Reporte Remisiones Por Cobrar");
            this.Finalizar();
        }

        private Stream GetResourceStream(string name) {
            return this._recursos.GetStream(string.Concat(this._dominio, name));
        }
    }
}
