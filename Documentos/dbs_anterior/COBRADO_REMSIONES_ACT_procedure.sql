SET TERM ^ ;
CREATE PROCEDURE COBRADO_REMSIONES_ACT (
    START_ID DOM_ID DEFAULT 0,
    END_ID DOM_ID DEFAULT 0,
    STATUS_ID DOM_ID DEFAULT 0 )
AS
declare variable id      integer;
declare variable cobrado dom_moneda;
begin
    for 
        select ctlrms.ctlrms_id
        from ctlrms
        where ctlrms.ctlrms_sttsdcs_id = :status_id
        and ctlrms.ctlrms_id between :start_id and :end_id
        order by ctlrms_id asc
            into :id
    do
        begin
            select sum(rlcncbrnz.cntbl_abono$)
            from cntbl rlcncbrnz, cntbl cbrnz
            where   rlcncbrnz.cntbl_a = 1
                and rlcncbrnz.cntbl_doc_id = 2800                --< solo documentos relacionados
                and cbrnz.cntbl_doc_id = 28  
                and cbrnz.cntbl_sttsdcs_id = 3
                and rlcncbrnz.cntbl_cntbl_id = cbrnz.cntbl_id
                and rlcncbrnz.cntbl_rem_id = :id
            into :cobrado;
            if (:cobrado is not null) then
                begin
                    update ctlrms
                        set 
                            ctlrms.ctlrms_cbrd$=:cobrado
                    where ctlrms.ctlrms_id = :id;
                end
        end
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE COBRADO_REMSIONES_ACT TO  SYSDBA;

