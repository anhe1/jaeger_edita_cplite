SET TERM ^ ;
CREATE PROCEDURE CBRNZ_RECIBOS_COBROS1 (
    INPUT_POLIZA_ID Integer,
    DIAS_PROM DOM_ID DEFAULT 0,
    INPUT_OPTION_UPDATE Integer )
AS
declare variable rlcnCbrnz_id dom_id;
declare variable rlcnCbrnz_rem_id dom_id;
declare variable drctr_id dom_id;
declare variable catprd_id dom_id;
declare variable cmsdsc_id dom_id; 
declare variable dias_trans dom_id;
declare variable cbrnz_fecpag dom_fh;
declare variable cbrnz_fecCal dom_fh;
declare variable rem_fecEmsn dom_fh;
declare variable rem_fecEntr dom_fh;
declare variable ctldsc DOM_STR50;
declare variable ddscn DOM_STR50;
declare variable dscnt DOM_MONEDA;
declare variable rem_dscnt$ dom_moneda;
declare variable sum_Venta$ dom_moneda;
declare variable rem_sbttl$ dom_moneda;
declare variable rem_impst DOM_MONEDA;
declare variable rem_impst$ dom_moneda;
declare variable rem_cobra$ dom_moneda;
declare variable rem_cbriva$ dom_moneda;
BEGIN
/*
Elabora: anhe 12122012
Proposito: aplicar descuento a las remisiones relacionadas al documento de cobro asi como verificar si es necesario recalcular el descuento
    si la fecha de pago de los mismos se modifico durante la autorización e ingreso a la boveda. Se creo de nuevo todo el procedimiento tomando en
    cuenta que el valor que guardamos como descuento es el total de la remision + iva menos lo que debe ser cobrado. Tambien se agrego al historico del
    recibo el descuento que le tocaba en ese momento de la historia. Tambien se graba en la remisión el descuento y se llama al procedimiento de calculo
    de lo cobrado en las remisiones para que se sume lo cobrado al momento.
Revisiones:
    12122012: se modifica salida
    03092013: se modifica procedimiento para suma de cobros de remision
 */

    if ( INPUT_POLIZA_ID is not null and INPUT_POLIZA_ID <> 0 ) then
        begin
            for
                select rlcncbrnz.cntbl_id, rlcncbrnz.cntbl_rem_id, rem.ctlrms_drctr_id, rem.ctlrms_catprd_id, cbrnz.cntbl_fecdocs, rem.ctlrms_fccal, rem.ctlrms_fcemsn, rem.ctlrms_fcentrg,
                    ( select sum ( ctlrms_sbttl$ ) from ctlrms where ctlrms_sttsdcs_id > 1 and ctlrms_fcemsn >= ( rem.ctlrms_fcemsn - :dias_prom ) and ctlrms_fcemsn <= ( rem.ctlrms_fcemsn ) and ctlrms_drctr_id = rem.ctlrms_drctr_id and ctlrms_id <> rem.ctlrms_id group by ctlrms_drctr_id),
                    rem.ctlrms_impst,
                    rem.ctlrms_sbttl$
                from cntbl rlcncbrnz, cntbl cbrnz, ctlrms rem
                where 
                        cbrnz.cntbl_poliza = :INPUT_POLIZA_ID       --< numero de poliza
                    and rlcnCbrnz.cntbl_poliza = Cbrnz.cntbl_poliza --< relacion partidas -> recibos
                    and rlcnCbrnz.cntbl_rem_id = rem.ctlrms_id      --< relacion con remisiones
                    and rlcnCbrnz.cntbl_doc_id = 2800               --< solo documentos relacionados
                    and Cbrnz.cntbl_doc_id = 28                     --< poliza
                into :rlcnCbrnz_id, :rlcnCbrnz_rem_id, :drctr_id, :catprd_id,:cbrnz_fecpag, :cbrnz_fecCal, :rem_fecEmsn, :rem_fecEntr, :sum_Venta$, :rem_impst, :rem_sbttl$
            do
                begin
                    execute procedure cbrnz_remision_cobros1 ( :rlcnCbrnz_rem_id , 0 );
                /*
                    dias_trans = :cbrnz_fecpag - :rem_fecEntr;
                    if ( dias_trans = 0 ) then dias_trans = 1; 
                    if ( dias_prom > 0) then             --< si son diferentes recalculamos el descuento.
                        begin
                            select p.output_cmsdsc_id, p.output_catalogo, p.output_descuent, p.output_factor
                            from cbrnz_descuent2(:drctr_id, :dias_trans, :catprd_id, :rem_fecEmsn, :sum_Venta$) p
                            into :cmsdsc_id, :ctldsc, :ddscn, :dscnt;
                            rem_cobra$ = :rem_sbttl$ * :dscnt;
                            rem_impst$ = :rem_cobra$ * rem_impst;
                            rem_cbriva$ = :rem_cobra$ + rem_impst$;
                            rem_dscnt$ = ( :rem_sbttl$ + ( :rem_sbttl$ * rem_impst) ) - rem_cbriva$; 
                            
                            if ( INPUT_OPTION_UPDATE = 1 ) then 
                                begin
                                    update ctlrms
                                        set 
                                            ctlrms_catdsc_id = :cmsdsc_id,
                                            ctlrms_ctldsc = :ctldsc,
                                            ctlrms_dscnt = :dscnt,
                                            ctlrms_total$ = :rem_cbriva$,
                                            --ctlrms_impst$ = :cbrmsn0_$impst,
                                            --ctlrms_dscnt$ = :rem_dscnt$ ,
                                            ctlrms_ddscn = :ddscn,
                                            ctlrms_fcultpg = :cbrnz_fecpag,
                                            ctlrms_fccal = current_date
                                            --ctlrms_hstrl = 'si paso'
                                    where ctlrms_id = :rlcnCbrnz_rem_id;
                                    
                                    UPDATE CNTBL 
                                        set 
                                            cntbl_fctr2 = :dscnt,
                                            cntbl_rfrnc1 = :ctldsc,
                                            cntbl_rfrnc2 = :ddscn,
                                            cntbl_sbtt$ = :rem_sbttl$,
                                            cntbl_total$ =  :rem_cbriva$,
                                            cntbl_dsct$ = :rem_dscnt$,
                                            cntbl_trns_id = :dias_trans
                                    WHERE CNTBL_id = :rlcnCbrnz_id;
                                    
                                    execute procedure cbrnz_remision_cobros1 ( :rlcnCbrnz_rem_id , :rem_cbriva$ );
                                end
                        end
                    else
                        begin
                        end*/
                    suspend;
                end
        end
    else
        begin
            exception ERROR_0018;
        end
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_RECIBOS_COBROS1 TO  SYSDBA;

