﻿using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Services {
    public static class GridDomicilioFiscalService {
        public static GridViewDataColumn[] GetColumns() {
            return new GridViewDataColumn[] {
                GridCommonService.ColCheckActivo,
                ColIdDirectorio,
                ColTipo,
                ColCalle,
                ColNoExterior,
                ColNoInterior,
                ColColonia,
                ColCodigoPostal,
                ColDelegacion,
                ColCiudad,
                ColEstado,
                ColPais,
                ColLocalidad,
                ColReferencia,
                ColTelefono,
                GridCommonService.ColCreo,
                GridCommonService.ColFechaNuevo
            };
        }

        public static GridViewTextBoxColumn ColIndice {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Id",
                    HeaderText = "Id",
                    IsVisible = false,
                    Name = "Id",
                    VisibleInColumnChooser = false
                };
            }
        }

        public static GridViewTextBoxColumn ColIdDirectorio {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "SubId",
                    HeaderText = "SubId",
                    IsVisible = false,
                    Name = "SubId",
                    VisibleInColumnChooser = false
                };
            }
        }

        /// <summary>
        /// combo con tipos de domicilio
        /// </summary>
        public static GridViewComboBoxColumn ColIdTipo {
            get {
                var combo = new GridViewComboBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdTipoDomicilio",
                    HeaderText = "Tipo",
                    Name = "Tipo",
                    Width = 75,
                };
                combo.DataSource = DirectorioService.GetTipoDomicilio();
                combo.DisplayMember = "Descripcion";
                combo.ValueMember = "Id";
                return combo;
            }
        }

        public static GridViewTextBoxColumn ColTipo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Tipo",
                    HeaderText = "Tipo",
                    Name = "Tipo",
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColCalle {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Calle",
                    HeaderText = "Calle",
                    Name = "Calle",
                    Width = 200
                };
            }
        }

        public static GridViewTextBoxColumn ColNoExterior {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NoExterior",
                    HeaderText = "No. Exterior",
                    Name = "noExterior"
                };
            }
        }

        public static GridViewTextBoxColumn ColNoInterior {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "NoInterior",
                    HeaderText = "No. Interior",
                    Name = "NoInterior"
                };
            }
        }

        public static GridViewTextBoxColumn ColColonia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Colonia",
                    HeaderText = "Colonia",
                    Name = "Colonia",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColCodigoPostal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "CodigoPostal",
                    HeaderText = "Codigo \r\nPostal",
                    Name = "CodigoPostal",
                    Width = 65,
                    MaxLength = 5, 
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
                };
            }
        }

        public static GridViewTextBoxColumn ColDelegacion {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Municipio",
                    HeaderText = "Delegación / Municipio",
                    Name = "Municipio",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColCiudad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Ciudad",
                    HeaderText = "Ciudad",
                    Name = "Ciudad",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColEstado {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Estado",
                    HeaderText = "Estado",
                    Name = "Estado",
                    Width = 150
                };
            }
        }

        public static GridViewTextBoxColumn ColPais {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Pais",
                    HeaderText = "País",
                    Name = "Pais"
                };
            }
        }

        public static GridViewTextBoxColumn ColLocalidad {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Localidad",
                    HeaderText = "Localidad",
                    Name = "Localidad",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColReferencia {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Referencia",
                    HeaderText = "Referencia",
                    Name = "Referencia",
                    Width = 85
                };
            }
        }

        public static GridViewTextBoxColumn ColTelefono {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Telefono",
                    HeaderText = "Teléfono",
                    Name = "Telefono",
                    Width = 95
                };
            }
        }
    }
}
