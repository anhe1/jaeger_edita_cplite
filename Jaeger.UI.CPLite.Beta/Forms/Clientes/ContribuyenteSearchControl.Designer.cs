﻿namespace Jaeger.UI.CPLite.Forms.Clientes {
    partial class ContribuyenteSearchControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.gridResult = new Telerik.WinControls.UI.RadGridView();
            this.Espera = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).BeginInit();
            this.SuspendLayout();
            // 
            // gridResult
            // 
            this.gridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResult.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridResult.MasterTemplate.AllowAddNewRow = false;
            this.gridResult.MasterTemplate.AllowColumnChooser = false;
            this.gridResult.MasterTemplate.AllowDeleteRow = false;
            this.gridResult.MasterTemplate.AllowDragToGroup = false;
            this.gridResult.MasterTemplate.AllowEditRow = false;
            this.gridResult.MasterTemplate.AllowRowResize = false;
            this.gridResult.MasterTemplate.ShowFilteringRow = false;
            this.gridResult.MasterTemplate.ShowRowHeaderColumn = false;
            this.gridResult.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridResult.Name = "gridResult";
            this.gridResult.ShowGroupPanel = false;
            this.gridResult.Size = new System.Drawing.Size(736, 249);
            this.gridResult.TabIndex = 16;
            this.gridResult.DoubleClick += new System.EventHandler(this.GridResult_DoubleClick);
            this.gridResult.Resize += new System.EventHandler(this.GridResult_Resize);
            // 
            // Espera
            // 
            this.Espera.Location = new System.Drawing.Point(301, 121);
            this.Espera.Name = "Espera";
            this.Espera.Size = new System.Drawing.Size(151, 24);
            this.Espera.TabIndex = 15;
            this.Espera.Text = "radWaitingBar1";
            this.Espera.Visible = false;
            this.Espera.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.Espera.WaitingSpeed = 80;
            this.Espera.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            this.dotsLineWaitingBarIndicatorElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dotsLineWaitingBarIndicatorElement1.UseCompatibleTextRendering = false;
            // 
            // ContribuyenteSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Espera);
            this.Controls.Add(this.gridResult);
            this.Name = "ContribuyenteSearchControl";
            this.Size = new System.Drawing.Size(736, 249);
            this.Load += new System.EventHandler(this.ContribuyenteSearchControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Espera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Telerik.WinControls.UI.RadGridView gridResult;
        private Telerik.WinControls.UI.RadWaitingBar Espera;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
    }
}
