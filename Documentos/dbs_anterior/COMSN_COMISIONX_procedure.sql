SET TERM ^ ;
CREATE PROCEDURE COMSN_COMISIONX (
    IN_VNDR DOM_ID DEFAULT 0,
    IN_DIAS DOM_ID DEFAULT 0,
    IN_FECH DOM_FH,
    IN_DESC DOM_MONEDA DEFAULT 0,
    IN_VNTA DOM_MONEDA DEFAULT 0 )
RETURNS (
    OUT_ID DOM_ID,
    OUT_CTLG DOM_STR255,
    OUT_CMSN DOM_STR1000,
    OUT_INTG DOM_STR1000,
    OUT_FCT1 DOM_MONEDA,
    OUT_FCT2 DOM_MONEDA )
AS
BEGIN
/*
Elabora: Anhe.28032013
Proposito: la consulta solo regresa la comision sin multas
Revisiones:
*/ 
    if (:IN_DIAS=0) then IN_DIAS=1;
    select first 1 com.cmsdsc_id, com.cmsdsc_nom,des.tbrngs_nom, des.tbrngs_factor
    from cmsdsc com, tbrngs des, rlcdrc vnd
    where   com.cmsdsc_doc_id = 1016                -- solo catalogo de comisiones
        and des.tbrngs_doc_id = 1037                -- comisi�n por descuento real aplicado al cliente

        and vnd.rlcdrc_doc_id = 1016                -- solo relacion de vendedor -> comisiones
        and com.cmsdsc_a = 1                        -- solo rangos activos
        and des.tbrngs_a = 1

        and vnd.rlcdrc_a = 1
        and des.tbrngs_cmsdsc_id = com.cmsdsc_id    -- rangos de descuentos aplicados al cliente

        and vnd.rlcdrc_cmsn_id = com.cmsdsc_id
        and vnd.rlcdrc_vnddr_id = :in_vndr                          --<-- vendedor
        and :in_desc between des.tbrngs_rng1 and des.tbrngs_rng2         --<-- rango de descuento aplicado
        
        and :in_fech between com.cmsdsc_fchini and com.cmsdsc_fchvig    --<-- fecha de vigencia
        into
            :out_id, :out_ctlg, :out_cmsn, :out_fct1;
    if ( :out_id is null or :out_id = 0 ) then
        begin
            out_id      = -1;
            out_ctlg    = 'no se pudo calcular';
            out_cmsn    = '0%';
            out_intg    = '0%';
            out_fct1    = 0;
            out_fct2    = 0;
        end
    SUSPEND;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE COMSN_COMISIONX TO  SYSDBA;

