﻿using System;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Almacen.Services;
using Jaeger.Domain.Almacen.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    internal class ProductoExistenciaForm : UI.Almacen.Forms.ProductoExistenciaForm {
        private Jaeger.Aplication.Tienda.Contracts.ICatalogoProductosService _productosCatalogoService;
        private UIMenuElement menuElement;

        public ProductoExistenciaForm(UIMenuElement menuElement) : base() {
            this.menuElement = menuElement;
            this.Load += ProductoExistenciaForm_Load;
        }

        private void ProductoExistenciaForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Existencias";
            this._productosCatalogoService = new Aplication.CPLite.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
            this._CatalogoService = new Aplication.CPLite.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
            this.Almacen.DataSource = this._productosCatalogoService.GetAlmacenes();
        }

        public override void TExistencia_TablaDinamica_Click(object sender, EventArgs e) {
            var tablaDinamica = new ProductoExistenciaTDForm(this.menuElement) { MdiParent = ParentForm };
            tablaDinamica.Show();
        }

        public override void Cargar() {
            if (this.Almacen.SelectedItem == null)
                return;
            var current = ((GridViewDataRowInfo)this.Almacen.SelectedItem).DataBoundItem as IAlmacenModel;
            this._DataSource = this._CatalogoService.GetList<ProductoModeloExistenciaModel>(CatalogoModeloService.Query().ByIdAlmacen(current.IdAlmacen+2).Activo().Build()).ToList<IProductoExistenciaModel>();
        }
    }
}
