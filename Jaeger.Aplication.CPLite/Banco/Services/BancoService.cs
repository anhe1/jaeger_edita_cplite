﻿using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Banco.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.FB.Banco.Repositories;

namespace Jaeger.Aplication.CPLite.Banco.Services {
    public class BancoService : Aplication.Banco.BancoService, Aplication.Banco.IBancoService, Aplication.Banco.IBancoCuentaService {
        public BancoService() : base() { }

        public override void OnLoad() {
            this._Empresa = new Aplication.CPLite.Services.ConfiguracionService();
            var parametros = this._Empresa.Get(Domain.Empresa.Enums.ConfigGroupEnum.Bancos);
            this.Configuration = new Jaeger.Aplication.CPLite.Banco.Builder.ConfigurationBuilder().Build(parametros);
            this.movimientoBancarioRepository = new SqlFbMovimientoBancarioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.beneficiarioRepository = new SqlFbBeneficiarioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.conceptoRepository = new SqlFbMovimientoConceptoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.formaPagoRepository = new SqlFbBancoFormaPagoRepository(GeneralService.Configuration.DataBase , ConfigService.Piloto.Clave);
            this.auxiliarRepository = new SqlFbMovimientoAuxiliarRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.cuentaBancariaRepository = new SqlFbCuentaBancariaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.cuentaSaldoRepository = new SqlFbCuentaSaldoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.s3 = new EditaBucketService();
        }

        public override BindingList<BeneficiarioDetailModel> GetBeneficiarios(BindingList<ItemSelectedModel> relationType, string search = "") {
            var indices = relationType.Select(it => it.Id).ToList();
            var query = new BeneficiarioQueryBuilder().Relacion(indices).IsActive().Build();
            return new BindingList<BeneficiarioDetailModel>(this.beneficiarioRepository.GetList<BeneficiarioDetailModel>(query).ToList());
        }
    }
}