/* *******************************************************************************************
Importacion de datos paso 5
con este procedimiento actualizamos los indice del catalogo de precios actual antes de importar
la informacion del directorio
******************************************************************************************* */
--ALTER TABLE DRCTRR DROP CONSTRAINT PK_DRCTRR_1D;
UPDATE DRCTR SET DRCTR.DRCTR_PRCS_ID = (
SELECT rel.RLCDRC_CTLPRCS_ID
FROM rlcdrc rel
WHERE rel.rlcdrc_doc_id = 1086             
    AND rel.rlcdrc_a = 1                   
    AND rel.RLCDRC_DRCTR_ID = DRCTR.DRCTR_ID
);

/* *******************************************************************************************
con este procedimiento actualizamos los indice del catalogo de comsiones actual
******************************************************************************************* */
UPDATE DRCTR SET DRCTR.DRCTR_CMSN_ID = (SELECT RLCDRC.RLCDRC_CMSN_ID FROM RLCDRC WHERE RLCDRC_DOC_ID = 1030 AND RLCDRC_A = 1 AND RLCDRC_LSTSYS_ID = 4 AND RLCDRC.RLCDRC_DRCTR_ID = DRCTR.DRCTR_ID);
