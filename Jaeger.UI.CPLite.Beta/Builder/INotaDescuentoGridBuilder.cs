﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.CPLite.Builder {
    public interface INotaDescuentoGridBuilder : IGridViewBuilder, IDisposable {
        INotaDescuentoTempletesGridBuilder Templetes();
    }

    public interface INotaDescuentoTempletesGridBuilder : IGridViewBuilder {
        INotaDescuentoTempletesGridBuilder Master();
        INotaDescuentoTempletesGridBuilder Documentos();
    }

    public interface INotaDescuentoColumnsGridBuilder : IGridViewBuilder {
        INotaDescuentoColumnsGridBuilder Folio();
        INotaDescuentoColumnsGridBuilder IdStatus();
        INotaDescuentoColumnsGridBuilder Clave();
        INotaDescuentoColumnsGridBuilder Cliente();
        INotaDescuentoColumnsGridBuilder FechaEmision();
        INotaDescuentoColumnsGridBuilder FechaCancela();
        /// <summary>
        /// obtener columna de motivo de devoluciones
        /// </summary>
        INotaDescuentoColumnsGridBuilder ClaveMotivo();
        INotaDescuentoColumnsGridBuilder IdMotivo();
        INotaDescuentoColumnsGridBuilder Importe();
        INotaDescuentoColumnsGridBuilder GranTotal();
        INotaDescuentoColumnsGridBuilder IdDocumento();
        INotaDescuentoColumnsGridBuilder Referencia();
        INotaDescuentoColumnsGridBuilder Contacto();
        INotaDescuentoColumnsGridBuilder Nota();
        INotaDescuentoColumnsGridBuilder Numeros();
        INotaDescuentoColumnsGridBuilder FechaNuevo();
        INotaDescuentoColumnsGridBuilder Creo();
        INotaDescuentoColumnsGridBuilder IdPedido();
        INotaDescuentoColumnsGridBuilder SubTotal();
        INotaDescuentoColumnsGridBuilder TotalTrasladoIVA();
    }
}
