﻿using System;
using System.Collections.Generic;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Util.Services;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.CPLite");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(PedidoClientePrinter)) {
                this.CrearPedido();
            } else if (this.CurrentObject.GetType() == typeof(List<PedidoClientePrinter>)) {
                this.CrearPedidoL();
            } else if (this.CurrentObject.GetType() == typeof(List<PedidosReportePrinter>)) {
                this.CrearPedidoL1();
            }
        }

        private void CrearPedido() {
            var current = (PedidoClientePrinter)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Tienda.Reports.PedidoClienteReporte.rdlc");

            this.ImagenQR = QRCodeExtension.GetQRBase64(current.QrText);
            this.Procesar();
            this.SetDisplayName("Pedido");
            var d = DbConvert.ConvertToDataTable<PedidoClientePrinter>(new List<PedidoClientePrinter>() { current });
            this.SetDataSource("Comprobante", d);
            this.SetDataSource("Conceptos", DbConvert.ConvertToDataTable(current.Partidas));
            this.Finalizar();
        }

        private void CrearPedidoL() {
            var current = (List<PedidoClientePrinter>)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Tienda.Reports.PedidoClienteLReporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Pedidos");
            this.SetDataSource("Pedido", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }

        private void CrearPedidoL1() {
            var current = (List<PedidosReportePrinter>)this.CurrentObject;
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Tienda.Reports.PedidoClienteL1Reporte.rdlc");
            this.Procesar();
            this.SetDisplayName("Pedidos");
            this.SetDataSource("Pedido", DbConvert.ConvertToDataTable(current));
            this.Finalizar();
        }
    }
}
