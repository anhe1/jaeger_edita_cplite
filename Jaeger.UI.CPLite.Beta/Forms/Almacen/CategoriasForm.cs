﻿using System;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Almacen.Builder;
using Jaeger.UI.Common.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    internal class CategoriasForm : UI.Almacen.Forms.CategoriasTreeViewForm {
        

        public CategoriasForm(UIMenuElement menuElement) {
            this.Load += CategoriasForm_Load;
        }

        private void CategoriasForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Categorías";
            using (IProductoGridBuilder view = new Jaeger.UI.Tienda.Builder.ProductoGridBuilder()) {
                this.TControl.GridData.Columns.AddRange(view.Templetes().ProductoModeloAlmacen().Build());
            }
            this._CatalogoService = new Aplication.CPLite.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT);
            this._ClasificacionService = new Aplication.CPLite.Almacen.Services.ClasificacionService();
        }

        protected override void TControl_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }

            var categoriaColumn = this.TControl.GridData.Columns["IdCategoria"] as GridViewMultiComboBoxColumn;
            categoriaColumn.DataSource = this._Clasificacion2;
            categoriaColumn.DisplayMember = "Descripcion";
            categoriaColumn.ValueMember = "IdCategoria";

            var IdUnidadAlmacen = this.TControl.GridData.Columns["IdUnidad"] as GridViewComboBoxColumn;
            IdUnidadAlmacen.DataSource = this._UnidadesAlmacen;
            IdUnidadAlmacen.DisplayMember = "Descripcion";
            IdUnidadAlmacen.ValueMember = "IdUnidad";

            var unidadXYColumn = this.TControl.GridData.Columns["UnidadXY"] as GridViewComboBoxColumn;
            unidadXYColumn.DataSource = this._UnidadesXY;
            unidadXYColumn.DisplayMember = "Descripcion";
            unidadXYColumn.ValueMember = "IdUnidad";

            var unidadZColumn = this.TControl.GridData.Columns["UnidadZ"] as GridViewComboBoxColumn;
            unidadZColumn.DataSource = this._UnidadesZ;
            unidadZColumn.DisplayMember = "Descripcion";
            unidadZColumn.ValueMember = "IdUnidad";

            this.TControl.ValueMember = "IdCategoria";
            this.TControl.TreeData.DataSource = this._Clasificacion;
            this.TControl.TreeData.DisplayMember = "Clase";
            this.TControl.TreeData.ValueMember = "IdCategoria";
            this.TControl.TreeData.ParentMember = "IdSubCategoria";
            this.TControl.TreeData.ChildMember = "IdCategoria";
            this.TControl.TreeData.SelectedNode = this.TControl.TreeData.Nodes[0];
            this.TControl.GridData.DataSource = this._DataSource;
        }
    }
}
