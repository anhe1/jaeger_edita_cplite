﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class PedidoPorSutirForm : PedidoCatalogoForm {
        public PedidoPorSutirForm(UIMenuElement menuElement) : base(menuElement) {
            this.TPedido.IsCosto = false;
            this.Load += this.PedidoCatalogoForm_Load;
        }

        private void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Pedidos por surtir";
            this.TPedido.ShowPeriodo = false;
        }
        /// <summary>
        /// sobre escibimos el metodo de la consulta
        /// </summary>
        public override void TPedido_Consultar() {
            this.TPedido._DataSource = new BindingList<PedidoClienteDetailModel>(
                this.TPedido.Service.GetList<PedidoClienteDetailModel>(
                    Aplication.Tienda.Services.PedidosService.Query().Year(this.TPedido.GetEjercicio()).Status(Domain.Tienda.ValueObjects.PedidoClienteStatusEnum.Por_Surtir).Build()
                    ).ToList());

            this.Vendedores();
            var IdVendedor = this.TPedido.GridData.Columns["IdVendedor"] as GridViewComboBoxColumn;
            IdVendedor.DataSource = this.TPedido.Vendedores;
            IdVendedor.DisplayMember = "Clave";
            IdVendedor.ValueMember = "IdVendedor";
        }
    }
}
