SET TERM ^ ;
CREATE TRIGGER CTLRMS_ASIGNACION_VENDEDOR FOR CTLRMS ACTIVE
BEFORE INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 24102012
    Purpose: verificar el campo de vendedor en la remision.
------------------------------------------------------------------------------------------------------------- */
    
    if ( new.ctlrms_doc_id = 26 and new.ctlrms_sttsdcs_id = 1 and new.ctlrms_ctlgvddr_id is null ) then
        begin
            exception error_0028;
        end
END^
SET TERM ; ^
