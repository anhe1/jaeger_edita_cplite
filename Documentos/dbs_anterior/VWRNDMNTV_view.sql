CREATE VIEW VWRNDMNTV (VWRNDMNTV_ID, VWRNDMNTV_A, VWRNDMNTV_VENDEDOR, VWRNDMNTV_REM$, VWRNDMNTV_TOTAL$, VWRNDMNTV_IMPUESTO$, VWRNDMNTV_DESCONTADO$, VWRNDMNTV_COBRADO$, VWRNDMNTV_DEUDA$, VWRNDMNTV_FACTOR, VWRNDMNTV_COSTO, VWRNDMNTV_DIF1, VWRNDMNTV_DIF2, VWRNDMNTV_DIF3, VWRNDMNTV_ANIO, VWRNDMNTV_MES)
AS           
select
    vwvnddr_id,
    vwvnddr_a,
    vwvnddr_nom, 
    sum(ctlrms_sbttl$) as remisionado,
    sum(ctlrms_total$) as cobrar,
    sum(ctlrms_impst$) as impuestos,
    sum(ctlrms_dscnt$) as descontado,
    sum(ctlrms_cbrd$) as cobrado,
    sum(ctlrms_sld$) as saldo,
    (sum(ctlrms_cbrd$)/sum(ctlrms_total$)) as factor,
    sum(ctlrms_cstprd$) as costo,
    sum(ctlrms_cbrd$) - sum(ctlrms_cstprd$) as diferencia1,
    sum(ctlrms_total$) - sum(ctlrms_cstprd$) as diferencia2,
    sum(ctlrms_sbttl$) - sum(ctlrms_cstprd$) as diferencia3,
    extract(year from ctlrms.ctlrms_fcentrg) as anio,
    extract(month from ctlrms.ctlrms_fcentrg) as mes
from
    ctlrms, vwvnddr
where 
    ctlrms_sttsdcs_id <> 0 and 
    ctlrms_ctlgvddr_id = vwvnddr_id and
    ctlrms_fcentrg is not null
group by
    vwvnddr_id,vwvnddr_a, vwvnddr_nom, extract(year from ctlrms.ctlrms_fcentrg), extract(month from ctlrms.ctlrms_fcentrg)
order by
    remisionado desc, extract(year from ctlrms.ctlrms_fcentrg) asc, extract(month from ctlrms.ctlrms_fcentrg) asc ;

UPDATE RDB$RELATIONS set
  RDB$DESCRIPTION = 'Bancos: catalogo de formas de pago'
  where RDB$RELATION_NAME = 'VWRNDMNTV';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWRNDMNTV TO  SYSDBA WITH GRANT OPTION;

