SET TERM ^ ;
CREATE PROCEDURE CBRNZ_UNITARIO (
    INPUT_CTLRMS_ID DOM_ID DEFAULT 0,
    INPUT_TAM_ID DOM_ID DEFAULT 0,
    INPUT_CAT_ID DOM_ID DEFAULT 0 )
RETURNS (
    OUTPUT_UNITA$ DOM_MONEDA,
    OUTPUT_COSTO$ DOM_MONEDA,
    OUTPUT_OBSER DOM_STR15 )
AS
BEGIN
/* 
Elabora: anhe 15102012
Proposito: devolver el precio unitario del tamanio asociado al catalogo de precios asignado al cliente de no
tener asignado alguno o no encontrar el precio en este se devuelve el precio predeterminado
Revisiones:
    17102012: el catalogo de precios tambien solo debe actuar sobre un cat. de productos especificados que son las excepciones
        a los precios regulares. de este modo se agrega el 
    06102014: se agrega esta linea "and rel.rlcdrc_a = 1 " para que solo se puedan seleccionar los catalogos que estan activos dentro del sistema
*/ 
    --select rel.rlcdrc_sec_id, reg.ctlprcs_id , reg.ctlprcs_ctlprcs_id, reg.ctlprcs_doc_id, tam.lstsys_doc_id, tam.LSTSYS_ID, tam.LSTSYS_SEC_ID, tam.LSTSYS_NOM, reg.ctlprcs_unit$
    select first 1 reg.ctlprcs_unit$, reg.ctlprcs_costo$
    from 
	rlcdrc rel, ctlprcs reg, lstsys tam
	where   rel.rlcdrc_drctr_id = ( select ctlrms_drctr_id from ctlrms where ctlrms_id = :input_ctlrms_id  ) --< Catalogos de precios asignado al cliente
        and rel.rlcdrc_doc_id = 1086                        --< Catalogo de relaciones
		and reg.ctlprcs_doc_id = 1097                       --< Catalogo de precios
		and tam.lstsys_doc_id = 1091                        --< Catalogo de tamanios
		and reg.ctlprcs_prdct_id = :input_cat_id            --< Cat. al que pertenece el producto
		and reg.ctlprcs_ctlprcs_id = rel.rlcdrc_ctlprcs_id 
		and reg.ctlprcs_tam_id = tam.lstsys_sec_id 
		and tam.lstsys_sec_id = :input_tam_id               --< Tamanio a buscar 
		and rel.rlcdrc_a = 1 
		and reg.ctlprcs_a=1
    order by rel.rlcdrc_sec_id desc
	into
        :output_unita$, :output_costo$;
    -- si el precio a buscar es nulo entonces aplicamos el catalogo de precios predeterminado
    if ( :output_unita$ is null or :output_unita$ = 0 ) then
        begin
            --select ctlprcs_id,ctlprcs_a,ctlprcs_doc_id,ctlprcs_ctlprcs_id,ctlprcs_tam_id,lstsys_sec_id,lstsys_a,lstsys_nom,lstsys_obsrv,lstsys_unt,ctlprcs_costo$,ctlprcs_impst,ctlprcs_unit$,ctlprcs_obsrv,ctlprcs_fn,ctlprcs_usu_n 
            select first 1 reg.ctlprcs_unit$, reg.ctlprcs_costo$
            from ctlprcs reg , lstsys tam, ctlprcs cat
            where reg.ctlprcs_doc_id = 1097 and             --< catalogo de precios
                  tam.lstsys_doc_id = 1091 and              --< catalogo de tamanios
                  cat.ctlprcs_sec_id = 1 and                --< catalogo de precios a buscar
                  reg.ctlprcs_tam_id = :input_tam_id and    --< catalogo de tamanios a buscar
                  reg.ctlprcs_tam_id = tam.lstsys_sec_id and
                  cat.ctlprcs_id = reg.ctlprcs_ctlprcs_id and
                  reg.ctlprcs_a = 1 and
                  tam.lstsys_a = 1 
            order by reg.ctlprcs_sec_id desc
            into
                :output_unita$, :output_costo$;
                output_obser = 'Predeterminado';
        end
    suspend;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_UNITARIO TO  SYSDBA;

