﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.Banco.Repositories;

namespace Jaeger.Aplication.CPLite.Banco.Services {
    public class BancoFormaPagoService : Aplication.Banco.BancoFormaPagoService, Aplication.Banco.IBancoFormaPagoService {
        public BancoFormaPagoService() : base() { }

        public override void OnLoad() {
            this.formaPagoRepository = new SqlFbBancoFormaPagoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}