﻿using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Tienda.Builder;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.Aplication.CPLite.Ventas.Services {
    public class VentasService : IVentasService {
        protected internal ISqlVentasRepository partidaRepository;

        public VentasService() {
            this.partidaRepository = new SqlFbVentasRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.partidaRepository.GetList<T1>(conditionals);
        }

        #region metodos estaticos
        public static List<VentasResumenTipo> GetResumenTipo() {
            return new List<VentasResumenTipo> {
                new VentasResumenTipo(1, "Pedidos"),
                new VentasResumenTipo(2, "Remisionado")
            };
        }

        public static IResumenQueryBuilder Query() {
            return new ResumenQueryBuilder();
        }
        #endregion
    }
}
