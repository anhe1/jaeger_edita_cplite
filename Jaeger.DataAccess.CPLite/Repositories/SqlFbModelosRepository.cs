﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbModelosRepository : Almacen.Repositories.SqlFbModelosRepository, Domain.Almacen.Contracts.ISqlModelosXRepository {
        public SqlFbModelosRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion, user) {
            this.User = user;
        }

        public new IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand();
            if (typeof(T1) == typeof(ProductoServicioModeloModel)) {
                sqlCommand.CommandText = @"SELECT * FROM CTPRD 
LEFT JOIN CTMDL ON (CTPRD_ID = CTMDL_CTPRD_ID) 
LEFT JOIN CTMDLX ON (CTMDL_ID = CTMDLX_CTMDL_ID)
LEFT JOIN CTMDLE ON CTMDLE.CTMDLE_CTMDL_ID = CTMDL.CTMDL_ID
LEFT JOIN CTESPC ON CTESPC.CTESPC_ID = CTMDLE.CTMDLE_CTESPC_ID @condiciones";
                var d0 = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (d0 != null) {
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@wcondiciones", "WHERE (P.CTPRD_NOM LIKE LOWER(CONCAT('%',@search,'%'))) OR LOWER((M.CTMDL_DSCRC LIKE CONCAT('%',@search,'%'))) @condiciones");
                    sqlCommand.CommandText = sqlCommand.CommandText.Replace("@search", "'" + d0.FieldValue + "'");
                    conditionals = conditionals.Where(it => !it.FieldName.ToLower().Contains("@search")).ToList();
                }
                return this.GetMapper<T1>(sqlCommand, conditionals).ToList();

            } else 
            if (typeof(T1) == typeof(ProductoXModelo)) {
                var search = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (search != null) {
                    conditionals.Add(new Conditional("SEARCH", search.FieldValue.ToLower().ToString(), ConditionalTypeEnum.Like));
                    conditionals.Remove(search);
                }
                sqlCommand = new FbCommand {
                    CommandText = @"SELECT VWMDLG.*, CTESPC.*, CTMDLX.*, CTMDLE.*
FROM VWMDLG
LEFT JOIN CTMDLE ON CTMDLE.CTMDLE_CTMDL_ID = VWMDLG.IDMODELO
LEFT JOIN CTESPC ON CTESPC.CTESPC_ID = CTMDLE.CTMDLE_CTESPC_ID
LEFT JOIN CTMDLX ON CTMDLX.CTMDLX_CTMDL_ID = VWMDLG.IDMODELO AND CTMDLX.CTMDLX_CTPRD_ID = VWMDLG.IDPRODUCTO AND CTMDLX.CTMDLX_CTESPC_ID = CTMDLE.CTMDLE_CTESPC_ID @condiciones"
                };
            } else if (typeof(T1) == typeof(ProductoXModelo)) {
                var d0 = conditionals.Where(it => it.FieldName.ToLower().Contains("@search")).FirstOrDefault();
                if (d0 != null) {
                    conditionals.Add(new Conditional("SEARCH", d0.FieldValue.ToString(), ConditionalTypeEnum.Like));
                    conditionals.Remove(d0);
                }
                // en caso de que no existe indice de especificación el precio se toma del modelo
                sqlCommand = new FbCommand {
                    CommandText = @"SELECT VWXMDL.*, IIF(CTMDLE.CTMDLE_CTESPC_ID IS NULL, -1, CTMDLE.CTMDLE_CTESPC_ID) AS CTMDLE_CTESPC_ID, CTESPC.CTESPC_NOM FROM VWXMDL LEFT JOIN CTMDLE ON CTMDLE.CTMDLE_CTMDL_ID = VWXMDL.VWXMDL_CTMDL_ID AND CTMDLE.CTMDLE_A = 1 LEFT JOIN CTESPC ON CTMDLE.CTMDLE_CTESPC_ID = CTESPC.CTESPC_ID AND CTESPC.CTESPC_A = 1 @condiciones"
                };
                return this.GetMapper<T1>(sqlCommand, conditionals);
            } else {
                sqlCommand = new FbCommand {
                    CommandText = @"SELECT * FROM CTMDL @condiciones"
                };
            }

            return GetMapper<T1>(sqlCommand, conditionals);
        }

        public ModeloXDetailModel Save(ModeloXDetailModel item) {
            if (item.IdModelo == 0) {
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
                item.IdModelo = this.Insert(item);
            } else {
                item.FechaModifica = DateTime.Now;
                item.Modifica = this.User;
                this.Update(item);
            }
            return item;
        }
    }
}
