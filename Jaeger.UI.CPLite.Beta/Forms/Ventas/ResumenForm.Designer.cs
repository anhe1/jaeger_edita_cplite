﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class ResumenForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Resumen = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.TResumen = new Jaeger.UI.Common.Forms.PivotStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.Resumen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resumen.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resumen.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Resumen
            // 
            this.Resumen.AutoSizeDropDownHeight = true;
            this.Resumen.AutoSizeDropDownToBestFit = true;
            // 
            // Resumen.NestedRadGridView
            // 
            this.Resumen.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Resumen.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Resumen.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Resumen.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Resumen.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Resumen.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Resumen.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "ID";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "Descriptor";
            gridViewTextBoxColumn2.HeaderText = "Descriptor";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "Descriptor";
            gridViewTextBoxColumn2.VisibleInColumnChooser = false;
            gridViewTextBoxColumn3.FieldName = "Descripcion";
            gridViewTextBoxColumn3.HeaderText = "Descripción";
            gridViewTextBoxColumn3.Name = "Descripcion";
            gridViewTextBoxColumn3.Width = 80;
            this.Resumen.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.Resumen.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Resumen.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Resumen.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Resumen.EditorControl.Name = "NestedRadGridView";
            this.Resumen.EditorControl.ReadOnly = true;
            this.Resumen.EditorControl.ShowGroupPanel = false;
            this.Resumen.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Resumen.EditorControl.TabIndex = 0;
            this.Resumen.Location = new System.Drawing.Point(387, 147);
            this.Resumen.Name = "Resumen";
            this.Resumen.Size = new System.Drawing.Size(82, 20);
            this.Resumen.TabIndex = 1;
            this.Resumen.TabStop = false;
            // 
            // TResumen
            // 
            this.TResumen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TResumen.Location = new System.Drawing.Point(0, 0);
            this.TResumen.Name = "TResumen";
            this.TResumen.Size = new System.Drawing.Size(1004, 679);
            this.TResumen.TabIndex = 0;
            // 
            // ResumenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 679);
            this.Controls.Add(this.Resumen);
            this.Controls.Add(this.TResumen);
            this.Name = "ResumenForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Ventas: Resumen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RemisionResumenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Resumen.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resumen.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Resumen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Common.Forms.PivotStandarControl TResumen;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Resumen;
    }
}