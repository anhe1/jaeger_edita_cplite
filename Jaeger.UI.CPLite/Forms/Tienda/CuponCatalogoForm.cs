﻿using System;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    public class CuponCatalogoForm : UI.Tienda.Forms.CuponCatalogoForm {
        public CuponCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += this.CuponCatalogoForm_Load;
        }

        private void CuponCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new CuponesService();
        }
    }
}
