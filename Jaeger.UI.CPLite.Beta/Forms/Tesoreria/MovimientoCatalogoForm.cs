﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Tesoreria {
    public class MovimientoCatalogoForm : UI.Banco.Forms.MovimientoCatalogoForm {
        public MovimientoCatalogoForm() {
            this.TMovimiento.Service = new Aplication.CPLite.Banco.Services.BancoService();
        }

        public MovimientoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.TMovimiento.Service = new Aplication.CPLite.Banco.Services.BancoService();
        }
    }
}
