﻿***************************************************************************************************
*** CP Lite (Bolsa de Regalo)
***************************************************************************************************
  + Archivo
    - Configuración
    - Menus
    - Perfiles
    - Usuarios
    - Salir
  + Administración
    + Cliente
      - Expediente
      - Pedidos
      + Remisionado
        - Remisión
        - Remisión por cobrar
        - Remisión Partida
      - Nota de Descuento
      - Cobranza
      + Reportes
        - Estado de Cuenta Cliente
    + Banco
      - Cuentas Bancarias
      - Formas de Pago
      - Tipos de Operación
      - Beneficiarios
      - Movimientos
    + Almacén
      + Catálogo de Productos
        - Produtos
        - Productos-Modelos
		- Catálogo de Unidades
		- Especificaciones
      + Pedidos
	    - Historial
		- Por Surtir
    - Movimientos
        - Vales de Almacén
        - Remisionado
        - Existencia
  + Tienda
    + Ajustes
	    - Secciones
		- Categorías
	- Cupones
	- Banner
	- FAQ's (Preguntas frecuentes)
  + Ventas
    - Catálogo de Precios
    - Catálogo de Descuentos (ya no funciona)
    - Pedidos
    + Vendedores
      - Catálogo
      - Comisiones
      - Comisión por Remisión
      - Recibos de Comisión
      + Reportes
        - Ventas por vendedor
        - Estado de Cuenta Vendedor
        - Resumen de Ventas
  - Ventana
  + Ayuda
    - Acerca de ...