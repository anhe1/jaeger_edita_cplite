﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Banco {
    public class BeneficiarioCatalogoForms : UI.Banco.Forms.BeneficiarioCatalogoForms {
        public BeneficiarioCatalogoForms() {
            this.Load += this.BeneficiarioCatalogoForms_Load;
        }

        public BeneficiarioCatalogoForms(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.BeneficiarioCatalogoForms_Load;
        }

        private void BeneficiarioCatalogoForms_Load(object sender, EventArgs e) {
            this.service = new Aplication.CPLite.Services.BancoService();
        }
    }
}
