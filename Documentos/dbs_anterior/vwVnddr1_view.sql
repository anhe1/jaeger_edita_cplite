SET AUTODDL ON;

DROP VIEW VWVNDDR1;

/**************** DROPPING COMPLETE ***************/

CREATE VIEW VWVNDDR1 (VWVNDDR1_ID, VWVNDDR1_A, VWVNDDR1_CLV, VWVNDDR1_NOM, VWVNDDR1_DOC_ID, VWVNDDR1_SEC_ID, VWVNDDR1_RELACION, VWVNDDR1_PSW)
AS               
select 
    rlcdrc.rlcdrc_drctr_id as VWVNDDR1_id,
    rlcdrc.rlcdrc_a as VWVNDDR1_a,
    drctr.drctr_clv as VWVNDDR1_clv,
    (
        drctr.drctr_nom || ' ' || 
        case when drctr.drctr_prmraplld is not null then drctr.drctr_prmraplld else '' end  || ' ' || 
        case when drctr.drctr_sgndaplld is not null then drctr.drctr_sgndaplld else '' end 
    ) as VWVNDDR1_nom,
    
    rlcdrc.rlcdrc_doc_id as VWVNDDR1_doc_id,
    lstsys.lstsys_sec_id as VWVNDDR1_sec_id,
    lstsys.lstsys_nom as VWVNDDR1_relacion,
    drctr_psw as VWVNDDR1_psw
from 
    rlcdrc , 
    lstsys  ,  
    drctr
where 
    rlcdrc.rlcdrc_lstsys_id = lstsys.lstsys_sec_id and 
    rlcdrc.rlcdrc_drctr_id = drctr.drctr_id and 
    lstsys.lstsys_doc_id = 1030 and 
    lstsys.lstsys_sec_id = 4 

;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWVNDDR1 TO  SYSDBA WITH GRANT OPTION;
