CREATE VIEW VWRCBCM (VWRCBCM_ID, VWRCBCM_A, VWRCBCM_DOC_ID, VWRCBCM_STTSDCS_ID, VWRCBCM_FRMPGS_ID, VWRCBCM_DRCTR_ID, VWRCBCM_POLIZA, VWRCBCM_FOLIO, VWRCBCM_STATUS, VWRCBCM_FECEMSN, VWRCBCM_CLV, VWRCBCM_VNDDR, VWRCBCM_PAGO, VWRCBCM_FECDOCS, VWRCBCM_FECCBRO, VWRCBCM_BANCO, VWRCBCM_RFRNC2, VWRCBCM_AUTORIZA, VWRCBCM_CAGO$, VWRCBCM_CNTCT, VWRCBCM_OBSRV, VWRCBCM_USU_N)
AS  
select 
    cntbl_id            vwrcbcm_id,
    cntbl_a             vwrcbcm_a,
    cntbl_doc_id        vwrcbcm_doc_id,
    cntbl_sttsdcs_id    vwrcbcm_sttsdcs_id,
    cntbl_frmpgs_id     vwrcbcm_frmpgs_id,
    cntbl_drctr_id      vwrcbcm_drctr_id,
    cntbl_poliza        vwrcbcm_poliza,
    cntbl_folio         vwrcbcm_folio,
    vwstatus_nom        vwrcbcm_status,
    cntbl_fecemsn       vwrcbcm_fecemsn,
    vwrlcn_clv          vwrcbcm_clv,
    vwrlcn_nom          vwrcbcm_vnddr,
    lstsys_nom          vwrcbcm_pago,
    cntbl_fecdocs       vwrcbcm_fecdocs,
    cntbl_feccbro       vwrcbcm_feccbro,
    cntbl_banco         vwrcbcm_banco,
    cntbl_rfrnc2        vwrcbcm_rfrnc2,
    cntbl_autoriza      vwrcbcm_autoriza,
    cntbl_cargo$        vwrcbcm_cago$,
    cntbl_cntct         vwrcbcm_cntct,
    cntbl_obsrv         vwrcbcm_obsrv,
    cntbl_usu_n         vwrcbcm_usu_n
from cntbl,vwstatus,vwrlcn,lstsys  
where cntbl_doc_id = 36 
    and cntbl_sttsdcs_id = vwstatus_sec_id 
    and vwstatus_doc_id = 36 
    and vwrlcn_id = cntbl_drctr_id 
    and vwrlcn_sec_id = 4 
    and cntbl_frmpgs_id = lstsys_sec_id 
    and lstsys_doc_id = 1025;

UPDATE RDB$RELATIONS set
  RDB$DESCRIPTION = 'Bancos: catalogo de formas de pago'
  where RDB$RELATION_NAME = 'VWRCBCM';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWRCBCM TO  SYSDBA WITH GRANT OPTION;

