SET TERM ^ ;
CREATE TRIGGER CTLRMS_CONTROL_STATUS FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 191120211501
    Purpose: 
------------------------------------------------------------------------------------------------------------- */
    
    if (new.ctlrms_doc_id = 26 and new.ctlrms_aut_id = 1) then begin 
        new.ctlrms_aut_id = 0;
    end

END^
SET TERM ; ^
