﻿using System;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class RemisionPorClienteForm : UI.Almacen.PT.Forms.RemisionadoClienteForm {
        protected internal RadMenuItem _ContextMenuReciboCobro = new RadMenuItem { Text = "Registrar Cobro", Name = "tcpl_gcliente_cobranza" };
        protected Aplication.Banco.IBancoService _BancoService;

        public RemisionPorClienteForm(UIMenuElement menuElement) : base(menuElement, new Aplication.CPLite.Almacen.PT.Services.RemisionadoService()) {
            this.Load += RemisionPorClienteForm_Load;
        }

        private void RemisionPorClienteForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes: Remisiones por cliente";
            // permisos
            var d3 = ConfigService.GeMenuElement(this._ContextMenuReciboCobro.Name);
            this._ContextMenuReciboCobro.Enabled = ConfigService.HasPermission(d3);
            this.TRemision.menuContextual.Items.Add(_ContextMenuReciboCobro);
            this._ContextMenuReciboCobro.Click += ContextMenuReciboCobro_Click;
        }

        private void ContextMenuReciboCobro_Click(object sender, EventArgs e) {
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            var correcto = seleccion.Where(it => it.Status == Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Count();
            if (seleccion.Count != correcto) {
                RadMessageBox.Show(this, "No es posible crear el registro de cobranza. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.",
                    "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var espera = new Waiting1Form(this.CrearReciboCobro)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var remision = this.Tag as MovimientoBancarioDetailModel;
            if (remision != null) {
                var _nuevoMovimiento = new ReciboCobroForm(remision, this._BancoService);
                _nuevoMovimiento.ShowDialog(this);
            }
            this.Tag = null;
        }

        // creacion del recibo de cobro
        private void CrearReciboCobro() {
            if (this._BancoService == null)
                this._BancoService = new Aplication.CPLite.Banco.Services.BancoService();
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = Aplication.Banco.BancoService.Create().New().Status().IdConcepto(this._BancoService.Configuration.IdReciboCobro);
                    foreach (var seleccionado in seleccion) {
                        cfdiV33.Add(Builder.ConversionHelper.ConverTo(seleccionado));
                    }
                    this.Tag = cfdiV33.Build();
                }
            }
        }
    }
}
