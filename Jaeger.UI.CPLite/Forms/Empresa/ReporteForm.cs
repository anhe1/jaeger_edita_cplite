﻿using System;
using System.IO;
using Jaeger.Aplication.Base;
using Jaeger.Aplication.Base.Services;
using Jaeger.Util.Services;

namespace Jaeger.UI.CPLite.Forms.Empresa {
    public class ReporteForm : Common.Forms.ReporteComunForm {
        private EmbeddedResources horizon = new EmbeddedResources("Jaeger.Domain.CPLite");

        public ReporteForm(object sender) : base(ConfigService.Synapsis.Empresa.RFC, ConfigService.Synapsis.Empresa.RazonSocial) {
            this.CurrentObject = sender;
            this.PathLogo = Path.Combine(ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media), string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            this.Load += ReporteForm_Load;
        }

        private void ReporteForm_Load(object sender, EventArgs e) {
            if (this.CurrentObject.GetType() == typeof(string)) {
                this.CrearSolicitud();
            }
        }
        private void CrearSolicitud() {
            this.LoadDefinition = this.horizon.GetStream("Jaeger.Domain.CPLite.Reportes.ClienteSolicitudCreditoReporte.rdlc");
            this.ImagenQR = QRCodeExtension.GetQRBase64(new string[] { "" });
            this.Procesar();
            this.SetDisplayName("Solicitud de Crédito");
            this.Finalizar();
        }
    }
}
