﻿namespace Jaeger.UI.CPLite.Forms.Tesoreria {
    /// <summary>
    /// Bancos: conceptos de movimientos bancarios
    /// </summary>
    public class ConceptoCatalogoForm : UI.Banco.Forms.ConceptoCatalogoForm {
        public ConceptoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) { 
            this._Service = new Aplication.CPLite.Banco.Services.BancoConceptoService();
        }
    }
}
