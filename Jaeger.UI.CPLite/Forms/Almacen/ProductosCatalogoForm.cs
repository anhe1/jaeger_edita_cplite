﻿using System;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class ProductosCatalogoForm : UI.Tienda.Forms.ProductosCatalogoForm {
        protected internal UIAction _permisos;
        public ProductosCatalogoForm(UIMenuElement menuElement) : base() {
            this._permisos = new UIAction(menuElement.Permisos);
            this.Load += this.ProductosCatalogoForm_Load;
        }

        private void ProductosCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new ProductosCatalogoService(AlmacenEnum.PT);

            this.gProducto.AllowEditRow = this._permisos.Editar;
            this.gModelo.AllowEditRow = this._permisos.Editar;
            this.gVariante.AllowEditRow = this._permisos.Editar;
            this.TProductos.Nuevo.Enabled = this._permisos.Agregar;
            this.TProductos.Editar.Enabled = this._permisos.Editar;
            this.TProductos.Remover.Enabled = this._permisos.Remover;
            this.TModelos.Nuevo.Enabled = this._permisos.Agregar;
            this.TModelos.Editar.Enabled = this._permisos.Editar;
            this.TModelos.Remover.Enabled = this._permisos.Remover;
            this.TModelos.Autorizar.Enabled = this._permisos.Autorizar;
            this.TDisponible.Nuevo.Enabled = this._permisos.Agregar;
            this.TDisponible.Editar.Enabled = this._permisos.Editar;
            this.TDisponible.Remover.Enabled = this._permisos.Remover;
        }
    }
}
