SET TERM ^ ;
CREATE PROCEDURE CBRNZ_DESCUENT1 (
    IN_PROM DOM_MONEDA DEFAULT 0,
    IN_DIAS DOM_MONEDA DEFAULT 0,
    IN_IDDE DOM_ID DEFAULT 0 )
RETURNS (
    OUT_ID DOM_ID,
    OUT_CATALOGO DOM_STR50,
    OUT_DESCUENTO DOM_STR50,
    OUT_FACTOR DOM_MONEDA )
AS
BEGIN
/*
Elabora: Anhe.13092012
Proposito: devolver el factor alcanzado en las tablas de rangos del descuento teniendo en cuenta que ya sabemos el catalogo que debe ser aplicado para
el cliente en cuestion
Revisiones:

*/ 

    if ( :in_IDDe is null or :in_IDDe = 0 ) then in_IDDe = 1;
    if ( in_dias is null or in_dias = 0 ) then in_dias = 1;
    select first 1 dsc1.cmsdsc_nom,
           tbl2.tbrngs_nom,
           tbl2.tbrngs_factor
    from   tbrngs tbl1, tbrngs tbl2, cmsdsc dsc1
    where  tbl1.tbrngs_doc_id = 1040 and                    --< $ Rangos de Montos de compra
           tbl2.tbrngs_doc_id = 1041 and                    --< % Por d�as de cobranza descuento a aplicar
           tbl1.tbrngs_a = 1 and
           tbl2.tbrngs_a = 1 and 
           dsc1.cmsdsc_a = 1 and 
           tbl1.tbrngs_id = tbl2.tbrngs_tbrngs_id and       --< dependencias entre ambas tablas
           tbl1.tbrngs_cmsdsc_id = dsc1.cmsdsc_id and
           :in_prom between tbl1.tbrngs_rng1 and tbl1.tbrngs_rng2 and --< promedio a buscar
           :in_dias between tbl2.tbrngs_rng1 and tbl2.tbrngs_rng2 and --< d�as transcurridos para la cobranza
           cmsdsc_id = :in_IDDe                                       --< id del descuento a usar.
    order by 
           tbl1.tbrngs_id asc
    into
            :out_catalogo,
            :out_descuento,
            :out_factor;
            out_ID = in_IDDe;
    
    /* en caso de no encontrar un valor que se ajuste a las reglas devolvemos un mensaje de error y regresamos el factor como 1 */
    if ( out_factor is null or out_factor = 0 ) then
        begin
            out_catalogo = 'Sin Cat. Asignado';
            out_descuento = '----';
            out_factor = 1;
        end
        suspend;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_DESCUENT1 TO  SYSDBA;

