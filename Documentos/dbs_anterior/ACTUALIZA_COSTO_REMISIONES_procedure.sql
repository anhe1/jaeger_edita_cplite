SET TERM ^ ;
CREATE PROCEDURE ACTUALIZA_COSTO_REMISIONES
AS
declare variable rem_id dom_id;
declare variable costo  dom_moneda;
begin
/* proposito: actualizar los costos de las remisiones con los movimientos del almancen, 
previamente se debe desactivar los disparadores de ambas tablas para evitar errores */
    for
        select ctlrms.ctlrms_id
        from ctlrms
        where ctlrms.ctlrms_id>1310
        into :rem_id
    do
        begin
            select sum(movapt.movapt_prdctv$)
            from movapt
            where movapt.movapt_a=1 and movapt.movapt_doc_id=26 and movapt.movapt_ctlgrmsn_id = :rem_id
            into :costo;
            if (:costo is null ) then
                begin
                    update ctlrms set ctlrms.ctlrms_cstprd$ = 0 where ctlrms.ctlrms_id = :rem_id ;
                end
            else
                begin
                    update ctlrms set ctlrms.ctlrms_cstprd$ = :costo where ctlrms.ctlrms_id = :rem_id;
                end 
            costo=0;
        end
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE ACTUALIZA_COSTO_REMISIONES TO  SYSDBA;

