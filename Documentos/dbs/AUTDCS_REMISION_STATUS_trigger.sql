SET TERM ^ ;

CREATE TRIGGER AUTDCS_REMISION_STATUS FOR AUTDCS
ACTIVE AFTER INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* --------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 10122021 22:44
    Purpose: Autorizacion de status del catalogo de remisiones
-------------------------------------------------------------------------------------------------------------- */
	if ( new.autdcs_doc_id = 26 ) then
        begin
            if ( new.autdcs_ctlrms_id is not null ) then
                begin
                    if ( new.autdcs_sttsaut_id = 0 ) then
                        begin
                            update ctlrms set ctlrms_sttsdcs_id = new.autdcs_sttsaut_id, ctlrms_aut_id = 1, CTLRMS_FCCNCL = new.AUTDCS_FN, CTLRMS_USU_M = new.AUTDCS_USU_N, CTLRMS_USU_CNCL = new.AUTDCS_USU_N
                                where ctlrms_id = new.autdcs_ctlrms_id ;
                        end
                    else 
                        begin
                            update ctlrms set ctlrms_sttsdcs_id = new.autdcs_sttsaut_id , ctlrms_aut_id = 1
                                where ctlrms_id = new.autdcs_ctlrms_id ;
                        end
                end
        end
END^

SET TERM ; ^ 
