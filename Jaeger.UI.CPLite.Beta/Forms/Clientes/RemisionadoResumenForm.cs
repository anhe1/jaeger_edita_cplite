﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.Pivot.Core.Aggregates;
using Telerik.Pivot.Core;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class RemisionadoResumenForm : RadForm {
        protected internal IRemisionadoService _Service;
        protected internal BindingList<RemisionSingleModel> _DataSource;
        private LocalDataSourceProvider dataProvider= new LocalDataSourceProvider {
            Culture = new System.Globalization.CultureInfo("es-MX")
        };

        public RemisionadoResumenForm(UIMenuElement menuElement) {
            PivotGridLocalizationProvider.CurrentProvider = new LocalizationProviderPivotGrid();
            InitializeComponent();
        }

        private void RemisionadoResumenForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes/Remisionado: Resumen";
            this._Service = new Aplication.CPLite.Almacen.PT.Services.RemisionadoService();
            var receptor = new PropertyGroupDescription() { PropertyName = "ReceptorNombre", CustomName = "Cliente", GroupComparer = new GroupNameComparer() };
            var acumulado = new PropertyAggregateDescription() { PropertyName = "Acumulado", AggregateFunction = AggregateFunctions.Sum, CustomName = "Cobrado", StringFormat = "N2" };
            var porCobrar = new PropertyAggregateDescription() { PropertyName = "PorCobrar", AggregateFunction = AggregateFunctions.Sum, CustomName = "Por Cobrar", StringFormat = "N2" };
            var total = new PropertyAggregateDescription() { PropertyName = "Total", AggregateFunction = AggregateFunctions.Sum, CustomName = "G. Total", StringFormat = "N2" };
            var saldo = new PropertyAggregateDescription() { PropertyName = "Saldo", AggregateFunction = AggregateFunctions.Sum, CustomName = "Saldo", StringFormat = "N2" };

            this.dataProvider.AggregateDescriptions.Add(porCobrar);
            this.dataProvider.AggregateDescriptions.Add(acumulado);
            this.dataProvider.AggregateDescriptions.Add(saldo);
            this.dataProvider.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() { PropertyName = "FechaEmision", CustomName = "Año", Step = DateTimeStep.Year, GroupComparer = new GroupNameComparer() });
            this.dataProvider.ColumnGroupDescriptions.Add(new DateTimeGroupDescription() { PropertyName = "FechaEmision", CustomName = "Mes", Step = DateTimeStep.Month, GroupComparer = new GroupNameComparer() });
            this.dataProvider.RowGroupDescriptions.Add(receptor);
            this.TResumen.PivotGrid.DataProvider = this.dataProvider;
            
            this.TResumen.TPivot.Actualizar.Click += TResumen_Actualizar_Click;
            this.TResumen.TPivot.Cerrar.Click += TResumen_Cerrar_Click;
        }

        #region barra de herramientas
        private void TResumen_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Actualizar)) {
                espera.Text = "Obteniendo infomración ...";
                espera.ShowDialog(this);
            }
            this.TResumen.PivotGrid.DataSource = this._DataSource;
            this.TResumen.PivotGrid.PivotGridElement.RowHeadersLayout = PivotLayout.Tabular;
        }

        private void TResumen_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        /// <summary>
        /// actualizar el listado
        /// </summary>
        public virtual void Actualizar() {
            this._DataSource = new BindingList<RemisionSingleModel>(
                this._Service.GetList<RemisionSingleModel>(
                    Aplication.Almacen.PT.Services.RemisionadoService.Query().WithYear(this.TResumen.TPivot.GetEjercicio()).WithIdStatus(new int[] { 1, 2, 3, 4, 5 }).Build()).ToList()
                );
        }
    }
}
