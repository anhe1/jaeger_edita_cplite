﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class TbReporteVentasControl {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Bar = new Telerik.WinControls.UI.RadCommandBar();
            this.Ejercicio = new Telerik.WinControls.UI.RadSpinEditor();
            this.cboVendedores = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.BarRowElement = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.lblDirectorio = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostItem1 = new Telerik.WinControls.UI.CommandBarHostItem();
            this.lblEjercicio = new Telerik.WinControls.UI.CommandBarLabel();
            this.HostEjercicio = new Telerik.WinControls.UI.CommandBarHostItem();
            this.Separator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.Actualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.Filtro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Autosuma = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Agrupar = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.Imprimir = new Telerik.WinControls.UI.CommandBarButton();
            this.Herramientas = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.Cerrar = new Telerik.WinControls.UI.CommandBarButton();
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).BeginInit();
            this.Bar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // Bar
            // 
            this.Bar.Controls.Add(this.Ejercicio);
            this.Bar.Controls.Add(this.cboVendedores);
            this.Bar.Dock = System.Windows.Forms.DockStyle.Top;
            this.Bar.Location = new System.Drawing.Point(0, 0);
            this.Bar.Name = "Bar";
            this.Bar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.BarRowElement});
            this.Bar.Size = new System.Drawing.Size(980, 55);
            this.Bar.TabIndex = 6;
            // 
            // Ejercicio
            // 
            this.Ejercicio.Location = new System.Drawing.Point(367, 3);
            this.Ejercicio.Name = "Ejercicio";
            this.Ejercicio.Size = new System.Drawing.Size(55, 20);
            this.Ejercicio.TabIndex = 4;
            this.Ejercicio.TabStop = false;
            this.Ejercicio.TextAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cboVendedores
            // 
            this.cboVendedores.AutoSizeDropDownToBestFit = true;
            this.cboVendedores.DisplayMember = "Nombre";
            // 
            // cboVendedores.NestedRadGridView
            // 
            this.cboVendedores.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.cboVendedores.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboVendedores.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cboVendedores.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.cboVendedores.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.cboVendedores.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdRlcdrc";
            gridViewTextBoxColumn1.HeaderText = "IdRlcdrc";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "IdRlcdrc";
            gridViewTextBoxColumn2.FieldName = "Clave";
            gridViewTextBoxColumn2.HeaderText = "Clave";
            gridViewTextBoxColumn2.Name = "Clave";
            gridViewTextBoxColumn3.FieldName = "Nombre";
            gridViewTextBoxColumn3.HeaderText = "Nombre";
            gridViewTextBoxColumn3.Name = "Nombre";
            this.cboVendedores.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.cboVendedores.EditorControl.MasterTemplate.EnableGrouping = false;
            this.cboVendedores.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.cboVendedores.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.cboVendedores.EditorControl.Name = "NestedRadGridView";
            this.cboVendedores.EditorControl.ReadOnly = true;
            this.cboVendedores.EditorControl.ShowGroupPanel = false;
            this.cboVendedores.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.cboVendedores.EditorControl.TabIndex = 0;
            this.cboVendedores.Location = new System.Drawing.Point(83, 3);
            this.cboVendedores.Name = "cboVendedores";
            this.cboVendedores.Size = new System.Drawing.Size(229, 20);
            this.cboVendedores.TabIndex = 2;
            this.cboVendedores.TabStop = false;
            this.cboVendedores.ValueMember = "IdRlcdrc";
            // 
            // BarRowElement
            // 
            this.BarRowElement.MinSize = new System.Drawing.Size(25, 25);
            this.BarRowElement.Name = "BarRowElement";
            this.BarRowElement.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            this.BarRowElement.Text = "";
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "commandBarStripElement1";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.lblDirectorio,
            this.HostItem1,
            this.lblEjercicio,
            this.HostEjercicio,
            this.Separator2,
            this.Actualizar,
            this.Filtro,
            this.Autosuma,
            this.Agrupar,
            this.Imprimir,
            this.Herramientas,
            this.Cerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // lblDirectorio
            // 
            this.lblDirectorio.DisplayName = "Etiqueta";
            this.lblDirectorio.Name = "lblDirectorio";
            this.lblDirectorio.Text = "Directorio:";
            // 
            // HostItem1
            // 
            this.HostItem1.DisplayName = "Comprobantes";
            this.HostItem1.MinSize = new System.Drawing.Size(250, 15);
            this.HostItem1.Name = "HostItem1";
            this.HostItem1.Text = "Comprobantes";
            // 
            // lblEjercicio
            // 
            this.lblEjercicio.DisplayName = "Etiqueta: Ejercicio";
            this.lblEjercicio.Name = "lblEjercicio";
            this.lblEjercicio.Text = "Ejercicio:";
            // 
            // HostEjercicio
            // 
            this.HostEjercicio.DisplayName = "Ejercicio";
            this.HostEjercicio.MaxSize = new System.Drawing.Size(52, 20);
            this.HostEjercicio.MinSize = new System.Drawing.Size(52, 20);
            this.HostEjercicio.Name = "HostEjercicio";
            this.HostEjercicio.Text = "";
            // 
            // Separator2
            // 
            this.Separator2.DisplayName = "commandBarSeparator2";
            this.Separator2.Name = "Separator2";
            this.Separator2.VisibleInOverflowMenu = false;
            // 
            // Actualizar
            // 
            this.Actualizar.DisplayName = "Actualizar";
            this.Actualizar.DrawText = true;
            this.Actualizar.Image = global::Jaeger.UI.CPLite.Properties.Resources.refresh_16;
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Text = "Actualizar";
            this.Actualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Filtro
            // 
            this.Filtro.DisplayName = "Filtro";
            this.Filtro.DrawText = true;
            this.Filtro.Image = global::Jaeger.UI.CPLite.Properties.Resources.filter_16px;
            this.Filtro.Name = "Filtro";
            this.Filtro.Text = "Filtro";
            this.Filtro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Autosuma
            // 
            this.Autosuma.DisplayName = "commandBarToggleButton1";
            this.Autosuma.DrawText = true;
            this.Autosuma.Name = "Autosuma";
            this.Autosuma.Text = "Autosuma";
            this.Autosuma.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Agrupar
            // 
            this.Agrupar.DisplayName = "commandBarToggleButton1";
            this.Agrupar.DrawText = true;
            this.Agrupar.FlipText = false;
            this.Agrupar.Name = "Agrupar";
            this.Agrupar.Text = "Agrupar";
            this.Agrupar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Imprimir
            // 
            this.Imprimir.DisplayName = "Imprimir";
            this.Imprimir.DrawText = true;
            this.Imprimir.Name = "Imprimir";
            this.Imprimir.Text = "Imprimir";
            this.Imprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Herramientas
            // 
            this.Herramientas.DisplayName = "commandBarDropDownButton1";
            this.Herramientas.DrawText = true;
            this.Herramientas.Name = "Herramientas";
            this.Herramientas.Text = "Herramientas";
            this.Herramientas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // Cerrar
            // 
            this.Cerrar.DisplayName = "Cerrar";
            this.Cerrar.DrawText = true;
            this.Cerrar.Image = global::Jaeger.UI.CPLite.Properties.Resources.close_window_16px;
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // TbReporteVentasControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Bar);
            this.MinimumSize = new System.Drawing.Size(0, 30);
            this.Name = "TbReporteVentasControl";
            this.Size = new System.Drawing.Size(980, 30);
            this.Load += new System.EventHandler(this.TbVendedorStandarControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Bar)).EndInit();
            this.Bar.ResumeLayout(false);
            this.Bar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Ejercicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboVendedores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar Bar;
        protected internal Telerik.WinControls.UI.RadSpinEditor Ejercicio;
        protected internal Telerik.WinControls.UI.RadMultiColumnComboBox cboVendedores;
        private Telerik.WinControls.UI.CommandBarRowElement BarRowElement;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        protected internal Telerik.WinControls.UI.CommandBarLabel lblDirectorio;
        private Telerik.WinControls.UI.CommandBarHostItem HostItem1;
        private Telerik.WinControls.UI.CommandBarLabel lblEjercicio;
        private Telerik.WinControls.UI.CommandBarHostItem HostEjercicio;
        private Telerik.WinControls.UI.CommandBarSeparator Separator2;
        protected internal Telerik.WinControls.UI.CommandBarButton Actualizar;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Filtro;
        protected internal Telerik.WinControls.UI.CommandBarButton Imprimir;
        protected internal Telerik.WinControls.UI.CommandBarButton Cerrar;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Autosuma;
        protected internal Telerik.WinControls.UI.CommandBarToggleButton Agrupar;
        protected internal Telerik.WinControls.UI.CommandBarDropDownButton Herramientas;
    }
}
