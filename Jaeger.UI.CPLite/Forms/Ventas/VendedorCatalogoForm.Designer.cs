﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class VendedorCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VendedorCatalogoForm));
            this.dataGridVendedores = new Telerik.WinControls.UI.RadGridView();
            this.gridClientes = new Telerik.WinControls.UI.GridViewTemplate();
            this.TVendedor = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVendedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVendedores.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridVendedores
            // 
            this.dataGridVendedores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridVendedores.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = False";
            expressionFormattingObject1.Name = "Activo";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Gray;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "A";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn1.Width = 75;
            gridViewTextBoxColumn2.FieldName = "Nombre";
            gridViewTextBoxColumn2.HeaderText = "Nombre";
            gridViewTextBoxColumn2.Name = "IdDrctr";
            gridViewTextBoxColumn2.Width = 250;
            gridViewTextBoxColumn3.FieldName = "Correo";
            gridViewTextBoxColumn3.HeaderText = "Correo";
            gridViewTextBoxColumn3.Name = "Correo";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 200;
            gridViewTextBoxColumn4.FieldName = "Telefono";
            gridViewTextBoxColumn4.HeaderText = "Teléfono";
            gridViewTextBoxColumn4.Name = "Telefono";
            gridViewTextBoxColumn4.Width = 120;
            gridViewComboBoxColumn1.DataType = typeof(int);
            gridViewComboBoxColumn1.FieldName = "IdComision";
            gridViewComboBoxColumn1.HeaderText = "Comisión";
            gridViewComboBoxColumn1.Name = "IdComision";
            gridViewComboBoxColumn1.ReadOnly = true;
            gridViewComboBoxColumn1.Width = 240;
            gridViewTextBoxColumn5.FieldName = "Nota1";
            gridViewTextBoxColumn5.HeaderText = "Nota";
            gridViewTextBoxColumn5.MaxLength = 50;
            gridViewTextBoxColumn5.Name = "Nota1";
            gridViewTextBoxColumn5.Width = 150;
            gridViewTextBoxColumn6.FieldName = "Modifica";
            gridViewTextBoxColumn6.HeaderText = "Modifica";
            gridViewTextBoxColumn6.Name = "Modifica";
            gridViewTextBoxColumn6.Width = 65;
            gridViewTextBoxColumn7.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn7.FieldName = "FechaModifica";
            gridViewTextBoxColumn7.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn7.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn7.Name = "FechaModifica";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 75;
            this.dataGridVendedores.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewComboBoxColumn1,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.dataGridVendedores.MasterTemplate.Templates.AddRange(new Telerik.WinControls.UI.GridViewTemplate[] {
            this.gridClientes});
            this.dataGridVendedores.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridVendedores.Name = "dataGridVendedores";
            this.dataGridVendedores.Size = new System.Drawing.Size(1259, 613);
            this.dataGridVendedores.TabIndex = 0;
            // 
            // gridClientes
            // 
            this.gridClientes.Caption = "Clientes";
            gridViewCheckBoxColumn2.FieldName = "Activo";
            gridViewCheckBoxColumn2.HeaderText = "A";
            gridViewCheckBoxColumn2.Name = "Activo";
            gridViewTextBoxColumn8.FieldName = "Clave";
            gridViewTextBoxColumn8.HeaderText = "Clave";
            gridViewTextBoxColumn8.Name = "Clave";
            gridViewTextBoxColumn8.Width = 75;
            gridViewTextBoxColumn9.FieldName = "Nombre";
            gridViewTextBoxColumn9.HeaderText = "Cliente";
            gridViewTextBoxColumn9.Name = "Nombre";
            gridViewTextBoxColumn9.Width = 250;
            gridViewTextBoxColumn10.FieldName = "Nota";
            gridViewTextBoxColumn10.HeaderText = "Nota";
            gridViewTextBoxColumn10.Name = "Nota";
            gridViewTextBoxColumn10.Width = 150;
            gridViewTextBoxColumn11.FieldName = "Creo";
            gridViewTextBoxColumn11.HeaderText = "Creó";
            gridViewTextBoxColumn11.Name = "Creo";
            gridViewTextBoxColumn11.Width = 65;
            gridViewTextBoxColumn12.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn12.FieldName = "FechaNuevo";
            gridViewTextBoxColumn12.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn12.HeaderText = "Fec. Sist.";
            gridViewTextBoxColumn12.Name = "FechaNuevo";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn12.Width = 75;
            this.gridClientes.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12});
            this.gridClientes.ViewDefinition = tableViewDefinition1;
            // 
            // TVendedor
            // 
            this.TVendedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.TVendedor.Etiqueta = "";
            this.TVendedor.Location = new System.Drawing.Point(0, 0);
            this.TVendedor.Name = "TVendedor";
            this.TVendedor.ShowActualizar = true;
            this.TVendedor.ShowAutorizar = false;
            this.TVendedor.ShowCerrar = true;
            this.TVendedor.ShowEditar = true;
            this.TVendedor.ShowExportarExcel = false;
            this.TVendedor.ShowFiltro = true;
            this.TVendedor.ShowGuardar = false;
            this.TVendedor.ShowHerramientas = true;
            this.TVendedor.ShowImagen = false;
            this.TVendedor.ShowImprimir = false;
            this.TVendedor.ShowNuevo = true;
            this.TVendedor.ShowRemover = true;
            this.TVendedor.Size = new System.Drawing.Size(1259, 30);
            this.TVendedor.TabIndex = 2;
            this.TVendedor.Nuevo.Click += this.TVendedor_Agregar_Click;
            this.TVendedor.Editar.Click += this.TVendedor_Editar_Click;
            this.TVendedor.Remover.Click += this.TVendedor_Remover_Click;
            this.TVendedor.Actualizar.Click += this.TVendedor_Actualizar_Click;
            this.TVendedor.Filtro.Click += this.TVendedor_Filtro_Click;
            this.TVendedor.Cerrar.Click += this.TVendedor_Cerrar_Click;
            // 
            // VendedorCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1259, 643);
            this.Controls.Add(this.dataGridVendedores);
            this.Controls.Add(this.TVendedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VendedorCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CP Lite: Vendedores";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.VendedorCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVendedores.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVendedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView dataGridVendedores;
        private Common.Forms.ToolBarStandarControl TVendedor;
        private Telerik.WinControls.UI.GridViewTemplate gridClientes;
    }
}