﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen {
    public class PedidoPorSutirForm : PedidoCatalogoForm {
        public PedidoPorSutirForm(UIMenuElement menuElement) : base(menuElement) {
            this.TPedido.IsCosto = false;
            this.Load += this.PedidoCatalogoForm_Load;
        }

        private void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén: Pedidos por surtir";
        }
    }
}
