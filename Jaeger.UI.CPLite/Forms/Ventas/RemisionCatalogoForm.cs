﻿using System;
using System.Collections.Generic;
using Jaeger.Domain.Almacen.PT.Entities;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public class RemisionCatalogoForm : Almacen.PT.RemisionadoForm {
        public RemisionCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Load += this.RemisionCatalogoForm_Load;
        }

        private void RemisionCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes: Remisionado";
        }

        public override void TImprimir_Listado_Click(object sender, EventArgs e) {
            var seleccion = this.TRemision.GetFiltro<RemisionSingleModel>();
            var printer = new List<RemisionDetailPrinter>();

            if (seleccion.Count > 0) {
                foreach (var item in seleccion) {
                    printer.Add(new RemisionDetailPrinter(item));
                }

                var _reporte = new Almacen.ReporteForm(printer);
                _reporte.Show();
            }
        }
    }
}
