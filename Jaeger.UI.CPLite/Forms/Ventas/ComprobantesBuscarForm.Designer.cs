﻿namespace Jaeger.UI.CPLite.Forms.Ventas {
    partial class ComprobantesBuscarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComprobantesBuscarForm));
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.ToolBar = new Telerik.WinControls.UI.CommandBarStripElement();
            this.ToolBarLabelBuscar = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.ToolBarButtonAgregar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonActualizar = new Telerik.WinControls.UI.CommandBarButton();
            this.ToolBarButtonFiltro = new Telerik.WinControls.UI.CommandBarToggleButton();
            this.ToolBarButtonCerrar = new Telerik.WinControls.UI.CommandBarButton();
            this.GridData = new Telerik.WinControls.UI.RadGridView();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsLineWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement();
            this.workerBuscar = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).BeginInit();
            this.GridData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(853, 30);
            this.radCommandBar1.TabIndex = 0;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.ToolBar});
            // 
            // ToolBar
            // 
            this.ToolBar.DisplayName = "Herramientas";
            this.ToolBar.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.ToolBarLabelBuscar,
            this.commandBarSeparator1,
            this.ToolBarButtonAgregar,
            this.ToolBarButtonActualizar,
            this.ToolBarButtonFiltro,
            this.ToolBarButtonCerrar});
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.StretchHorizontally = true;
            // 
            // ToolBarLabelBuscar
            // 
            this.ToolBarLabelBuscar.DisplayName = "Etiqueta: Buscar";
            this.ToolBarLabelBuscar.Name = "ToolBarLabelBuscar";
            this.ToolBarLabelBuscar.Text = "Comprobantes";
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // ToolBarButtonAgregar
            // 
            this.ToolBarButtonAgregar.DisplayName = "Agregar";
            this.ToolBarButtonAgregar.DrawText = true;
            this.ToolBarButtonAgregar.Image = global::Jaeger.UI.CPLite.Properties.Resources.add_16px;
            this.ToolBarButtonAgregar.Name = "ToolBarButtonAgregar";
            this.ToolBarButtonAgregar.Text = "Agregar";
            this.ToolBarButtonAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonAgregar.Click += new System.EventHandler(this.TComprobante_Agregar_click);
            // 
            // ToolBarButtonActualizar
            // 
            this.ToolBarButtonActualizar.DisplayName = "Actualizar";
            this.ToolBarButtonActualizar.DrawText = true;
            this.ToolBarButtonActualizar.Image = global::Jaeger.UI.CPLite.Properties.Resources.refresh_16;
            this.ToolBarButtonActualizar.Name = "ToolBarButtonActualizar";
            this.ToolBarButtonActualizar.Text = "Actualizar";
            this.ToolBarButtonActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonActualizar.Click += new System.EventHandler(this.TComprobante_Actualizar_Click);
            // 
            // ToolBarButtonFiltro
            // 
            this.ToolBarButtonFiltro.DisplayName = "Filtro";
            this.ToolBarButtonFiltro.DrawText = true;
            this.ToolBarButtonFiltro.Image = global::Jaeger.UI.CPLite.Properties.Resources.filter_16px;
            this.ToolBarButtonFiltro.Name = "ToolBarButtonFiltro";
            this.ToolBarButtonFiltro.Text = "Filtro";
            this.ToolBarButtonFiltro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // ToolBarButtonCerrar
            // 
            this.ToolBarButtonCerrar.DisplayName = "Cerrar";
            this.ToolBarButtonCerrar.DrawText = true;
            this.ToolBarButtonCerrar.Image = global::Jaeger.UI.CPLite.Properties.Resources.close_window_16px;
            this.ToolBarButtonCerrar.Name = "ToolBarButtonCerrar";
            this.ToolBarButtonCerrar.Text = "Cerrar";
            this.ToolBarButtonCerrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ToolBarButtonCerrar.Click += new System.EventHandler(this.TComprobante_Cerrar_Click);
            // 
            // GridData
            // 
            this.GridData.Controls.Add(this.radWaitingBar1);
            this.GridData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridData.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.GridData.MasterTemplate.AllowAddNewRow = false;
            this.GridData.MasterTemplate.AllowDeleteRow = false;
            this.GridData.MasterTemplate.AllowDragToGroup = false;
            this.GridData.MasterTemplate.AllowEditRow = false;
            this.GridData.MasterTemplate.AllowRowResize = false;
            this.GridData.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.FieldName = "Id";
            gridViewTextBoxColumn1.HeaderText = "Id";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "Id";
            gridViewTextBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "TipoComprobante";
            gridViewTextBoxColumn2.HeaderText = "Tipo";
            gridViewTextBoxColumn2.Name = "TipoComprobante";
            gridViewTextBoxColumn3.DataType = typeof(int);
            gridViewTextBoxColumn3.FieldName = "Folio";
            gridViewTextBoxColumn3.HeaderText = "Folio";
            gridViewTextBoxColumn3.Name = "Folio";
            gridViewTextBoxColumn3.Width = 75;
            gridViewTextBoxColumn4.FieldName = "Serie";
            gridViewTextBoxColumn4.HeaderText = "Serie";
            gridViewTextBoxColumn4.Name = "Serie";
            gridViewTextBoxColumn4.Width = 65;
            gridViewTextBoxColumn5.FieldName = "Status";
            gridViewTextBoxColumn5.HeaderText = "Status";
            gridViewTextBoxColumn5.IsVisible = false;
            gridViewTextBoxColumn5.Name = "Status";
            gridViewTextBoxColumn5.Width = 85;
            gridViewTextBoxColumn6.FieldName = "EmisorRFC";
            gridViewTextBoxColumn6.HeaderText = "Emisor RFC";
            gridViewTextBoxColumn6.IsVisible = false;
            gridViewTextBoxColumn6.Name = "EmisorRFC";
            gridViewTextBoxColumn6.Width = 90;
            gridViewTextBoxColumn7.FieldName = "EmisorNombre";
            gridViewTextBoxColumn7.HeaderText = "Emisor";
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "EmisorNombre";
            gridViewTextBoxColumn7.Width = 200;
            gridViewTextBoxColumn8.FieldName = "ReceptorRFC";
            gridViewTextBoxColumn8.HeaderText = "Receptor RFC";
            gridViewTextBoxColumn8.IsVisible = false;
            gridViewTextBoxColumn8.Name = "ReceptorRFC";
            gridViewTextBoxColumn8.Width = 90;
            gridViewTextBoxColumn9.FieldName = "ReceptorNombre";
            gridViewTextBoxColumn9.HeaderText = "Receptor";
            gridViewTextBoxColumn9.Name = "Receptor";
            gridViewTextBoxColumn9.Width = 200;
            gridViewTextBoxColumn10.FieldName = "IdDocumento";
            gridViewTextBoxColumn10.HeaderText = "IdDocumento";
            gridViewTextBoxColumn10.Name = "IdDocumento";
            gridViewTextBoxColumn10.Width = 190;
            gridViewTextBoxColumn11.FieldName = "FechaEmision";
            gridViewTextBoxColumn11.FormatString = "{0:d}";
            gridViewTextBoxColumn11.HeaderText = "Fecha Emision";
            gridViewTextBoxColumn11.Name = "FechaEmision";
            gridViewTextBoxColumn11.Width = 85;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "Total";
            gridViewTextBoxColumn12.FormatString = "{0:n}";
            gridViewTextBoxColumn12.HeaderText = "Comisión";
            gridViewTextBoxColumn12.Name = "Total";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.DataType = typeof(decimal);
            gridViewTextBoxColumn13.FieldName = "PorCobrar";
            gridViewTextBoxColumn13.FormatString = "{0:n}";
            gridViewTextBoxColumn13.HeaderText = "Pagar";
            gridViewTextBoxColumn13.Name = "PorCobrar";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn13.Width = 75;
            this.GridData.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.GridData.MasterTemplate.EnableFiltering = true;
            this.GridData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.GridData.Name = "GridData";
            this.GridData.ShowGroupPanel = false;
            this.GridData.Size = new System.Drawing.Size(853, 226);
            this.GridData.TabIndex = 2;
            this.GridData.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.GridData_CellDoubleClick);
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.AssociatedControl = this.GridData;
            this.radWaitingBar1.Location = new System.Drawing.Point(379, 115);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(130, 24);
            this.radWaitingBar1.TabIndex = 1;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsLineWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingSpeed = 80;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsLine;
            // 
            // dotsLineWaitingBarIndicatorElement1
            // 
            this.dotsLineWaitingBarIndicatorElement1.Name = "dotsLineWaitingBarIndicatorElement1";
            // 
            // workerBuscar
            // 
            this.workerBuscar.WorkerReportsProgress = true;
            this.workerBuscar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerSearch_DoWork);
            this.workerBuscar.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.WorkerSearch_ProgressChanged);
            this.workerBuscar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerSearch_RunWorkerCompleted);
            // 
            // ComprobantesBuscarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 256);
            this.Controls.Add(this.GridData);
            this.Controls.Add(this.radCommandBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ComprobantesBuscarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Buscar Remisión";
            this.Load += new System.EventHandler(this.ComprobantesBuscarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridData)).EndInit();
            this.GridData.ResumeLayout(false);
            this.GridData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement ToolBar;
        private Telerik.WinControls.UI.RadGridView GridData;
        private Telerik.WinControls.UI.CommandBarToggleButton ToolBarButtonFiltro;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonCerrar;
        private Telerik.WinControls.UI.CommandBarLabel ToolBarLabelBuscar;
        private System.ComponentModel.BackgroundWorker workerBuscar;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsLineWaitingBarIndicatorElement dotsLineWaitingBarIndicatorElement1;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonAgregar;
        private Telerik.WinControls.UI.CommandBarButton ToolBarButtonActualizar;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
    }
}
