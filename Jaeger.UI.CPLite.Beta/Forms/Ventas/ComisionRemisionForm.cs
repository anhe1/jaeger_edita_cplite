﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    internal class ComisionRemisionForm : UI.Ventas.Forms.ComisionRemisionForm {
        public ComisionRemisionForm(UIMenuElement menuElement) : base(menuElement) {
        }

        public override void ComisionRemisionForm_Load(object sender, EventArgs e) {
            this.Text = "Vendedores: Comisión por Remisión";
            this.service = new Aplication.CPLite.Ventas.Services.ComisionService();
            base.ComisionRemisionForm_Load(sender, e);
        }
    }
}
