﻿using System;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbPedidoClientePartidaRepository : Abstractions.RepositoryMaster<PedidoClientePartidaModel>, ISqlPedidoClientePartidaRepository {
        public SqlFbPedidoClientePartidaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM PDCLP WHERE ((PDCLNP_ID = @PDCLNP_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@PDCLNP_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PedidoClientePartidaModel GetById(int index) {
            throw new NotImplementedException();
        }

        public int Insert(PedidoClientePartidaModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"INSERT INTO PDCLP (PDCLP_ID, PDCLP_A, PDCLP_DOC_ID, PDCLP_PDCLN_ID, PDCLP_DRCTR_ID, PDCLP_CTDPT_ID, PDCLP_CTALM_ID, PDCLP_CTPRD_ID, PDCLP_CTMDL_ID, PDCLP_CTPRC_ID, PDCLP_CTESPC_ID, PDCLP_CTUND_ID, PDCLP_UNDF, PDCLP_UNDN, PDCLP_CNTD, PDCLP_CTCLS, PDCLP_PRDN, PDCLP_MDLN, PDCLP_MRC, PDCLP_ESPC, PDCLP_ESPN, PDCLP_UNTC, PDCLP_UNDC, PDCLP_UNTR, PDCLP_UNTR2, PDCLP_SBTTL, PDCLP_DESC, PDCLP_IMPRT, PDCLP_TSIVA, PDCLP_TRIVA, PDCLP_TOTAL, PDCLP_SKU, PDCLP_USR_N, PDCLP_NOTA, PDCLP_FN, PDCLP_USR_M, PDCLP_FM, PDCLP_CTPRCP_ID) VALUES (@PDCLP_ID, @PDCLP_A, @PDCLP_DOC_ID, @PDCLP_PDCLN_ID, @PDCLP_DRCTR_ID, @PDCLP_CTDPT_ID, @PDCLP_CTALM_ID, @PDCLP_CTPRD_ID, @PDCLP_CTMDL_ID, @PDCLP_CTPRC_ID, @PDCLP_CTESPC_ID, @PDCLP_CTUND_ID, @PDCLP_UNDF, @PDCLP_UNDN, @PDCLP_CNTD, @PDCLP_CTCLS, @PDCLP_PRDN, @PDCLP_MDLN, @PDCLP_MRC, @PDCLP_ESPC, @PDCLP_ESPN, @PDCLP_UNTC, @PDCLP_UNDC, @PDCLP_UNTR, @PDCLP_UNTR2, @PDCLP_SBTTL, @PDCLP_DESC, @PDCLP_IMPRT, @PDCLP_TSIVA, @PDCLP_TRIVA, @PDCLP_TOTAL, @PDCLP_SKU, @PDCLP_USR_N, @PDCLP_NOTA, @PDCLP_FN, @PDCLP_USR_M, @PDCLP_FM, @PDCLP_CTPRCP_ID)" };

            sqlCommand.Parameters.AddWithValue("@PDCLP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@PDCLP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_DOC_ID", 0);
            sqlCommand.Parameters.AddWithValue("@PDCLP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTPRCP_ID", item.IdPrecioP);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNDF", item.FactorUnidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CNTD", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTCLS", item.Catalogo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_PRDN", item.Producto);
            sqlCommand.Parameters.AddWithValue("@PDCLP_MDLN", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@PDCLP_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@PDCLP_ESPN", item.Tamanio);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNTC", item.CostoUnitario);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNDC", item.CostoUnidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNTR", item.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNTR2", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@PDCLP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@PDCLP_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@PDCLP_IMPRT", item.Importe);
            sqlCommand.Parameters.AddWithValue("@PDCLP_TSIVA", item.TasaIVA);
            sqlCommand.Parameters.AddWithValue("@PDCLP_TRIVA", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@PDCLP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@PDCLP_SKU", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@PDCLP_NOTA", item.Observaciones);
            sqlCommand.Parameters.AddWithValue("@PDCLP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@PDCLP_FM", item.FechaModifica);

            item.IdPartida = this.ExecuteScalar(sqlCommand);
            if (item.IdPartida > 0)
                return item.IdPartida;
            return 0;
        }

        public int Update(PedidoClientePartidaModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"UPDATE PDCLP SET PDCLP_A = @PDCLP_A, PDCLP_DOC_ID = @PDCLP_DOC_ID, PDCLP_PDCLN_ID = @PDCLP_PDCLN_ID, PDCLP_DRCTR_ID = @PDCLP_DRCTR_ID, PDCLP_CTDPT_ID = @PDCLP_CTDPT_ID, PDCLP_CTALM_ID = @PDCLP_CTALM_ID, PDCLP_CTPRD_ID = @PDCLP_CTPRD_ID, PDCLP_CTMDL_ID = @PDCLP_CTMDL_ID, PDCLP_CTPRC_ID = @PDCLP_CTPRC_ID, PDCLP_CTESPC_ID = @PDCLP_CTESPC_ID, PDCLP_CTUND_ID = @PDCLP_CTUND_ID, PDCLP_UNDF = @PDCLP_UNDF, PDCLP_UNDN = @PDCLP_UNDN, PDCLP_CNTD = @PDCLP_CNTD, PDCLP_CTCLS = @PDCLP_CTCLS, PDCLP_PRDN = @PDCLP_PRDN, PDCLP_MDLN = @PDCLP_MDLN, PDCLP_MRC = @PDCLP_MRC, PDCLP_ESPC = @PDCLP_ESPC, PDCLP_ESPN = @PDCLP_ESPN, PDCLP_UNTC = @PDCLP_UNTC, PDCLP_UNDC = @PDCLP_UNDC, PDCLP_UNTR = @PDCLP_UNTR, PDCLP_UNTR2 = @PDCLP_UNTR2, PDCLP_SBTTL = @PDCLP_SBTTL, PDCLP_DESC = @PDCLP_DESC, PDCLP_IMPRT = @PDCLP_IMPRT, PDCLP_TSIVA = @PDCLP_TSIVA, PDCLP_TRIVA = @PDCLP_TRIVA, PDCLP_TOTAL = @PDCLP_TOTAL, PDCLP_SKU = @PDCLP_SKU, PDCLP_USR_N = @PDCLP_USR_N, PDCLP_NOTA = @PDCLP_NOTA, PDCLP_FN = @PDCLP_FN, PDCLP_USR_M = @PDCLP_USR_M, PDCLP_FM = @PDCLP_FM, PDCLP_CTPRCP_ID = @PDCLP_CTPRCP_ID WHERE ((PDCLP_ID = @PDCLP_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@PDCLNP_ID", item.IdPartida);
            sqlCommand.Parameters.AddWithValue("@PDCLP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_DOC_ID", 0);
            sqlCommand.Parameters.AddWithValue("@PDCLP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@PDCLP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTPRD_ID", item.IdProducto);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTMDL_ID", item.IdModelo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTPRC_ID", item.IdPrecio);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTPRCP_ID", item.IdPrecioP);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTESPC_ID", item.IdEspecificacion);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTUND_ID", item.IdUnidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNDF", item.FactorUnidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNDN", item.Unidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CNTD", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_CTCLS", item.Catalogo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_PRDN", item.Producto);
            sqlCommand.Parameters.AddWithValue("@PDCLP_MDLN", item.Modelo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_MRC", item.Marca);
            sqlCommand.Parameters.AddWithValue("@PDCLP_ESPC", item.Especificacion);
            sqlCommand.Parameters.AddWithValue("@PDCLP_ESPN", item.Tamanio);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNTC", item.CostoUnitario);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNDC", item.CostoUnidad);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNTR", item.ValorUnitario);
            sqlCommand.Parameters.AddWithValue("@PDCLP_UNTR2", item.Unitario);
            sqlCommand.Parameters.AddWithValue("@PDCLP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@PDCLP_DESC", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@PDCLP_IMPRT", item.Importe);
            sqlCommand.Parameters.AddWithValue("@PDCLP_TSIVA", item.TasaIVA);
            sqlCommand.Parameters.AddWithValue("@PDCLP_TRIVA", item.TrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@PDCLP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@PDCLP_SKU", item.Identificador);
            sqlCommand.Parameters.AddWithValue("@PDCLP_NOTA", item.Observaciones);
            sqlCommand.Parameters.AddWithValue("@PDCLP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@PDCLP_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@PDCLP_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<PedidoClientePartidaModel> GetList() {
            return this.GetList(new List<IConditional>());
        }

        public PedidoClientePartidaDetailModel Save(PedidoClientePartidaDetailModel model) {
            if (model.IdPartida == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdPartida = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }


        public IEnumerable<PedidoClientePartidaDetailModel> GetList(List<IConditional> condiciones) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT PDCLP.* FROM PDCLP @wcondiciones"
            };
            return this.GetMapper<PedidoClientePartidaDetailModel>(sqlCommand, condiciones);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM PDCLN LEFT JOIN PDCLP ON PDCLP.PDCLP_ID = PDCLN.PDCLN_ID @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }
    }
}
