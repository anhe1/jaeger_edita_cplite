﻿using Jaeger.UI.Common.Services;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Services {
    public static class GridRemisionService {
        public static GridViewDataColumn[] GetColumnasConceptos() {
            return new List<GridViewDataColumn>() { GridCommonService.ColCantidad, GridCommonService.ColIdPedido, ColCboUnidad, ColProducto, GridCommonService.ColIdentificador }.ToArray();
        }

        public static GridViewDataColumn[] GetColumnasConceptosImportes() {
            var unitario = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Unitario",
                FormatString = "{0:N2}",
                HeaderText = "P. Unitario",
                Name = "Unitario",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            };
            var subTotal = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SubTotal",
                FormatString = "{0:N2}",
                HeaderText = "Importe 1",
                Name = "SubTotal",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            };
            var descuento = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Descuento",
                FormatString = "{0:N2}",
                HeaderText = "Descuento",
                Name = "Descuento",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            };
            var importe = new GridViewTextBoxColumn() {
                DataType = typeof(decimal),
                FieldName = "Importe",
                FormatString = "{0:N2}",
                HeaderText = "Sub Total",
                Name = "Importe",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            };

            var tasaIVA = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TasaIVA",
                FormatString = "{0:P0}",
                HeaderText = "% IVA",
                Name = "TasaIVA",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                ReadOnly = true
            };

            var trasladoIVA = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "TrasladoIVA",
                FormatString = "{0:N2}",
                HeaderText = "T. IVA",
                Name = "TasladoIVA",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            };

            var total = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "Total",
                FormatString = "{0:N2}",
                HeaderText = "Sub Total",
                Name = "Total",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                ReadOnly = true
            };


            var _columnas = new List<GridViewDataColumn>();
            _columnas.AddRange(new List<GridViewDataColumn> {
            unitario,
            subTotal,
            descuento,
            importe,
            tasaIVA,
            trasladoIVA,
            total });

            return _columnas.ToArray();
        }

        public static GridViewDataColumn[] GetColumnasDomicilio() {
            var gridViewCommandColumn1 = new GridViewCommandColumn {
                FieldName = "AutorizadoText",
                HeaderText = "Autorizado",
                Name = "AutorizadoText",
                Width = 75
            };

            var conditionalFormattingObject1 = new ConditionalFormattingObject {
                ApplyToRow = true,
                CellBackColor = System.Drawing.Color.Empty,
                CellForeColor = System.Drawing.Color.Empty,
                Name = "Registro Activo",
                RowBackColor = System.Drawing.Color.Empty,
                RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = System.Drawing.Color.Gray,
                TValue1 = "0",
                TValue2 = "0"
            };

            var activo = new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo"
            };

            activo.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);

            var idTipoDomicilio = new GridViewComboBoxColumn {
                FieldName = "IdTipoDomicilio",
                HeaderText = "Tipo Domicilio",
                Name = "IdTipoDomicilio",
                Width = 100
            };

            var calle = new GridViewTextBoxColumn {
                FieldName = "Calle",
                HeaderText = "Calle",
                Name = "Calle",
                Width = 200
            };

            var noExterior = new GridViewTextBoxColumn {
                FieldName = "NoExterior",
                HeaderText = "Núm. Ext.",
                Name = "NoExterior"
            };

            var noInterior = new GridViewTextBoxColumn {
                FieldName = "NoInterior",
                HeaderText = "Núm. Int.",
                Name = "NoInterior"
            };

            var idTipoAsentamiento = new GridViewComboBoxColumn {
                FieldName = "IdTipoAsentamiento",
                HeaderText = "Asentamiento",
                Name = "IdTipoAsentamiento",
                Width = 150,
                IsVisible = false
            };

            var colonia = new GridViewTextBoxColumn {
                FieldName = "Colonia",
                HeaderText = "Colonia",
                Name = "Colonia",
                Width = 150
            };

            var delegacion = new GridViewTextBoxColumn {
                FieldName = "Delegacion",
                HeaderText = "Delegación",
                Name = "Delegacion",
                Width = 150
            };

            var codigoPostal = new GridViewTextBoxColumn {
                FieldName = "CodigoPostal",
                HeaderText = "C. P.",
                Name = "CodigoPostal"
            };

            var ciudad = new GridViewTextBoxColumn {
                FieldName = "Ciudad",
                HeaderText = "Ciudad",
                Name = "Ciudad",
                Width = 150
            };

            var estado = new GridViewTextBoxColumn {
                FieldName = "Estado",
                HeaderText = "Estado",
                Name = "Estado",
                Width = 150
            };

            var pais = new GridViewTextBoxColumn {
                FieldName = "Pais",
                HeaderText = "País",
                Name = "Pais"
            };

            var telefono = new GridViewTextBoxColumn {
                FieldName = "Telefono",
                HeaderText = "Teléfono",
                Name = "Telefono",
                Width = 100
            };

            var localidad = new GridViewTextBoxColumn {
                FieldName = "Localidad",
                HeaderText = "Localidad",
                Name = "Localidad",
                Width = 100
            };

            var referencia = new GridViewTextBoxColumn {
                FieldName = "Referencia",
                HeaderText = "Referencia",
                Name = "Referencia",
                Width = 100
            };

            var creo = new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                ReadOnly = true,
                Width = 75
            };

            var fechaNuevo = new GridViewTextBoxColumn {
                FieldName = "FechaNuevo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            };

            var completo = new GridViewTextBoxColumn {
                FieldName = "Completo",
                HeaderText = "Completo",
                Name = "Completo",
                ReadOnly = true,
                Width = 75,
                IsVisible = false
            };

            var Columns = new List<GridViewDataColumn>();
            Columns.AddRange(new GridViewDataColumn[] {
            gridViewCommandColumn1,
            activo,
            idTipoDomicilio,
            calle,
            noExterior,
            noInterior,
            idTipoAsentamiento,
            colonia,
            delegacion,
            codigoPostal,
            ciudad,
            estado,
            pais,
            telefono,
            localidad,
            referencia,
            creo,
            fechaNuevo,
                completo});
            return Columns.ToArray();
        }

        public static GridViewDataColumn[] GetColumnsStatus() {
            return new GridViewDataColumn[] { GridCommonService.ColCboIdStatus, ClaveDeMotivo, GridCommonService.ColNota, GridCommonService.ColFechaNuevo, GridCommonService.ColCreo };
        }

        public static GridViewDataColumn[] GetRemisionVista(bool valores = true) {
            if (valores) {
                return new GridViewDataColumn[] { GridCommonService.ColFolio, GridCommonService.ColIdPedido, GridCommonService.ColStatus, ColReceptor, ColContacto, GridCommonService.ColFechaEmision, ColFechaEntrega, ColFechaCobranza, ColFechaUltimoPago, GridCommonService.ColFechaCancela, ColSubTotal, ColTotalTrasladoIVA, ColTotal, ColPorCobrar, ColAcumulado, ColSaldo, GridCommonService.ColCreo};
            }
            return new GridViewDataColumn[] { GridCommonService.ColFolio, GridCommonService.ColIdPedido, GridCommonService.ColStatus, ColReceptor, ColContacto, GridCommonService.ColFechaEmision, ColFechaEntrega, ColFechaCobranza, ColFechaUltimoPago, GridCommonService.ColFechaCancela, GridCommonService.ColCreo };
        }

        public static GridViewDataColumn[] GetRemision() {
            var folio = GridCommonService.ColFolio;
            folio.IsPinned = true;
            var idpedido = GridCommonService.ColIdPedido;
            idpedido.IsPinned = true;
            var status = GridCommonService.ColStatus;
            status.IsPinned = true;
            status.ConditionalFormattingObjectList.Add(GridTelerikCommon.RegistroActivo("IdStatus = 0"));
            var receptor = GridRemisionService.ColReceptor;
            receptor.IsPinned = true;
            var claveCliente = GridContribuyenteService.ColClave;
            claveCliente.FieldName = "ReceptorClave";
            var saldo = ColSaldo;
            saldo.ConditionalFormattingObjectList.Add(GridTelerikCommon.SaldoNegativo("Saldo < 0"));
            var idDocumento = GridTelerikCommon.ColIdDocumento;
            idDocumento.IsVisible = false;

            return new GridViewDataColumn[] {
                folio,
                idpedido,
                status,
                receptor,
                claveCliente,
                ColContacto,
                GridCommonService.ColFechaEmision,
                ColFechaEntrega,
                ColFechaVencimiento,
                ColFechaCobranza,
                ColFechaUltimoPago,
                GridCommonService.ColCancela,
                GridCommonService.ColFechaCancela,
                ColSubTotal,
                ColTotalDescuento,
                ColImporte,
                ColTotalTrasladoIVA,
                ColTotal,
                ColTasaIVAPactado,
                ColTotalIVAPactado,
                ColGranTotal,
                ColFactorPactado,
                ColPorCobrarPactado,
                ColDescuentoTipo,
                ColDescuentoFactor,
                ColDescuentoTotal,
                ColPorCobrar,
                ColAcumulado,
                saldo,
                ColClaveMoneda,
                ColDiasTranscurridos,
                ColDiasUltCobro,
                ColDiasCobranza,
                ColVendedor,
                idDocumento,
                GridCommonService.ColNota,
                UrlFilePDF,
                GridCommonService.ColCreo,
                GridCommonService.ColFechaNuevo,
                GridCommonService.ColModifica,
                GridCommonService.ColFechaModifica
            };
        }

        /// <summary>
        /// obtener columnas de remisiones relacionadas
        /// </summary>
        public static GridViewDataColumn[] GetRelacion() {
            return new GridViewDataColumn[] {
                GridCommonService.ColFolio,
                GridCommonService.ColSerie,
                GridRemisionService.ColReceptor,
                GridCommonService.ColFechaEmision,
                GridCommonService.ColTotal,
                GridCommonService.ColIdDocumento,
                GridCommonService.ColCreo
            };
        }

        public static GridViewDataColumn[] GetReporteVentasCForm() {
            var folio = GridCommonService.ColFolio;
            folio.IsPinned = true;
            var idpedido = GridCommonService.ColIdPedido;
            idpedido.IsPinned = true;
            var status = GridCommonService.ColStatus;
            status.IsPinned = true;
            status.ConditionalFormattingObjectList.Add(GridTelerikCommon.RegistroActivo("IdStatus = 0"));
            var receptor = GridRemisionService.ColReceptor;
            receptor.IsPinned = true;
            var claveCliente = GridContribuyenteService.ColClave;
            claveCliente.FieldName = "ReceptorClave";
            var saldo = ColSaldo;
            saldo.ConditionalFormattingObjectList.Add(GridTelerikCommon.SaldoNegativo("Saldo < 0"));
            var idDocumento = GridTelerikCommon.ColIdDocumento;
            idDocumento.IsVisible = false;

            return new GridViewDataColumn[] {
                folio,
                idpedido,
                status,
                receptor,
                claveCliente,
                ColContacto,
                GridCommonService.ColFechaEmision,
                ColFechaEntrega,
                ColFechaCobranza,
                ColFechaUltimoPago,
                ColSubTotal,
                ColTotalDescuento,
                ColImporte,
                ColTotalTrasladoIVA,
                ColTotal,
                ColTasaIVAPactado,
                ColTotalIVAPactado,
                ColGranTotal,
                ColFactorPactado,
                ColPorCobrarPactado,
                ColDescuentoTipo,
                ColDescuentoFactor,
                ColDescuentoTotal,
                ColPorCobrar,
                ColAcumulado,
                saldo,
                ColClaveMoneda,
                ColDiasTranscurridos,
                ColDiasUltCobro,
                ColDiasCobranza,
                ColVendedor,
                idDocumento,
                GridCommonService.ColNota,
                UrlFilePDF,
                GridCommonService.ColCreo,
                GridCommonService.ColFechaNuevo,
                GridCommonService.ColModifica,
                GridCommonService.ColFechaModifica
            };
        }

        public static GridViewComboBoxColumn ColCboUnidad {
            get {
                return new GridViewComboBoxColumn {
                    FieldName = "Unidad",
                    HeaderText = "Unidad",
                    Name = "Unidad",
                    ReadOnly = false,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColProducto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Descripcion",
                    HeaderText = "Descripcion",
                    Name = "Descripcion",
                    Width = 510,
                    ReadOnly = true
                };
            }
        }

        public static GridViewTextBoxColumn ClaveDeMotivo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "CvMotivo",
                    HeaderText = "CvMotivo",
                    Name = "CvMotivo",
                    Width = 275
                };
            }
        }

        #region columnas para grid de remisiones
        /// <summary>
        /// indice de remision
        /// </summary>
        public static GridViewTextBoxColumn ColIdRemision {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdRemision",
                    HeaderText = "IdRemision",
                    Name = "IdRemision",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColReceptor {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ReceptorNombre",
                    HeaderText = "Cliente",
                    Name = "ReceptorNombre",
                    Width = 220
                };
            }
        }

        public static GridViewTextBoxColumn ColContacto {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Contacto",
                    HeaderText = "Recibe",
                    Name = "ClienteRecibe",
                    Width = 150,
                    ReadOnly = true,
                    IsVisible = false
                };
            }
        }

        /// <summary>
        /// columna de fecha de cobranza
        /// </summary>
        public static GridViewDateTimeColumn ColFechaCobranza {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaCobranza",
                    FormatString = GridCommonService.FormatStringDate,
                    HeaderText = "Fec. Cobranza",
                    Name = "FechaCobranza",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// columna fecha de entrega
        /// </summary>
        public static GridViewDateTimeColumn ColFechaEntrega {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaEntrega",
                    FormatString = GridCommonService.FormatStringDate,
                    HeaderText = "Fec. Entrega",
                    Name = "FechaEntrega",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// columa fehca de ultimo pago de la remision
        /// </summary>
        public static GridViewDateTimeColumn ColFechaUltimoPago {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaUltPago",
                    FormatString = GridCommonService.FormatStringDate,
                    HeaderText = "Últ. Cobro",
                    Name = "FechaUltPago",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// columna de fecha de vencimineto del pagare
        /// </summary>
        public static GridViewDateTimeColumn ColFechaVencimiento {
            get {
                return new GridViewDateTimeColumn {
                    DataType = typeof(DateTime),
                    FieldName = "FechaVence",
                    FormatString = GridCommonService.FormatStringDate,
                    HeaderText = "Fec. Vence",
                    Name = "FechaVence",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// columna del importe total del descuento 
        /// </summary>
        public static GridViewTextBoxColumn ColTotalDescuento {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalDescuento",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Descuento",
                    Name = "TotalDescuento",
                    ReadOnly = true,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        /// <summary>
        /// STotal - Desc.
        /// </summary>
        public static GridViewTextBoxColumn ColImporte {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Importe",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "SubTotal \r\n- Desc.",
                    Name = "Importe",
                    ReadOnly = true,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColSubTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "SubTotal",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "SubTotal",
                    Name = "SubTotal",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColTotalTrasladoIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalTrasladoIVA",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Traslado \r\n IVA",
                    Name = "TotalTrasladoIVA",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn TotalRetencionISR {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalRetencionISR",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Ret.\r\n ISR",
                    Name = "TotalRetencionISR",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn TotalRetencionIVA {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalRetencionIVA",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Ret.\r\n IVA",
                    Name = "TotalRetencionIVA",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn TotalRetencionIEPS {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalRetencionIEPS",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Ret.\r\n IEPS",
                    Name = "TotalRetencionIEPS",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn TotalTrasladoIEPS {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalTrasladoIEPS",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Traslado\r\n IEPS",
                    Name = "TotalTrasladoIEPS",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FormatString = GridCommonService.FormatStringMoney,
                    FieldName = "Total",
                    HeaderText = "Total",
                    Name = "Total",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColGranTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FormatString = GridCommonService.FormatStringMoney,
                    FieldName = "GTotal",
                    HeaderText = "GTotal",
                    Name = "GTotal",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColClaveMoneda {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "ClaveMoneda",
                    HeaderText = "Moneda",
                    Name = "ClaveMoneda",
                    IsVisible = false
                };
            }
        }

        public static GridViewTextBoxColumn ColDiasTranscurridos {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Standard,
                    FieldName = "DiasTranscurridos",
                    FormatString = "{0:N0}",
                    HeaderText = "Días Trans.",
                    Name = "DiasTranscurridos",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColDiasUltCobro {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Standard,
                    FieldName = "DiasUltCobro",
                    FormatString = "{0:N0}",
                    HeaderText = "Días Trans.",
                    Name = "DiasUltCobro",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColDiasCobranza {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "DiasCobranza",
                    FormatString = "{0:N0}",
                    HeaderText = "Días Cobranza",
                    Name = "DiasCobranza",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColVendedor {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "Vendedor",
                    HeaderText = "Vendedor",
                    Name = "Vendedor",
                    ReadOnly = true,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn UrlFilePDF {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "UrlFilePDF",
                    HeaderText = "PDF",
                    Name = "UrlFilePDF",
                    Width = 30,
                };
            }
        }

        /// <summary>
        /// obtener o establecer la dirección de entrega en modo texto (RMSN_DRCCN)
        /// </summary>
        public static GridViewTextBoxColumn DomicilioEntrega {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "DomicilioEntrega",
                    HeaderText = "Embarque",
                    Name = "DomicilioEntrega",
                    Width = 30,
                };
            }
        }
        #endregion

        #region cobranza
        public static GridViewTextBoxColumn ColFactorPactado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "FactorPactado",
                    FormatString = "{0:N4}",
                    HeaderText = "Fac. \r\nPactado",
                    Name = "FactorPactado",
                    ReadOnly = true,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                };
            }
        }

        public static GridViewTextBoxColumn ColTasaIVAPactado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TasaIVAPactado",
                    FormatString = GridCommonService.FormatStringP,
                    HeaderText = "% IVA \r\nPact.",
                    Name = "TasaIVAPactado",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight
                };
            }
        }

        public static GridViewTextBoxColumn ColTotalIVAPactado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "TotalIVAPactado",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "IVA Pact.",
                    Name = "TotalIVAPactado",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75
                };
            }
        }

        public static GridViewTextBoxColumn ColPorCobrarPactado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "PorCobrarPactado",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "X Cobrar Pactado",
                    Name = "PorCobrarPactadoD",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 75,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColDescuentoTipo {
            get {
                return new GridViewTextBoxColumn {
                    FieldName = "DescuentoTipo",
                    HeaderText = "Tipo Desc. Aplicado",
                    Name = "DescuentoTipo",
                    ReadOnly = true,
                    Width = 120,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColDescuentoFactor {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "DescuentoFactor",
                    FormatString = "{0:N4}",
                    HeaderText = "FAC Desc.",
                    Name = "DescuentoFactor",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight
                };
            }
        }

        public static GridViewTextBoxColumn ColDescuentoTotal {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "DescuentoTotal",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Desc. Actual",
                    Name = "DescuentoTotal",
                    ReadOnly = true,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    Width = 85,
                    WrapText = true
                };
            }
        }

        public static GridViewTextBoxColumn ColPorCobrar {
            get {
                return new GridViewTextBoxColumn {
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    DataType = typeof(decimal),
                    FieldName = "PorCobrar",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Por Cobrar",
                    Name = "PorCobrar",
                    Width = 80
                };
            }
        }

        public static GridViewTextBoxColumn ColAcumulado {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Acumulado",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Cobrado",
                    Width = 80,
                    Name = "Acumulado",
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight
                };
            }
        }

        public static GridViewTextBoxColumn ColSaldo {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(decimal),
                    FieldName = "Saldo",
                    FormatString = GridCommonService.FormatStringMoney,
                    HeaderText = "Saldo",
                    Name = "Saldo",
                    Width = 80,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight
                };
            }
        }

        public static GridViewTextBoxColumn ColCondicionPago {
            get {
                return new GridViewTextBoxColumn {
                    DataType = typeof(string),
                    FieldName = "CondicionPago",
                    HeaderText = "Condición de pago",
                    Name = "CondicionPago",
                    Width = 100,
                    TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                    IsVisible = false
                };
            }
        }
        #endregion
    }
}
