/*
*** INSTRUCCIONES PARA LA IMPORTACION DE LA BASE DE DATOS DE BOLSA DE REGALO 40 A LA VERSION NUEVA DE BASE DE DATOS

   PASO 1:
   crea un respaldo de la base de datos nBolsaRegalo40.fdb con la instrucción siguiente

    gbak -b -v ipo.mx/3050:/dbs/fb/nBolsaRegalo40.fdb c:\dbs\nbolsaregalo40_.fbk -user sysdba -pass masterkey

   PASO 2:
   crea una base de datos temporal para realizar la importación de los datos los comandos siguientes
   
    gbak -rep -v -user SYSDBA -password masterkey c:\dbs\nbolsaregalo40_.fbk localhost/3050:/dbs/nBolsaRegalo40old.fdb
    gfix -online localhost/3050:/dbs/jaeger_ipr981125pn9b.fdb -user sysdba -pass masterkey

   PASO 3:
   utiliza el archivo llamadado 01_update_dbase.sql para crear los esquemas temporales para preparar la base de datos origen 
   (en este caso la base de datos temporal); es posible que algunos esquemas ya existan en la base de datos como CTUND,
   CTESPC para este caso el script tiene comentado la creación de estas tablas.

   PASO 4: *este paso lo vamos a omitir
   Ejecuta el archivo 02_update_catalogos.sql para importar los catálogos anteriores, y sigue las instrucciones de este archivo.

   PASO 5:

   PASO 6:
   Ejectura archivo 5_update_relaciones_del_directorio.sql 

   PASO 7:
   Ejecuta archivo 6_insert_directorio_temporal.sql para los datos del directorio a la tabla temporal

   PASO 8:
   En el caso de bancos, en la tabla de conceptos (BNCCNP) el campo "BNCCNP_IDSTTS" debe cambiar a "BNCCNP_STTS_ID" que es el status
   del comprobante a consultar. Verifica o actualiza los valores de la columna para evitar errores

      ALTER TABLE BNCCNP ADD 
      BNCCNP_STTS_ID Integer NOT NULL;

      COMMIT;
      UPDATE BNCCNP 
      SET BNCCNP_STTS_ID = '0' 
      WHERE BNCCNP_STTS_ID IS NULL;

      UPDATE BNCCNP SET BNCCNP_STTS_ID = BNCCNP_IDSTTS;

   ** OJO **
   Revisa los disparadores del indice incremental, si es necesario actualiza el generador de cada tabla en la base de datos destino

   PASO 9:
   Agregar lista de perfiles y usuarios activos, utiliza el archivo 20_insert_perfiles
*/