﻿namespace Jaeger.UI.CPLite.Forms
{
    partial class MainRibbonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRibbonForm));
            this.BEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.RadDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.RadContainer = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.MenuRibbonBar = new Telerik.WinControls.UI.RadRibbonBar();
            this.TCPLite = new Telerik.WinControls.UI.RibbonTab();
            this.tcpl_grp_cliente = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_gcliente_expediente = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gcliente_subgrupo = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.tcpl_gcliente_pedido = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gcliente_remisionado = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gcliente_ndescuento = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gcliente_cobranza = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gcliente_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_gcliente_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_grp_banco = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_gbanco_ctabancaria = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gbanco_banco = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_gbanco_formapago = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gbanco_concepto = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gbanco_benefic = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gbanco_movimiento = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_grp_almacen = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_galmacen_producto = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_galmacen_categoria = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_pcatalogo = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_pmodelo = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_unidad = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_especif = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_pedido = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_galmacen_pedido1 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_pedido2 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_movimiento = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_galmacen_vales = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_devolucion = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_remisionado = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_existencia = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_galmacen_reporte = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_grp_tienda = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_gtienda_conf = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_gtienda_seccion = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gtienda_categoria = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gtienda_grupo1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.tcpl_gtienda_cupon = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gtienda_banner = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gtienda_faqs = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_grp_ventas = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_gventas_cprecio = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gventas_pedido = new Telerik.WinControls.UI.RadButtonElement();
            this.tcpl_gventas_vendedor = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_gventas_vendedores = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_ccomision = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_pcomision = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_crecibo = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_reportes = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_gventas_reporte1 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_reporte2 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_reporte3 = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gventas_reporte4 = new Telerik.WinControls.UI.RadMenuItem();
            this.TCPLiteTools = new Telerik.WinControls.UI.RibbonTab();
            this.tcpl_grp_configura = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_gconfigura_empresa = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.tcpl_gconfig_emisor = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gconfigura_avanzado = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gconfigura_usuarios = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gConfigura_menu = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_gConfigura_perfil = new Telerik.WinControls.UI.RadMenuItem();
            this.tcpl_grp_ayuda = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.tcpl_gayuda_acercade = new Telerik.WinControls.UI.RadButtonElement();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.tcpl_gcliente_reporte2 = new Telerik.WinControls.UI.RadMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).BeginInit();
            this.RadDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // BEstado
            // 
            this.BEstado.Location = new System.Drawing.Point(0, 603);
            this.BEstado.Name = "BEstado";
            this.BEstado.Size = new System.Drawing.Size(1253, 26);
            this.BEstado.SizingGrip = false;
            this.BEstado.TabIndex = 1;
            // 
            // RadDock
            // 
            this.RadDock.AutoDetectMdiChildren = true;
            this.RadDock.Controls.Add(this.RadContainer);
            this.RadDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadDock.IsCleanUpTarget = true;
            this.RadDock.Location = new System.Drawing.Point(0, 162);
            this.RadDock.MainDocumentContainer = this.RadContainer;
            this.RadDock.Name = "RadDock";
            // 
            // 
            // 
            this.RadDock.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadDock.Size = new System.Drawing.Size(1253, 441);
            this.RadDock.TabIndex = 6;
            this.RadDock.TabStop = false;
            // 
            // RadContainer
            // 
            this.RadContainer.Name = "RadContainer";
            // 
            // 
            // 
            this.RadContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadContainer.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // MenuRibbonBar
            // 
            this.MenuRibbonBar.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.TCPLite,
            this.TCPLiteTools});
            // 
            // 
            // 
            this.MenuRibbonBar.ExitButton.Text = "Exit";
            this.MenuRibbonBar.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.MenuRibbonBar.Location = new System.Drawing.Point(0, 0);
            this.MenuRibbonBar.Name = "MenuRibbonBar";
            // 
            // 
            // 
            this.MenuRibbonBar.OptionsButton.Text = "Options";
            this.MenuRibbonBar.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.MenuRibbonBar.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.MenuRibbonBar.Size = new System.Drawing.Size(1253, 162);
            this.MenuRibbonBar.StartButtonImage = global::Jaeger.UI.CPLite.Properties.Resources.shopping_bag_16px;
            this.MenuRibbonBar.TabIndex = 0;
            this.MenuRibbonBar.Text = "MainRibbonForm";
            this.MenuRibbonBar.Visible = false;
            // 
            // TCPLite
            // 
            this.TCPLite.IsSelected = true;
            this.TCPLite.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_grp_cliente,
            this.tcpl_grp_banco,
            this.tcpl_grp_almacen,
            this.tcpl_grp_tienda,
            this.tcpl_grp_ventas});
            this.TCPLite.Name = "TCPLite";
            this.TCPLite.Text = "TCPLite";
            this.TCPLite.UseMnemonic = false;
            // 
            // tcpl_grp_cliente
            // 
            this.tcpl_grp_cliente.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gcliente_expediente,
            this.tcpl_gcliente_subgrupo,
            this.tcpl_gcliente_cobranza,
            this.tcpl_gcliente_reportes});
            this.tcpl_grp_cliente.Name = "tcpl_grp_cliente";
            this.tcpl_grp_cliente.Text = "gClientes";
            // 
            // tcpl_gcliente_expediente
            // 
            this.tcpl_gcliente_expediente.Image = global::Jaeger.UI.CPLite.Properties.Resources.user_groups_30px;
            this.tcpl_gcliente_expediente.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gcliente_expediente.Name = "tcpl_gcliente_expediente";
            this.tcpl_gcliente_expediente.Text = "bExpediente";
            this.tcpl_gcliente_expediente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tcpl_gcliente_expediente.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gcliente_subgrupo
            // 
            this.tcpl_gcliente_subgrupo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gcliente_pedido,
            this.tcpl_gcliente_remisionado,
            this.tcpl_gcliente_ndescuento});
            this.tcpl_gcliente_subgrupo.Name = "tcpl_gcliente_subgrupo";
            this.tcpl_gcliente_subgrupo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tcpl_gcliente_subgrupo.Text = "g.Cliente";
            // 
            // tcpl_gcliente_pedido
            // 
            this.tcpl_gcliente_pedido.MinSize = new System.Drawing.Size(0, 20);
            this.tcpl_gcliente_pedido.Name = "tcpl_gcliente_pedido";
            this.tcpl_gcliente_pedido.ShowBorder = false;
            this.tcpl_gcliente_pedido.Text = "b.Pedido";
            this.tcpl_gcliente_pedido.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tcpl_gcliente_pedido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gcliente_remisionado
            // 
            this.tcpl_gcliente_remisionado.MinSize = new System.Drawing.Size(0, 20);
            this.tcpl_gcliente_remisionado.Name = "tcpl_gcliente_remisionado";
            this.tcpl_gcliente_remisionado.ShowBorder = false;
            this.tcpl_gcliente_remisionado.Text = "bRemisionado";
            this.tcpl_gcliente_remisionado.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tcpl_gcliente_remisionado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gcliente_ndescuento
            // 
            this.tcpl_gcliente_ndescuento.MinSize = new System.Drawing.Size(0, 20);
            this.tcpl_gcliente_ndescuento.Name = "tcpl_gcliente_ndescuento";
            this.tcpl_gcliente_ndescuento.ShowBorder = false;
            this.tcpl_gcliente_ndescuento.Text = "bNDescuento";
            this.tcpl_gcliente_ndescuento.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tcpl_gcliente_ndescuento.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gcliente_cobranza
            // 
            this.tcpl_gcliente_cobranza.Image = global::Jaeger.UI.CPLite.Properties.Resources.cash_register_30px;
            this.tcpl_gcliente_cobranza.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gcliente_cobranza.Name = "tcpl_gcliente_cobranza";
            this.tcpl_gcliente_cobranza.Text = "bCobranza";
            this.tcpl_gcliente_cobranza.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tcpl_gcliente_cobranza.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gcliente_reportes
            // 
            this.tcpl_gcliente_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_gcliente_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_gcliente_reportes.ExpandArrowButton = false;
            this.tcpl_gcliente_reportes.Image = global::Jaeger.UI.CPLite.Properties.Resources.ratings_30px;
            this.tcpl_gcliente_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gcliente_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gcliente_reporte1,
            this.tcpl_gcliente_reporte2});
            this.tcpl_gcliente_reportes.Name = "tcpl_gcliente_reportes";
            this.tcpl_gcliente_reportes.Text = "bReportes";
            this.tcpl_gcliente_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_gcliente_reporte1
            // 
            this.tcpl_gcliente_reporte1.Name = "tcpl_gcliente_reporte1";
            this.tcpl_gcliente_reporte1.Text = "bReporte1";
            this.tcpl_gcliente_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_grp_banco
            // 
            this.tcpl_grp_banco.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gbanco_ctabancaria,
            this.tcpl_gbanco_banco});
            this.tcpl_grp_banco.Name = "tcpl_grp_banco";
            this.tcpl_grp_banco.Text = "gTesoreria";
            // 
            // tcpl_gbanco_ctabancaria
            // 
            this.tcpl_gbanco_ctabancaria.Image = global::Jaeger.UI.CPLite.Properties.Resources.bank_cards_30px;
            this.tcpl_gbanco_ctabancaria.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gbanco_ctabancaria.Name = "tcpl_gbanco_ctabancaria";
            this.tcpl_gbanco_ctabancaria.Text = "bCtaBancaria";
            this.tcpl_gbanco_ctabancaria.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tcpl_gbanco_ctabancaria.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gbanco_banco
            // 
            this.tcpl_gbanco_banco.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_gbanco_banco.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_gbanco_banco.ExpandArrowButton = false;
            this.tcpl_gbanco_banco.Image = global::Jaeger.UI.CPLite.Properties.Resources.bank_30px;
            this.tcpl_gbanco_banco.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gbanco_banco.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gbanco_formapago,
            this.tcpl_gbanco_concepto,
            this.tcpl_gbanco_benefic,
            this.tcpl_gbanco_movimiento});
            this.tcpl_gbanco_banco.Name = "tcpl_gbanco_banco";
            this.tcpl_gbanco_banco.Text = "b.Banco";
            this.tcpl_gbanco_banco.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_gbanco_formapago
            // 
            this.tcpl_gbanco_formapago.Name = "tcpl_gbanco_formapago";
            this.tcpl_gbanco_formapago.Text = "bFormaPago";
            this.tcpl_gbanco_formapago.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gbanco_concepto
            // 
            this.tcpl_gbanco_concepto.Name = "tcpl_gbanco_concepto";
            this.tcpl_gbanco_concepto.Text = "TOperacion";
            this.tcpl_gbanco_concepto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gbanco_benefic
            // 
            this.tcpl_gbanco_benefic.Name = "tcpl_gbanco_benefic";
            this.tcpl_gbanco_benefic.Text = "bBeneficiarios";
            this.tcpl_gbanco_benefic.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gbanco_movimiento
            // 
            this.tcpl_gbanco_movimiento.Name = "tcpl_gbanco_movimiento";
            this.tcpl_gbanco_movimiento.Text = "bMovimientos";
            this.tcpl_gbanco_movimiento.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_grp_almacen
            // 
            this.tcpl_grp_almacen.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_galmacen_producto,
            this.tcpl_galmacen_pedido,
            this.tcpl_galmacen_movimiento,
            this.tcpl_galmacen_reporte});
            this.tcpl_grp_almacen.Name = "tcpl_grp_almacen";
            this.tcpl_grp_almacen.Text = "gAlmacen";
            // 
            // tcpl_galmacen_producto
            // 
            this.tcpl_galmacen_producto.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_galmacen_producto.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_galmacen_producto.ExpandArrowButton = false;
            this.tcpl_galmacen_producto.Image = global::Jaeger.UI.CPLite.Properties.Resources.new_product_30px;
            this.tcpl_galmacen_producto.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_galmacen_producto.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_galmacen_categoria,
            this.tcpl_galmacen_pcatalogo,
            this.tcpl_galmacen_pmodelo,
            this.tcpl_galmacen_unidad,
            this.tcpl_galmacen_especif});
            this.tcpl_galmacen_producto.Name = "tcpl_galmacen_producto";
            this.tcpl_galmacen_producto.Text = "bProducto";
            this.tcpl_galmacen_producto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_galmacen_categoria
            // 
            this.tcpl_galmacen_categoria.Name = "tcpl_galmacen_categoria";
            this.tcpl_galmacen_categoria.Text = "b.Categoria";
            this.tcpl_galmacen_categoria.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_pcatalogo
            // 
            this.tcpl_galmacen_pcatalogo.Name = "tcpl_galmacen_pcatalogo";
            this.tcpl_galmacen_pcatalogo.Text = "bCatalogo";
            this.tcpl_galmacen_pcatalogo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_pmodelo
            // 
            this.tcpl_galmacen_pmodelo.Name = "tcpl_galmacen_pmodelo";
            this.tcpl_galmacen_pmodelo.Text = "bModelo";
            this.tcpl_galmacen_pmodelo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_unidad
            // 
            this.tcpl_galmacen_unidad.Name = "tcpl_galmacen_unidad";
            this.tcpl_galmacen_unidad.Text = "bUnidad";
            this.tcpl_galmacen_unidad.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_especif
            // 
            this.tcpl_galmacen_especif.Name = "tcpl_galmacen_especif";
            this.tcpl_galmacen_especif.Text = "radMenuItem1";
            this.tcpl_galmacen_especif.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_pedido
            // 
            this.tcpl_galmacen_pedido.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_galmacen_pedido.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_galmacen_pedido.ExpandArrowButton = false;
            this.tcpl_galmacen_pedido.Image = global::Jaeger.UI.CPLite.Properties.Resources.order_history_30px;
            this.tcpl_galmacen_pedido.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_galmacen_pedido.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_galmacen_pedido1,
            this.tcpl_galmacen_pedido2});
            this.tcpl_galmacen_pedido.Name = "tcpl_galmacen_pedido";
            this.tcpl_galmacen_pedido.Text = "Pedidos";
            this.tcpl_galmacen_pedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_galmacen_pedido1
            // 
            this.tcpl_galmacen_pedido1.Name = "tcpl_galmacen_pedido1";
            this.tcpl_galmacen_pedido1.Text = "bHistorial";
            this.tcpl_galmacen_pedido1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_pedido2
            // 
            this.tcpl_galmacen_pedido2.Name = "tcpl_galmacen_pedido2";
            this.tcpl_galmacen_pedido2.Text = "bPorSurtir";
            this.tcpl_galmacen_pedido2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_movimiento
            // 
            this.tcpl_galmacen_movimiento.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_galmacen_movimiento.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_galmacen_movimiento.ExpandArrowButton = false;
            this.tcpl_galmacen_movimiento.Image = global::Jaeger.UI.CPLite.Properties.Resources.move_stock_30px;
            this.tcpl_galmacen_movimiento.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_galmacen_movimiento.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_galmacen_vales,
            this.tcpl_galmacen_devolucion,
            this.tcpl_galmacen_remisionado,
            this.tcpl_galmacen_existencia});
            this.tcpl_galmacen_movimiento.Name = "tcpl_galmacen_movimiento";
            this.tcpl_galmacen_movimiento.Text = "bMovimientos";
            this.tcpl_galmacen_movimiento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_galmacen_vales
            // 
            this.tcpl_galmacen_vales.Name = "tcpl_galmacen_vales";
            this.tcpl_galmacen_vales.Text = "bVales";
            this.tcpl_galmacen_vales.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_devolucion
            // 
            this.tcpl_galmacen_devolucion.Name = "tcpl_galmacen_devolucion";
            this.tcpl_galmacen_devolucion.Text = "bDevoluciones";
            this.tcpl_galmacen_devolucion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_remisionado
            // 
            this.tcpl_galmacen_remisionado.Name = "tcpl_galmacen_remisionado";
            this.tcpl_galmacen_remisionado.Text = "bRemisionado";
            this.tcpl_galmacen_remisionado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_galmacen_existencia
            // 
            this.tcpl_galmacen_existencia.Name = "tcpl_galmacen_existencia";
            this.tcpl_galmacen_existencia.Text = "bExistencia";
            // 
            // tcpl_galmacen_reporte
            // 
            this.tcpl_galmacen_reporte.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_galmacen_reporte.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_galmacen_reporte.ExpandArrowButton = false;
            this.tcpl_galmacen_reporte.Image = global::Jaeger.UI.CPLite.Properties.Resources.ratings_30px;
            this.tcpl_galmacen_reporte.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_galmacen_reporte.Name = "tcpl_galmacen_reporte";
            this.tcpl_galmacen_reporte.Text = "bReportes";
            this.tcpl_galmacen_reporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_grp_tienda
            // 
            this.tcpl_grp_tienda.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gtienda_conf,
            this.tcpl_gtienda_grupo1});
            this.tcpl_grp_tienda.Name = "tcpl_grp_tienda";
            this.tcpl_grp_tienda.Text = "g.Tienda";
            // 
            // tcpl_gtienda_conf
            // 
            this.tcpl_gtienda_conf.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_gtienda_conf.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_gtienda_conf.ExpandArrowButton = false;
            this.tcpl_gtienda_conf.Image = global::Jaeger.UI.CPLite.Properties.Resources.adjustment_30px;
            this.tcpl_gtienda_conf.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gtienda_conf.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gtienda_seccion,
            this.radMenuItem2,
            this.tcpl_gtienda_categoria});
            this.tcpl_gtienda_conf.Name = "tcpl_gtienda_conf";
            this.tcpl_gtienda_conf.Text = "b.Ajustes";
            this.tcpl_gtienda_conf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_gtienda_seccion
            // 
            this.tcpl_gtienda_seccion.Name = "tcpl_gtienda_seccion";
            this.tcpl_gtienda_seccion.Text = "b.Secciones";
            this.tcpl_gtienda_seccion.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "b.Banner";
            // 
            // tcpl_gtienda_categoria
            // 
            this.tcpl_gtienda_categoria.Name = "tcpl_gtienda_categoria";
            this.tcpl_gtienda_categoria.Text = "b.Categorias";
            this.tcpl_gtienda_categoria.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gtienda_grupo1
            // 
            this.tcpl_gtienda_grupo1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gtienda_cupon,
            this.tcpl_gtienda_banner,
            this.tcpl_gtienda_faqs});
            this.tcpl_gtienda_grupo1.Name = "tcpl_gtienda_grupo1";
            this.tcpl_gtienda_grupo1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tcpl_gtienda_grupo1.Text = "Sub grupo";
            // 
            // tcpl_gtienda_cupon
            // 
            this.tcpl_gtienda_cupon.Image = global::Jaeger.UI.CPLite.Properties.Resources.coupon_16px;
            this.tcpl_gtienda_cupon.MinSize = new System.Drawing.Size(0, 20);
            this.tcpl_gtienda_cupon.Name = "tcpl_gtienda_cupon";
            this.tcpl_gtienda_cupon.ShowBorder = false;
            this.tcpl_gtienda_cupon.Text = "b.Cupones";
            this.tcpl_gtienda_cupon.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tcpl_gtienda_cupon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.tcpl_gtienda_cupon.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gtienda_banner
            // 
            this.tcpl_gtienda_banner.Image = global::Jaeger.UI.CPLite.Properties.Resources.banner_16px;
            this.tcpl_gtienda_banner.MinSize = new System.Drawing.Size(0, 20);
            this.tcpl_gtienda_banner.Name = "tcpl_gtienda_banner";
            this.tcpl_gtienda_banner.ShowBorder = false;
            this.tcpl_gtienda_banner.Text = "b.Banner";
            this.tcpl_gtienda_banner.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tcpl_gtienda_banner.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.tcpl_gtienda_banner.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gtienda_faqs
            // 
            this.tcpl_gtienda_faqs.Image = global::Jaeger.UI.CPLite.Properties.Resources.faq_16px;
            this.tcpl_gtienda_faqs.MinSize = new System.Drawing.Size(0, 20);
            this.tcpl_gtienda_faqs.Name = "tcpl_gtienda_faqs";
            this.tcpl_gtienda_faqs.ShowBorder = false;
            this.tcpl_gtienda_faqs.Text = "b.FAQ";
            this.tcpl_gtienda_faqs.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tcpl_gtienda_faqs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.tcpl_gtienda_faqs.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_grp_ventas
            // 
            this.tcpl_grp_ventas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gventas_cprecio,
            this.tcpl_gventas_pedido,
            this.tcpl_gventas_vendedor,
            this.tcpl_gventas_reportes});
            this.tcpl_grp_ventas.Name = "tcpl_grp_ventas";
            this.tcpl_grp_ventas.Text = "gVentas";
            // 
            // tcpl_gventas_cprecio
            // 
            this.tcpl_gventas_cprecio.Image = global::Jaeger.UI.CPLite.Properties.Resources.price_tag_30px;
            this.tcpl_gventas_cprecio.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gventas_cprecio.Name = "tcpl_gventas_cprecio";
            this.tcpl_gventas_cprecio.Text = "bCPrecio";
            this.tcpl_gventas_cprecio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tcpl_gventas_cprecio.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_pedido
            // 
            this.tcpl_gventas_pedido.Image = global::Jaeger.UI.CPLite.Properties.Resources.purchase_order_30px;
            this.tcpl_gventas_pedido.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gventas_pedido.Name = "tcpl_gventas_pedido";
            this.tcpl_gventas_pedido.Text = "bPedido";
            this.tcpl_gventas_pedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tcpl_gventas_pedido.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_vendedor
            // 
            this.tcpl_gventas_vendedor.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_gventas_vendedor.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_gventas_vendedor.ExpandArrowButton = false;
            this.tcpl_gventas_vendedor.Image = global::Jaeger.UI.CPLite.Properties.Resources.businessman_30px;
            this.tcpl_gventas_vendedor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gventas_vendedor.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gventas_vendedores,
            this.tcpl_gventas_ccomision,
            this.tcpl_gventas_pcomision,
            this.tcpl_gventas_crecibo});
            this.tcpl_gventas_vendedor.Name = "tcpl_gventas_vendedor";
            this.tcpl_gventas_vendedor.Text = "bVendedores";
            this.tcpl_gventas_vendedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_gventas_vendedores
            // 
            this.tcpl_gventas_vendedores.Name = "tcpl_gventas_vendedores";
            this.tcpl_gventas_vendedores.Text = "bCatalogo";
            this.tcpl_gventas_vendedores.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_ccomision
            // 
            this.tcpl_gventas_ccomision.Name = "tcpl_gventas_ccomision";
            this.tcpl_gventas_ccomision.Text = "bCComision";
            this.tcpl_gventas_ccomision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_pcomision
            // 
            this.tcpl_gventas_pcomision.Name = "tcpl_gventas_pcomision";
            this.tcpl_gventas_pcomision.Text = "bPComision";
            this.tcpl_gventas_pcomision.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_crecibo
            // 
            this.tcpl_gventas_crecibo.Name = "tcpl_gventas_crecibo";
            this.tcpl_gventas_crecibo.Text = "bRecibo";
            this.tcpl_gventas_crecibo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_reportes
            // 
            this.tcpl_gventas_reportes.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_gventas_reportes.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_gventas_reportes.ExpandArrowButton = false;
            this.tcpl_gventas_reportes.Image = global::Jaeger.UI.CPLite.Properties.Resources.ratings_30px;
            this.tcpl_gventas_reportes.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gventas_reportes.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gventas_reporte1,
            this.tcpl_gventas_reporte2,
            this.tcpl_gventas_reporte3,
            this.tcpl_gventas_reporte4});
            this.tcpl_gventas_reportes.Name = "tcpl_gventas_reportes";
            this.tcpl_gventas_reportes.Text = "bReportes";
            this.tcpl_gventas_reportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_gventas_reporte1
            // 
            this.tcpl_gventas_reporte1.Name = "tcpl_gventas_reporte1";
            this.tcpl_gventas_reporte1.Text = "b.Reporte1";
            this.tcpl_gventas_reporte1.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_reporte2
            // 
            this.tcpl_gventas_reporte2.Name = "tcpl_gventas_reporte2";
            this.tcpl_gventas_reporte2.Text = "b.Reporte2";
            this.tcpl_gventas_reporte2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_reporte3
            // 
            this.tcpl_gventas_reporte3.Name = "tcpl_gventas_reporte3";
            this.tcpl_gventas_reporte3.Text = "b.Reporte3";
            this.tcpl_gventas_reporte3.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gventas_reporte4
            // 
            this.tcpl_gventas_reporte4.Name = "tcpl_gventas_reporte4";
            this.tcpl_gventas_reporte4.Text = "b.Reporte4";
            this.tcpl_gventas_reporte4.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TCPLiteTools
            // 
            this.TCPLiteTools.IsSelected = false;
            this.TCPLiteTools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_grp_configura,
            this.tcpl_grp_ayuda});
            this.TCPLiteTools.Name = "TCPLiteTools";
            this.TCPLiteTools.Text = "THerramientas";
            this.TCPLiteTools.UseMnemonic = false;
            // 
            // tcpl_grp_configura
            // 
            this.tcpl_grp_configura.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gconfigura_empresa});
            this.tcpl_grp_configura.Name = "tcpl_grp_configura";
            this.tcpl_grp_configura.Text = "bConfiguración";
            // 
            // tcpl_gconfigura_empresa
            // 
            this.tcpl_gconfigura_empresa.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.tcpl_gconfigura_empresa.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.tcpl_gconfigura_empresa.ExpandArrowButton = false;
            this.tcpl_gconfigura_empresa.Image = global::Jaeger.UI.CPLite.Properties.Resources.control_panel_30px;
            this.tcpl_gconfigura_empresa.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gconfigura_empresa.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gconfig_emisor,
            this.tcpl_gconfigura_avanzado,
            this.tcpl_gconfigura_usuarios,
            this.tcpl_gConfigura_menu,
            this.tcpl_gConfigura_perfil});
            this.tcpl_gconfigura_empresa.Name = "tcpl_gconfigura_empresa";
            this.tcpl_gconfigura_empresa.Text = "bEmpresa";
            this.tcpl_gconfigura_empresa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tcpl_gconfig_emisor
            // 
            this.tcpl_gconfig_emisor.Name = "tcpl_gconfig_emisor";
            this.tcpl_gconfig_emisor.Text = "b.Emisor";
            this.tcpl_gconfig_emisor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gconfigura_avanzado
            // 
            this.tcpl_gconfigura_avanzado.Name = "tcpl_gconfigura_avanzado";
            this.tcpl_gconfigura_avanzado.Text = "bAvanzado";
            this.tcpl_gconfigura_avanzado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gconfigura_usuarios
            // 
            this.tcpl_gconfigura_usuarios.Name = "tcpl_gconfigura_usuarios";
            this.tcpl_gconfigura_usuarios.Text = "bUsuarios";
            this.tcpl_gconfigura_usuarios.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gConfigura_menu
            // 
            this.tcpl_gConfigura_menu.Name = "tcpl_gConfigura_menu";
            this.tcpl_gConfigura_menu.Text = "bMenus";
            this.tcpl_gConfigura_menu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_gConfigura_perfil
            // 
            this.tcpl_gConfigura_perfil.Name = "tcpl_gConfigura_perfil";
            this.tcpl_gConfigura_perfil.Text = "bPerfil";
            this.tcpl_gConfigura_perfil.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // tcpl_grp_ayuda
            // 
            this.tcpl_grp_ayuda.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.tcpl_gayuda_acercade});
            this.tcpl_grp_ayuda.Name = "tcpl_grp_ayuda";
            this.tcpl_grp_ayuda.Text = "Ayuda";
            // 
            // tcpl_gayuda_acercade
            // 
            this.tcpl_gayuda_acercade.Image = global::Jaeger.UI.CPLite.Properties.Resources.about_30px;
            this.tcpl_gayuda_acercade.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tcpl_gayuda_acercade.Name = "tcpl_gayuda_acercade";
            this.tcpl_gayuda_acercade.Text = "bAcercaDe";
            this.tcpl_gayuda_acercade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tcpl_gayuda_acercade.Click += new System.EventHandler(this.AcercaDe_Click);
            // 
            // tcpl_gcliente_reporte2
            // 
            this.tcpl_gcliente_reporte2.Name = "tcpl_gcliente_reporte2";
            this.tcpl_gcliente_reporte2.Text = "bReporte1";
            this.tcpl_gcliente_reporte2.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // MainRibbonForm
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 629);
            this.Controls.Add(this.RadDock);
            this.Controls.Add(this.BEstado);
            this.Controls.Add(this.MenuRibbonBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = null;
            this.Name = "MainRibbonForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainRibbonForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainRibbonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).EndInit();
            this.RadDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar MenuRibbonBar;
        private Telerik.WinControls.UI.RadStatusStrip BEstado;
        private Telerik.WinControls.UI.RibbonTab TCPLite;
        private Telerik.WinControls.UI.RibbonTab TCPLiteTools;
        private Telerik.WinControls.UI.Docking.RadDock RadDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer RadContainer;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_configura;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_gconfigura_empresa;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gconfigura_avanzado;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gconfigura_usuarios;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gConfigura_menu;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gConfigura_perfil;
        private Telerik.WinControls.Themes.Office2010BlueTheme visualStudio2012LightTheme1;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_cliente;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_banco;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_almacen;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_ventas;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gcliente_expediente;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gcliente_cobranza;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_gcliente_reportes;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gcliente_reporte1;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_galmacen_pedido;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_galmacen_producto;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_pcatalogo;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_pmodelo;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_unidad;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_especif;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gventas_cprecio;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gventas_pedido;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_gventas_vendedor;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_vendedores;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_ccomision;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_pcomision;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_crecibo;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_gventas_reportes;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_reporte1;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_reporte2;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_reporte3;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gbanco_ctabancaria;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_gbanco_banco;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gbanco_formapago;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gbanco_concepto;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gbanco_benefic;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gbanco_movimiento;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup tcpl_gcliente_subgrupo;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gcliente_pedido;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gcliente_remisionado;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gcliente_ndescuento;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_ayuda;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gayuda_acercade;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_galmacen_movimiento;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_vales;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_devolucion;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_remisionado;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_existencia;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_galmacen_reporte;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_pedido1;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_pedido2;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gventas_reporte4;
        private Telerik.WinControls.UI.RadRibbonBarGroup tcpl_grp_tienda;
        private Telerik.WinControls.UI.RadDropDownButtonElement tcpl_gtienda_conf;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gtienda_seccion;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gtienda_categoria;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup tcpl_gtienda_grupo1;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gtienda_cupon;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gtienda_banner;
        private Telerik.WinControls.UI.RadButtonElement tcpl_gtienda_faqs;
        private Telerik.WinControls.UI.RadMenuItem tcpl_galmacen_categoria;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gconfig_emisor;
        private Telerik.WinControls.UI.RadMenuItem tcpl_gcliente_reporte2;
    }
}
