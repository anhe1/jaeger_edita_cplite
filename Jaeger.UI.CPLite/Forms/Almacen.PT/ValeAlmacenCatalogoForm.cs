﻿using System;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class ValeAlmacenCatalogoForm : UI.Almacen.PT.Forms.ValeAlmacenCatalogoForm {
        public ValeAlmacenCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ValeAlmacenCatalogoForm_Load;
        }

        private void ValeAlmacenCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new ValeAlmacenService();
            this.TVale.Nuevo.Enabled = true;
            this.TVale.Almacen.DataSource = this.Service.GetAlmacenes();
        }

        public override void TVale_Nuevo_Click(object sender, EventArgs e) {
            var nuevo = new ValeAlmacenForm(this.Service) { MdiParent = this.ParentForm };
            nuevo.Show();
        }
    }
}
