﻿using System.Collections.Generic;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.Aplication.CPLite.Contracts {
    public interface IComisionService : Aplication.Ventas.Contracts.IComisionService, IVendedoresService {
        /// <summary>
        /// listado de prioridades
        /// </summary>
        List<PrioridadModel> GetPrioridad();
    }
}