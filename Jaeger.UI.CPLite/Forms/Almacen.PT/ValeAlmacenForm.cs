﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class ValeAlmacenForm : UI.Almacen.PT.Forms.ValeAlmacenForm {
        public ValeAlmacenForm(IValeAlmacenService service) : base(service) {
            this.Unidad = new UnidadService();
            this.Load += ValeAlmacenForm_Load;
        }

        private void ValeAlmacenForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén PT: Vale";
            this.TVale.Receptor.Relacion = Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente;
            this.TVale.Receptor.Service = new DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
            this.Conceptos.Columns["Unidad"].IsVisible = false;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
        }

        public override void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TVale.Comprobante.IdDirectorio == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }

            using (var _catalogoProductos = new ProductoModeloBuscarForm(new ProductosCatalogoService(Domain.Base.ValueObjects.AlmacenEnum.PT))) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }
    }
}
