﻿using System;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.CPLite.Almacen.PT.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class NotaDescuentoCancelarForm : RadForm {
        protected internal NotaDescuentoDetailModel _Current;
        public event EventHandler<NotaDescuentoStatusModel> Selected;
        public void OnSelected(NotaDescuentoStatusModel e) {
            if (this.Selected != null)
                this.Selected(this, e);
        }

        public NotaDescuentoCancelarForm(NotaDescuentoDetailModel model) {
            InitializeComponent();
            this._Current = model;
        }

        private void PedidoCancelarForm_Load(object sender, EventArgs e) {
            this.FechaEntrega.Value = DateTime.Now;
            this.FechaEntrega.Enabled = false;
            this.IdDocumento.Text = this._Current.Folio.ToString();
            this.Cliente.Text = this._Current.Nombre;
            this.IdStatus.DataSource = NotaDescuentoService.GetStatus();
            this.MotivoCancelacion.DataSource = NotaDescuentoService.GetMotivosCancelacion();
        }

        private void Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Autorizar_Click(object sender, EventArgs e) {
            var motivoCancelacion = (this.MotivoCancelacion.SelectedItem as GridViewRowInfo).DataBoundItem as NotaDescuentoMotivoCancelacion;
            if (motivoCancelacion != null) {
                var response = new NotaDescuentoStatusModel {
                    IdCliente = this._Current.IdDirectorio,
                    Activo = true,
                    IdNota = this._Current.IdNota,
                    IdStatusA = this._Current.IdStatus,
                    IdStatusB = (int)this.IdStatus.SelectedValue,
                    CvMotivo = motivoCancelacion.Descriptor,
                    IdCVMotivo = motivoCancelacion.Id,
                    Nota = this.Nota.Text,
                    User = ConfigService.Piloto.Clave
                };
                this.OnSelected(response);
                this.Close();
            } else {
                RadMessageBox.Show(this, "Selecciona un motivo valido.", "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }
    }
}
