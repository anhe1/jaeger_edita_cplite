SET TERM ^ ;
CREATE PROCEDURE CBRNZ_CATDESC0 (
    IN_DRCTR_ID DOM_ID DEFAULT 0,
    IN_CATPRD DOM_ID DEFAULT 0,
    IN_FECEMSN DOM_FH )
RETURNS (
    OUT_CMSDSC_ID DOM_ID,
    OUT_CMSDSC_NOM DOM_STR50,
    OUT_OBSERV DOM_STR100 )
AS
begin
/*
Elabora: anhe 13102012
Proposito: calcular el catalogo de descuento correspondiente al cliente especifico a partir de los datos de la remisi�n.
Revisiones:
    15102012: se agrega la consulta para localizar el cat�logo predeterminado por el adminsitrador.
    18102012: se agrega parametro y a la consulta el id del catalogo de productos al que se puede aplicar
        el descuento asignado al cliente.
*/
    --select rlcdrc_id,rlcdrc_a,rlcdrc_drctr_id,rlcdrc_doc_id,rlcdrc_dscn_id,cmsdsc_id,cmsdsc_a,cmsdsc_doc_id,cmsdsc_opc_id0,cmsdsc_nom,cmsdsc_fchini,cmsdsc_fchvig

    select first 1 des.cmsdsc_id, des.cmsdsc_nom
    from 
        rlcdrc rel , cmsdsc des
    where 
            des.cmsdsc_id = rel.rlcdrc_dscn_id 
        and des.cmsdsc_doc_id = 1039                                    --< catalogo de descuentos
        and rel.rlcdrc_drctr_id = :in_drctr_id                          --< indice del directorio
        and des.cmsdsc_opc_id1 = :in_catPrd                             --< catalogo buscado
        and :in_fecemsn between des.cmsdsc_fchini and des.cmsdsc_fchvig --< dentro de la vigencia
        and rel.rlcdrc_a = 1
        and des.cmsdsc_a = 1
    into :out_cmsdsc_id, :out_cmsdsc_nom;
               
    /* en caso de no encontrar ninguno asiganamos el descuento que el administrador selecciono como predeterminado */
    if ( out_cmsdsc_id is null or out_cmsdsc_id = 0 ) then
        begin
            select first 1 com.cmsdsc_id
            from cmsdsc com
            where 
                    com.cmsdsc_doc_id = 1039
                and com.cmsdsc_a = 1 
                and com.cmsdsc_sec_id = 1
            into :out_cmsdsc_id;
            
            if ( out_cmsdsc_id is null or out_cmsdsc_id = 0 ) then out_cmsdsc_id = 1;
            out_observ = 'Si descuento asignado.';
        end 
    suspend;
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRNZ_CATDESC0 TO  SYSDBA;

