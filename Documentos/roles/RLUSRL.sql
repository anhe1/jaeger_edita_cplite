CREATE TABLE RLUSRL
(
  RLUSRL_ID Integer NOT NULL,
  RLUSRL_A Smallint NOT NULL,
  RLUSRL_USR_ID Integer,
  RLUSRL_RLSUSR_ID Integer,
  CONSTRAINT RLUSRL_ID_PK PRIMARY KEY (RLUSRL_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RLUSRL_ID' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RLUSRL_A' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del usuario'  where RDB$FIELD_NAME = 'RLUSRL_USR_ID' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del ROL'  where RDB$FIELD_NAME = 'RLUSRL_RLSUSR_ID' and RDB$RELATION_NAME = 'RLUSRL';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RLUSRL TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_RLUSRL_ID;

SET TERM !! ;
CREATE TRIGGER RLUSRL_BI FOR RLUSRL
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 260920221718
    Purpose: disparador para el auto numerico
------------------------------------------------------------------------------------------------------------- */  IF (NEW.RLUSRL_ID IS NULL) THEN
    NEW.RLUSRL_ID = GEN_ID(GEN_RLUSRL_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RLUSRL_ID, 0);
    if (tmp < new.RLUSRL_ID) then
      tmp = GEN_ID(GEN_RLUSRL_ID, new.RLUSRL_ID-tmp);
  END
END!!
SET TERM ; !!
