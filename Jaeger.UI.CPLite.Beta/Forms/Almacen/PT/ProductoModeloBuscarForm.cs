﻿using System;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class ProductoModeloBuscarForm : UI.Almacen.PT.Forms.ProductoModeloBuscarForm {
        public ProductoModeloBuscarForm(ICatalogoProductosService service) : base(service) {
        }

        public override void TModelo_Actualizar_Click(object sender, EventArgs e) {
            this.modelos = new BindingList<ProductoXModelo>(this.service.GetList<ProductoXModelo>(new System.Collections.Generic.List<Domain.Base.Builder.IConditional>() {
                new Conditional("@search", this.ToolBar.Descripcion.Text),
                new Conditional("ACTIVO", "1")
            }).ToList());
            this.dataGridResult.DataSource = this.modelos;
        }
    }
}
