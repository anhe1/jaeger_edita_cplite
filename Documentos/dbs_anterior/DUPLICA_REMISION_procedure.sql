SET TERM ^ ;
CREATE PROCEDURE DUPLICA_REMISION (
    INPUT_REMISION DOM_ID DEFAULT 0,
    INPUT_COPIA DOM_ID DEFAULT 0 )
AS
declare variable newID dom_id;
declare variable oldID dom_id;
BEGIN
/*
*/ 
    if (:input_remision is not null) then
        begin
            for
                select movapt_id --,movapt_a,movapt_doc_id,movapt_ctlgrmsn_id,movapt_cntdd_sld,movapt_undd_id,movapt_ctlgmdls_id,movapt_tam_id,movapt_untr$
                from movapt
                where movapt_doc_id = 26
                and movapt_ctlgrmsn_id=:input_remision
                into :oldID
            do
                begin
                    select max (movapt_id) + 1
                    from movapt 
                        into :newID;
                        insert into MOVAPT (movapt_id,movapt_a,movapt_doc_id,movapt_ctlgrmsn_id,movapt_cntdd_sld,movapt_undd_id,movapt_ctlgmdls_id,movapt_tam_id,movapt_untr$)
                        select :newID,movapt_a,movapt_doc_id,:input_copia,movapt_cntdd_sld,movapt_undd_id,movapt_ctlgmdls_id,movapt_tam_id,movapt_untr$
                        from movapt
                        where movapt_doc_id = 26
                        and movapt_id=:oldID;
                end
        end
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE DUPLICA_REMISION TO  SYSDBA;

