/*
-- TABLA DE AUTORIZACIONES
-- TIPOS DE DOCUMENTOS 

26 = Remisión de Cliente
28 = Recibo de Cobro
30 = Devolución de Producto
33 = Pedido
34 = Remisión interna
35 = Nota de descuento
36 = Recibo de comisión

*/
--- SCRIPT PARA LA CREACION DE LA TABLA
/*
CREATE TABLE PDCLS
(
  PDCLS_PDCLN_ID Integer DEFAULT 0 NOT NULL,
  PDCLS_STTS_ID Smallint DEFAULT 0 NOT NULL,
  PDCLS_CLMTV_ID Smallint DEFAULT 0 NOT NULL,
  PDCLS_CLMTV Varchar(80),
  PDCLS_OBSRV Varchar(255),
  PDCLS_TTL Numeric(18,4) DEFAULT 0,
  PDCLS_URL_PDF Varchar(1000),
  PDCLS_USU_M Varchar(10),
  PDCLS_FM Timestamp DEFAULT 'now',
  CONSTRAINT PK_PDCLS_ID PRIMARY KEY (PDCLS_PDCLN_ID,PDCLS_STTS_ID,PDCLS_CLMTV_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del peido asociado'  where RDB$FIELD_NAME = 'PDCLS_PDCLN_ID' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status'  where RDB$FIELD_NAME = 'PDCLS_STTS_ID' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la clave de motivo de cambio de status'  where RDB$FIELD_NAME = 'PDCLS_CLMTV_ID' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion de la clave de motivo de cancelacion'  where RDB$FIELD_NAME = 'PDCLS_CLMTV' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'PDCLS_OBSRV' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del pedido'  where RDB$FIELD_NAME = 'PDCLS_TTL' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'url del archivo relacionado al pedido'  where RDB$FIELD_NAME = 'PDCLS_URL_PDF' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que modifica el registro'  where RDB$FIELD_NAME = 'PDCLS_USU_M' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'PDCLS_FM' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'pedidos cliente, registro de cambios de status'
where RDB$RELATION_NAME = 'PDCLS';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON PDCLS TO  SYSDBA WITH GRANT OPTION;



-- IMPORTACIÓN DE AUTORIZACIONES DEL CAMBIO DE STATUS DE PEDIDOS
SELECT 
a.AUTDCS_STTSAUT_ID   AS PDCLS_STTS_ID,
a.AUTDCS_CTLALMPT0_ID AS PDCLS_PDCLN_ID,
a.AUTDCS_OBSRV        AS PDCLS_OBSRV,
a.AUTDCS_USU_N        AS PDCLS_USU_M,
a.AUTDCS_FN           AS PDCLS_FM,
PDDCLN.PDDCLN_TTL$    AS PDCLS_TTL ,
'00: No definido'     AS PDCLS_CLMTV
FROM AUTDCS a
LEFT JOIN PDDCLN ON a.AUTDCS_CTLALMPT0_ID = PDDCLN.PDDCLN_ID
WHERE AUTDCS_DOC_ID=33

/* LISTA DE CANCELACIONES DE PEDIDOS POR DIFERENTES MOTIVOS Y APARECEN DOS VECES
4866
4936
5218
5872
8710
11715
12141
19171
*/