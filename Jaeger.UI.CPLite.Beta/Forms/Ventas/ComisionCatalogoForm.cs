﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    internal class ComisionCatalogoForm : UI.Ventas.Forms.ComisionCatalogoForm {
        public ComisionCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += ComisionCatalogoForm_Load;
        }

        private void ComisionCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Vendedores: Catálogo de Comisiones";
            this.service = new Aplication.CPLite.Ventas.Services.ComisionService();
        }
    }
}
