﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;

namespace Jaeger.Aplication.CPLite.Almacen.PT.Services {
    public class DevolucionService : Aplication.Almacen.PT.Services.DevolucionService, Aplication.Almacen.PT.Contracts.IDevolucionService {

        public DevolucionService() {
            this.valeAlmacenRepository = new SqlFbValeAlmacenRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.movimientoAlmacenPT = new SqlFbMovimientoAlmacenRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
