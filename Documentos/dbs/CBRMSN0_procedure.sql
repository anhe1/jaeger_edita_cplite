SET TERM ^ ;
CREATE PROCEDURE CBRMSN0
RETURNS (
    CBRMSN0_ID DOM_ID,
    CBRMSN0_A DOM_ID,
    CBRMSN0_DRCTR_ID DOM_ID,
    CBRMSN0_CTLGVDDR_ID DOM_ID,
    CBRMSN0_CATPRD_ID DOM_ID,
    CBRMSN0_CMSDSC_ID DOM_ID,
    CBRMSN0_FOLIO DOM_ID,
    CBRMSN0_FCEMSN DOM_FH,
    CBRMSN0_FCENTR DOM_FH,
    CBRMSN0_FCHVNC DOM_FH,
    CBRMSN0_FCHCAL DOM_FH,
    CBRMSN0_FCULTP DOM_FH,
    CBRMSN0_DSEMSN DOM_ID,
    CBRMSN0_DSENTR DOM_ID,
    CBRMSN0_DSHVNC DOM_ID,
    CBRMSN0_DSULTP DOM_ID,
    CBRMSN0_DSCRDT DOM_ID,
    CBRMSN0_CTLPRD DOM_STR50,
    CBRMSN0_VDDR DOM_STR10,
    CBRMSN0_CLV DOM_STR10,
    CBRMSN0_NOM DOM_STR255,
    CBRMSN0_CTLDSC DOM_STR50,
    CBRMSN0_DDSCN DOM_STR50,
    CBRMSN0_FACTOR DOM_MONEDA,
    CBRMSN0_SUMSBTTL$ DOM_MONEDA,
    CBRMSN0_AVGSBTTL$ DOM_MONEDA,
    CBRMSN0_CREDITO$ DOM_MONEDA,
    CBRMSN0_SBTTL$ DOM_MONEDA,
    CBRMSN0_DSCNT DOM_MONEDA,
    CBRMSN0_IMPST DOM_MONEDA,
    CBRMSN0_IMPST$ DOM_MONEDA,
    CBRMSN0_TOTAL$ DOM_MONEDA,
    CBRMSN0_TOTAL2$ DOM_MONEDA,
    CBRMSN0_DSCNT$ DOM_MONEDA,
    CBRMSN0_CBRD$ DOM_MONEDA,
    CBRMSN0_SLDO$ DOM_MONEDA,
    CBRMSN0_DIV$ DOM_MONEDA,
    CBRMSN0_DIV2 DOM_MONEDA,
    CBRMSN0_TOTALIVA$ DOM_MONEDA,
    CBRMSN0_SUBTOTAL$ DOM_MONEDA,
    CBRMSN0_SUBTOTAL2$ DOM_MONEDA,
    CBRMSN0_MSG DOM_STR10,
    CBRMSN0_OBS DOM_STR50,
    CBRMSN0_MES DOM_ID,
    CBRMSN0_ANIO DOM_ID,
    CBRMSN0_TRANS DOM_ID )
AS
declare variable prom_dias dom_id;
declare variable forzar dom_id;
declare variable saveData dom_id;

begin 

/*
elabora: anhe 13102012
proposito: crear listado de remisiones con su respectivo descuento y almacenar el resultado en la remision con la actualizacion de la fecha
en que se calcula dicho descuento. evitando calcular dos veces la misma remision si fue calculada previamente para hacer mas rapido el proceso.
revisiones:
	27102012: se agrega columna de saldo de la remision
	10112012: se modifica completo el procedimiento esta es la version 2
	1112012: se aumenta linea para las remisiones que son inferiores al folio 2310 ya tienen incluido el iva lo que hacemos es omitir y corregir si
        estaba calculado. tambien se agrega el modo forzado para que se calcule el descuento. si los dias de entrega es nulo o no hay fecha de entrega
        utilizamos los dias transcurridos desde la fecha de emision
    14112012: se elimina la linea para separar los ivas debido a que se aplico el procedimiento CBRMSN00 que es el que separo el iva y lo guardo en
        cada remision con unas restricciones por los folios; debido a la historia que ya se tenia.
    11122012: corregida la subconsulta de montos de compra por cliente, no se estaba calculando adecuadamente pues suma las remisiones de otros dias
        cuando solo debe sumar las remisiones emitidas 30 dias antes y omitir la actual.
        Tambien se agrega CBRMSN0_TOTALIVA$ para que el usuario pueda saber cuanto es sin descuento y con iva
    06032013: el iva siempre se debe calcular antes del descuento (correccion), tambien se cambia la aplicacion del descuento al precio de catalogo
        poque esta mal calculado, tambien se quita la division porque ya se encuntra calculada en la tabla de remisiones solo tenemos que reflejar y ya.
    17092013: se modifica la regla para sumar la remision actual 
*/

    prom_dias = 30;
    forzar = 1;
    saveData = 1;
    for
        select
            rem.ctlrms_id,
            rem.ctlrms_a,
            rem.ctlrms_drctr_id,
            rem.ctlrms_ctlgvddr_id,
            rem.ctlrms_catprd_id,
            rem.ctlrms_folio,
            rem.ctlrms_fcemsn,
            rem.ctlrms_fcentrg,
            rem.ctlrms_fchvnc,
            rem.ctlrms_fccal,
            rem.ctlrms_fcultpg,
            ceil ( current_timestamp - rem.ctlrms_fcemsn ),
            ceil ( current_timestamp - rem.ctlrms_fcentrg ),
            ceil ( current_timestamp - rem.ctlrms_fchvnc ) ,
            ( case when ceil ( current_timestamp - rem.ctlrms_fcultpg ) > 0 then ceil ( current_timestamp - rem.ctlrms_fcultpg ) else 0 end ),
            ctl.ctlprdct_cls1,
            vdd.vwvnddr_clv,
            dir.vwalldrctr_clv,
            dir.vwalldrctr_nom,
            dir.vwallDrctr_lmtcrdt$,
            dir.vwalldrctr_factor,
            dir.vwAllDrctr_dscrdt,
            rem.ctlrms_ctldsc,
            rem.ctlrms_ddscn,
            ( 0),
            ( 0),
            rem.ctlrms_sbttl$,
            rem.ctlrms_dscnt,
            rem.ctlrms_impst,
            rem.ctlrms_impst$,
            rem.ctlrms_total$,
            rem.ctlrms_dscnt$,
            rem.ctlrms_cbrd$,
            rem.ctlrms_sld$ as saldo$,
            rem.ctlrms_div,
            extract(month from ctlrms_fcentrg) ,
            extract(year from ctlrms_fcentrg)  ,
            rem.CTLRMS_DSTRS
        from
            ctlrms rem,
            ctlprdct ctl,
            vwalldrctr dir,
            vwvnddr vdd
        where   rem.ctlrms_sttsdcs_id = 4 --:input_status
            and rem.ctlrms_drctr_id = dir.vwalldrctr_id
            and rem.ctlrms_ctlgvddr_id = vdd.vwvnddr_id
            and rem.ctlrms_catprd_id = ctl.ctlprdct_id
            --and rem.ctlrms_folio between :input_start and :input_end
        order by rem.ctlrms_id desc
        into
            :cbrmsn0_id ,
            :cbrmsn0_a ,
            :cbrmsn0_drctr_id,
            :cbrmsn0_ctlgvddr_id,
            :cbrmsn0_catprd_id,
            :cbrmsn0_folio ,
            :cbrmsn0_fcemsn ,
            :cbrmsn0_fcentr ,
            :cbrmsn0_fchvnc ,
            :cbrmsn0_fchcal ,
            :cbrmsn0_fcultp ,
            :cbrmsn0_dsemsn ,
            :cbrmsn0_dsentr ,
            :cbrmsn0_dshvnc ,
            :cbrmsn0_dsultp ,
            :cbrmsn0_ctlprd ,
            :cbrmsn0_vddr ,
            :cbrmsn0_clv ,
            :cbrmsn0_nom ,
            :cbrmsn0_credito$ ,
            :cbrmsn0_factor ,
            :cbrmsn0_dscrdt ,
            :cbrmsn0_ctldsc ,
            :cbrmsn0_ddscn ,
            :cbrmsn0_sumsbttl$ ,
            :cbrmsn0_avgsbttl$ ,
            :cbrmsn0_sbttl$ ,
            :cbrmsn0_dscnt ,
            :cbrmsn0_impst ,
            :cbrmsn0_impst$ ,
            :cbrmsn0_total$ ,
            :cbrmsn0_dscnt$ ,
            :cbrmsn0_cbrd$ ,
            :cbrmsn0_sldo$ ,
            :cbrmsn0_div$,
            :cbrmsn0_mes,
            :cbrmsn0_anio ,
            :cbrmsn0_trans
    do
        begin
            cbrmsn0_ctldsc='';
            if ( cbrmsn0_sumsbttl$ is null ) then cbrmsn0_sumsbttl$ = 0;
            if ( cbrmsn0_avgsbttl$ is null ) then cbrmsn0_avgsbttl$ = 0;
            if ( cbrmsn0_dsentr is null or cbrmsn0_dsentr <= 0 ) then cbrmsn0_dsentr = cbrmsn0_dsemsn;
            if ( cbrmsn0_cbrd$ is null ) then cbrmsn0_cbrd$ = 0;

            if ( cbrmsn0_fchcal <> current_date or forzar = 1  ) then
                begin   -- en el caso de que el cliente tenga un descuento autorizado 
                    if ( :cbrmsn0_dscrdt >= :cbrmsn0_dsentr and :cbrmsn0_factor > 0 and :cbrmsn0_fcemsn >= '05/01/2013') then
                        begin
                            cbrmsn0_ctldsc = 'Autorizado';
                            cbrmsn0_ddscn = 'Autorizado';
                            cbrmsn0_dscnt = cbrmsn0_factor;
                        end 
                    else    -- en el caso de que el cliente pierda o no tenga un descuento autorizado, calculamos descuentos
                        begin
                            /*cbrmsn0_msg = 's/c';
                            select p.output_cmsdsc_id, p.output_catalogo, p.output_descuent, p.output_factor
                            from cbrnz_descuent2(:cbrmsn0_drctr_id, :cbrmsn0_dsentr, :cbrmsn0_catprd_id, :cbrmsn0_fcemsn, :cbrmsn0_sumsbttl$) p
                            into :cbrmsn0_cmsdsc_id, :cbrmsn0_ctldsc, :cbrmsn0_ddscn, cbrmsn0_dscnt;*/
                            cbrmsn0_cmsdsc_id=-1;
                            cbrmsn0_ctldsc='Sin descuento';
                            cbrmsn0_ddscn='Sin descuento';
                            cbrmsn0_dscnt=1;
                        end
                        
                    cbrmsn0_totaliva$ = cbrmsn0_sbttl$ * 1.16 ;             -- iva al 16%
                    cbrmsn0_subtotal$ = ( cbrmsn0_sbttl$ );
                    cbrmsn0_impst$ = cbrmsn0_sbttl$ * cbrmsn0_impst;
                    cbrmsn0_total$ = (cbrmsn0_subtotal$ + cbrmsn0_impst$) * cbrmsn0_dscnt;
                    cbrmsn0_div2 = 0.00001;--cbrmsn0_cbrd$/cbrmsn0_total$;
                    cbrmsn0_subtotal2$ = cbrmsn0_sbttl$ + cbrmsn0_impst$;
                    if ( :cbrmsn0_factor is null ) then cbrmsn0_factor=0;
                    if (:cbrmsn0_factor > 0 ) then
                        begin
                            cbrmsn0_total2$ = ( cbrmsn0_subtotal2$ * cbrmsn0_factor ) ;
                        end 
                    else
                        begin 
                            cbrmsn0_total2$ = ( cbrmsn0_subtotal2$ * 1 ) ;
                        end
                    if ( saveData = 1 ) then
                                begin
                                    update ctlrms set
                                        ctlrms_catdsc_id = :cbrmsn0_cmsdsc_id,
                                        ctlrms_fccal     = current_date,
                                        ctlrms_ctldsc    = :cbrmsn0_ctldsc,
                                        ctlrms_ddscn     = :cbrmsn0_ddscn,
                                        ctlrms_dscnt     = :cbrmsn0_dscnt,
                                        ctlrms_impst     = :cbrmsn0_impst,
                                        ctlrms_impst$    = :cbrmsn0_impst$,
                                        ctlrms_total$    = :cbrmsn0_total$,
                                        --ctlrms_dscnt$    = :cbrmsn0_dscnt$,
                                        ctlrms_dda$      = :cbrmsn0_sldo$
                                    where ctlrms_id = :cbrmsn0_id;
                                end
                end
            else
                begin
                    cbrmsn0_msg = 'n/c';
                end
            suspend;
        end
        
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE CBRMSN0 TO  SYSDBA;

