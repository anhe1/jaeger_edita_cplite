﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.Pivot.Core;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class ResumenForm : RadForm {
        protected internal IVentasService _Service;
        protected List<VentasResumenModel> _DataSource;

        public ResumenForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void RemisionResumenForm_Load(object sender, EventArgs e) {
            this.Ejercicio.HostedItem = this.SpinEjericio.SpinElement;
            this.SpinEjericio.Minimum = 2011;
            this.SpinEjericio.Maximum = DateTime.Now.Year;
            this.SpinEjericio.Value = DateTime.Now.Year;

            ///configuracion de la tabla pivote
            var sumAggregateFunctionCantidad = new SumAggregateFunction();
            var propertyAggregateDescriptionCantidad = new PropertyAggregateDescription() {
                AggregateFunction = sumAggregateFunctionCantidad,
                CustomName = "Cantidad",
                PropertyName = "Cantidad",
                StringFormat = "#,###0",
                StringFormatSelector = null,
                TotalFormat = null
            };

            var groupNameComparerCatalogo = new GroupNameComparer();
            var propertyGroupDescriptionCatalogo = new PropertyGroupDescription() {
                CustomName = "Catálogo",
                GroupComparer = groupNameComparerCatalogo,
                GroupFilter = null,
                PropertyName = "Catalogo",
                ShowGroupsWithNoData = false,
                SortOrder = Telerik.Pivot.Core.SortOrder.Ascending
            };

            var groupNameComparerModelo = new GroupNameComparer();
            var propertyGroupDescriptionModelo = new PropertyGroupDescription() {
                CustomName = "Modelo",
                GroupComparer = groupNameComparerModelo,
                GroupFilter = null,
                PropertyName = "Modelo",
                ShowGroupsWithNoData = false,
                SortOrder = Telerik.Pivot.Core.SortOrder.Ascending
            };

            this.GridData.AggregateDescriptions.Add(propertyAggregateDescriptionCantidad);
            //this.GridData.ColumnGroupDescriptions.Add(dateTimeGroupDescriptionAnio);
            //this.GridData.ColumnGroupDescriptions.Add(dateTimeGroupDescriptionMes);
            this.GridData.RowGroupDescriptions.Add(propertyGroupDescriptionCatalogo);
            this.GridData.RowGroupDescriptions.Add(propertyGroupDescriptionModelo);
            this.GridData.PivotGridElement.RowHeadersLayout = PivotLayout.Compact;

            this._Service = new VentasService();
            this.Resumen.DisplayMember = "Descriptor";
            this.Resumen.ValueMember = "Id";
            this.Resumen.DataSource = VentasService.GetResumenTipo();
            this.Actualizar.Click += TResumen_Actualizar_Click;
        }

        private void TResumen_Actualizar_Click(object sender, EventArgs e) {
            using(var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this._DataSource;
        }

        private void Consultar() {
            bool isRemision;
            if (this.Resumen.SelectedValue != null) {
                isRemision = (int)this.Resumen.SelectedValue == 2;
            } else {
                isRemision = this.Resumen.Text.ToLower().Contains("remis");
            }
            int year = int.Parse(this.SpinEjericio.Value.ToString());
            if (isRemision) {
                this._DataSource = this._Service.GetList<VentasResumenModel>(VentasService.Query().WithRemisionado().Year(year).Build()).ToList();
            } else {
                this._DataSource = this._Service.GetList<VentasResumenModel>(VentasService.Query().WithPedido().Year(year).Build()).ToList();
            }
        }
    }
}
