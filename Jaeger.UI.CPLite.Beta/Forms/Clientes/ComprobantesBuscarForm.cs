﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Almacen.PT.Services;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public partial class ComprobantesBuscarForm : RadForm {
        protected INotaDescuentoService service;
        private BindingList<NotaDescuentoRelacionDetailModel> datos;

        public void OnAgregar(NotaDescuentoRelacionDetailModel e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public event EventHandler<NotaDescuentoRelacionDetailModel> AgregarComprobante;

        public ComprobantesBuscarForm() {
            InitializeComponent();
        }

        private void ComprobantesBuscarForm_Load(object sender, EventArgs e) {
            this.service = new NotaDescuentoService();
        }

        private void ToolBarButtonAgregar_Click(object sender, EventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                var seleccionado = this.gridSearchResult.CurrentRow.DataBoundItem as NotaDescuentoRelacionDetailModel;
                if (seleccionado != null) {
                    this.OnAgregar(seleccionado);
                }
            }
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            if (this.workerBuscar.IsBusy == false) {
                this.radWaitingBar1.StartWaiting();
                this.workerBuscar.RunWorkerAsync();
            }
        }

        private void gridSearchResult_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.gridSearchResult.CurrentRow != null) {
                this.ToolBarButtonAgregar.PerformClick();
                this.Close();
            }
        }

        private void workerBuscar_DoWork(object sender, DoWorkEventArgs e) {
            this.datos = new BindingList<NotaDescuentoRelacionDetailModel>(this.service.GetPartidas());
            e.Result = true;
        }

        private void workerBuscar_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            
        }

        private void workerBuscar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.gridSearchResult.DataSource = this.datos;
            this.radWaitingBar1.StopWaiting();
        }
    }
}
