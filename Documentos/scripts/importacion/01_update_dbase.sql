/* **************************************************************************************************************
  +  ACTUALIZACIONES PARA LA BASE DE DATOS A EXPORTAR
 ************************************************************************************************************** */

/* DOMINIOS UTILIZADOS */
CREATE DOMAIN DOM_CP AS Varchar(5) ;
CREATE DOMAIN DOM_CURP AS Varchar(20) ;
CREATE DOMAIN DOM_RFC AS Varchar(14) ;

/* CATALOGO DE COMISIONES vamos a omitir
ALTER TABLE DRCTR ADD DRCTR_CMSN_ID Integer;
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de comisiones en caso de ser vendedor'  where RDB$FIELD_NAME = 'DRCTR_CMSN_ID' and RDB$RELATION_NAME = 'DRCTR';
*/
/* **************************************************************************************************************
  + CATÁLOGO DE CLASIFICACIONES O CATEGORIAS
************************************************************************************************************** */
CREATE TABLE CTCLS
(
  CTCLS_ID Integer NOT NULL,
  CTCLS_A Smallint DEFAULT 1 NOT NULL,
  CTCLS_ALM_ID Smallint DEFAULT 1 NOT NULL,
  CTCLS_SBID Integer,
  CTCLS_CLASS1 Varchar(128),
  CTCLS_CLASS2 Varchar(128),
  CTCLS_USR_N Varchar(10),
  CTCLS_FN Timestamp NOT NULL,
  CONSTRAINT PK_CTCLS_ID PRIMARY KEY (CTCLS_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de la tabla'  where RDB$FIELD_NAME = 'CTCLS_ID' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTCLS_A' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de almacen'  where RDB$FIELD_NAME = 'CTCLS_ALM_ID' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion en si mismo'  where RDB$FIELD_NAME = 'CTCLS_SBID' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub clasificacion'  where RDB$FIELD_NAME = 'CTCLS_CLASS1' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la categoria'  where RDB$FIELD_NAME = 'CTCLS_CLASS2' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTCLS_USR_N' and RDB$RELATION_NAME = 'CTCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de sistema'  where RDB$FIELD_NAME = 'CTCLS_FN' and RDB$RELATION_NAME = 'CTCLS';
CREATE INDEX IDX_CTCLS_ID ON CTCLS (CTCLS_SBID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'categorias / clasificaciones'
where RDB$RELATION_NAME = 'CTCLS';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTCLS TO  SYSDBA WITH GRANT OPTION;

/* **************************************************************************************************************
  +  TABLA DEL CATALOGO DE PRODUCTOS
 ************************************************************************************************************** */
CREATE TABLE CTPRD
(
  CTPRD_ID Integer NOT NULL,
  CTPRD_A Smallint NOT NULL,
  CTPRD_ALM_ID Smallint NOT NULL,
  CTPRD_TIPO Smallint NOT NULL,
  CTPRD_CTCLS_ID Smallint NOT NULL,
  CTPRV_CNVR Decimal(14,4),
  CTPRD_CNVR Decimal(14,4),
  CTPRD_CLV Varchar(128),
  CTPRD_NOM Varchar(128),
  CTPRD_USR_N Varchar(20),
  CTPRD_FN Timestamp,
  CONSTRAINT PK_CTPRD_ID PRIMARY KEY (CTPRD_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTPRD_ID' and RDB$RELATION_NAME = 'CTPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del almacen'  where RDB$FIELD_NAME = 'CTPRD_ALM_ID' and RDB$RELATION_NAME = 'CTPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de categorias'  where RDB$FIELD_NAME = 'CTPRD_CTCLS_ID' and RDB$RELATION_NAME = 'CTPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de conversion a la unidad de almacen'  where RDB$FIELD_NAME = 'CTPRV_CNVR' and RDB$RELATION_NAME = 'CTPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de producto para formar URL'  where RDB$FIELD_NAME = 'CTPRD_CLV' and RDB$RELATION_NAME = 'CTPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion del producto'  where RDB$FIELD_NAME = 'CTPRD_NOM' and RDB$RELATION_NAME = 'CTPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de usuario que crea el registro'  where RDB$FIELD_NAME = 'CTPRD_USR_N' and RDB$RELATION_NAME = 'CTPRD';
CREATE INDEX IDX_CTPRD_ALM_ID ON CTPRD (CTPRD_ALM_ID);
CREATE INDEX IDX_CTPRD_CTCLS_ID ON CTPRD (CTPRD_CTCLS_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de productos'
where RDB$RELATION_NAME = 'CTPRD';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTPRD TO  SYSDBA WITH GRANT OPTION;

CREATE TABLE CTMDL
(
  CTMDL_ID Integer NOT NULL,
  CTMDL_A Smallint NOT NULL,
  CTMDL_CTCLS_ID Integer NOT NULL,
  CTMDL_CTPRD_ID Integer NOT NULL,
  CTMDL_CTPRC_ID Integer NOT NULL,
  CTMDL_ALM_ID Smallint NOT NULL,
  CTMDL_ATRZD Smallint NOT NULL,
  CTMDL_TIPO Smallint NOT NULL,
  CTMDL_VIS Smallint NOT NULL,
  CTMDL_UNDDXY Integer NOT NULL,
  CTMDL_UNDDZ Integer NOT NULL,
  CTMDL_UNDDA Integer NOT NULL,
  CTMDL_RETISR Numeric(14,4),
  CTMDL_RETISRF Varchar(5),
  CTMDL_RETIVA Numeric(14,4),
  CTMDL_RETIVAF Varchar(5),
  CTMDL_TRSIVA Numeric(14,4) DEFAULT 0.16 NOT NULL,
  CTMDL_TRSIVAF Varchar(6) DEFAULT 'Tasa' NOT NULL,
  CTMDL_RETIEPS Numeric(14,4),
  CTMDL_RETIEPSF Varchar(6),
  CTMDL_TRSIEPS Numeric(14,4),
  CTMDL_TRSIEPSF Varchar(6),
  CTMDL_UNTR Numeric(11,4),
  CTMDL_UNTR1 Numeric(11,4),
  CTMDL_UNTR2 Numeric(11,4),
  CTMDL_CANT1 Smallint,
  CTMDL_CANT2 Smallint,
  CTMDL_EXT Numeric(11,4),
  CTMDL_MIN Numeric(11,4),
  CTMDL_MAX Numeric(11,4),
  CTMDL_REORD Numeric(11,4),
  CTMDL_LARGO Numeric(14,4),
  CTMDL_ANCHO Numeric(14,4),
  CTMDL_ALTO Numeric(14,4),
  CTMDL_PESO Numeric(14,4),
  CTMDL_CLVUND Varchar(3),
  CTMDL_CLVPRDS Varchar(8),
  CTMDL_CLVOBJ Varchar(2),
  CTMDL_CDG Varchar(12),
  CTMDL_UNDD Varchar(20),
  CTMDL_CTAPRE Varchar(20),
  CTMDL_SKU Varchar(100),
  CTMDL_NUMREQ Varchar(50),
  CTMDL_CLV Varchar(128),
  CTMDL_MRC Varchar(128),
  CTMDL_ESPC Varchar(128),
  CTMDL_DSCRC Varchar(1000),
  CTMDL_ETQTS Varchar(254),
  CTMDL_DSCR Varchar(8000),
  CTMDL_USR_N Varchar(10),
  CTMDL_FN Timestamp NOT NULL,
  CTMDL_USR_M Varchar(10),
  CTMDL_FM Timestamp,
  CONSTRAINT PK_CTMDL_ID PRIMARY KEY (CTMDL_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice principal de la tabla'  where RDB$FIELD_NAME = 'CTMDL_ID' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTMDL_A' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de categorias'  where RDB$FIELD_NAME = 'CTMDL_CTCLS_ID' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de productos'  where RDB$FIELD_NAME = 'CTMDL_CTPRD_ID' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de precios'  where RDB$FIELD_NAME = 'CTMDL_CTPRC_ID' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del almacen mp = 1, pt = 2, tw = 3'  where RDB$FIELD_NAME = 'CTMDL_ALM_ID' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'precio autorizado'  where RDB$FIELD_NAME = 'CTMDL_ATRZD' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: tipo de producto o servicio (1-producto, 2-servicio, 3-kit, 4-grupo de productos). Es utilizado en CFDI para hacer la distincion de producto o servicio.'  where RDB$FIELD_NAME = 'CTMDL_TIPO' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'para indicar si es visible en la tienda web (0=ninguno, 1=distribuidores, 2=tienda web, 3=ambos)'  where RDB$FIELD_NAME = 'CTMDL_VIS' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt| desc: indice de la unidad utilizada en largo y ancho'  where RDB$FIELD_NAME = 'CTMDL_UNDDXY' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt| desc: indice de la unidad utiliza en alto o calibre eje Z'  where RDB$FIELD_NAME = 'CTMDL_UNDDZ' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt| desc: indice de la unidad de almacenamiento en el el almacen'  where RDB$FIELD_NAME = 'CTMDL_UNDDA' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pt,tw| desc: retencion del impuesto sobre la renta (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_RETISR' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Factor de ISR Retenido (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_RETISRF' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pt,tw| desc: importe de la retencion del IVA (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_RETIVA' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Factor IVA Retenido (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_RETIVAF' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pt,tw| desc: importe del impuesto del IVA trasladado'  where RDB$FIELD_NAME = 'CTMDL_TRSIVA' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Factor de IVA Trasladado'  where RDB$FIELD_NAME = 'CTMDL_TRSIVAF' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pt,tw| desc: importe de la retencion del impuesto IEPS (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_RETIEPS' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Factor de IESP Retenido (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_RETIEPSF' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pt,tw| desc: impuesto especial sobre productos y servicios (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_TRSIEPS' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Factor de IEPS Trasladado (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_TRSIEPSF' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: precio unitario'  where RDB$FIELD_NAME = 'CTMDL_UNTR' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: precio unitario de primer mayoreo (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_UNTR1' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: precio unitario de segundo mayoreo (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_UNTR2' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: cantidad de unidades para primer mayoreo (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_CANT1' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pm,pt,tw| desc: cantidad de unidades para segundo mayoreo (por eliminar)'  where RDB$FIELD_NAME = 'CTMDL_CANT2' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: cantidad de existencia'  where RDB$FIELD_NAME = 'CTMDL_EXT' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: stock minimo del almacen'  where RDB$FIELD_NAME = 'CTMDL_MIN' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: stock maximo del almacen'  where RDB$FIELD_NAME = 'CTMDL_MAX' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: punto de reorden'  where RDB$FIELD_NAME = 'CTMDL_REORD' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: largo'  where RDB$FIELD_NAME = 'CTMDL_LARGO' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: ancho'  where RDB$FIELD_NAME = 'CTMDL_ANCHO' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: alto o calibre'  where RDB$FIELD_NAME = 'CTMDL_ALTO' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: alto o calibre'  where RDB$FIELD_NAME = 'CTMDL_PESO' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: clave de unidad SAT'  where RDB$FIELD_NAME = 'CTMDL_CLVUND' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: clave del producto o del servicio SAT'  where RDB$FIELD_NAME = 'CTMDL_CLVPRDS' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: clave si la operacion es objeto o no de impuesto SAT'  where RDB$FIELD_NAME = 'CTMDL_CLVOBJ' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: codigo de barras'  where RDB$FIELD_NAME = 'CTMDL_CDG' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: unidad personalizada'  where RDB$FIELD_NAME = 'CTMDL_UNDD' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt| desc: numero de cuenta predial con el que fue registrado el inmueble, en el sistema catastral de la entidad federativa de que trate, o bien para incorporar los datos de identificacion del certificado de participacion inmobiliaria no amortizable.'  where RDB$FIELD_NAME = 'CTMDL_CTAPRE' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: obtener o establecer el numero de parte, identificador del producto o del servicio, la clave de producto o servicio, SKU o equivalente, propia de la operacion del emisor (en cfdi: NoIdentificacion)'  where RDB$FIELD_NAME = 'CTMDL_SKU' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: numero de requerimiento de aduana'  where RDB$FIELD_NAME = 'CTMDL_NUMREQ' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|pt,tw| desc: clave del modelo (url)'  where RDB$FIELD_NAME = 'CTMDL_CLV' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt| desc: marca o fabricante'  where RDB$FIELD_NAME = 'CTMDL_MRC' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt| desc: espcificaciones'  where RDB$FIELD_NAME = 'CTMDL_ESPC' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: descripcion corta'  where RDB$FIELD_NAME = 'CTMDL_DSCRC' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|mp,pt,tw| desc: etiquetas de busqueda'  where RDB$FIELD_NAME = 'CTMDL_ETQTS' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'alm:|tw| desc: descripcion larga del producto'  where RDB$FIELD_NAME = 'CTMDL_DSCR' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que crea el registro'  where RDB$FIELD_NAME = 'CTMDL_USR_N' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTMDL_FN' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'CTMDL_USR_M' and RDB$RELATION_NAME = 'CTMDL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'CTMDL_FM' and RDB$RELATION_NAME = 'CTMDL';
CREATE INDEX IDX_CTMDL_CTCLS_ID ON CTMDL (CTMDL_CTCLS_ID);
CREATE INDEX IDX_CTMDL_CTPRC_ID ON CTMDL (CTMDL_CTPRC_ID);
CREATE INDEX IDX_CTMDL_CTPRD_ID ON CTMDL (CTMDL_CTPRD_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de modelos de productos'
where RDB$RELATION_NAME = 'CTMDL';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTMDL TO  SYSDBA WITH GRANT OPTION;


CREATE TABLE CTMDLE
(
  CTMDLE_ID Integer NOT NULL,
  CTMDLE_A Smallint NOT NULL,
  CTMDLE_CTPRD_ID Integer NOT NULL,
  CTMDLE_CTMDL_ID Integer NOT NULL,
  CTMDLE_CTESPC_ID Integer NOT NULL,
  CTMDLE_ATRZD Smallint NOT NULL,
  CTMDLE_ALM_ID Smallint NOT NULL,
  CTMDLE_VIS Smallint NOT NULL,
  CTMDLE_EXT Numeric(11,4),
  CTMDLE_MIN Numeric(11,4),
  CTMDLE_MAX Numeric(11,4),
  CTMDLE_REORD Numeric(11,4),
  CTMDLE_USR_N Varchar(10),
  CTMDLE_FN Timestamp NOT NULL,
  CTMDLE_USR_M Varchar(10),
  CTMDLE_FM Timestamp,
  CONSTRAINT PK_CTMDLE_ID PRIMARY KEY (CTMDLE_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTMDLE_ID' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTMDLE_A' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de productos'  where RDB$FIELD_NAME = 'CTMDLE_CTPRD_ID' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de modelos'  where RDB$FIELD_NAME = 'CTMDLE_CTMDL_ID' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de CTESPC_ID'  where RDB$FIELD_NAME = 'CTMDLE_CTESPC_ID' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'autorizacion'  where RDB$FIELD_NAME = 'CTMDLE_ATRZD' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del tipo de almacen'  where RDB$FIELD_NAME = 'CTMDLE_ALM_ID' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'bandera para el registro visible'  where RDB$FIELD_NAME = 'CTMDLE_VIS' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'existencia'  where RDB$FIELD_NAME = 'CTMDLE_EXT' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'minimo'  where RDB$FIELD_NAME = 'CTMDLE_MIN' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'maximo'  where RDB$FIELD_NAME = 'CTMDLE_MAX' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'reorden'  where RDB$FIELD_NAME = 'CTMDLE_REORD' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTMDLE_USR_N' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTMDLE_FN' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima clave del usuario que modifica el registro'  where RDB$FIELD_NAME = 'CTMDLE_USR_M' and RDB$RELATION_NAME = 'CTMDLE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion'  where RDB$FIELD_NAME = 'CTMDLE_FM' and RDB$RELATION_NAME = 'CTMDLE';
CREATE INDEX IDX_CTMDLE_ALM_ID ON CTMDLE (CTMDLE_ALM_ID);
CREATE INDEX IDX_CTMDLE_CTESPC_ID ON CTMDLE (CTMDLE_CTESPC_ID);
CREATE INDEX IDX_CTMDLE_CTMDL_ID ON CTMDLE (CTMDLE_CTMDL_ID);
CREATE INDEX IDX_CTMDLE_ID ON CTMDLE (CTMDLE_CTPRD_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'tabla de relacion modelos vs especificaciones (tamanios)'
where RDB$RELATION_NAME = 'CTMDLE';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTMDLE TO  SYSDBA WITH GRANT OPTION;

/* **************************************************************************************************************
  +  TABLA DEL CATALOGO DE IMAGENES
 ************************************************************************************************************** */
CREATE TABLE CTMDLM
(
  CTMDLM_ID Integer NOT NULL,
  CTMDLM_A Smallint DEFAULT 1 NOT NULL,
  CTMDLM_CTMDL_ID Integer,
  CTMDLM_URL Varchar(8000),
  CONSTRAINT PK_CTMDLM_ID PRIMARY KEY (CTMDLM_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTMDLM_ID' and RDB$RELATION_NAME = 'CTMDLM';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTMDLM_A' and RDB$RELATION_NAME = 'CTMDLM';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ID de modelo'  where RDB$FIELD_NAME = 'CTMDLM_CTMDL_ID' and RDB$RELATION_NAME = 'CTMDLM';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'URL amazon'  where RDB$FIELD_NAME = 'CTMDLM_URL' and RDB$RELATION_NAME = 'CTMDLM';
CREATE INDEX IDX_CTMDLM_CTMDL_ID ON CTMDLM (CTMDLM_CTMDL_ID);
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTMDLM TO  SYSDBA WITH GRANT OPTION;

SET TERM ^ ;
CREATE TRIGGER CTMDLM_BI FOR CTMDLM ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 22112021 2151
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTMDLM_ID IS NULL) THEN
    NEW.CTMDLM_ID = GEN_ID(GEN_CTMDLM_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTMDLM_ID, 0);
    if (tmp < new.CTMDLM_ID) then
      tmp = GEN_ID(GEN_CTMDLM_ID, new.CTMDLM_ID-tmp);
  END
END^
SET TERM ; ^


/* **************************************************************************************************************
  +  TABLA DEL CATALOGO DE PRECIOS (LISTA)
 ************************************************************************************************************** */
CREATE TABLE CTPRC
(
  CTPRC_ID Integer DEFAULT 0 NOT NULL,
  CTPRC_A Smallint DEFAULT 1 NOT NULL,
  CTPRC_SEC_ID Smallint,
  CTPRC_STTS_ID Smallint DEFAULT 0,
  CTPRC_NOM Varchar(50),
  CTPRC_FECINI Date,
  CTPRC_FECFIN Date,
  CTPRC_OBSRV Varchar(100),
  CTPRC_FN Timestamp DEFAULT 'now',
  CTPRC_USU_N Varchar(10),
  CTPRC_FM Timestamp,
  CTPRC_USU_M Varchar(10),
  PRIMARY KEY (CTPRC_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTPRC_ID' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTPRC_A' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'status de la lista'  where RDB$FIELD_NAME = 'CTPRC_STTS_ID' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion de la lista'  where RDB$FIELD_NAME = 'CTPRC_NOM' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de inicio de la vigencia'  where RDB$FIELD_NAME = 'CTPRC_FECINI' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha final de la vigencia'  where RDB$FIELD_NAME = 'CTPRC_FECFIN' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'CTPRC_OBSRV' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTPRC_FN' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTPRC_USU_N' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de la ultima modificacion del registro'  where RDB$FIELD_NAME = 'CTPRC_FM' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'CTPRC_USU_M' and RDB$RELATION_NAME = 'CTPRC';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de lista de precios'
where RDB$RELATION_NAME = 'CTPRC';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTPRC TO  SYSDBA WITH GRANT OPTION;

CREATE TABLE CTPRCP
(
  CTPRCP_ID Integer DEFAULT 0 NOT NULL,
  CTPRCP_A Smallint DEFAULT 1 NOT NULL,
  CTPRCP_CTPRC_ID Integer DEFAULT 0 NOT NULL,
  CTPRCP_CTESPC_ID Integer DEFAULT 0,
  CTPRCP_UNITC Numeric(18,4),
  CTPRCP_UNIT Numeric(18,4) DEFAULT 0,
  CTPRCP_OBSRV Varchar(100),
  CTPRCP_FN Timestamp DEFAULT 'now',
  CTPRCP_USU_N Varchar(10),
  CTPRCP_FM Timestamp,
  CTPRCP_USU_M Varchar(10),
  PRIMARY KEY (CTPRCP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTPRCP_ID' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTPRCP_A' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el catalogo de lista de precios'  where RDB$FIELD_NAME = 'CTPRCP_CTPRC_ID' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de especificaciones'  where RDB$FIELD_NAME = 'CTPRCP_CTESPC_ID' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor unitario'  where RDB$FIELD_NAME = 'CTPRCP_UNIT' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'CTPRCP_OBSRV' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTPRCP_FN' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTPRCP_USU_N' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de la ultima modificacion del registro'  where RDB$FIELD_NAME = 'CTPRCP_FM' and RDB$RELATION_NAME = 'CTPRCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifca el registro'  where RDB$FIELD_NAME = 'CTPRCP_USU_M' and RDB$RELATION_NAME = 'CTPRCP';
CREATE INDEX IDX_CTPRCP_CTPRC_ID ON CTPRCP (CTPRCP_CTPRC_ID);
CREATE INDEX IDX_CTPRCP_CTESPC_ID ON CTPRCP (CTPRCP_CTESPC_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'tabla de partidas de la lista de precios'
where RDB$RELATION_NAME = 'CTPRCP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTPRCP TO  SYSDBA WITH GRANT OPTION;


/* ****************************************
  DIRECTORIO DE LA APLICACION
**************************************** */
CREATE TABLE DRCTRT
(
  DRCTR_ID Integer DEFAULT 0 NOT NULL,
  DRCTR_CVROLL Varchar(32),
  DRCTR_APIKEY Varchar(32),
  DRCTR_A Smallint DEFAULT 1 NOT NULL,
  DRCTR_EXTRNJR Smallint,
  DRCTR_PRNCPL Smallint,
  DRCTR_CMSN_ID Integer,
  DRCTR_DSCNT_ID Integer DEFAULT 0,
  DRCTR_USR Smallint,
  DRCTR_DSP Integer DEFAULT 0,
  DRCTR_USOCFDI Varchar(3),
  DRCTR_USR_N Varchar(10),
  DRCTR_USR_M Varchar(10),
  DRCTR_RFCV Smallint,
  DRCTR_RGSTRPTRNL Varchar(12),
  DRCTR_RFC DOM_RFC,
  DRCTR_CLV Varchar(10),
  DRCTR_CURP DOM_CURP,
  DRCTR_RESFIS Varchar(20),
  DRCTR_NMREG Varchar(40),
  DRCTR_DOMFIS Varchar(5),
  DRCTR_PSW Varchar(64),
  DRCTR_RGF Varchar(10),
  DRCTR_RLCN Varchar(64),
  DRCTR_WEB Varchar(64),
  DRCTR_TEL Varchar(64),
  DRCTR_MAIL Varchar(100),
  DRCTR_NOM Varchar(255),
  DRCTR_NOMC Varchar(255),
  DRCTR_RGFSC Varchar(3),
  DRCTR_NTS Varchar(100),
  DRCTR_CRD Numeric(18,4) DEFAULT 0,
  DRCTR_SCRD Numeric(18,4) DEFAULT 0,
  DRCTR_PCTD Numeric(18,4) DEFAULT 0,
  DRCTR_IVA Numeric(18,4) DEFAULT 0,
  DRCTR_DOC_ID Integer DEFAULT 0,
  DRCTR_USR_ID Smallint,
  DRCTR_WEB_ID Integer DEFAULT 0,
  DRCTR_CTRG_ID Integer DEFAULT 0,
  DRCTR_SYNC_ID Integer DEFAULT 0,
  DRCTR_DSCRD Integer DEFAULT 0,
  DRCTR_CTPRC_ID Integer DEFAULT 0,
  DRCTR_CDD Varchar(100),
  DRCTR_FN Timestamp DEFAULT 'now',
  DRCTR_FM Date,
  DRCTR_FILE Blob sub_type 0,
  PRIMARY KEY (DRCTR_ID)
);

 /* ****************************************
  RELACIONES COMERCIALES DEL DIRECTORIO 
**************************************** */
CREATE TABLE DRCTRR
(
  DRCTRR_DRCTR_ID Integer NOT NULL,
  DRCTRR_CTREL_ID Smallint DEFAULT 1 NOT NULL,
  DRCTRR_A Smallint,
  DRCTRR_CTCMS_ID Integer DEFAULT 0,
  DRCTRR_CTDSC_ID Integer DEFAULT 0,
  DRCTRR_CTPRC_ID Integer,
  DRCTRR_NOTA Varchar(100),
  DRCTRR_USR_N Varchar(10),
  DRCTRR_FN Timestamp DEFAULT current_timestamp     NOT NULL,
  DRCTRR_USR_M Varchar(10),
  DRCTRR_FM Timestamp,
  CONSTRAINT PK_DRCTRR_1D PRIMARY KEY (DRCTRR_DRCTR_ID,DRCTRR_CTREL_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio'  where RDB$FIELD_NAME = 'DRCTRR_DRCTR_ID' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del tipo de relacion comercial'  where RDB$FIELD_NAME = 'DRCTRR_CTREL_ID' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'DRCTRR_A' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con tabla de comisiones en caso de ser una relacion con vendedor'  where RDB$FIELD_NAME = 'DRCTRR_CTCMS_ID' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de descuentos en caso de ser cliente'  where RDB$FIELD_NAME = 'DRCTRR_CTDSC_ID' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de precios en caso de que la relacion sea cliente'  where RDB$FIELD_NAME = 'DRCTRR_CTPRC_ID' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nota de la relacion'  where RDB$FIELD_NAME = 'DRCTRR_NOTA' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'DRCTRR_USR_N' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'DRCTRR_USR_M' and RDB$RELATION_NAME = 'DRCTRR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'DRCTRR_FM' and RDB$RELATION_NAME = 'DRCTRR';
CREATE INDEX IDX_DRCTRR_CMSNT_ID ON DRCTRR (DRCTRR_CTCMS_ID);
CREATE INDEX IDX_DRCTRR_CTLPRC_ID ON DRCTRR (DRCTRR_CTPRC_ID);
CREATE INDEX IDX_DRCTRR_DRCTR_ID ON DRCTRR (DRCTRR_DRCTR_ID);
CREATE INDEX IDX_DRCTRR_DSCNT_ID ON DRCTRR (DRCTRR_CTDSC_ID);
CREATE INDEX IDX_DRCTRR_TPREL_ID ON DRCTRR (DRCTRR_CTREL_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'directorio: relacion comercial del directorio'
where RDB$RELATION_NAME = 'DRCTRR';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON DRCTRR TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_DRCTR_ID;

SET TERM ^ ;
CREATE TRIGGER DRCTR_BI FOR DRCTR ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 19082022 1222
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.DRCTR_ID IS NULL) THEN
    NEW.DRCTR_ID = GEN_ID(GEN_DRCTR_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_DRCTR_ID, 0);
    if (tmp < new.DRCTR_ID) then
      tmp = GEN_ID(GEN_DRCTR_ID, new.DRCTR_ID-tmp);
  END
END^
SET TERM ; ^

CREATE TABLE DRCTRC
(
  DRCTRC_A Smallint DEFAULT 1 NOT NULL,
  DRCTRC_VNDR_ID Integer NOT NULL,
  DRCTRC_DRCTR_ID Integer NOT NULL,
  DRCTRC_FM Timestamp,
  DRCTRC_USR_M Varchar(10),
  CONSTRAINT PK_DRCTRC_VNDR_ID PRIMARY KEY (DRCTRC_VNDR_ID,DRCTRC_DRCTR_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'DRCTRC_A' and RDB$RELATION_NAME = 'DRCTRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de vendedores'  where RDB$FIELD_NAME = 'DRCTRC_VNDR_ID' and RDB$RELATION_NAME = 'DRCTRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio de clientes'  where RDB$FIELD_NAME = 'DRCTRC_DRCTR_ID' and RDB$RELATION_NAME = 'DRCTRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'DRCTRC_FM' and RDB$RELATION_NAME = 'DRCTRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'DRCTRC_USR_M' and RDB$RELATION_NAME = 'DRCTRC';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Vendedores: cartera de clientes, relacion entre clientes y vendedores'
where RDB$RELATION_NAME = 'DRCTRC';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON DRCTRC TO  SYSDBA WITH GRANT OPTION;

 /* **************************************************************************************************************
  +  TABLA DEL CATALOGO DE PEDIDOS DE CLIENTE (NUEVO)
 ************************************************************************************************************** */
CREATE TABLE PDCLN
(
  PDCLN_ID Integer DEFAULT 0 NOT NULL,
  PDCLN_A Smallint DEFAULT 1 NOT NULL,
  PDCLN_STTS_ID Smallint DEFAULT 0,
  PDCLN_ORGN_ID Smallint DEFAULT 0,
  PDCLN_FRMPG_ID Integer DEFAULT 0,
  PDCLN_DRCTR_ID Integer DEFAULT 0,
  PDCLN_DRCCN_ID Integer DEFAULT 0,
  PDCLN_VNDR_ID Integer DEFAULT 0,
  PDCLN_RQIRFCTR Smallint DEFAULT 0,
  PDCLN_AUT_ID Integer DEFAULT 0,
  PDCLN_ENVCST Numeric(18,4) DEFAULT 0,
  PDCLN_IVA Numeric(18,4) DEFAULT 0,
  PDCLN_SBTTL Numeric(18,4) DEFAULT 0,
  PDCLN_TTL Numeric(18,4) DEFAULT 0,
  PDCLN_MTDENV Varchar(60),
  PDCLN_RCB Varchar(100),
  PDCLN_OBSRV Varchar(255),
  PDCLN_MTVCL Varchar(50),
  PDCLN_FCPED Timestamp DEFAULT 'now',
  PDCLN_FCREQ Timestamp,
  PDCLN_FCCNCL Timestamp,
  PDCLN_FCHAUT Timestamp,
  PDCLN_FCENTR Timestamp,
  PDCLN_URL_PDF Varchar(1000),
  PDCLN_SYNC_ID Integer DEFAULT 0,
  PDCLN_USU_CNCL Varchar(10),
  PDCLN_USU_AUT Varchar(10),
  PDCLN_USU_N Varchar(10),
  PDCLN_FN Timestamp DEFAULT 'now',
  PDCLN_USU_M Varchar(10),
  PDCLN_FM Timestamp,
  CONSTRAINT PK_PDCLN_ID PRIMARY KEY (PDCLN_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla (numero de pedido)'  where RDB$FIELD_NAME = 'PDCLN_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'PDCLN_A' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'status del pedido'  where RDB$FIELD_NAME = 'PDCLN_STTS_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'origen del pedido (local = 0, Vendedor = 1, tienda web= 2)'  where RDB$FIELD_NAME = 'PDCLN_ORGN_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'forma de pago'  where RDB$FIELD_NAME = 'PDCLN_FRMPG_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla del directorio (clientes)'  where RDB$FIELD_NAME = 'PDCLN_DRCTR_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la direccion del envio (tabla de direcciones)'  where RDB$FIELD_NAME = 'PDCLN_DRCCN_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de vendedores'  where RDB$FIELD_NAME = 'PDCLN_VNDR_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'requiere factura = 1'  where RDB$FIELD_NAME = 'PDCLN_RQIRFCTR' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del costo de envio'  where RDB$FIELD_NAME = 'PDCLN_ENVCST' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del iva al 16'  where RDB$FIELD_NAME = 'PDCLN_IVA' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal'  where RDB$FIELD_NAME = 'PDCLN_SBTTL' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total'  where RDB$FIELD_NAME = 'PDCLN_TTL' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'metodo de envio'  where RDB$FIELD_NAME = 'PDCLN_MTDENV' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la persona que recibe'  where RDB$FIELD_NAME = 'PDCLN_RCB' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'PDCLN_OBSRV' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de motivo de la cancelacion del pedido'  where RDB$FIELD_NAME = 'PDCLN_MTVCL' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha del pedido'  where RDB$FIELD_NAME = 'PDCLN_FCPED' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha requerida para el pedido'  where RDB$FIELD_NAME = 'PDCLN_FCREQ' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion'  where RDB$FIELD_NAME = 'PDCLN_FCCNCL' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de autorizacion'  where RDB$FIELD_NAME = 'PDCLN_FCHAUT' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de entrega al cliente'  where RDB$FIELD_NAME = 'PDCLN_FCENTR' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'url para archivo adjunto'  where RDB$FIELD_NAME = 'PDCLN_URL_PDF' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del pedido sincronizado'  where RDB$FIELD_NAME = 'PDCLN_SYNC_ID' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el pedido'  where RDB$FIELD_NAME = 'PDCLN_USU_CNCL' and RDB$RELATION_NAME = 'PDCLN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que autoriza el pedido'  where RDB$FIELD_NAME = 'PDCLN_USU_AUT' and RDB$RELATION_NAME = 'PDCLN';
CREATE INDEX IDX_PDCLN_DRCCN_ID ON PDCLN (PDCLN_DRCCN_ID);
CREATE INDEX IDX_PDCLN_DRCTR_ID ON PDCLN (PDCLN_DRCTR_ID);
CREATE INDEX IDX_PDCLN_VNDR_ID ON PDCLN (PDCLN_VNDR_ID);
UPDATE RDB$RELATIONS set RDB$DESCRIPTION = 'catalogo de pedidos' where RDB$RELATION_NAME = 'PDCLN';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON PDCLN TO  SYSDBA WITH GRANT OPTION;

CREATE GENERATOR GEN_PDCLN_ID;

SET TERM !! ;
CREATE TRIGGER PDCLN_BI FOR PDCLN
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 07072022 2302
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.PDCLN_ID IS NULL) THEN
    NEW.PDCLN_ID = GEN_ID(GEN_PDCLN_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_PDCLN_ID, 0);
    if (tmp < new.PDCLN_ID) then
      tmp = GEN_ID(GEN_PDCLN_ID, new.PDCLN_ID-tmp);
  END
END!!
SET TERM ; !!

CREATE TABLE PDCLP
(
  PDCLP_ID Integer DEFAULT 0 NOT NULL,
  PDCLP_A Smallint DEFAULT 1 NOT NULL,
  PDCLP_DOC_ID Integer DEFAULT 0,
  PDCLP_PDCLN_ID Integer DEFAULT 0,
  PDCLP_DRCTR_ID Integer,
  PDCLP_CTDPT_ID Integer DEFAULT 0,
  PDCLP_CTALM_ID Integer DEFAULT 0,
  PDCLP_CTPRD_ID Integer DEFAULT 0,
  PDCLP_CTMDL_ID Integer DEFAULT 0,
  PDCLP_CTPRC_ID Integer DEFAULT 0,
  PDCLP_CTESPC_ID Integer DEFAULT 0,
  PDCLP_CTUND_ID Integer DEFAULT 0,
  PDCLP_UNDF Numeric(18,4),
  PDCLP_UNDN Varchar(50),
  PDCLP_CNTD Numeric(18,4),
  PDCLP_CTCLS Varchar(128),
  PDCLP_PRDN Varchar(128),
  PDCLP_MDLN Varchar(128),
  PDCLP_MRC Varchar(128),
  PDCLP_ESPC Varchar(128),
  PDCLP_ESPN Varchar(50),
  PDCLP_UNTC Numeric(18,4),
  PDCLP_UNDC Numeric(18,4),
  PDCLP_UNTR Numeric(18,4) DEFAULT 0,
  PDCLP_UNTR2 Numeric(18,4),
  PDCLP_SBTTL Numeric(18,4) DEFAULT 0,
  PDCLP_DESC Numeric(18,4),
  PDCLP_IMPRT Numeric(18,4),
  PDCLP_TSIVA Numeric(18,4) DEFAULT 0,
  PDCLP_TRIVA Numeric(18,4) DEFAULT 0,
  PDCLP_TOTAL Numeric(18,4) DEFAULT 0,
  PDCLP_SKU Varchar(20),
  PDCLP_USR_N Varchar(10),
  PDCLP_NOTA Varchar(50),
  PDCLP_FN Timestamp DEFAULT 'now',
  PDCLP_USR_M Varchar(10),
  PDCLP_FM Date,
  PDCLP_CTPRCP_ID Integer,
  CONSTRAINT PK_PDCLP_ID PRIMARY KEY (PDCLP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'PDCLP_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'PDCLP_A' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tipos de documento (26 = remisiones)'  where RDB$FIELD_NAME = 'PDCLP_DOC_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de remisiones'  where RDB$FIELD_NAME = 'PDCLP_PDCLN_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio, receptor (idCliente)'  where RDB$FIELD_NAME = 'PDCLP_DRCTR_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)'  where RDB$FIELD_NAME = 'PDCLP_CTDPT_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de almacenes'  where RDB$FIELD_NAME = 'PDCLP_CTALM_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'PDCLP_CTPRD_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de modelos'  where RDB$FIELD_NAME = 'PDCLP_CTMDL_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de precios'  where RDB$FIELD_NAME = 'PDCLP_CTPRC_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de unidades'  where RDB$FIELD_NAME = 'PDCLP_CTUND_ID' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de unidad'  where RDB$FIELD_NAME = 'PDCLP_UNDF' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la unidad utilizada'  where RDB$FIELD_NAME = 'PDCLP_UNDN' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad salida, generalmente el cantidad en una remision al cliente'  where RDB$FIELD_NAME = 'PDCLP_CNTD' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del catalogo'  where RDB$FIELD_NAME = 'PDCLP_CTCLS' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del modelo'  where RDB$FIELD_NAME = 'PDCLP_MDLN' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la marca del producto o modelo'  where RDB$FIELD_NAME = 'PDCLP_MRC' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'especificacion'  where RDB$FIELD_NAME = 'PDCLP_ESPC' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la descripcion'  where RDB$FIELD_NAME = 'PDCLP_ESPN' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario del producto / modelo'  where RDB$FIELD_NAME = 'PDCLP_UNTC' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario por la unidad'  where RDB$FIELD_NAME = 'PDCLP_UNDC' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor unitario por pieza'  where RDB$FIELD_NAME = 'PDCLP_UNTR' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unitario de la unidad (valor unitario por el factor de la unidad)'  where RDB$FIELD_NAME = 'PDCLP_UNTR2' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'PDCLP_SBTTL' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento aplicado al producto'  where RDB$FIELD_NAME = 'PDCLP_DESC' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe = subtotal - descuento'  where RDB$FIELD_NAME = 'PDCLP_IMPRT' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tasa del iva aplicable'  where RDB$FIELD_NAME = 'PDCLP_TSIVA' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del impuesto traslado IVA'  where RDB$FIELD_NAME = 'PDCLP_TRIVA' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total = importe + importe del iva'  where RDB$FIELD_NAME = 'PDCLP_TOTAL' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'identificador: IdProducto + IdModelo + IdEspecificacion (Tamanio)'  where RDB$FIELD_NAME = 'PDCLP_SKU' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que creo el registro'  where RDB$FIELD_NAME = 'PDCLP_USR_N' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'PDCLP_FN' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del utlimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'PDCLP_USR_M' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'PDCLP_FM' and RDB$RELATION_NAME = 'PDCLP';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'movimientos del almacen de producto terminado'
where RDB$RELATION_NAME = 'PDCLP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON PDCLP TO  SYSDBA WITH GRANT OPTION;



CREATE GENERATOR GEN_PDCLP_ID;

SET TERM !! ;
CREATE TRIGGER PDCLP_BI FOR PDCLP
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 11072022 2152
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.PDCLP_ID IS NULL) THEN
    NEW.PDCLP_ID = GEN_ID(GEN_PDCLP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_PDCLP_ID, 0);
    if (tmp < new.PDCLP_ID) then
      tmp = GEN_ID(GEN_PDCLP_ID, new.PDCLP_ID-tmp);
  END
END!!
SET TERM ; !!

/* **************************************************************************************************************
  +  TABLA DE COMPROBANTES RE REMISION
 ************************************************************************************************************** */
CREATE TABLE RMSN
(
  RMSN_ID Integer DEFAULT 0 NOT NULL,
  RMSN_A Smallint DEFAULT 0 NOT NULL,
  RMSN_CTDOC_ID Smallint NOT NULL,
  RMSN_DECI Smallint NOT NULL,
  RMSN_VER Varchar(3),
  RMSN_FOLIO Integer,
  RMSN_CTSR_ID Integer,
  RMSN_SERIE Varchar(10),
  RMSN_STTS_ID Integer,
  RMSN_PDD_ID Integer,
  RMSN_CTCLS_ID Integer,
  RMSN_CTENV_ID Integer,
  RMSN_DRCTR_ID Integer,
  RMSN_DRCCN_ID Integer,
  RMSN_VNDDR_ID Integer,
  RMSN_GUIA_ID Integer,
  RMSN_CTREL_ID Integer,
  RMSN_RFCE Varchar(14),
  RMSN_RFCR Varchar(14),
  RMSN_NOMR Varchar(255),
  RMSN_CLVR Varchar(10),
  RMSN_CNTCT Varchar(255),
  RMSN_UUID Varchar(36),
  RMSN_FECEMS Timestamp,
  RMSN_FECENT Timestamp,
  RMSN_FECUPC Timestamp,
  RMSN_FECCBR Timestamp,
  RMSN_FCCNCL Date,
  RMSN_FECVNC Date,
  RMSN_TPCMB Numeric(18,4),
  RMSN_SBTTL Numeric(18,4),
  RMSN_DSCNT Numeric(18,4),
  RMSN_TRSIVA Numeric(18,4),
  RMSN_GTOTAL Numeric(18,4),
  RMSN_FACIVA Numeric(18,4),
  RMSN_IVAPAC Numeric(18,4),
  RMSN_TOTAL Numeric(18,4),
  RMSN_FACPAC Numeric(18,4),
  RMSN_XCBPAC Numeric(18,4),
  RMSN_DESC Varchar(100),
  RMSN_FACDES Numeric(18,4),
  RMSN_DESCT Numeric(18,4),
  RMSN_XCBRR Numeric(18,4),
  RMSN_CBRD Numeric(18,4),
  RMSN_MONEDA Varchar(3),
  RMSN_MTDPG Varchar(3),
  RMSN_FRMPG Varchar(2),
  RMSN_NOTA Varchar(255),
  RMSN_VNDDR Varchar(10),
  RMSN_USR_C Varchar(10),
  RMSN_FN Timestamp NOT NULL,
  RMSN_USR_N Varchar(10),
  RMSN_FM Timestamp,
  RMSN_USR_M Varchar(10),
  RMSN_DRCCN Varchar(400),
  RMSN_CNDNS Varchar(30),
  RMSN_URL_PDF Varchar(100),
  CONSTRAINT PK_RMSN_ID PRIMARY KEY (RMSN_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RMSN_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSN_A' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de documentos (generalmente para la remision a cliente es 26)'  where RDB$FIELD_NAME = 'RMSN_CTDOC_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de decimales'  where RDB$FIELD_NAME = 'RMSN_DECI' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'version de la remision'  where RDB$FIELD_NAME = 'RMSN_VER' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de control interno'  where RDB$FIELD_NAME = 'RMSN_FOLIO' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de series y folios'  where RDB$FIELD_NAME = 'RMSN_CTSR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie de control interno del  documento en modo texto'  where RDB$FIELD_NAME = 'RMSN_SERIE' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status de la remision'  where RDB$FIELD_NAME = 'RMSN_STTS_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice o numero de pedido del cliente'  where RDB$FIELD_NAME = 'RMSN_PDD_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'RMSN_CTCLS_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla del metodo de envio'  where RDB$FIELD_NAME = 'RMSN_CTENV_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'RMSN_DRCTR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del domicilio del directorio'  where RDB$FIELD_NAME = 'RMSN_DRCCN_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id del vendedor'  where RDB$FIELD_NAME = 'RMSN_VNDDR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de guia o referencia del metodo de envio'  where RDB$FIELD_NAME = 'RMSN_GUIA_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice o clave de relacion con otros comprobantes'  where RDB$FIELD_NAME = 'RMSN_CTREL_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del emisor'  where RDB$FIELD_NAME = 'RMSN_RFCE' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del receptor'  where RDB$FIELD_NAME = 'RMSN_RFCR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o razon social del receptor'  where RDB$FIELD_NAME = 'RMSN_NOMR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de control interno del cliente'  where RDB$FIELD_NAME = 'RMSN_CLVR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'RMSN_CNTCT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'RMSN_UUID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de emision del comprobante'  where RDB$FIELD_NAME = 'RMSN_FECEMS' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de entrega'  where RDB$FIELD_NAME = 'RMSN_FECENT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de pago del comprobante'  where RDB$FIELD_NAME = 'RMSN_FECUPC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de recepcion de cobranza'  where RDB$FIELD_NAME = 'RMSN_FECCBR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion del comprobante'  where RDB$FIELD_NAME = 'RMSN_FCCNCL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = ' fecha de vencimiento del pagare'  where RDB$FIELD_NAME = 'RMSN_FECVNC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de cambio'  where RDB$FIELD_NAME = 'RMSN_TPCMB' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal del comprobante'  where RDB$FIELD_NAME = 'RMSN_SBTTL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento'  where RDB$FIELD_NAME = 'RMSN_DSCNT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto trasladado IVA'  where RDB$FIELD_NAME = 'RMSN_TRSIVA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total (subtotal - descuento + traslado de iva)'  where RDB$FIELD_NAME = 'RMSN_GTOTAL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor del iva pactado con el cliente'  where RDB$FIELD_NAME = 'RMSN_FACIVA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del iva pactado (SUBTOTAL * IVAPAC)'  where RDB$FIELD_NAME = 'RMSN_IVAPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del comprobante'  where RDB$FIELD_NAME = 'RMSN_TOTAL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor pactado, '  where RDB$FIELD_NAME = 'RMSN_FACPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'por cobrar pactado'  where RDB$FIELD_NAME = 'RMSN_XCBPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion del tipo de descuento aplicado'  where RDB$FIELD_NAME = 'RMSN_DESC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor del descuento aplicado'  where RDB$FIELD_NAME = 'RMSN_FACDES' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total del descuento (TOTAL X FACDES)'  where RDB$FIELD_NAME = 'RMSN_DESCT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe por cobrar del comprobante'  where RDB$FIELD_NAME = 'RMSN_XCBRR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'acumulado de la cobranza del comprobante'  where RDB$FIELD_NAME = 'RMSN_CBRD' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificacion ISO 4217.'  where RDB$FIELD_NAME = 'RMSN_MONEDA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del metodo de pago que aplica para este comprobante fiscal digital por Internet, conforme al Articulo 29-A fraccion VII incisos a y b del CFF.'  where RDB$FIELD_NAME = 'RMSN_MTDPG' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.'  where RDB$FIELD_NAME = 'RMSN_FRMPG' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSN_NOTA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor asociada'  where RDB$FIELD_NAME = 'RMSN_VNDDR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el comprobante'  where RDB$FIELD_NAME = 'RMSN_USR_C' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSN_FN' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que crea el registro'  where RDB$FIELD_NAME = 'RMSN_USR_N' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSN_FM' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica'  where RDB$FIELD_NAME = 'RMSN_USR_M' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'direccion del envio en modo texto'  where RDB$FIELD_NAME = 'RMSN_DRCCN' and RDB$RELATION_NAME = 'RMSN';
ALTER TABLE RMSN ADD RMSN_SALDO COMPUTED BY (CAST((RMSN_XCBRR - RMSN_CBRD) AS NUMERIC(18, 4)));
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '/remision/{0:36}.pdf'  where RDB$FIELD_NAME = 'RMSN_URL_PDF' and RDB$RELATION_NAME = 'RMSN';
CREATE INDEX IDX_RMSN_CTCLS_ID ON RMSN (RMSN_CTCLS_ID);
CREATE INDEX IDX_RMSN_CTENV_ID ON RMSN (RMSN_CTENV_ID);
CREATE INDEX IDX_RMSN_DRCTR_ID ON RMSN (RMSN_DRCTR_ID);
CREATE INDEX IDX_RMSN_PDD_ID ON RMSN (RMSN_PDD_ID);
CREATE INDEX IDX_RMSN_SERIE_ID ON RMSN (RMSN_CTSR_ID);
CREATE INDEX IDX_RMSN_STTS_ID ON RMSN (RMSN_STTS_ID);
UPDATE RDB$RELATIONS set RDB$DESCRIPTION = 'remisiones del almacen de producto terminado, remision a cliente' where RDB$RELATION_NAME = 'RMSN';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSN TO  SYSDBA WITH GRANT OPTION;

/* -------------------------------------------------------------------------------------------------------------
    Creación de generador para el indice de la tabla
------------------------------------------------------------------------------------------------------------- */
CREATE GENERATOR GEN_RMSN_ID;

SET TERM !! ;
CREATE TRIGGER RMSN_BI FOR RMSN
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 19072022 2053
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RMSN_ID IS NULL) THEN
    NEW.RMSN_ID = GEN_ID(GEN_RMSN_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RMSN_ID, 0);
    if (tmp < new.RMSN_ID) then
      tmp = GEN_ID(GEN_RMSN_ID, new.RMSN_ID-tmp);
  END
END!!
SET TERM ; !!


/* **************************************************************************************************************
  +  TABLA DE DESCUENTOS RELACIONADOS A LA REMISION AL CLIENTE (NO SE ESTA UTILIZANDO)
 ************************************************************************************************************** */
CREATE TABLE RMSND
(
  RMSND_ID Integer NOT NULL,
  RMSND_A Smallint,
  RMSND_RMSN_ID Integer,
  RMSND_DESC Varchar(60),
  RMSND_FCTR Decimal(18,4),
  RMSND_SBTTL Numeric(18,4),
  RMSND_USR_N Varchar(10),
  RMSND_FN Timestamp NOT NULL,
  CONSTRAINT PK_RMSND_ID PRIMARY KEY (RMSND_ID)
);

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSND TO  SYSDBA WITH GRANT OPTION;

/* ************************************************************************************************
 TABLA DE COMISIONES A VENDEDORES POR REMSIONES
************************************************************************************************ */
CREATE TABLE RMSNC
(
  RMSNC_RMSN_ID Integer NOT NULL,
  RMSNC_A Smallint DEFAULT 1 NOT NULL,
  RMSNC_CTCMS_ID Integer,
  RMSNC_DRCTR_ID Integer,
  RMSNC_VNDR_ID Integer,
  RMSNC_CTDIS_ID Integer,
  RMSNC_DESC Varchar(150),
  RMSNC_FCPAC Numeric(18,4),
  RMSNC_FCACT Numeric(18,4),
  RMSNC_IMPR Numeric(18,4),
  RMSNC_ACUML Numeric(18,4),
  RMSNC_VNDR Varchar(10),
  RMSNC_FN Timestamp,
  RMSNC_USR_N Varchar(10),
  CONSTRAINT PK_RMSNC_RMSN_ID PRIMARY KEY (RMSNC_RMSN_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla remision'  where RDB$FIELD_NAME = 'RMSNC_RMSN_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSNC_A' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de comisiones'  where RDB$FIELD_NAME = 'RMSNC_CTCMS_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'RMSNC_DRCTR_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de vendedores'  where RDB$FIELD_NAME = 'RMSNC_VNDR_ID' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion'  where RDB$FIELD_NAME = 'RMSNC_DESC' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor pactado'  where RDB$FIELD_NAME = 'RMSNC_FCPAC' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor actualizado'  where RDB$FIELD_NAME = 'RMSNC_FCACT' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe calculado de la comision'  where RDB$FIELD_NAME = 'RMSNC_IMPR' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe pagado o acumulado'  where RDB$FIELD_NAME = 'RMSNC_ACUML' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor'  where RDB$FIELD_NAME = 'RMSNC_VNDR' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSNC_FN' and RDB$RELATION_NAME = 'RMSNC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima clave del usuario que modifica el registro'  where RDB$FIELD_NAME = 'RMSNC_USR_N' and RDB$RELATION_NAME = 'RMSNC';
CREATE INDEX IDX_RMSNC_CTCMS_ID ON RMSNC (RMSNC_CTCMS_ID);
CREATE INDEX IDX_RMSNC_DRCTR_ID ON RMSNC (RMSNC_DRCTR_ID);
CREATE INDEX IDX_RMSNC_RMSN_ID ON RMSNC (RMSNC_RMSN_ID);
CREATE INDEX IDX_RMSNC_VNDR_ID ON RMSNC (RMSNC_VNDR_ID);
UPDATE RDB$RELATIONS set RDB$DESCRIPTION = 'comisiones a vendedores' where RDB$RELATION_NAME = 'RMSNC';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSNC TO  SYSDBA WITH GRANT OPTION;

/* ************************************************************************************************
     TABLA DEL CONTROL DE STATUS DE REMISIONES
************************************************************************************************ */
CREATE TABLE RMSNS
(
  RMSNS_ID Integer NOT NULL,
  RMSNS_A Smallint DEFAULT 1 NOT NULL,
  RMSNS_RMSN_ID Integer,
  RMSNS_RMSN_STTS_ID Integer,
  RMSNS_RMSN_STTSA_ID Integer,
  RMSNS_DRCTR_ID Integer,
  RMSNS_CVMTV Varchar(100),
  RMSNS_NOTA Varchar(100),
  RMSNS_USR_N Varchar(10),
  RMSNS_FN Timestamp DEFAULT 'Now' NOT NULL,
  CONSTRAINT PK_RMSNS_ID PRIMARY KEY (RMSNS_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice incremental'  where RDB$FIELD_NAME = 'RMSNS_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSNS_A' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de remisiones'  where RDB$FIELD_NAME = 'RMSNS_RMSN_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status anterior'  where RDB$FIELD_NAME = 'RMSNS_RMSN_STTS_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status autorizado'  where RDB$FIELD_NAME = 'RMSNS_RMSN_STTSA_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla del directorio'  where RDB$FIELD_NAME = 'RMSNS_DRCTR_ID' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del motivo del cambio de status'  where RDB$FIELD_NAME = 'RMSNS_CVMTV' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSNS_NOTA' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que autoriza el cambio'  where RDB$FIELD_NAME = 'RMSNS_USR_N' and RDB$RELATION_NAME = 'RMSNS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSNS_FN' and RDB$RELATION_NAME = 'RMSNS';
CREATE INDEX IDX_RMSNS_DRCTR_ID ON RMSNS (RMSNS_DRCTR_ID);
CREATE INDEX IDX_RMSNS_RMSN_ID ON RMSNS (RMSNS_RMSN_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'remisionado: tabla de registro de cambios de status'
where RDB$RELATION_NAME = 'RMSNS';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSNS TO  SYSDBA WITH GRANT OPTION;

 /* ************************************************************************************************
     TABLA DEL ALMACEN DE PRODUCTO TERMINADO
************************************************************************************************ */
CREATE TABLE ALMPT1
(
  ALMPT_ID Integer DEFAULT 0 NOT NULL,
  ALMPT_A Smallint DEFAULT 1 NOT NULL,
  ALMPT_VER Varchar(3),
  ALMPT_CTALM_ID Integer,
  ALMPT_CTEFC_ID Smallint,
  ALMPT_TPALM_ID Integer,
  ALMPT_DOC_ID Integer DEFAULT 0,
  ALMPT_STTS_ID Integer DEFAULT 0,
  ALMPT_CTREL_ID Smallint,
  ALMPT_CTPRD_ID Integer DEFAULT 0,
  ALMPT_PDCLN_ID Integer,
  ALMPT_NTDSC_ID Integer,
  ALMPT_CTDP_ID Integer,
  ALMPT_DRCTR_ID Integer DEFAULT 0,
  ALMPT_VNDDR_ID Integer,
  ALMPT_CVDEV Smallint,
  ALMPT_FOLIO Integer DEFAULT 0,
  ALMPT_SERIE Varchar(10),
  ALMPT_FECEMS Date,
  ALMPT_CLV Varchar(10),
  ALMPT_NOM Varchar(255),
  ALMPT_CNTCT Varchar(100),
  ALMPT_RFRNC Varchar(100),
  ALMPT_SBTTL Numeric(18,4),
  ALMPT_DSCT Numeric(18,4),
  ALMPT_TRIVA Numeric(18,4),
  ALMPT_GTOTAL Numeric(18,4),
  ALMPT_FACIVA Numeric(18,4),
  ALMPT_OBSRV Varchar(100),
  ALMPT_VNDDR Varchar(10),
  ALMPT_FCING Date,
  ALMPT_USU_C Varchar(10),
  ALMPT_FCCNCL Date,
  ALMPT_CVCAN Smallint,
  ALMPT_USU_N Varchar(10),
  ALMPT_FN Timestamp DEFAULT 'now',
  ALMPT_USU_M Varchar(10),
  ALMPT_FM Date,
  ALMPT_URL_PDF Varchar(1000),
  ALMPT_UUID Varchar(36),
  CONSTRAINT PK_ALMPT_ID PRIMARY KEY (ALMPT_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'ALMPT_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'ALMPT_A' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'version del documento'  where RDB$FIELD_NAME = 'ALMPT_VER' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del almacen'  where RDB$FIELD_NAME = 'ALMPT_CTALM_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'efecto del comprobante (1 = Ingreso, 2 = Egreso)'  where RDB$FIELD_NAME = 'ALMPT_CTEFC_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del tipo de documento'  where RDB$FIELD_NAME = 'ALMPT_DOC_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de descuentos'  where RDB$FIELD_NAME = 'ALMPT_STTS_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de relacion con otros comprobantes'  where RDB$FIELD_NAME = 'ALMPT_CTREL_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'ALMPT_CTPRD_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del pedido de control interno del cliente'  where RDB$FIELD_NAME = 'ALMPT_PDCLN_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de  las notas de descuento, este campo lo utilice para marcar las devoluciones que ya fueron asignadas a una nota de descuento'  where RDB$FIELD_NAME = 'ALMPT_NTDSC_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del departamento emisor'  where RDB$FIELD_NAME = 'ALMPT_CTDP_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'ALMPT_DRCTR_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el catalogo de vendedores'  where RDB$FIELD_NAME = 'ALMPT_VNDDR_ID' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del tipo de devolucion'  where RDB$FIELD_NAME = 'ALMPT_CVDEV' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de control interno'  where RDB$FIELD_NAME = 'ALMPT_FOLIO' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie de control interno'  where RDB$FIELD_NAME = 'ALMPT_SERIE' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha emision del comprobante'  where RDB$FIELD_NAME = 'ALMPT_FECEMS' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de control interno'  where RDB$FIELD_NAME = 'ALMPT_CLV' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion de receptor'  where RDB$FIELD_NAME = 'ALMPT_NOM' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'ALMPT_CNTCT' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'referencia'  where RDB$FIELD_NAME = 'ALMPT_RFRNC' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'ALMPT_SBTTL' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total del descuento aplicado '  where RDB$FIELD_NAME = 'ALMPT_DSCT' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto traslado IVA'  where RDB$FIELD_NAME = 'ALMPT_TRIVA' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total SubTotal + TrasladoIVA'  where RDB$FIELD_NAME = 'ALMPT_GTOTAL' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor del IVA pactado'  where RDB$FIELD_NAME = 'ALMPT_FACIVA' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'ALMPT_OBSRV' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor asociado'  where RDB$FIELD_NAME = 'ALMPT_VNDDR' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de ingreso'  where RDB$FIELD_NAME = 'ALMPT_FCING' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el comprobante'  where RDB$FIELD_NAME = 'ALMPT_USU_C' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion del comprobante'  where RDB$FIELD_NAME = 'ALMPT_FCCNCL' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del tipo de cancelacion'  where RDB$FIELD_NAME = 'ALMPT_CVCAN' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'usuario creador del registro'  where RDB$FIELD_NAME = 'ALMPT_USU_N' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del comprobante'  where RDB$FIELD_NAME = 'ALMPT_FN' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'ALMPT_USU_M' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'ALMPT_FM' and RDB$RELATION_NAME = 'ALMPT1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'ALMPT_UUID' and RDB$RELATION_NAME = 'ALMPT1';
CREATE INDEX IDX_ALMPT_CTDP_ID ON ALMPT1 (ALMPT_CTDP_ID);
CREATE INDEX IDX_ALMPT_CTPRD_ID ON ALMPT1 (ALMPT_CTPRD_ID);
CREATE INDEX IDX_ALMPT_CTREL_ID ON ALMPT1 (ALMPT_CTREL_ID);
CREATE INDEX IDX_ALMPT_DRCTR_ID ON ALMPT1 (ALMPT_DRCTR_ID);
CREATE INDEX IDX_ALMPT_NTDSC_ID ON ALMPT1 (ALMPT_NTDSC_ID);
CREATE INDEX IDX_ALMPT_PDCLN_ID ON ALMPT1 (ALMPT_PDCLN_ID);
CREATE INDEX IDX_ALMPT_STTS_ID ON ALMPT1 (ALMPT_STTS_ID);
UPDATE RDB$RELATIONS SET RDB$DESCRIPTION = 'Almacen de Producto Terminado, vales y devoluciones' WHERE RDB$RELATION_NAME = 'ALMPT1';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON ALMPT TO  SYSDBA WITH GRANT OPTION;

 /* **************************************************************************************************************
  +  TABLA DE MOVIMIENTOS DE ALMACEN DE PRODUCTO TERMINADO (APT)
 ************************************************************************************************************** */
CREATE TABLE MVAPT
(
  MVAPT_ID Integer DEFAULT 0 NOT NULL,
  MVAPT_A Smallint DEFAULT 1 NOT NULL,
  MVAPT_DOC_ID Integer DEFAULT 0,
  MVAPT_CTALM_ID Integer DEFAULT 0,
  MVAPT_ALMPT_ID Integer,
  MVAPT_RMSN_ID Integer DEFAULT 0,
  MVAPT_CTEFC_ID Smallint,
  MVAPT_DRCTR_ID Integer,
  MVAPT_PDCLN_ID Integer,
  MVAPT_CTDPT_ID Integer DEFAULT 0,
  MVAPT_CTPRD_ID Integer DEFAULT 0,
  MVAPT_CTMDL_ID Integer DEFAULT 0,
  MVAPT_CTPRC_ID Integer DEFAULT 0,
  MVAPT_CTESPC_ID Integer DEFAULT 0,
  MVAPT_CTUND_ID Integer DEFAULT 0,
  MVAPT_UNDF Numeric(18,4),
  MVAPT_UNDN Varchar(50),
  MVAPT_CANTE Numeric(18,4),
  MVAPT_CANTS Numeric(18,4),
  MVAPT_CTCLS Varchar(128),
  MVAPT_PRDN Varchar(128),
  MVAPT_MDLN Varchar(128),
  MVAPT_MRC Varchar(128),
  MVAPT_ESPC Varchar(128),
  MVAPT_ESPN Varchar(50),
  MVAPT_UNTC Numeric(18,4),
  MVAPT_UNDC Numeric(18,4),
  MVAPT_UNTR Numeric(18,4) DEFAULT 0,
  MVAPT_UNTR2 Numeric(18,4),
  MVAPT_SBTTL Numeric(18,4) DEFAULT 0,
  MVAPT_DESC Numeric(18,4),
  MVAPT_IMPRT Numeric(18,4),
  MVAPT_TSIVA Numeric(18,4) DEFAULT 0,
  MVAPT_TRIVA Numeric(18,4) DEFAULT 0,
  MVAPT_TOTAL Numeric(18,4) DEFAULT 0,
  MVAPT_SKU Varchar(20),
  MVAPT_USR_N Varchar(10),
  MVAPT_FN Timestamp DEFAULT 'now',
  MVAPT_USR_M Varchar(10),
  MVAPT_FM Date,
  CONSTRAINT PK_MVAPT_ID PRIMARY KEY (MVAPT_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'MVAPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'MVAPT_A' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tipos de documento (26 = remisiones)'  where RDB$FIELD_NAME = 'MVAPT_DOC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de almacenes'  where RDB$FIELD_NAME = 'MVAPT_CTALM_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla ALMPT'  where RDB$FIELD_NAME = 'MVAPT_ALMPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de remisiones'  where RDB$FIELD_NAME = 'MVAPT_RMSN_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'efecto del comprobante (1 = Ingreso, 2 = Egreso, 3 = Traslado)'  where RDB$FIELD_NAME = 'MVAPT_CTEFC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio, receptor (idCliente)'  where RDB$FIELD_NAME = 'MVAPT_DRCTR_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el numero de pedido'  where RDB$FIELD_NAME = 'MVAPT_PDCLN_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)'  where RDB$FIELD_NAME = 'MVAPT_CTDPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'MVAPT_CTPRD_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de modelos'  where RDB$FIELD_NAME = 'MVAPT_CTMDL_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de precios'  where RDB$FIELD_NAME = 'MVAPT_CTPRC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de unidades'  where RDB$FIELD_NAME = 'MVAPT_CTUND_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de unidad'  where RDB$FIELD_NAME = 'MVAPT_UNDF' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la unidad utilizada'  where RDB$FIELD_NAME = 'MVAPT_UNDN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad entrante'  where RDB$FIELD_NAME = 'MVAPT_CANTE' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad salida, generalmente el cantidad en una remision al cliente'  where RDB$FIELD_NAME = 'MVAPT_CANTS' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del catalogo'  where RDB$FIELD_NAME = 'MVAPT_CTCLS' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del modelo'  where RDB$FIELD_NAME = 'MVAPT_MDLN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la marca del producto o modelo'  where RDB$FIELD_NAME = 'MVAPT_MRC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'especificacion'  where RDB$FIELD_NAME = 'MVAPT_ESPC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la descripcion'  where RDB$FIELD_NAME = 'MVAPT_ESPN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario del producto / modelo'  where RDB$FIELD_NAME = 'MVAPT_UNTC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'costo unitario por la unidad'  where RDB$FIELD_NAME = 'MVAPT_UNDC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor unitario por pieza'  where RDB$FIELD_NAME = 'MVAPT_UNTR' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unitario de la unidad (valor unitario por el factor de la unidad)'  where RDB$FIELD_NAME = 'MVAPT_UNTR2' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'MVAPT_SBTTL' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento aplicado al producto'  where RDB$FIELD_NAME = 'MVAPT_DESC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe = subtotal - descuento'  where RDB$FIELD_NAME = 'MVAPT_IMPRT' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tasa del iva aplicable'  where RDB$FIELD_NAME = 'MVAPT_TSIVA' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del impuesto traslado IVA'  where RDB$FIELD_NAME = 'MVAPT_TRIVA' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total = importe + importe del iva'  where RDB$FIELD_NAME = 'MVAPT_TOTAL' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'identificador: IdProducto + IdModelo + IdEspecificacion (Tamanio)'  where RDB$FIELD_NAME = 'MVAPT_SKU' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que creo el registro'  where RDB$FIELD_NAME = 'MVAPT_USR_N' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'MVAPT_FN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del utlimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'MVAPT_USR_M' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'MVAPT_FM' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATIONS set RDB$DESCRIPTION = 'movimientos del almacen de producto terminado' where RDB$RELATION_NAME = 'MVAPT';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON MVAPT TO  SYSDBA WITH GRANT OPTION;

/*
--- SCRIPT PARA LA CREACION DE LA TABLA
*/

CREATE TABLE PDCLS
(
  PDCLS_PDCLN_ID Integer DEFAULT 0 NOT NULL,
  PDCLS_STTS_ID Smallint DEFAULT 0 NOT NULL,
  PDCLS_CLMTV_ID Smallint DEFAULT 0 NOT NULL,
  PDCLS_CLMTV Varchar(80),
  PDCLS_OBSRV Varchar(255),
  PDCLS_TTL Numeric(18,4) DEFAULT 0,
  PDCLS_URL_PDF Varchar(1000),
  PDCLS_USU_M Varchar(10),
  PDCLS_FM Timestamp DEFAULT 'now',
  CONSTRAINT PK_PDCLS_ID PRIMARY KEY (PDCLS_PDCLN_ID,PDCLS_STTS_ID,PDCLS_CLMTV_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del peido asociado'  where RDB$FIELD_NAME = 'PDCLS_PDCLN_ID' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status'  where RDB$FIELD_NAME = 'PDCLS_STTS_ID' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la clave de motivo de cambio de status'  where RDB$FIELD_NAME = 'PDCLS_CLMTV_ID' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descripcion de la clave de motivo de cancelacion'  where RDB$FIELD_NAME = 'PDCLS_CLMTV' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'PDCLS_OBSRV' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del pedido'  where RDB$FIELD_NAME = 'PDCLS_TTL' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'url del archivo relacionado al pedido'  where RDB$FIELD_NAME = 'PDCLS_URL_PDF' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que modifica el registro'  where RDB$FIELD_NAME = 'PDCLS_USU_M' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'PDCLS_FM' and RDB$RELATION_NAME = 'PDCLS';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'pedidos cliente, registro de cambios de status'
where RDB$RELATION_NAME = 'PDCLS';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON PDCLS TO  SYSDBA WITH GRANT OPTION;


CREATE TABLE TWUSR
(
  TWUSR_DRCTR_ID Integer DEFAULT 0 NOT NULL,
  TWUSR_TIP_ID Smallint DEFAULT 0 NOT NULL,
  TWUSR_RLSUSR_ID Integer DEFAULT 0 NOT NULL,
  TWUSR_A Smallint DEFAULT 1 NOT NULL,
  TWUSR_KEY Varchar(64),
  TWUSR_MAIL Varchar(255),
  TWUSR_USR_M Varchar(10),
  TWUSR_FM Timestamp DEFAULT 'NOW' NOT NULL,
  TWUSR_NOM Varchar(255),
  CONSTRAINT PK_TWUSR_ID PRIMARY KEY (TWUSR_DRCTR_ID,TWUSR_TIP_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'TWUSR_DRCTR_ID' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de acceso 0 = ninguno, 1 = tienda distribuidores, 2 = tienda minorista, 3 = ambos'  where RDB$FIELD_NAME = 'TWUSR_TIP_ID' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '3 = administrador, 2 = vendedor, 1 = cliente'  where RDB$FIELD_NAME = 'TWUSR_RLSUSR_ID' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'TWUSR_A' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'llave o password'  where RDB$FIELD_NAME = 'TWUSR_KEY' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'correo electronico'  where RDB$FIELD_NAME = 'TWUSR_MAIL' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'TWUSR_USR_M' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'TWUSR_FM' and RDB$RELATION_NAME = 'TWUSR';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'tabla de accesos a tienda web'
where RDB$RELATION_NAME = 'TWUSR';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON TWUSR TO  SYSDBA WITH GRANT OPTION;


 CREATE TABLE NTDSCP
(
  NTDSCP_ID Integer DEFAULT 0 NOT NULL,
  NTDSCP_A Smallint DEFAULT 1 NOT NULL,
  NTDSCP_DOC_ID Integer NOT NULL,
  NTDSCP_ALMPT_ID Integer DEFAULT 0 NOT NULL,
  NTDSCP_NTDSC_ID Integer DEFAULT 0 NOT NULL,
  NTDSCP_PDCLN_ID Integer,
  NTDSCP_DRCTR_ID Integer,
  NTDSCP_TIPO Varchar(25),
  NTDSCP_FOLIO Integer,
  NTDSCP_SERIE Varchar(10),
  NTDSCP_CLV Varchar(10),
  NTDSCP_NOM Varchar(255),
  NTDSCP_RFC Varchar(14),
  NTDSCP_FECEMS Date,
  NTDSCP_SBTTL Numeric(18,4),
  NTDSCP_DSCNT Numeric(18,4),
  NTDSCP_TRSIVA Numeric(18,4),
  NTDSCP_GTOTAL Numeric(18,4),
  NTDSCP_UUID Varchar(36),
  NTDSCP_USU_N Varchar(10),
  NTDSCP_FN Timestamp DEFAULT 'now',
  NTDSCP_USU_M Varchar(10),
  NTDSCP_FM Date,
  CONSTRAINT PK_NTDSCP_1 PRIMARY KEY (NTDSCP_A,NTDSCP_DOC_ID,NTDSCP_ALMPT_ID,NTDSCP_NTDSC_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice'  where RDB$FIELD_NAME = 'NTDSCP_ID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'regsitro activo'  where RDB$FIELD_NAME = 'NTDSCP_A' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del tipo de documento'  where RDB$FIELD_NAME = 'NTDSCP_DOC_ID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del documento de almacen'  where RDB$FIELD_NAME = 'NTDSCP_ALMPT_ID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de notas de descuento (NTDSC)'  where RDB$FIELD_NAME = 'NTDSCP_NTDSC_ID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del pedido de control interno del cliente'  where RDB$FIELD_NAME = 'NTDSCP_PDCLN_ID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio'  where RDB$FIELD_NAME = 'NTDSCP_DRCTR_ID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Tipo de documento Devolucion'  where RDB$FIELD_NAME = 'NTDSCP_TIPO' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de control interno'  where RDB$FIELD_NAME = 'NTDSCP_FOLIO' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie'  where RDB$FIELD_NAME = 'NTDSCP_SERIE' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del sistema'  where RDB$FIELD_NAME = 'NTDSCP_CLV' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.'  where RDB$FIELD_NAME = 'NTDSCP_NOM' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes'  where RDB$FIELD_NAME = 'NTDSCP_RFC' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de emision del comprobante'  where RDB$FIELD_NAME = 'NTDSCP_FECEMS' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal del comprobante'  where RDB$FIELD_NAME = 'NTDSCP_SBTTL' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento'  where RDB$FIELD_NAME = 'NTDSCP_DSCNT' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto trasladado IVA'  where RDB$FIELD_NAME = 'NTDSCP_TRSIVA' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total (subtotal - descuento + traslado de iva)'  where RDB$FIELD_NAME = 'NTDSCP_GTOTAL' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'NTDSCP_UUID' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que crea el registro'  where RDB$FIELD_NAME = 'NTDSCP_USU_N' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'NTDSCP_FN' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'NTDSCP_USU_M' and RDB$RELATION_NAME = 'NTDSCP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de la ultima modificacion del registro'  where RDB$FIELD_NAME = 'NTDSCP_FM' and RDB$RELATION_NAME = 'NTDSCP';
CREATE INDEX IDX_NTDSCP_DRCTR_ID ON NTDSCP (NTDSCP_DRCTR_ID);
CREATE INDEX IDX_NTDSCP_NTDSC_ID ON NTDSCP (NTDSCP_NTDSC_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Nota de Descuento: relacion de documentos'
where RDB$RELATION_NAME = 'NTDSCP';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NTDSCP TO  SYSDBA WITH GRANT OPTION;


CREATE TABLE NTDSC1
(
  NTDSC_ID Integer DEFAULT 0 NOT NULL,
  NTDSC_A Smallint DEFAULT 1 NOT NULL,
  NTDSC_STTS_ID Smallint DEFAULT 1 NOT NULL,
  NTDSC_FOLIO Integer DEFAULT 0 NOT NULL,
  NTDSC_DRCTR_ID Integer DEFAULT 1 NOT NULL,
  NTDSC_CLV Varchar(10),
  NTDSC_NOM Varchar(255),
  NTDSC_RFC Varchar(14),
  NTDSC_CNTCT Varchar(100),
  NTDSC_REF Varchar(100),
  NTDSC_IMPORTE Numeric(18,4) DEFAULT 0,
  NTDSC_SBTTL Numeric(18,4),
  NTDSC_DSCNT Numeric(18,4),
  NTDSC_TRSIVA Numeric(18,4),
  NTDSC_GTOTAL Numeric(18,4),
  NTDSC_UUID Varchar(36),
  NTDSC_OBSRV Varchar(255),
  NTDSC_CTMTV_ID Char(1),
  NTDSC_CTMTV Varchar(100),
  NTDSC_USU_C Varchar(10),
  NTDSC_FCCNCL Date,
  NTDSC_USU_N Varchar(10),
  NTDSC_FN Timestamp DEFAULT 'now',
  NTDSC_USU_M Varchar(10),
  NTDSC_FM Date,
  PRIMARY KEY (NTDSC_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'NTDSC_ID' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'NTDSC_A' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status'  where RDB$FIELD_NAME = 'NTDSC_STTS_ID' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de la nota de descuento'  where RDB$FIELD_NAME = 'NTDSC_FOLIO' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del directorio'  where RDB$FIELD_NAME = 'NTDSC_DRCTR_ID' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del sistema'  where RDB$FIELD_NAME = 'NTDSC_CLV' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'denominacion que identifica a la empresa en el trafico mercantil y que sirve para distinguirla de las demas empresas que desarrollan actividades identicas o similares.'  where RDB$FIELD_NAME = 'NTDSC_NOM' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes'  where RDB$FIELD_NAME = 'NTDSC_RFC' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'NTDSC_CNTCT' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'referencia'  where RDB$FIELD_NAME = 'NTDSC_REF' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe de la nota'  where RDB$FIELD_NAME = 'NTDSC_IMPORTE' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal del comprobante'  where RDB$FIELD_NAME = 'NTDSC_SBTTL' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'mporte del descuento'  where RDB$FIELD_NAME = 'NTDSC_DSCNT' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto trasladado IVA'  where RDB$FIELD_NAME = 'NTDSC_TRSIVA' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total (subtotal - descuento + traslado de iva)'  where RDB$FIELD_NAME = 'NTDSC_GTOTAL' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'NTDSC_UUID' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'NTDSC_OBSRV' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de movitos de la nota de descuento'  where RDB$FIELD_NAME = 'NTDSC_CTMTV_ID' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el documento'  where RDB$FIELD_NAME = 'NTDSC_USU_C' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion'  where RDB$FIELD_NAME = 'NTDSC_FCCNCL' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'NTDSC_USU_N' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro y fecha de emision'  where RDB$FIELD_NAME = 'NTDSC_FN' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que crea el registro'  where RDB$FIELD_NAME = 'NTDSC_USU_M' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'NTDSC_FM' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ejercicio'  where RDB$FIELD_NAME = 'NTDSC_ANIO' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'periodo'  where RDB$FIELD_NAME = 'NTDSC_MES' and RDB$RELATION_NAME = 'NTDSC1';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Cobranza: Notas de descuento al cliente'
where RDB$RELATION_NAME = 'NTDSC1';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NTDSC1 TO  SYSDBA WITH GRANT OPTION;


CREATE TABLE NTDSCS
(
  NTDSCS_A Smallint DEFAULT 1 NOT NULL,
  NTDSCS_NTDSC_ID Integer,
  NTDSCS_NTDSC_STTS_ID Integer,
  NTDSCS_NTDSC_STTSA_ID Integer,
  NTDSCS_DRCTR_ID Integer,
  NTDSCS_CVMTV Varchar(100),
  NTDSCS_NOTA Varchar(100),
  NTDSCS_USR_N Varchar(10),
  NTDSCS_FN Timestamp DEFAULT 'Now' NOT NULL
);
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'NTDSCS_A' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con tabla de notas de descuento'  where RDB$FIELD_NAME = 'NTDSCS_NTDSC_ID' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status anterior'  where RDB$FIELD_NAME = 'NTDSCS_NTDSC_STTS_ID' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status autorizado'  where RDB$FIELD_NAME = 'NTDSCS_NTDSC_STTSA_ID' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla del directorio'  where RDB$FIELD_NAME = 'NTDSCS_DRCTR_ID' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del motivo del cambio de status'  where RDB$FIELD_NAME = 'NTDSCS_CVMTV' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'NTDSCS_NOTA' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que autoriza el cambio'  where RDB$FIELD_NAME = 'NTDSCS_USR_N' and RDB$RELATION_NAME = 'NTDSCS';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'NTDSCS_FN' and RDB$RELATION_NAME = 'NTDSCS';
CREATE INDEX IDX_NTDSCS_DRCTR_ID ON NTDSCS (NTDSCS_DRCTR_ID);
CREATE INDEX IDX_NTDSCS_NTDSC_ID ON NTDSCS (NTDSCS_NTDSC_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'notas de descuento: registro de cambios de status'
where RDB$RELATION_NAME = 'NTDSCS';

CREATE TABLE CTMDLX
(
  CTMDLX_CTPRD_ID Integer DEFAULT 0 NOT NULL,
  CTMDLX_CTMDL_ID Integer DEFAULT 0 NOT NULL,
  CTMDLX_CTESPC_ID Integer DEFAULT -1 NOT NULL,
  CTMDLX_TPALM_ID Integer DEFAULT 0 NOT NULL,
  CTMDLX_CTALM_ID Integer DEFAULT 0 NOT NULL,
  CTMDLX_EXT Numeric(11,4) DEFAULT 0,
  CTMDLX_MIN Numeric(11,4) DEFAULT 0,
  CTMDLX_MAX Numeric(11,4) DEFAULT 0,
  CTMDLX_REORD Numeric(11,4) DEFAULT 0
);
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de productos'  where RDB$FIELD_NAME = 'CTMDLX_CTPRD_ID' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de modelos'  where RDB$FIELD_NAME = 'CTMDLX_CTMDL_ID' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de CTESPC_ID'  where RDB$FIELD_NAME = 'CTMDLX_CTESPC_ID' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tipo de almacen'  where RDB$FIELD_NAME = 'CTMDLX_TPALM_ID' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo del almacen al que pertenece'  where RDB$FIELD_NAME = 'CTMDLX_CTALM_ID' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'existencia'  where RDB$FIELD_NAME = 'CTMDLX_EXT' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'minimo'  where RDB$FIELD_NAME = 'CTMDLX_MIN' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'maximo'  where RDB$FIELD_NAME = 'CTMDLX_MAX' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'reorden'  where RDB$FIELD_NAME = 'CTMDLX_REORD' and RDB$RELATION_NAME = 'CTMDLX';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Almacen: tabla de existencias por almacen'
where RDB$RELATION_NAME = 'CTMDLX';

CREATE TABLE CTCAT
(
  CTCAT_ID Integer NOT NULL,
  CTCAT_CTCAT_ID Integer,
  CTCAT_A Smallint DEFAULT 1,
  CTCAT_SEC Integer,
  CTCAT_CLV Varchar(128),
  CTCAT_NOM Varchar(128),
  CTCAT_USR_N Varchar(10),
  CTCAT_FN Timestamp DEFAULT 'now' NOT NULL,
  CONSTRAINT PK_CTCAT_ID PRIMARY KEY (CTCAT_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la categoria'  where RDB$FIELD_NAME = 'CTCAT_ID' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con categorias'  where RDB$FIELD_NAME = 'CTCAT_CTCAT_ID' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTCAT_A' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave'  where RDB$FIELD_NAME = 'CTCAT_CLV' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion de la categoria'  where RDB$FIELD_NAME = 'CTCAT_NOM' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTCAT_USR_N' and RDB$RELATION_NAME = 'CTCAT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'CTCAT_FN' and RDB$RELATION_NAME = 'CTCAT';
CREATE INDEX IDX_CTCAT_CTCAT_ID ON CTCAT (CTCAT_CTCAT_ID);
CREATE INDEX IDX_CTCAT_ID ON CTCAT (CTCAT_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'categorias de bienes, productos y servicios (BPS) '
where RDB$RELATION_NAME = 'CTCAT';

