﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Banco.Entities;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class ComprobantesBuscarForm : RadForm {
        protected IComisionService service;
        protected Domain.Base.ValueObjects.CFDISubTipoEnum subTipo;
        private BindingList<MovimientoBancarioComprobanteDetailModel> _DataSource;
        //private MovimientoConceptoDetailModel currentConcepto;
        private int iddirctorio;

        public void OnAgregar(MovimientoBancarioComprobanteDetailModel e) {
            if (this.AgregarComprobante != null)
                this.AgregarComprobante(this, e);
        }

        public event EventHandler<MovimientoBancarioComprobanteDetailModel> AgregarComprobante;

        public ComprobantesBuscarForm(MovimientoConceptoDetailModel conceptoDetailModel, int idDirectorio) {
            InitializeComponent();
            //this.currentConcepto = conceptoDetailModel;
            this.iddirctorio = idDirectorio;
        }

        private void ComprobantesBuscarForm_Load(object sender, EventArgs e) {
            this.service = new ComisionService();
            if (this.subTipo == Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido) {

            }
        }

        private void TComprobante_Agregar_click(object sender, EventArgs e) {
            if (this.GridData.CurrentRow != null) {
                var seleccionado = this.GridData.CurrentRow.DataBoundItem as MovimientoBancarioComprobanteDetailModel;
                if (seleccionado != null) {
                    this.OnAgregar(seleccionado);
                }
            }
        }

        private void TComprobante_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void TComprobante_Actualizar_Click(object sender, EventArgs e) {
            if (this.workerBuscar.IsBusy == false) {
                this.radWaitingBar1.StartWaiting();
                this.workerBuscar.RunWorkerAsync();
            }
        }

        private void GridData_CellDoubleClick(object sender, GridViewCellEventArgs e) {
            if (this.GridData.CurrentRow != null) {
                this.ToolBarButtonAgregar.PerformClick();
                //this.Close();
            }
        }

        private void WorkerSearch_DoWork(object sender, DoWorkEventArgs e) {
            var result = this.service.GetTo(new Domain.CPLite.Builder.ComisionRemisionQueryBuilder().YearGreaterThan(2022)
                .IdVendedor(this.iddirctorio).IdStatus( Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Cobrado)
                .IdStatusComision(Domain.Almacen.PT.ValueObjects.RemisionComisionStatusEnum.SinRelacion).Build());

            this._DataSource = new BindingList<MovimientoBancarioComprobanteDetailModel>();
            foreach (var item in result) {
                this._DataSource.Add(new MovimientoBancarioComprobanteDetailModel {
                    Version = item.Version,
                    IdComprobante = item.IdRemision,
                    Folio = item.Folio.ToString(),
                    Serie = item.Serie,
                    TipoComprobante = Domain.Banco.ValueObjects.TipoCFDIEnum.Ingreso,
                    SubTipoComprobante = Domain.Base.ValueObjects.CFDISubTipoEnum.Emitido,
                    ReceptorNombre = item.ReceptorNombre,
                    ReceptorRFC = item.ReceptorRFC,
                    EmisorNombre = Aplication.Base.ConfigService.Synapsis.Empresa.RazonSocial,
                    EmisorRFC = Aplication.Base.ConfigService.Synapsis.Empresa.RFC,
                    ClaveMetodoPago = "PPD",
                    ClaveFormaPago = "99",
                    ClaveMoneda = "MXN",
                    NumParcialidad = 0,
                    Total = item.ComisionPorPagar,
                    PorCobrar = item.ComisionPagada,
                    //Acumulado = item.Acumulado,
                    Abono = item.ComisionPagada,
                    Tag = item.ComisionPagada,
                    Estado = "Vigente",
                    TipoDocumento = Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision,
                    Activo = true,
                    FechaEmision = item.FechaEmision,
                    //IdDocumento = "00000000-0000-0000-0000-000000000000".Substring(0, 36 - item.Folio.ToString().Length) + item.Folio.ToString(), // para esta version ya esta implementado el uuid
                    IdDocumento = item.IdDocumento,
                    Status = item.IdStatus.ToString()
                });
                this.workerBuscar.ReportProgress(Convert.ToInt32((this._DataSource.Count * 100) / result.Count));
            }
            e.Result = true;
        }

        private void WorkerSearch_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            this.Text = e.ProgressPercentage.ToString();
        }

        private void WorkerSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.GridData.DataSource = this._DataSource;
            this.radWaitingBar1.StopWaiting();
            this.Text = "Buscar Remisión";
        }
    }
}
