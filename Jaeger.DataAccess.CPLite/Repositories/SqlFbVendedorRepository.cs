﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.DataAccess.FB.Contribuyentes.Repositories;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    /// <summary>
    /// Vendedores
    /// </summary>
    public class SqlFbVendedorRepository : Abstractions.RepositoryMaster<Vendedor2Model>, ISqlVendedorRepository {
        protected ISqlRelacionComercialRepository relacionComercial;

        /// <summary>
        /// Vendedores
        /// </summary>
        public SqlFbVendedorRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
            this.relacionComercial = new SqlFbRelacionComercialRepository(configuracion, user);
        }

        public int Insert(Vendedor2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @""
            };

            //if (ExecuteTransaction(sqlCommand) > 0)
            //    return item.IdRelacion;
            return 0;
        }

        public int Update(Vendedor2Model item) {
            var sqlCommand = new FbCommand {
                CommandText = @""
            };
            return ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @""
            };
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public bool Saveable(IVendedor2DetailModel vendedor) {
            return this.relacionComercial.Saveable((RelacionComercialDetailModel)vendedor) > 0; 
        }

        /// <summary>
        /// metodo no esta disponible para esta operacion
        /// </summary>
        [Obsolete("El metodo no esta disponible para esta operacion")]
        public Vendedor2Model GetById(int id) {
            throw new NotImplementedException();
        }

        public IEnumerable<Vendedor2Model> GetList() {
            return this.GetList<Vendedor2Model>(new List<Conditional>());
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            // la relacion siempre es 4
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID WHERE DRCTRR.DRCTRR_CTREL_ID = 4 @condiciones ORDER BY DRCTR_NOM ASC"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        /// <summary>
        /// actualizar el password para entrar a la pagina web
        /// </summary>
        /// <param name="index">indice</param>
        /// <param name="password">password en md5</param>
        public bool Acceso(int index, string password) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE DRCTR SET DRCTR_PSW = @password WHERE DRCTR_ID =@index"
            };
            sqlCommand.Parameters.AddWithValue("@password", password);
            sqlCommand.Parameters.AddWithValue("@index", index);
            return ExecuteTransaction(sqlCommand) > 0;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            // la relacion siempre es 4
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT DRCTRR.*, DRCTR.* FROM DRCTRR LEFT JOIN DRCTR ON DRCTRR.DRCTRR_DRCTR_ID = DRCTR.DRCTR_ID WHERE DRCTRR.DRCTRR_CTREL_ID = 4 @condiciones ORDER BY DRCTR_NOM ASC"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
