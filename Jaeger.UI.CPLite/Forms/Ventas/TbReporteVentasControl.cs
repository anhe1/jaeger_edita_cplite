﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public partial class TbReporteVentasControl : UserControl {
        protected internal Aplication.Contribuyentes.Contracts.IDirectorioService Service;
        protected internal RadMenuItem GridLayout = new RadMenuItem { Text = "Grid layout" };
        protected internal RadMenuItem LayoutSave = new RadMenuItem { Text = "Guardar" };
        protected internal RadMenuItem LayoutReset = new RadMenuItem { Text = "Reset" };
        protected BackgroundWorker bkWorker = new BackgroundWorker();

        public TbReporteVentasControl() {
            InitializeComponent();
        }

        private void TbVendedorStandarControl_Load(object sender, EventArgs e) {
            this.HostItem1.HostedItem = this.cboVendedores.MultiColumnComboBoxElement;
            this.HostEjercicio.HostedItem = this.Ejercicio.SpinElement;
            this.ToolBar.Enabled = false;
            this.bkWorker.DoWork += this.BkWorker_DoWork;
            this.bkWorker.RunWorkerCompleted += this.BkWorker_RunWorkerCompleted;
            this.Ejercicio.Minimum = 2014;
            this.Ejercicio.Maximum = DateTime.Now.Year;
            this.Ejercicio.Value = DateTime.Now.Year;
            this.GridLayout.Items.Add(this.LayoutSave);
            this.GridLayout.Items.Add(this.LayoutReset);
            this.Herramientas.Items.Add(this.GridLayout);
        }

        public bool ShowEjercicio {
            get { return this.Ejercicio.Visible; }
            set { this.Ejercicio.Visible = value;
                if (this.Ejercicio.Visible) {
                    this.HostEjercicio.Visibility = Telerik.WinControls.ElementVisibility.Visible;
                } else {
                    this.HostEjercicio.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                }
                this.lblEjercicio.Visibility = this.HostEjercicio.Visibility;
            } 
        }

        private void BkWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.ToolBar.Enabled = true;
        }

        private void BkWorker_DoWork(object sender, DoWorkEventArgs e) {
            this.cboVendedores.DataSource = this.Service.GetList<Domain.Contribuyentes.Entities.ContribuyenteModel>(true);
        }

        public virtual void Start() {
            if (!this.bkWorker.IsBusy) {
                this.bkWorker.RunWorkerAsync();
            }
        }
    }
}
