SET TERM ^ ;
CREATE PROCEDURE COBRADO_REMSIONES
AS
declare variable id integer;
DECLARE VARIABLE cobrar$ dom_moneda;
BEGIN
    for 
        SELECT CTLRMS_ID, CTLRMS.CTLRMS_TOTAL$
        FROM CTLRMS
        where CTLRMS_STTSDCS_ID = 4 -- and CTLRMS.CTLRMS_FOLIO = 4238
        
        order by ctlrms_id asc
            into :id, :cobrar$
    do
        begin
            EXECUTE PROCEDURE CBRNZ_REMISION_COBROS1(:id, :cobrar$);
            --suspend;
        end
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE COBRADO_REMSIONES TO  SYSDBA;

