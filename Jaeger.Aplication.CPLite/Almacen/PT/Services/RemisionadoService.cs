﻿using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.Domain.Almacen.PT.Builder;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.Aplication.CPLite.Almacen.PT.Services {
    public partial class RemisionadoService : RemisionService, Aplication.Almacen.PT.Contracts.IRemisionadoService {
        
        public RemisionadoService() : base() {
            this.remisionRepository = new SqlFbRemisionadoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.relacionesRepository = new SqlFbRemisionadoRRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.movimientoAlmacenPT = new SqlFbMovimientoAlmacenRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.statusRepository = new SqlFbRemisionStatusRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public new static IRemisionQueryBuilder Create() {
            return new RemisionQueryBuilder();
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            if (typeof(T1) == typeof(Domain.Almacen.PT.Entities.RemisionReceptorModel))
                return this.remisionRepository.GetListR<T1>(conditionals);
            return this.remisionRepository.GetList<T1>(conditionals);
        }
    }
}
