SET TERM ^ ;
CREATE TRIGGER CTLRMS_STATUS_EN_RUTA FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
declare variable subtotal numeric(18, 4);
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 191120211501
    Purpose: cambio de status 'En Ruta', verificacion para el cambio de status siempre y cuando no este cancelado
------------------------------------------------------------------------------------------------------------- */
    if (new.ctlrms_doc_id = 26 
        and old.ctlrms_sttsdcs_id = 1 
         and new.ctlrms_sttsdcs_id = 2 
          and new.ctlrms_sttsdcs_id <> 0 
           and new.ctlrms_aut_id = 0 ) then begin
           
       select sum (movapt.movapt_sbtt$)
       from movapt where movapt.movapt_a = 1 and movapt.movapt_ctlgrmsn_id = new.ctlrms_id
            into :subtotal;
            
       if (subtotal is null or subtotal = 0) then begin
            exception error_0031;
        end

        if (new.ctlrms_sbttl$ = 0 or new.ctlrms_sbttl$ is null) then begin 
            new.ctlrms_sbttl$ = subtotal;
        end
    end 
END^
SET TERM ; ^
