SET TERM ^ ;
CREATE PROCEDURE FECHA_VENCE_REMISIONES
AS
declare variable id    dom_id;
declare variable fecha dom_fh;
declare variable dias  dom_id;
begin
/*
proposito: este procedimiento actualiza la fecha de vencimiento de las remisiones donde la fecha este como nula
*/ 

    for 
        select ctlrms.ctlrms_id, ctlrms.ctlrms_fcentrg, drctr.drctr_dscrdt
        from ctlrms, drctr
        where ctlrms.ctlrms_drctr_id = drctr.drctr_id and ctlrms.ctlrms_sttsdcs_id = 4 and ctlrms.ctlrms_fchvnc is null
        into :id, :fecha, :dias
    do
        begin
            if ( dias is null or :dias = 0) then
                begin
                    update ctlrms set ctlrms_fchvnc = :fecha where ctlrms_id = :id;
                end
            else
                begin
                    update ctlrms set ctlrms_fchvnc = :fecha + :dias where ctlrms_id = :id;
                end
        end

end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE FECHA_VENCE_REMISIONES TO  SYSDBA;

