﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Aplication.Almacen.PT.Builder;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class RemisionFiscalForm : UI.Almacen.PT.Forms.RemisionFiscalForm {
        protected List<LPrecioModel> lPrecios;
        protected internal IPrecioCatalogoService sPrecios;

        public RemisionFiscalForm() : base() { }

        public RemisionFiscalForm(IRemisionService service, bool showValues = false) : base(service, showValues) {
            this.Load += this.RemisionFiscalForm_Load;
        }

        public RemisionFiscalForm(IRemisionService service, RemisionDetailModel model, bool showValues = false) : base(service, model, showValues) {
            this.Load += this.RemisionFiscalForm_Load;
        }

        private void RemisionFiscalForm_Load(object sender, EventArgs e) {
            this.Text = "Almacén PT: Remisión a Cliente";
            this.TRemision.Receptor.Service = new DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
            this.TRemision.Receptor.Seleccionado += this.Receptor_Seleccionado;
            this.sPrecios = new Aplication.CPLite.Tienda.Services.PrecioCatalogoService();
        }

        private void Receptor_Seleccionado(object sender, Domain.Contribuyentes.Entities.ContribuyenteDetailModel e) {
            if (e != null) {
                this.lPrecios = this.sPrecios.GetPrecio(e.IdDirectorio);
                if (this.TRemision.Comprobante.Conceptos != null) {
                    if (this.TRemision.Comprobante.Conceptos.Count > 0) {
                        using(var espera = new Waiting1Form(this.TVerificar)) {
                            espera.Text = "un momento por favor";
                            espera.ShowDialog(this);
                        }
                    }
                }
            }
        }
        
        public override void TConcepto_Productos_Click(object sender, EventArgs e) {
            if (this.TRemision.Comprobante.IdCliente == 0) {
                RadMessageBox.Show(this, "Es necesario primero seleccionar un cliente", "Atención", MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var _catalogoProductos = new ProductoModeloBuscarForm(new Aplication.CPLite.Almacen.Services.CatalogoProductoService(Domain.Base.ValueObjects.AlmacenEnum.PT))) {
                _catalogoProductos.StartPosition = FormStartPosition.CenterParent;
                _catalogoProductos.Selected += this.TConceptos_Agregar_Producto;
                _catalogoProductos.ShowDialog(this);
            }
        }

        /// <summary>
        /// agregra producto seleccionado
        /// </summary>
        public virtual void TConceptos_Agregar_Producto(object sender, ProductoXModelo e) {
            if (this.TRemision.Comprobante.Conceptos.Count == 0) {
                this.TRemision.Comprobante.Conceptos = new BindingList<RemisionConceptoDetailModel>();
                this.GridConceptos.DataSource = this.TRemision.Comprobante.Conceptos;
                this.GridConceptoParte.DataSource = this.TRemision.Comprobante.Conceptos;
                //this.GridConceptoParte.DataMember = "Parte";
            }

            if (e != null) {
                // sino esta autorizado
                if (e.IdAutorizado == 0) {
                    RadMessageBox.Show(this, "El modelo seleccionado no esta autorizado.", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }

                if (e.Unitario == 0 && e.IdEspecificacion == 0) {
                    RadMessageBox.Show(this, "El modelo seleccionado no tiene definido precio unitario o variante.", "Atención!", MessageBoxButtons.OK, RadMessageIcon.Info);
                    return;
                }
                
                var nuevo = RemisionBuilder.Create(e);

                if (this.SinDuplicados.IsChecked == true) {
                    var existe = this.TRemision.Comprobante.Conceptos.Where(it => it.Identificador == nuevo.Identificador);
                    if (existe.Count() > 0) {
                        RadMessageBox.Show(this, "El producto o modelo ya se encuentra en la lista actual.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        return;
                    }
                }
                // en caso de que exita un valor unitario
                if (e.Unitario > 0) {
                    nuevo.IdPrecio = 0;
                    nuevo.ValorUnitario = e.Unitario;
                    nuevo.CostoUnitario = 0;
                    this.TRemision.Comprobante.Conceptos.Add((RemisionConceptoDetailModel)nuevo);
                } else {
                    try {
                        var precio = this.AsignarUnitario(e.IdEspecificacion);
                        if (precio != null) {
                            nuevo.IdPrecio = precio.IdPrecio;
                            nuevo.ValorUnitario = precio.Unitario;
                            nuevo.CostoUnitario = precio.Costo;
                        }
                        this.TRemision.Comprobante.Conceptos.Add((RemisionConceptoDetailModel)nuevo);
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, "No existe precio asignado a este producto, informe el error al administrador.", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
        }

        private LPrecioModel AsignarUnitario(int idEspecificacion) {
            try {
                var precio = lPrecios.Where(it => it.IdEspecificacion == idEspecificacion).FirstOrDefault();
                return precio;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public virtual void TVerificar() {
            if (this.TRemision.Comprobante.Conceptos != null) {
                if (lPrecios != null) {
                    for (int i = 0; i < this.TRemision.Comprobante.Conceptos.Count; i++) {
                        try {
                            // si la especificacion es -1 entonces el precio es directamente del modelo
                            if (this.TRemision.Comprobante.Conceptos[i].IdEspecificacion != -1) {
                                var precio = this.AsignarUnitario(this.TRemision.Comprobante.Conceptos[i].IdEspecificacion);
                                if (precio != null) {
                                    if (precio.Unitario.Equals(this.TRemision.Comprobante.Conceptos[i].ValorUnitario)) {
                                        Console.WriteLine("Correcto");
                                    } else {
                                        this.TRemision.Comprobante.Conceptos[i].ValorUnitario = precio.Unitario;
                                        Console.WriteLine("Error es diferente");
                                    }
                                }
                            }
                        } catch (Exception) {

                            throw;
                        }
                    }
                }
            }
        }
    }
}
