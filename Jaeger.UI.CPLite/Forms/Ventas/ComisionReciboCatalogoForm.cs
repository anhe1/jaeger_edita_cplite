﻿using System;
using System.Linq;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Banco.ValueObjects;
using Telerik.WinControls.UI;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    public class ComisionReciboCatalogoForm : UI.Banco.Forms.MovimientosForm {
        protected internal RadMenuItem NuevoRecibo = new RadMenuItem { Text = "Recibo", Name = "NuevoRecibo", ToolTipText = "Nuevo recibo de cobro" };
        public ComisionReciboCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(MovimientoBancarioEfectoEnum.Egreso) {
            this.Load += ComisionReciboCatalogoForm_Load;
        }

        private void ComisionReciboCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Vendedores: Recibos de Comisiones";
            this.TMovimiento.Nuevo.Items.Add(this.NuevoRecibo);
            this._Service = new Aplication.CPLite.Services.BancoService();
            this.NuevoRecibo.Click += Nuevo_Click;
        }

        public virtual void Nuevo_Click(object sender, EventArgs e) {
            using (var nuevo = new ComisionReciboForm(_Service, null)) {
                nuevo.ShowDialog(this);
            }
        }

        public override void Consultar() {
            var query = Aplication.Banco.BancoService.Query().Year(this.TMovimiento.GetEjercicio()).Month(this.TMovimiento.GetMes()).IdConcepto(7).WithEfecto(this.efecto).Build();
            var data = this._Service.GetList<MovimientoBancarioDetailModel>(query).ToList<IMovimientoBancarioDetailModel>();
            this.Movimientos.Data = new System.ComponentModel.BindingList<IMovimientoBancarioDetailModel>(data);
        }
    }
}
