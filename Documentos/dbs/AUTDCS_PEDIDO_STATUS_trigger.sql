SET TERM ^ ;
CREATE TRIGGER AUTDCS_PEDIDO_STATUS FOR AUTDCS ACTIVE
AFTER INSERT OR UPDATE POSITION 0
AS 
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: anhe 07122021 1834
    Purpose: CAMBIO DE STATUS
------------------------------------------------------------------------------------------------------------- */
    if ( new.autdcs_doc_id = 33 ) then
        begin
            if ( new.autdcs_ctlalmpt0_id is not null  ) then
                begin
                    if (new.autdcs_sttsaut_id = 0) then
                        begin
                            update pddcln set pddcln.pddcln_sttsdcs_id=new.autdcs_sttsaut_id, 
                                pddcln.pddcln_usu_cncl=new.autdcs_usu_n, pddcln.pddcln_fccncl=new.autdcs_fn
                                    where pddcln.pddcln_id=new.autdcs_ctlalmpt0_id ;
                        end 
                    if (new.autdcs_sttsaut_id > 0) then
                        begin
                            update pddcln set pddcln.pddcln_sttsdcs_id=new.autdcs_sttsaut_id
                                where pddcln.pddcln_id=new.autdcs_ctlalmpt0_id ;
                        end
                end
            else
                begin
                    if ( new.autdcs_ctlalmpt0_id is not null and new.autdcs_ctlaplccn_id = -1 ) then
                        begin
                            update pddcln set pddcln.pddcln_fecaco=new.autdcs_fcemsn
                                where pddcln.pddcln_id=new.autdcs_ctlalmpt0_id ;
                        end
                end
        end 
END^
SET TERM ; ^
