﻿using System.Linq;
using System.Collections.Generic;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.Aplication.CPLite.Alpha.Services {
    public class ProfileService : Kaiju.Services.ProfileService, Kaiju.Contracts.IProfileService {
        public ProfileService() : base() { }

        public override List<UIMenuElement> GetMenus() {
            return this._MenuRepository.GetCPLite().ToList();
        }
    }
}
