﻿using System;
using System.Linq;
using System.Collections.Generic;
using Telerik.WinControls.UI;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Aplication.CPLite.Ventas.Services;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Tienda.Entities;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class PedidoCatalogoForm : UI.Tienda.Forms.PedidoCatalogoForm {
        protected internal IVendedoresService vendedorService;
        protected internal Aplication.Almacen.PT.Contracts.IRemisionadoService _Remisionado;
        protected internal GridViewTemplate GridRemisionado = new GridViewTemplate() { Caption = "Remisionado" };
        protected internal RadMenuItem menuContextualAutorizar = new RadMenuItem { Text = "Autorizar" };
        protected internal List<Domain.Almacen.PT.Entities.RemisionSingleModel> _Remisiones;

        public PedidoCatalogoForm(UIMenuElement menuElement) : base() {
            this.TPedido.Permisos = new UIAction(menuElement.Permisos);
            this.Load += this.PedidoCatalogoForm_Load;
        }

        public virtual void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Cliente: Pedidos";
            this.TPedido.Service = new Aplication.CPLite.Tienda.Services.PedidosService();    
            this.vendedorService = new VendedoresService();
            this.TPedido.menuContextual.Items.Add(this.menuContextualAutorizar);
            this.TPedido.Nuevo.Enabled = this.TPedido.Permisos.Agregar;
            this.TPedido.Cancelar.Enabled = this.TPedido.Permisos.Cancelar;
            this.TPedido.Editar.Enabled = this.TPedido.Permisos.Editar;
            this.TPedido.ShowExportarExcel = this.TPedido.Permisos.Exportar;
            this.menuContextualAutorizar.Enabled = this.TPedido.Permisos.Autorizar;
            this.GridRemisionado.Standard();
            this.GridRemisionado.Columns.AddRange(new UI.Almacen.PT.Builder.RemisionadoGridBuilder().Templetes().Simple().Build());
            this.TPedido.GridData.MasterTemplate.Templates.Add(this.GridRemisionado);
            this.GridRemisionado.HierarchyDataProvider = new GridViewEventDataProvider(this.GridRemisionado);
            this.TPedido.GridData.RowSourceNeeded += this.GridData_RowSourceNeeded;
            this._Remisionado = new Aplication.CPLite.Almacen.PT.Services.RemisionadoService();
        }

        public virtual void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.GridRemisionado.Caption) {
                using (var espera = new Waiting1Form(this.Consulta)) {
                    espera.Text = "Consultando partidas ...";
                    espera.ShowDialog(this);
                }
                if (this._Remisiones != null) {
                    foreach (var remision in this._Remisiones) {
                        var row = e.Template.Rows.NewRow();
                        UI.Almacen.PT.Builder.RemisionadoGridBuilder.ConvertTo(row, remision);
                        e.SourceCollection.Add(row);
                    }
                }
            }
        }

        public override void Vendedores() {
            this.TPedido.Vendedores = this.vendedorService.GetList(false).ToList();
        }

        private void Consulta() {
            var seleccionado = this.TPedido.GridData.CurrentRow.DataBoundItem as PedidoClienteDetailModel;
            var q0 = new Domain.Almacen.PT.Builder.RemisionQueryBuilder().IdPedido(seleccionado.IdPedido).Build();
            this._Remisiones = this._Remisionado.GetList<Domain.Almacen.PT.Entities.RemisionSingleModel>(q0).ToList();
        }
    }
}
