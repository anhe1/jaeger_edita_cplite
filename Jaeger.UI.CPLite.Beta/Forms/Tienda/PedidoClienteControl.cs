﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Contribuyentes.Entities;

namespace Jaeger.UI.CPLite.Forms {
    public partial class PedidoClienteControl : UserControl {
        protected Aplication.Contribuyentes.Contracts.IDirectorioService service;
        private BindingList<ContribuyenteDetailModel> contribuyenteDetailModels;

        #region eventos
        public event EventHandler<EventArgs> BindingCompleted;
        public event EventHandler<EventArgs> ButtonCerrar_Click;

        public void OnButtonCerrarClick(object sender, EventArgs e) {
            if (this.ButtonCerrar_Click != null) {
                this.ButtonCerrar_Click(sender, e);
            }
        }

        public void OnBindingClompleted(EventArgs e) {
            if (this.BindingCompleted != null)
                this.BindingCompleted(this, e);
        }
        #endregion

        public void CreateService() {
            this.service = new DirectorioService(Domain.Base.ValueObjects.TipoRelacionComericalEnum.Cliente);
            this.Cliente.Enabled = false;
            this.backClientes.RunWorkerAsync();
        }

        public PedidoClienteControl() {
            InitializeComponent();
        }

        private void PedidoClienteControl_Load(object sender, EventArgs e) {
            this.Embarque.DisplayMember = "Completo";
            this.Embarque.ValueMember = "IdDrccn";

            this.Vendedor.DisplayMember = "ClaveVendedor";
            this.Vendedor.ValueMember = "IdVendedor";

            this.Status.DisplayMember = "Descripcion";
            this.Status.ValueMember = "Id";
        }

        private void BackClientes_DoWork(object sender, DoWorkEventArgs e) {
            this.contribuyenteDetailModels = new BindingList<ContribuyenteDetailModel>(this.service.GetList<ContribuyenteDetailModel>(true));
        }

        private void BackClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Cliente.DataSource = this.contribuyenteDetailModels;
            this.Cliente.Enabled = true;
            this.OnBindingClompleted(e);
        }

        private void CboPersona_SelectedIndexChanged(object sender, EventArgs e) {
            var t = this.Cliente.SelectedItem as GridViewRowInfo;
            if (t != null) {
                var s = t.DataBoundItem as ContribuyenteDetailModel;
                if (s != null) {
                    this.Embarque.DataSource = s.Domicilios;
                    this.Vendedor.DataSource = s.Vendedores;
                    this.Clave.Text = s.Clave;
                }
            }
        }

        private void Buscar_Click(object sender, EventArgs e) {
            var _buscar = new Clientes.ClienteBuscarForm();
            _buscar.Selected += Buscar_Selected;
            _buscar.ShowDialog(this);
        }

        private void Buscar_Selected(object sender, ContribuyenteDetailModel e) {
            if (e != null) {
                this.Cliente.Text = e.Nombre;
            }
        }
    }
}
