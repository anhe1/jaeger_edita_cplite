﻿using System;
using System.Linq;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.CPLite.Ventas.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    public class PedidoCatalogoForm : UI.Tienda.Forms.PedidoCatalogoForm {
        protected internal IVendedoresService vendedorService;

        public PedidoCatalogoForm(UIMenuElement menuElement) : base() {
            this.TPedido.Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += this.PedidoCatalogoForm_Load;
        }

        public virtual void PedidoCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Ventas: Pedidos";
            this.TPedido.Service = new Aplication.CPLite.Tienda.Services.PedidosService();
            this.vendedorService = new VendedoresService();

            this.TPedido.Nuevo.Enabled = this.TPedido.Permisos.Agregar;
            this.TPedido.Cancelar.Enabled = this.TPedido.Permisos.Cancelar;
            this.TPedido.Editar.Enabled = this.TPedido.Permisos.Editar;
            this.TPedido.ShowExportarExcel = this.TPedido.Permisos.Exportar;
        }

        public override void Vendedores() {
            this.TPedido.Vendedores = this.vendedorService.GetList(false).ToList();
        }
    }
}
