/* **********************************************************************************************************************************
+ PROCEDIMIENTO ALMACENADO VERSION FIREBIRD PARA CALCULO POR COBRAR DE REMISIONADO AL CLIENTE
*********************************************************************************************************************************** */
SET TERM ^ ;
CREATE PROCEDURE PORCOBRAR
RETURNS (
    ctlrms_id Integer,
    ctlrms_a Smallint,
    ctlrms_drctr_id Integer,
    ctlrms_ctlgvddr_id Integer,
    ctlrms_catprd_id Integer,
    ctlrms_folio Integer,
    ctlrms_pddclnt_id Integer,
    ctlrms_fcemsn Timestamp,
    ctlrms_fcentrg Timestamp,
    ctlrms_fchvnc Timestamp,
    ctlrms_fcultpg Timestamp,
    DIASEMISION Integer,
    DIASENTREGA Integer,
    DIASULTCOBRO Integer,
    CATALOGO Varchar(100),
    CLAVECLIENTE Varchar(10),
    CLIENTE Varchar(200),
    LIMITECREDITO Numeric(18,4),
    TASAIVATRASLADOPACTADO Numeric(18,4),
    FACTORPACTADO Numeric(18,4),
    DIASCREDITO Integer,
    CATDESCUENTO Varchar(150),
    DESCUENTO Varchar(200),
    SUBTOTAL Numeric(18,4),
    FACTORDESCUENTO Numeric(18,4),
    TASAIVATRASLADO Numeric(18,4),
    IVATRASLADO Numeric(18,4),
    PORCOBRAR Numeric(18,4),
    DESCUENTOIMPORTE Numeric(18,4),
    ACUMULADO Numeric(18,4),
    SALDO Numeric(18,4),
    VENDEDOR Varchar(10),
    ctlrms_usu_n Varchar(10),
    ctlrms_fn Timestamp )
AS
declare variable factorTemporal numeric(18, 4);
declare variable almacenar smallint;
begin 
/* -------------------------------------------------------------------------------------------------------------
    Develop: anhe 09112021 2143
    Purpose: procedimiento para el calculo de remisiones por cobrar, la politica actual para cobranza indica
    que el cliente es acreedor al descuento siempre y cuando no supere sus dias de credito tomando en cuenta
    la ctlrms_id de entrega de la remision
    almacenar = 1; actualizara los registros de las remisiones
------------------------------------------------------------------------------------------------------------- */
    almacenar = 0;
    for
        select
            rem.ctlrms_id ctlrms_id,
            rem.ctlrms_a ctlrms_a,
            rem.ctlrms_drctr_id ctlrms_drctr_id,
            rem.ctlrms_ctlgvddr_id ctlrms_ctlgvddr_id,
            rem.ctlrms_catprd_id ctlrms_catprd_id,
            rem.ctlrms_folio ctlrms_folio,
            rem.ctlrms_pddclnt_id ctlrms_pddclnt_id,
            rem.ctlrms_fcemsn ctlrms_fcemsn,
            rem.ctlrms_fcentrg ctlrms_fcentrg,
            rem.ctlrms_fchvnc ctlrms_fchvnc,
            rem.ctlrms_fcultpg ctlrms_fcultpg,
            ceil ( current_timestamp - rem.ctlrms_fcemsn ) DiasEmision,
            ceil ( current_timestamp - rem.ctlrms_fcentrg ) DiasEntrega,
            ( case when ceil ( current_timestamp - rem.ctlrms_fcultpg ) > 0 then ceil ( current_timestamp - rem.ctlrms_fcultpg ) else 0 end ) DiasUltCobro,
            ctl.ctlprdct_cls1 Catalogo,
            dir.drctr_clv ClaveCliente,
            dir.drctr_nom Cliente,
            dir.drctr_lmtcrdt$ LimiteCredito,
            (case when dir.drctr_ffctr is null then 0 else dir.drctr_ffctr end) TasaIvaTrasladoPactado,
            (case when dir.drctr_factor is null then 0 else dir.drctr_factor end) FactorPactado,
            (case when dir.drctr_dscrdt is null then 0 else dir.drctr_dscrdt end) DiasCredito,
            rem.ctlrms_ctldsc CatDescuento,
            rem.ctlrms_ddscn Descuento,
            rem.ctlrms_sbttl$ SubTotal,
            rem.ctlrms_dscnt FactorDescuento,
            rem.ctlrms_impst TasaIvaTraslado,
            rem.ctlrms_impst$ IvaTraslado,
            rem.ctlrms_total$ PorCobrar,
            rem.ctlrms_dscnt$ DescuentoImporte,
            rem.ctlrms_cbrd$ Acumulado,
            rem.ctlrms_sld$ Saldo,
            vnddr.drctr_clv Vendedor,
            rem.ctlrms_usu_n ctlrms_usu_n,
            rem.ctlrms_fn ctlrms_fn
        from
            ctlrms rem
        left join ctlprdct ctl on rem.ctlrms_catprd_id = ctl.ctlprdct_id
        left join drctr dir on rem.ctlrms_drctr_id = dir.drctr_id
        left join drctr vnddr on rem.CTLRMS_ctlgvddr_id = vnddr.drctr_id
        where rem.ctlrms_sttsdcs_id = 4 
        order by rem.ctlrms_id desc
        into
            :ctlrms_id,
            :ctlrms_a,
            :ctlrms_drctr_id,
            :ctlrms_ctlgvddr_id,
            :ctlrms_catprd_id,
            :ctlrms_folio,
            :ctlrms_pddclnt_id,
            :ctlrms_fcemsn,
            :FECENTREGA,
            :FECVENCE,
            :ULTCOBRO,
            :DIASEMISION,
            :DIASENTREGA,
            :DIASULTCOBRO,
            :CATALOGO,
            :CLAVECLIENTE,
            :CLIENTE,
            :LIMITECREDITO,
            :TASAIVATRASLADOPACTADO,
            :FACTORPACTADO,
            :DIASCREDITO,
            :CATDESCUENTO,
            :DESCUENTO,
            :SUBTOTAL,
            :FACTORDESCUENTO,
            :TASAIVATRASLADO,
            :IVATRASLADO,
            :PORCOBRAR,
            :DESCUENTOIMPORTE,
            :ACUMULADO,
            :SALDO,
            :VENDEDOR,
            :ctlrms_usu_n,
            :ctlrms_fn
    do
        begin
            
            /* si los dias de entrega no superan los dias de credito y existe factor pactado entonces 
            descuento autorizado por el factor pactado*/
           if (DiasCredito >= DiasEntrega and FactorPactado > 0 and ctlrms_fcemsn >= '05/01/2013') then
               begin
                    CatDescuento = 'Factor Pactado';
                    Descuento = 'Autorizado';
                    FactorDescuento = FactorPactado;
               end
           else
                begin
                    /* en caso contrario entonces */
                    CatDescuento = 'Sin descuento';
                    Descuento = 'Sin descuento';
                    FactorDescuento = 0;
                end
            /* calcular los totales */
            
            if (FactorDescuento = 0) then
                begin
                    factorTemporal = 0;
                end
            else 
                begin 
                    factorTemporal = (1 - FactorDescuento);
                end 

            TasaIvaTraslado = TasaIvaTrasladoPactado;
            DescuentoImporte = SubTotal * factorTemporal;
            IvaTraslado = (SubTotal - DescuentoImporte) * TasaIvaTraslado;
            PorCobrar = ((SubTotal - DescuentoImporte) + IvaTraslado) - Acumulado;
            Saldo = PorCobrar - Acumulado;
            
            /* por ultimo actualizar la tabla de remisiones */
            if (almacenar = 1) then
                begin
                    update ctlrms set
                        ctlrms_catdsc_id = -1,
                        ctlrms_fccal     = current_date,
                        ctlrms_ctldsc    = :CatDescuento,
                        ctlrms_ddscn     = :Descuento,
                        ctlrms_dscnt     = :FactorDescuento,
                        ctlrms_impst     = :TasaIvaTraslado,
                        ctlrms_impst$    = :IvaTraslado,
                        ctlrms_total$    = :PorCobrar,
                        ctlrms_dda$      = :Saldo
                        where ctlrms_id = :IdRemision;
                    end
            suspend;
        end
        
end^
SET TERM ; ^

UPDATE RDB$PROCEDURES set
  RDB$DESCRIPTION = 'Procedimiento para el calculo de remisiones por cobrar'
  where RDB$PROCEDURE_NAME = 'PORCOBRAR';
GRANT EXECUTE
 ON PROCEDURE PORCOBRAR TO  SYSDBA;

