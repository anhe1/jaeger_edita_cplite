SET TERM ^ ;
CREATE TRIGGER CTLRMS_STATUS_ENTREGADO FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
declare variable dias integer;
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE 191120211255
    Purpose: cambios de status 'Entregado'
    Para obtener el cambios de status se considera:
    1. tipo de documento 26
    2. fecha de entrega valida mayor o igual a la fecha de emision
    3. el status anterior debe ser 'En Ruta' (2)
    4. el status actual no debe ser 'Cancelado'
    Por ultimo se crea la fecha de vencimiento
------------------------------------------------------------------------------------------------------------- */
	if (new.ctlrms_doc_id = 26 
        and new.ctlrms_fcentrg is not null 
         and new.ctlrms_sttsdcs_id = 2 
          and new.ctlrms_sttsdcs_id <> 0 
           and new.ctlrms_aut_id = 0) then
        begin
            -- cambio de status a 'Entregado' (3) siempre y cuando se tenga el nombre de quien recibe
            new.ctlrms_sttsdcs_id = 3;
            if (new.ctlrms_cntt is null or new.ctlrms_cntt = '') then
                begin
                    exception error_0032;
                end
            
            if (new.ctlrms_fcentrg < new.CTLRMS_FCEMSN) then
                begin
                    exception error_0036;
                end
            -- se calcula la fecha de vencimiento a partir de la fecha de entrega al cliente mas los dias de credito
            select (case when drctr.drctr_dscrdt is null then 0 else drctr.drctr_dscrdt end)
            from drctr where drctr.drctr_id = new.ctlrms_drctr_id
            into :dias;
            
            new.ctlrms_fchvnc = dateadd(:dias day to new.ctlrms_fcentrg);
            if (new.ctlrms_pddclnt_id is not null and new.ctlrms_pddclnt_id > 0) then
                begin
                    -- actualizamos el status del pedido 
                    update pddcln set pddcln_sttsdcs_id=4 where pddcln_id=new.ctlrms_pddclnt_id;
                 end

        end 
END^
SET TERM ; ^
