﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.Banco.Repositories;

namespace Jaeger.Aplication.CPLite.Banco.Services {
    public class BancoMovientosSearchService : Aplication.Banco.BancoMovientosSearchService, Aplication.Banco.IBancoMovientosSearchService {
        public BancoMovientosSearchService() : base() { }

        public override void OnLoad() {
            this.movimientoBancarioRepository = new SqlFbMovimientoBancarioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
