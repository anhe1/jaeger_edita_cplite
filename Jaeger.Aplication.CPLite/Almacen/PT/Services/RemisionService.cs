﻿using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Contribuyentes.Contracts;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Domain.Tienda.Contracts;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.DataAccess.FB.Contribuyentes.Repositories;

namespace Jaeger.Aplication.CPLite.Almacen.PT.Services {
    public class RemisionService : Aplication.Almacen.PT.Services.RemisionService, Aplication.Almacen.PT.Contracts.IRemisionService {
        protected ISqlPrecioRepository precioRepository;
        protected ISqlContribuyenteRepository directorioRepository;

        public RemisionService() : base() { }

        public override void OnLoad() {
            this.remisionRepository = new SqlFbRemisionadoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.movimientoAlmacenPT = new SqlFbMovimientoAlmacenRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.relacionesRepository = new SqlFbRemisionadoRRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.precioRepository = new SqlFbPrecioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.directorioRepository = new SqlFbContribuyenteRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.statusRepository = new SqlFbRemisionStatusRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._Configuration = Configuration().Default().Build();
            this._Configuration.ShowPagare = true;
        }

        /// <summary>
        /// catalogo de precios pos cliente
        /// </summary>
        /// <param name="index">indice del cliente</param>
        public virtual List<LPrecioModel> GetPrecio(int index) {
            return this.precioRepository.GetList(index, 0).ToList();
        }

        public override int DiasCredito(int idCliente) {
            var d = this.directorioRepository.GetById<ContribuyenteModel>(idCliente);
            if (d != null) {
                return d.DiasCredito;
            }
            return 0;
        }

        /// <summary>
        /// verificar la informacion de los conceptos con el receptor y la lista de precios
        /// </summary>
        public override IRemisionDetailModel Checked(IRemisionDetailModel remision) {
            if (remision.Conceptos != null) {
                if (remision.Conceptos.Count() > 0) {
                    var lista = this.precioRepository.GetList(remision.IdCliente);
                    if (lista != null) {
                        for (int i = 0; i < remision.Conceptos.Count; i++) {
                            try {
                                // si la especificacion es -1 entonces el precio es directamente del modelo
                                if (remision.Conceptos[i].IdEspecificacion != -1) {
                                    var precio = lista.Where(it => it.IdEspecificacion == remision.Conceptos[i].IdEspecificacion).FirstOrDefault();
                                    if (precio != null) {
                                        if (!((precio.Unitario * remision.Conceptos[i].UnidadFactor) == remision.Conceptos[i].Unitario)) {
                                            remision.Conceptos[i].Unitario = precio.Unitario;
                                        }
                                    }
                                }
                            } catch (System.Exception ex) {

                            }
                        }
                    }
                }
            }
            return remision;
        }
    }
}
