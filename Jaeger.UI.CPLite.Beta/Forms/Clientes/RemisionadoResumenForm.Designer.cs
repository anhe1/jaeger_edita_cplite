﻿namespace Jaeger.UI.CPLite.Forms.Clientes {
    partial class RemisionadoResumenForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TResumen = new Jaeger.UI.Common.Forms.PivotStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TResumen
            // 
            this.TResumen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TResumen.Location = new System.Drawing.Point(0, 0);
            this.TResumen.Name = "TResumen";
            this.TResumen.Size = new System.Drawing.Size(800, 450);
            this.TResumen.TabIndex = 0;
            // 
            // RemisionadoResumenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TResumen);
            this.Name = "RemisionadoResumenForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "RemisionadoResumenForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RemisionadoResumenForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.PivotStandarControl TResumen;
    }
}