﻿using System;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Almacen.PT {
    public class UnidadCatalogoForm : UI.Almacen.Forms.UnidadCatalogoForm {
        
        public UnidadCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.UnidadCatalogoForm_Load;
        }

        private void UnidadCatalogoForm_Load(object sender, EventArgs e) {
            this.Service = new UnidadService();
        }
    }
}
