CREATE VIEW VWCMSRMS (VWCMSRMS_ID, VWCMSRMS_A, VWCMSRMS_DRCTR_ID, VWCMSRMS_VNDDR_ID, VWCMSRMS_CATPRD_ID, VWCMSRMS_CATCMS_ID, VWCMSRMS_FOLIO, VWCMSRMS_FCEMSN, VWCMSRMS_FCENTRG, VWCMSRMS_FCHVNC, VWCMSRMS_FCULTPG, VWCMSRMS_D1EMS, VWCMSRMS_D2ENT, VWCMSRMS_D3VNC, VWCMSRMS_D4PGO, VWCMSRMS_CTLG, VWCMSRMS_VNDDR, VWCMSRMS_CLV, VWCMSRMS_CLIENTE, VWCMSRMS_CLTDSC, VWCMSRMS_DDSCN, VWCMSRMS_SBTTL$, VWCMSRMS_DSCNT, VWCMSRMS_IMPST, VWCMSRMS_IMPST$, VWCMSRMS_TOTAL$, VWCMSRMS_DSCNT$, VWCMSRMS_CBRD$, VWCMSRMS_SLD$, VWCMSRMS_DIV, VWCMSRMS_DIV2, VWCMSRMS_CMSN, VWCMSRMS_CMSN$, VWCMSRMS_CMSP$, VWCMSRMS_CMSB$)
AS                      
        select
            rem.ctlrms_id 			vwcmsrms_id,
            rem.ctlrms_a  			vwcmsrms_a,
            rem.ctlrms_drctr_id 	vwcmsrms_drctr_id,
            rem.ctlrms_ctlgvddr_id 	vwcmsrms_vnddr_id,
            rem.ctlrms_catprd_id 	vwcmsrms_catprd_id,
            rem.ctlrms_catcms_id    vwcmsrms_catcms_id,
            rem.ctlrms_folio 		vwcmsrms_folio,
            rem.ctlrms_fcemsn 		vwcmsrms_fcemsn,
            rem.ctlrms_fcentrg 		vwcmsrms_fcentrg,
            rem.ctlrms_fchvnc 		vwcmsrms_fchvnc,
            rem.ctlrms_fcultpg 		vwcmsrms_fcultpg,
            ceil ( current_timestamp - rem.ctlrms_fcemsn ) 	vwcmsrms_d1ems,
            ceil ( current_timestamp - rem.ctlrms_fcentrg ) vwcmsrms_d2ent,
            ceil ( current_timestamp - rem.ctlrms_fchvnc ) 	vwcmsrms_d3vnc,
            ( case when ceil ( rem.ctlrms_fcultpg - rem.ctlrms_fcentrg ) > 0 then ceil ( rem.ctlrms_fcultpg - rem.ctlrms_fcentrg ) else 0 end ) vwcmsrms_d4pgo,
            ctl.ctlprdct_cls1 		vwcmsrms_ctlg,
            vdd.vwvnddr_clv 		vwcmsrms_vnddr,
            dir.vwalldrctr_clv 		vwcmsrms_clv,
            dir.vwalldrctr_nom 		vwcmsrms_cliente,
            rem.ctlrms_ctldsc 		vwcmsrms_cltdsc,
            rem.ctlrms_ddscn 		vwcmsrms_ddscn,
            rem.ctlrms_sbttl$ 		vwcmsrms_sbttl$,
            rem.ctlrms_dscnt 		vwcmsrms_dscnt,
            rem.ctlrms_impst 		vwcmsrms_impst,
            rem.ctlrms_impst$ 		vwcmsrms_impst$,
            rem.ctlrms_total$ 		vwcmsrms_total$,
            rem.ctlrms_dscnt$ 		vwcmsrms_dscnt$,
            rem.ctlrms_cbrd$ 		vwcmsrms_cbrd$,
            rem.ctlrms_sld$ 		vwcmsrms_sld$,
            rem.ctlrms_div 			vwcmsrms_div,
            rem.ctlrms_divrr			vwcmsrms_div2,
            rem.ctlrms_cmsn 		vwcmsrms_cmsn,
            rem.ctlrms_cmsn$ 		vwcmsrms_cmsn$,
            rem.ctlrms_cmsp$ 		vwcmsrms_cmsp$,
            rem.ctlrms_cmsb$ 		vwcmsrms_cmsb$
        from
            ctlrms rem,
            ctlprdct ctl,
            vwalldrctr dir,
            vwvnddr vdd
        where   rem.ctlrms_sttsdcs_id  = 5 
            --and rem.ctlrms_catcms_id   = 0 
            and rem.ctlrms_drctr_id    = dir.vwalldrctr_id
            and rem.ctlrms_ctlgvddr_id = vdd.vwvnddr_id
            and rem.ctlrms_catprd_id   = ctl.ctlprdct_id

        order by rem.ctlrms_id desc;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWCMSRMS TO  SYSDBA WITH GRANT OPTION;

