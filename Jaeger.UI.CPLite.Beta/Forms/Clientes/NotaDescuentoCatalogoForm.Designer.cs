﻿namespace Jaeger.UI.CPLite.Forms.Clientes {
    partial class NotaDescuentoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotaDescuentoCatalogoForm));
            this.TNota = new Jaeger.UI.CPLite.Forms.Clientes.NotaDescuentoGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TNota
            // 
            this.TNota.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TNota.Location = new System.Drawing.Point(0, 0);
            this.TNota.Name = "TNota";
            this.TNota.PDF = null;
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TNota.Permisos = uiAction1;
            this.TNota.ShowActualizar = true;
            this.TNota.ShowAgrupar = false;
            this.TNota.ShowAutosuma = false;
            this.TNota.ShowCancelar = false;
            this.TNota.ShowCerrar = true;
            this.TNota.ShowEditar = true;
            this.TNota.ShowEjercicio = true;
            this.TNota.ShowExportarExcel = false;
            this.TNota.ShowFiltro = true;
            this.TNota.ShowHerramientas = false;
            this.TNota.ShowImprimir = true;
            this.TNota.ShowItem = false;
            this.TNota.ShowNuevo = true;
            this.TNota.ShowPeriodo = true;
            this.TNota.ShowSeleccionMultiple = true;
            this.TNota.Size = new System.Drawing.Size(1357, 653);
            this.TNota.TabIndex = 0;
            // 
            // NotaDescuentoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1357, 653);
            this.Controls.Add(this.TNota);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NotaDescuentoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "CPLite: Notas de Descuento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.NotaDescuentoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private NotaDescuentoGridControl TNota;
    }
}