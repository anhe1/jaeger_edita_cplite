﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Almacen.Contracts;


namespace Jaeger.UI.CPLite.Forms.Almacen {
    internal class ModelosImportarForm : UI.Almacen.Forms.ModeloImportarForm {
        #region declaraciones
        protected internal ICatalogoProductosService _Service;
        protected internal List<IClasificacionSingle> _Categorias;
        protected internal Aplication.Almacen.Services.ImportacionService ImportacionService;
        protected internal List<ModeloXDetailModel> ModeloDetailModel;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public ModelosImportarForm(ICatalogoProductosService service) : base() {
            this._Service = service;
            this.ImportacionService = new Aplication.Almacen.Services.ImportacionService();
            this.TControl.ShowNuevo = true;
            this.TControl.ShowEditar = false;
            this.TControl.ShowActualizar = false;
            this.TControl.Nuevo.Enabled = true;
            this.TControl.Nuevo.Click += TControl_Nuevo_Click;
            this.TControl.CreateView();
            this.Load += ModelosImportarForm_Load;
        }

        private void ModelosImportarForm_Load(object sender, EventArgs e) {
            this._Categorias = this._Service.GetClasificacion();
            this.TControl.GridData.AllowEditRow = true;
            var cboCategoria = this.TControl.GridData.Columns["IdProducto"] as GridViewComboBoxColumn;
            cboCategoria.DataSource = this._Service.GetList<Domain.Almacen.Entities.ClasificacionProductoModel>(new List<Domain.Base.Builder.IConditional>());
            cboCategoria.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboCategoria.AutoCompleteMode = AutoCompleteMode.Suggest;
            cboCategoria.DropDownStyle = RadDropDownStyle.DropDownList;
            cboCategoria.DisplayMember = "Descriptor";
            cboCategoria.ValueMember = "IdProducto";

            var cboTipo = this.TControl.GridData.Columns["Tipo"] as GridViewComboBoxColumn;
            cboTipo.DisplayMember = "Descripcion";
            cboTipo.ValueMember = "Id";
            cboTipo.DataSource = Aplication.Almacen.Services.CatalogoProductoService.GetTipos();
        }

        private void TControl_Nuevo_Click(object sender, EventArgs e) {
            var path = new System.Windows.Forms.OpenFileDialog();
            if (path.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                using (var espera = new Waiting1Form(this.Importacion)) {
                    this.Tag = path.FileName;
                    espera.ShowDialog(this);
                }
            }
            this.TControl.GridData.DataSource = this.ModeloDetailModel;
        }

        protected override void TControl_Guardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Guardar)) {
                espera.Text = "Guardando...";
                espera.ShowDialog(this);
            }
        }

        protected override void RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.Conceptos.Caption) {
                var t1 = this.TControl.GetCurrent<ModeloXDetailModel>();
                if (t1 != null) {
                    var row = e.Template.Rows.NewRow();
                    row.Cells["Contenido"].Value = t1.Publicacion.Contenido;
                    e.SourceCollection.Add(row);
                }
            }
        }

        protected override void RadGridView_CellBeginEdit(object sender, GridViewCellCancelEventArgs e) {
            if (this.TControl.GridData.CurrentRow is GridViewFilteringRowInfo) {
                return;
            }
            if (e.Column.Name == "IdProducto") {
                if (e.Column.Tag == null) {
                    e.Column.Tag = true;
                    var editor = (RadMultiColumnComboBoxElement)this.TControl.GridData.ActiveEditor;
                    editor.EditorControl.MasterTemplate.AutoGenerateColumns = false;
                    editor.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("IdProducto", "IdProducto") { IsVisible = false });
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Descripción", "Nombre"));
                    editor.EditorControl.Columns.Add(new GridViewTextBoxColumn("Categoría / Producto", "Descriptor"));
                    editor.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
                    editor.EditorControl.BestFitColumns();
                    editor.EditorControl.AllowSearchRow = true;
                    editor.AutoSizeDropDownToBestFit = true;
                    editor.AutoSizeMode = RadAutoSizeMode.WrapAroundChildren;
                    editor.AutoSizeDropDownColumnMode = BestFitColumnMode.DisplayedCells;
                    editor.AutoSizeDropDownToBestFit = true;
                    editor.AutoSizeDropDownHeight = true;
                }
            }
        }

        public void Guardar() {
            foreach (var item in this.ModeloDetailModel) {
                this._Service.Save(item);
            }
        }
        public void Importacion() {
            var path = (string)this.Tag;
            this._DataSource = this.ImportacionService.Importar(path);
            this.ModeloDetailModel = new List<ModeloXDetailModel>();
            foreach (var item in this._DataSource) {
                var nuevo = new ModeloXDetailModel {
                    IdCategoria = item.IdCategoria,
                    IdProducto = item.IdProducto,
                    Clave = item.Clave,
                    Largo = item.Largo,
                    Ancho = item.Ancho,
                    Peso = item.Peso,
                    ObjetoImp = item.ObjetoImp,
                    Descripcion = item.Modelo,
                    Marca = item.Marca,
                    Especificacion = item.Especificacion,
                    ValorTrasladoIVA = item.TasaIVA,
                    FactorTrasladoIVA = "Tasa",
                    ClaveUnidad = item.ClaveUnidad,
                    Unitario = item.Unitario,
                    ClaveProdServ = item.ClaveProdServ,
                    Publicacion = new PublicacionModel {
                        Contenido = item.Publicacion,
                        Descripcion = item.Producto
                    }
                };
                this.ModeloDetailModel.Add(nuevo);
            }
        }
    }
}
