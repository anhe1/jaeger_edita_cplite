SET TERM ^ ;
CREATE PROCEDURE PORCOBRAR1 (
    INPUT_IDREMISION Integer )
AS
declare variable factorTemporal numeric(18, 4);
declare variable almacenar smallint;
declare variable IDREMISION Integer;
declare variable IDDIRECTORIO Integer;
declare variable FECEMISION Timestamp;
declare variable FECENTREGA Timestamp;
declare variable FECVENCE Timestamp;
declare variable ULTCOBRO Timestamp;
declare variable DIASEMISION Integer;
declare variable DIASENTREGA Integer;
declare variable DIASULTCOBRO Integer;
declare variable TASAIVATRASLADOPACTADO Numeric(18,4);
declare variable FACTORPACTADO Numeric(18,4);
declare variable DIASCREDITO Integer;
declare variable CATDESCUENTO Varchar(150);
declare variable DESCUENTO Varchar(200);
declare variable SUBTOTAL Numeric(18,4);
declare variable FACTORDESCUENTO Numeric(18,4);
declare variable TASAIVATRASLADO Numeric(18,4);
declare variable IVATRASLADO Numeric(18,4);
declare variable PORCOBRAR Numeric(18,4);
declare variable DESCUENTOIMPORTE Numeric(18,4);
declare variable ACUMULADO Numeric(18,4);
declare variable SALDO Numeric(18,4);

begin 
/* -------------------------------------------------------------------------------------------------------------
    Develop: anhe 09112021 2143
    Purpose: procedimiento para el calculo de remisiones por cobrar, la politica actual para cobranza indica
    que el cliente es acreedor al descuento siempre y cuando no supere sus dias de credito tomando en cuenta
    la fecha de entrega de la remision
    almacenar = 1; actualizara los registros de las remisiones
------------------------------------------------------------------------------------------------------------- */
    almacenar = 1;
    for
        select
            rem.ctlrms_id IdRemision,
            rem.ctlrms_drctr_id IdDirectorio,
            rem.ctlrms_fcemsn FecEmision,
            rem.ctlrms_fcentrg FecEntrega,
            rem.ctlrms_fchvnc FecVence,
            rem.ctlrms_fcultpg UltCobro,
            ceil ( current_timestamp - rem.ctlrms_fcemsn ) DiasEmision,
            ceil ( current_timestamp - rem.ctlrms_fcentrg ) DiasEntrega,
            (case when ceil ( current_timestamp - rem.ctlrms_fcultpg ) > 0 then ceil ( current_timestamp - rem.ctlrms_fcultpg ) else 0 end) DiasUltCobro,
            (case when dir.drctr_ffctr is null then 0 else dir.drctr_ffctr end) TasaIvaTrasladoPactado,
            (case when dir.drctr_factor is null then 0 else dir.drctr_factor end) FactorPactado,
            (case when dir.drctr_dscrdt is null then 0 else dir.drctr_dscrdt end) DiasCredito,
            rem.ctlrms_ctldsc CatDescuento,
            rem.ctlrms_ddscn Descuento,
            rem.ctlrms_sbttl$ SubTotal,
            rem.ctlrms_dscnt FactorDescuento,
            rem.ctlrms_impst TasaIvaTraslado,
            rem.ctlrms_impst$ IvaTraslado,
            rem.ctlrms_total$ PorCobrar,
            rem.ctlrms_dscnt$ DescuentoImporte,
            rem.ctlrms_cbrd$ Acumulado,
            rem.ctlrms_sld$ Saldo
        from
            ctlrms rem
        left join ctlprdct ctl on rem.ctlrms_catprd_id = ctl.ctlprdct_id
        left join drctr dir on rem.ctlrms_drctr_id = dir.drctr_id
        where rem.ctlrms_id = :input_idRemision
        order by rem.ctlrms_id desc
        into
            :IdRemision,
            :IdDirectorio,
            :FecEmision,
            :FecEntrega,
            :FecVence,
            :UltCobro,
            :DiasEmision,
            :DiasEntrega,
            :DiasUltCobro,
            :TasaIvaTrasladoPactado,
            :FactorPactado,
            :DiasCredito,
            :CatDescuento,
            :Descuento,
            :SubTotal,
            :FactorDescuento,
            :TasaIvaTraslado,
            :IvaTraslado,
            :PorCobrar,
            :DescuentoImporte,
            :Acumulado,
            :Saldo
    do
        begin
            
            /* si los dias de entrega no superan los dias de credito y existe factor pactado entonces 
            descuento autorizado por el factor pactado*/
           if (DiasCredito >= DiasEntrega and FactorPactado > 0 and FecEmision >= '05/01/2013') then
               begin
                    CatDescuento = 'Factor Pactado';
                    Descuento = 'Autorizado';
                    FactorDescuento = FactorPactado;
               end
           else
                begin
                    /* en caso contrario entonces */
                    CatDescuento = 'Sin descuento';
                    Descuento = 'Sin descuento';
                    FactorDescuento = 0;
                end
            /* calcular los totales */
            
            if (FactorDescuento = 0) then
                begin
                    factorTemporal = 0;
                end
            else 
                begin 
                    factorTemporal = (1 - FactorDescuento);
                end 
                
            TasaIvaTraslado = TasaIvaTrasladoPactado;
            DescuentoImporte = SubTotal * factorTemporal;
            IvaTraslado = (SubTotal - DescuentoImporte) * TasaIvaTraslado;
            PorCobrar = ((SubTotal - DescuentoImporte) + IvaTraslado) - Acumulado;
            Saldo = PorCobrar - Acumulado;
            
            /* por ultimo actualizar la tabla de remisiones */
            if (almacenar = 1) then
                begin
                    update ctlrms set
                        ctlrms_catdsc_id = -1,
                        ctlrms_fccal     = current_date,
                        ctlrms_ctldsc    = :CatDescuento,
                        ctlrms_ddscn     = :Descuento,
                        ctlrms_dscnt     = :FactorDescuento,
                        ctlrms_impst     = :TasaIvaTraslado,
                        ctlrms_impst$    = :IvaTraslado,
                        ctlrms_total$    = :PorCobrar,
                        ctlrms_dda$      = :Saldo
                        where ctlrms_id = :IdRemision;
                    end
            suspend;
        end
        
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE PORCOBRAR1 TO  SYSDBA;

