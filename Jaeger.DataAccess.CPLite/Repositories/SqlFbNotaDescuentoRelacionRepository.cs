﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Ventas.Contracts;
using Jaeger.Domain.Ventas.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    public class SqlFbNotaDescuentoRelacionRepository : Abstractions.RepositoryMaster<NotaDescuentoRelacionModel>, ISqlNotaDescuentoRelacionRepository {
        public SqlFbNotaDescuentoRelacionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NTDSCP SET NTDSCP_A = 0 WHERE NTDSCP_ID = @NTDSCP_ID;"
            };

            sqlCommand.Parameters.AddWithValue("@NTDSCP_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public NotaDescuentoRelacionModel GetById(int index) {
            throw new NotImplementedException();
        }

        public int Update(NotaDescuentoRelacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NTDSCP SET NTDSCP_ID = @NTDSCP_ID, NTDSCP_A = @NTDSCP_A, NTDSCP_DOC_ID = @NTDSCP_DOC_ID, NTDSCP_ALMPT_ID = @NTDSCP_ALMPT_ID, NTDSCP_NTDSC_ID = @NTDSCP_NTDSC_ID, NTDSCP_PDCLN_ID = @NTDSCP_PDCLN_ID, NTDSCP_DRCTR_ID = @NTDSCP_DRCTR_ID, NTDSCP_TIPO = @NTDSCP_TIPO, NTDSCP_FOLIO = @NTDSCP_FOLIO, NTDSCP_SERIE = @NTDSCP_SERIE, NTDSCP_CLV = @NTDSCP_CLV, NTDSCP_NOM = @NTDSCP_NOM, NTDSCP_RFC = @NTDSCP_RFC, NTDSCP_FECEMS = @NTDSCP_FECEMS, NTDSCP_SBTTL = @NTDSCP_SBTTL, NTDSCP_DSCNT = @NTDSCP_DSCNT, NTDSCP_TRSIVA = @NTDSCP_TRSIVA, NTDSCP_GTOTAL = @NTDSCP_GTOTAL, NTDSCP_UUID = @NTDSCP_UUID, NTDSCP_USU_M = @NTDSCP_USU_M, NTDSCP_FM = @NTDSCP_FM 
WHERE ((NTDSCP_A = @NTDSCP_A) AND (NTDSCP_DOC_ID = @NTDSCP_DOC_ID) AND (NTDSCP_ALMPT_ID = @NTDSCP_ALMPT_ID) AND (NTDSCP_NTDSC_ID = @NTDSCP_NTDSC_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@NTDSCP_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DOC_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_NTDSC_ID", item.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_TIPO", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_NOM", item.Receptor);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DSCNT", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_GTOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public int Insert(NotaDescuentoRelacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NTDSCP (NTDSCP_ID, NTDSCP_A, NTDSCP_DOC_ID, NTDSCP_ALMPT_ID, NTDSCP_NTDSC_ID, NTDSCP_PDCLN_ID, NTDSCP_DRCTR_ID, NTDSCP_TIPO, NTDSCP_FOLIO, NTDSCP_SERIE, NTDSCP_CLV, NTDSCP_NOM, NTDSCP_RFC, NTDSCP_FECEMS, NTDSCP_SBTTL, NTDSCP_DSCNT, NTDSCP_TRSIVA, NTDSCP_GTOTAL, NTDSCP_UUID, NTDSCP_USU_N, NTDSCP_FN) 
VALUES (@NTDSCP_ID, @NTDSCP_A, @NTDSCP_DOC_ID, @NTDSCP_ALMPT_ID, @NTDSCP_NTDSC_ID, @NTDSCP_PDCLN_ID, @NTDSCP_DRCTR_ID, @NTDSCP_TIPO, @NTDSCP_FOLIO, @NTDSCP_SERIE, @NTDSCP_CLV, @NTDSCP_NOM, @NTDSCP_RFC, @NTDSCP_FECEMS, @NTDSCP_SBTTL, @NTDSCP_DSCNT, @NTDSCP_TRSIVA, @NTDSCP_GTOTAL, @NTDSCP_UUID, @NTDSCP_USU_N, @NTDSCP_FN)"
            };

            sqlCommand.Parameters.AddWithValue("@NTDSCP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DOC_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_NTDSC_ID", item.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_TIPO", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_NOM", item.Receptor);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DSCNT", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_GTOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FM", item.FechaModifica);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public IEnumerable<NotaDescuentoRelacionModel> GetList() {
            throw new NotImplementedException();
        }
        #endregion

        public int Saveable(NotaDescuentoRelacionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE OR INSERT INTO NTDSCP (NTDSCP_ID, NTDSCP_A, NTDSCP_DOC_ID, NTDSCP_ALMPT_ID, NTDSCP_NTDSC_ID, NTDSCP_PDCLN_ID, NTDSCP_DRCTR_ID, NTDSCP_TIPO, NTDSCP_FOLIO, NTDSCP_SERIE, NTDSCP_CLV, NTDSCP_NOM, NTDSCP_RFC, NTDSCP_FECEMS, NTDSCP_SBTTL, NTDSCP_DSCNT, NTDSCP_TRSIVA, NTDSCP_GTOTAL, NTDSCP_UUID, NTDSCP_USU_N, NTDSCP_FN, NTDSCP_USU_M, NTDSCP_FM) 
VALUES (@NTDSCP_ID, @NTDSCP_A, @NTDSCP_DOC_ID, @NTDSCP_ALMPT_ID, @NTDSCP_NTDSC_ID, @NTDSCP_PDCLN_ID, @NTDSCP_DRCTR_ID, @NTDSCP_TIPO, @NTDSCP_FOLIO, @NTDSCP_SERIE, @NTDSCP_CLV, @NTDSCP_NOM, @NTDSCP_RFC, @NTDSCP_FECEMS, @NTDSCP_SBTTL, @NTDSCP_DSCNT, @NTDSCP_TRSIVA, @NTDSCP_GTOTAL, @NTDSCP_UUID, @NTDSCP_USU_N, @NTDSCP_FN, @NTDSCP_USU_M, @NTDSCP_FM)
MATCHING (NTDSCP_A, NTDSCP_DOC_ID, NTDSCP_ALMPT_ID, NTDSCP_NTDSC_ID);"
            };

            // solo para el caso de indice incremental
            if (item.Id > 0) {
                sqlCommand.Parameters.AddWithValue("@NTDSCP_ID", item.Id);
                item.Modifica = this.User;
                item.FechaModifica = DateTime.Now;
            } else {
                sqlCommand.Parameters.AddWithValue("@NTDSCP_ID", DBNull.Value);
                item.Creo = this.User;
                item.FechaNuevo = DateTime.Now;
            }

            sqlCommand.Parameters.AddWithValue("@NTDSCP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DOC_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_ALMPT_ID", item.IdComprobante);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_NTDSC_ID", item.IdNota);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_PDCLN_ID", item.IdPedido);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_TIPO", item.Tipo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FOLIO", item.Folio);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_SERIE", item.Serie);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_NOM", item.Receptor);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FECEMS", item.FechaEmision);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_SBTTL", item.SubTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_DSCNT", item.Descuento);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_TRSIVA", item.TotalTrasladoIVA);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_GTOTAL", item.GTotal);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_UUID", item.IdDocumento);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_USU_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_USU_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NTDSCP_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<Conditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT NTDSCP.* FROM NTDSCP @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }

        public NotaDescuentoRelacionDetailModel Save(NotaDescuentoRelacionDetailModel model) {
            if (model.Id == 0) {
                model.IdNota = model.IdNota;
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.Id = this.Insert(model);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model);
            }
            return model;
        }
    }
}
