﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.UI.Almacen.PT.Builder;
using Jaeger.UI.Common.Forms;

namespace Jaeger.UI.CPLite.Forms.Ventas {
    internal class EstadoCuentaClienteControl : GridCommonControl {
        #region declaraciones
        public Aplication.Almacen.PT.Contracts.IRemisionadoService _Remisionado;
        public IDirectorioService _Directorio;
        private List<RemisionSingleModel> DataSource;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Clientes;
        #endregion

        public EstadoCuentaClienteControl() : base() {
            this.Clientes = new RadMultiColumnComboBox();
            this.ItemHost.HostedItem = this.Clientes.MultiColumnComboBoxElement;
            this.Load += EstadoCuentaControl_Load;
        }

        private void EstadoCuentaControl_Load(object sender, EventArgs e) {
            this.ShowItem = true;
            this.ShowNuevo = false;
            this.ShowEditar = false;
            this.ShowImprimir = true;
            this.ShowPeriodo = false;
            this.ItemLbl.Text = "Cliente: ";
            this._Directorio = new Aplication.CPLite.Services.DirectorioService();
            this._Remisionado = new Aplication.CPLite.Almacen.PT.Services.RemisionadoService();

            this.Clientes.AutoFilter = true;
            FilterDescriptor descriptor = new FilterDescriptor(this.Clientes.DisplayMember, FilterOperator.StartsWith, string.Empty);
            this.Clientes.EditorControl.FilterDescriptors.Add(descriptor);
            this.Clientes.AutoFilter = true;
            this.Clientes.EditorControl.AllowRowResize = false;
            this.Clientes.EditorControl.AllowSearchRow = true;
            this.Clientes.EditorControl.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            this.Clientes.EditorControl.GridViewElement.ShowRowErrors = false;
            this.Clientes.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Clientes.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Clientes.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.Clientes.EditorControl.Name = "NestedRadGridView";
            this.Clientes.EditorControl.ReadOnly = true;
            this.Clientes.EditorControl.ShowGroupPanel = false;
            this.Clientes.EditorControl.Size = new System.Drawing.Size(380, 150);
            this.Clientes.EditorControl.TabIndex = 0;
            this.Clientes.Columns.Add(new GridViewTextBoxColumn { FieldName = "IdDirectorio", HeaderText = "Id", IsVisible = false, DataType = typeof(int) });
            this.Clientes.Columns.Add(new GridViewTextBoxColumn { FieldName = "Clave", HeaderText = "Clave", Width = 75 });
            this.Clientes.Columns.Add(new GridViewTextBoxColumn { FieldName = "Nombre", HeaderText = "Vendedor", Width = 200 });
            this.Clientes.DisplayMember = "Nombre";
            this.Clientes.ValueMember = "IdDirectorio";
            this.Clientes.DataSource = this._Directorio.GetList<ContribuyenteDetailModel>(true);

            using (IRemisionadoGridBuilder builder = new RemisionadoGridBuilder()) {
                this.GridData.Columns.AddRange(builder.Templetes().Remisionado().Build());
            }

            this.Actualizar.Click += TRemision_Actualizar_Click;
            this.Imprimir.Click += TReporte_Imprimir_Click;
        }

        private void TReporte_Imprimir_Click(object sender, EventArgs e) {
            if (this.DataSource != null) {
                var seleccion = this.GridData.ChildRows.Select(x => x.DataBoundItem as RemisionSingleModel);
                if (seleccion.Count() > 0) {
                    var _vendedor = seleccion.Select(it => it.ReceptorNombre).FirstOrDefault().ToUpper();
                    var _reporte = new ReporteForm(new EdoCuentaClientePrinter() { Vendedor = _vendedor, Conceptos = seleccion.ToList() });
                    _reporte.Show();
                }
            }
        }

        private void TRemision_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consulta)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.GridData.DataSource = this.DataSource;
        }

        private void Consulta() {
            var selectedValue = this.Clientes.SelectedItem as GridViewRowInfo;
            if (selectedValue != null) {
                var seleccionado = selectedValue.DataBoundItem as ContribuyenteDetailModel;
                var query = Aplication.Almacen.PT.Services.RemisionadoService.Query().WithYear(this.GetEjercicio()).WithIdReceptor(seleccionado.IdDirectorio).WithIdStatus(Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Build();
                this.DataSource = this._Remisionado.GetList<RemisionSingleModel>(query).ToList();
            }
        }
    }
}
