﻿using System;
using Jaeger.Aplication.Almacen.PT.Contracts;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class RemisionadoPartidaForm : UI.Almacen.PT.Forms.RemisionadoPartidaForm {
        public RemisionadoPartidaForm(UIMenuElement menuElement, IRemisionadoService service) : base(menuElement, service) {
            this.Load += RemisionesFiscalesPartidasForm_Load;
        }

        private void RemisionesFiscalesPartidasForm_Load(object sender, EventArgs e) {
            this._Service = new Aplication.CPLite.Services.RemisionadoService();
        }
    }
}
