﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Aplication.Tienda.Contracts;

namespace Jaeger.Aplication.CPLite.Tienda.Services {
    public class CuponesService : Aplication.Tienda.Services.CuponesService, ICuponesService {
        
        public CuponesService() {
            this.cuponRepository = new DataAccess.FB.CPLite.Repositories.SqlFbCuponRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
