﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Almacen.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Base.Entities;

namespace Jaeger.DataAccess.FB.CPLite.Repositories {
    /// <summary>
    /// clase para el manejo del catalogo de clasificaciones de Bienes, Productos y Servicios (BPS) del almacen de producto terminado
    /// </summary>
    public class SqlFbCategoriaRepository : Abstractions.RepositoryMaster<CategoriaModel>, Domain.Almacen.Contracts.ISqlCategoriaRepository {
        public SqlFbCategoriaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTCLS WHERE ((CTCLS_ID = @CTCLS_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTCLS_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public CategoriaModel GetById(int index) {
            return this.GetList(new List<Conditional>() { new Conditional("CTCLS_ID", index.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<CategoriaModel> GetList() {
            return this.GetList(new List<Conditional>());
        }

        public int Insert(CategoriaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTCLS (CTCLS_ID, CTCLS_A, CTCLS_ALM_ID, CTCLS_SBID, CTCLS_CLASS1, CTCLS_CLASS2, CTCLS_USR_N, CTCLS_FN) 
                                    VALUES (@CTCLS_ID, @CTCLS_A, @CTCLS_ALM_ID, @CTCLS_SBID, @CTCLS_CLASS1, @CTCLS_CLASS2, @CTCLS_USR_N, @CTCLS_FN) RETURNING CTCLS_ID"
            };
            item.FechaNuevo = DateTime.Now;
            item.Creo = this.User;
            sqlCommand.Parameters.AddWithValue("@CTCLS_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTCLS_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTCLS_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTCLS_SBID", item.IdSubCategoria);
            sqlCommand.Parameters.AddWithValue("@CTCLS_CLASS1", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTCLS_CLASS2", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTCLS_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTCLS_FN", item.FechaNuevo);

            item.IdCategoria = this.ExecuteScalar(sqlCommand);
            if (item.IdCategoria > 0)
                return item.IdCategoria;
            return 0;
        }

        public int Update(CategoriaModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTCLS SET CTCLS_A = @CTCLS_A, CTCLS_ALM_ID = @CTCLS_ALM_ID, CTCLS_SBID = @CTCLS_SBID, CTCLS_CLASS1 = @CTCLS_CLASS1, CTCLS_CLASS2 = @CTCLS_CLASS2, CTCLS_USR_N = @CTCLS_USR_N, CTCLS_FN = @CTCLS_FN WHERE ((CTCLS_ID = @CTCLS_ID))"
            };
            item.FechaNuevo = DateTime.Now;
            item.Creo = this.User;
            sqlCommand.Parameters.AddWithValue("@CTCLS_ID", item.IdCategoria);
            sqlCommand.Parameters.AddWithValue("@CTCLS_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTCLS_ALM_ID", item.IdAlmacen);
            sqlCommand.Parameters.AddWithValue("@CTCLS_SBID", item.IdSubCategoria);
            sqlCommand.Parameters.AddWithValue("@CTCLS_CLASS1", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTCLS_CLASS2", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTCLS_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTCLS_FN", item.FechaNuevo);
            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<CategoriaModel> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CTCLS @condiciones"
            };

            if (conditionals.Count > 0) {
                var d = FireBird.Services.ExpressionTool.ConditionalModelToSql(conditionals);
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "where" + d.Key);
                sqlCommand.Parameters.AddRange(d.Value);
            } else {
                sqlCommand.CommandText = sqlCommand.CommandText.Replace("@condiciones", "");
            }
            return this.GetMapper<CategoriaModel>(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT CTCLS.* FROM CTCLS @condiciones"
            };
            if (typeof(T1) == typeof(CategoriaModel)) {
                return GetMapper<T1>(sqlCommand, conditionals);
            } else if (typeof(T1) == typeof(CategoriaSingle)) {
                sqlCommand = new FbCommand {
                    CommandText = @"
                                WITH RECURSIVE Categorias (IDCATEGORIA, SUBID,ACTIVO, CLASE, DESCRIPCION, URL, NIVEL, CHILDS) AS
                                (
                                  SELECT CTCLS_ID AS IDCATEGORIA, CTCLS_SBID AS SUBID, CTCLS_A AS ACTIVO,CTCLS_CLASS1 AS CLASE, CTCLS_CLASS1 || ' | ' AS DESCRIPCION, CTCLS_URL AS URL, 1,
                                  (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = CTCLS_ID) AS CHILDS
                                    FROM CTCLS
                                    WHERE CTCLS_ID = 1 -- THE TREE NODE
                                  UNION ALL
                                  SELECT T.CTCLS_ID AS IDCATEGORIA, T.CTCLS_SBID AS SUBID, (T.CTCLS_A * TP.ACTIVO) AS ACTIVO, T.CTCLS_CLASS1 AS  CLASE, (TP.DESCRIPCION || T.CTCLS_CLASS1|| ' | ') AS DESCRIPCION, CTCLS_URL AS URL, TP.NIVEL + 1,
                                  (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = T.CTCLS_ID) AS CHILDS
                                    FROM Categorias AS TP 
	                                 JOIN CTCLS AS T ON TP.IDCATEGORIA = T.CTCLS_SBID
                                )
                                SELECT * FROM Categorias
                                ORDER BY SUBID, DESCRIPCION, IDCATEGORIA,  NIVEL,  CHILDS;"
                };
                return GetMapper<T1>(sqlCommand, conditionals);
            } else if (typeof(T1) == typeof(CategoriaProductoModel)) {
                sqlCommand = new FbCommand {
                    CommandText = @"WITH RECURSIVE Clasificacion (IDCATEGORIA, SUBID,ACTIVO, CLASE, DESCRIPCION, URL, NIVEL, CHILDS) AS
                                    (
                                      SELECT CTCLS_ID AS IDCATEGORIA, CTCLS_SBID AS SUBID, CTCLS_A AS ACTIVO,CTCLS_CLASS1 AS CLASE, CTCLS_CLASS1 || '/' AS DESCRIPCION, CTCLS_URL AS URL, 1,
                                      (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = CTCLS_ID) AS CHILDS
                                        FROM CTCLS
                                        WHERE CTCLS_ID = 1 -- THE TREE NODE
                                      UNION ALL
                                      SELECT T.CTCLS_ID AS IDCATEGORIA, T.CTCLS_SBID AS SUBID, (T.CTCLS_A * TP.ACTIVO) AS ACTIVO, T.CTCLS_CLASS1 AS  CLASE, (TP.DESCRIPCION || T.CTCLS_CLASS1|| ' | ') AS DESCRIPCION, CTCLS_URL AS URL, TP.NIVEL + 1,
                                      (SELECT COUNT(CT.CTCLS_ID) FROM CTCLS CT WHERE CT.CTCLS_SBID = T.CTCLS_ID) AS CHILDS
                                        FROM Clasificacion AS TP 
	                                     JOIN CTCLS AS T ON TP.IDCATEGORIA = T.CTCLS_SBID
                                    )
                                    SELECT * FROM Clasificacion
                                    RIGHT JOIN CTPRD ON Clasificacion.IdCategoria = CTPRD.CTPRD_CTCLS_ID 
                                    WHERE CTPRD.CTPRD_A = 1
                                    ORDER BY NIVEL, CHILDS;"
                };
                return GetMapper<T1>(sqlCommand, conditionals);
            } 
            throw new NotImplementedException();
        }
    }
}
