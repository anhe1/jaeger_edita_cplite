/* **************************************************************************************************************
  +  TABLA DE COMPROBANTES RE REMISIÓN
 ************************************************************************************************************** */
CREATE TABLE RMSN
(
  RMSN_ID Integer DEFAULT 0 NOT NULL,
  RMSN_A Smallint DEFAULT 0 NOT NULL,
  RMSN_CTDOC_ID Smallint NOT NULL,
  RMSN_DECI Smallint NOT NULL,
  RMSN_VER Varchar(3),
  RMSN_FOLIO Integer,
  RMSN_CTSR_ID Integer,
  RMSN_SERIE Varchar(10),
  RMSN_STTS_ID Integer,
  RMSN_PDD_ID Integer,
  RMSN_CTCLS_ID Integer,
  RMSN_CTENV_ID Integer,
  RMSN_DRCTR_ID Integer,
  RMSN_DRCCN_ID Integer,
  RMSN_VNDDR_ID Integer,
  RMSN_GUIA_ID Integer,
  RMSN_CTREL_ID Integer,
  RMSN_RFCE Varchar(14),
  RMSN_RFCR Varchar(14),
  RMSN_NOMR Varchar(255),
  RMSN_CNTCT Varchar(255),
  RMSN_UUID Varchar(36),
  RMSN_FECEMS Timestamp,
  RMSN_FECENT Timestamp,
  RMSN_FECUPC Timestamp,
  RMSN_FECCBR Timestamp,
  RMSN_FCCNCL Date,
  RMSN_FECVNC Date,
  RMSN_SBTTL Numeric(18,4),
  RMSN_DSCNT Numeric(18,4),
  RMSN_TRSIVA Numeric(18,4),
  RMSN_GTOTAL Numeric(18,4),
  RMSN_TPCMB Numeric(18,4),
  RMSN_FACIVA Numeric(18,4),
  RMSN_TOTAL Numeric(18,4),
  RMSN_DESIVA Numeric(18,4),
  RMSN_FACPAC Numeric(18,4),
  RMSN_FACDES Numeric(18,4),
  RMSN_XCBRR Numeric(18,4),
  RMSN_CBRD Numeric(18,4),
  RMSN_MONEDA Varchar(3),
  RMSN_MTDPG Varchar(3),
  RMSN_FRMPG Varchar(2),
  RMSN_NOTA Varchar(255),
  RMSN_VNDDR Varchar(10),
  RMSN_USR_C Varchar(10),
  RMSN_FN Timestamp NOT NULL,
  RMSN_USR_N Varchar(10),
  RMSN_FM Timestamp,
  RMSN_USR_M Varchar(10),
  CONSTRAINT PK_RMSN_ID PRIMARY KEY (RMSN_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RMSN_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSN_A' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de decimales'  where RDB$FIELD_NAME = 'RMSN_DECI' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'version de la remision'  where RDB$FIELD_NAME = 'RMSN_VER' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'folio de control interno'  where RDB$FIELD_NAME = 'RMSN_FOLIO' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de series y folios'  where RDB$FIELD_NAME = 'RMSN_CTSR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie de control interno del  documento en modo texto'  where RDB$FIELD_NAME = 'RMSN_SERIE' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del status de la remision'  where RDB$FIELD_NAME = 'RMSN_STTS_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'RMSN_CTCLS_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla del metodo de envio'  where RDB$FIELD_NAME = 'RMSN_CTENV_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del directorio'  where RDB$FIELD_NAME = 'RMSN_DRCTR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del domicilio del directorio'  where RDB$FIELD_NAME = 'RMSN_DRCCN_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id del vendedor'  where RDB$FIELD_NAME = 'RMSN_VNDDR_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de guia o referencia del metodo de envio'  where RDB$FIELD_NAME = 'RMSN_GUIA_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice o clave de relacion con otros comprobantes'  where RDB$FIELD_NAME = 'RMSN_CTREL_ID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del emisor'  where RDB$FIELD_NAME = 'RMSN_RFCE' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro federal de contribuyentes del receptor'  where RDB$FIELD_NAME = 'RMSN_RFCR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o razon social del receptor'  where RDB$FIELD_NAME = 'RMSN_NOMR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del contacto'  where RDB$FIELD_NAME = 'RMSN_CNTCT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'id de documento'  where RDB$FIELD_NAME = 'RMSN_UUID' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de emision del comprobante'  where RDB$FIELD_NAME = 'RMSN_FECEMS' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de entrega'  where RDB$FIELD_NAME = 'RMSN_FECENT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de pago del comprobante'  where RDB$FIELD_NAME = 'RMSN_FECUPC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de recepcion de cobranza'  where RDB$FIELD_NAME = 'RMSN_FECCBR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de cancelacion del comprobante'  where RDB$FIELD_NAME = 'RMSN_FCCNCL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = ' fecha de vencimiento del pagare'  where RDB$FIELD_NAME = 'RMSN_FECVNC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'subtotal del comprobante'  where RDB$FIELD_NAME = 'RMSN_SBTTL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del descuento'  where RDB$FIELD_NAME = 'RMSN_DSCNT' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del impuesto trasladado IVA'  where RDB$FIELD_NAME = 'RMSN_TRSIVA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'gran total (subtotal - descuento + traslado de iva)'  where RDB$FIELD_NAME = 'RMSN_GTOTAL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de cambio'  where RDB$FIELD_NAME = 'RMSN_TPCMB' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del comprobante'  where RDB$FIELD_NAME = 'RMSN_TOTAL' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor pactado, '  where RDB$FIELD_NAME = 'RMSN_FACPAC' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe por cobrar del comprobante'  where RDB$FIELD_NAME = 'RMSN_XCBRR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'acumulado de la cobranza del comprobante'  where RDB$FIELD_NAME = 'RMSN_CBRD' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la moneda utilizada para expresar los montos, cuando se usa moneda nacional se registra MXN. Conforme con la especificación ISO 4217.'  where RDB$FIELD_NAME = 'RMSN_MONEDA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del método de pago que aplica para este comprobante fiscal digital por Internet, conforme al Artículo 29-A fracción VII incisos a y b del CFF.'  where RDB$FIELD_NAME = 'RMSN_MTDPG' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de la forma de pago de los bienes o servicios amparados por el comprobante, Si no se conoce la forma de pago este atributo se debe omitir.'  where RDB$FIELD_NAME = 'RMSN_FRMPG' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'observaciones'  where RDB$FIELD_NAME = 'RMSN_NOTA' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del vendedor asociada'  where RDB$FIELD_NAME = 'RMSN_VNDDR' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela el comprobante'  where RDB$FIELD_NAME = 'RMSN_USR_C' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSN_FN' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que crea el registro'  where RDB$FIELD_NAME = 'RMSN_USR_N' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSN_FM' and RDB$RELATION_NAME = 'RMSN';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica'  where RDB$FIELD_NAME = 'RMSN_USR_M' and RDB$RELATION_NAME = 'RMSN';
CREATE INDEX IDX_RMSN_CTCLS_ID ON RMSN (RMSN_CTCLS_ID);
CREATE INDEX IDX_RMSN_CTENV_ID ON RMSN (RMSN_CTENV_ID);
CREATE INDEX IDX_RMSN_DRCTR_ID ON RMSN (RMSN_DRCTR_ID);
CREATE INDEX IDX_RMSN_PDD_ID ON RMSN (RMSN_PDD_ID);
CREATE INDEX IDX_RMSN_SERIE_ID ON RMSN (RMSN_CTSR_ID);
CREATE INDEX IDX_RMSN_STTS_ID ON RMSN (RMSN_STTS_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'remisiones del almacen de producto terminado, remision a cliente'
where RDB$RELATION_NAME = 'RMSN';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSN TO  SYSDBA WITH GRANT OPTION;

/* -------------------------------------------------------------------------------------------------------------
    Creación de generador para el indice de la tabla
------------------------------------------------------------------------------------------------------------- */
CREATE GENERATOR GEN_RMSN_ID;

SET TERM !! ;
CREATE TRIGGER RMSN_BI FOR RMSN
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 19072022 2053
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RMSN_ID IS NULL) THEN
    NEW.RMSN_ID = GEN_ID(GEN_RMSN_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RMSN_ID, 0);
    if (tmp < new.RMSN_ID) then
      tmp = GEN_ID(GEN_RMSN_ID, new.RMSN_ID-tmp);
  END
END!!
SET TERM ; !!

/* **************************************************************************************************************
  +  TABLA DE DESCUENTOS RELACIONADOS A LA REMISION AL CLIENTE
 ************************************************************************************************************** */
CREATE TABLE RMSND
(
  RMSND_ID Integer NOT NULL,
  RMSND_A Smallint,
  RMSND_RMSN_ID Integer,
  RMSND_DESC Varchar(60),
  RMSND_FCTR Decimal(18,4),
  RMSND_SBTTL Numeric(18,4),
  RMSND_USR_N Varchar(10),
  RMSND_FN Timestamp NOT NULL,
  CONSTRAINT PK_RMSND_ID PRIMARY KEY (RMSND_ID)
);

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RMSND TO  SYSDBA WITH GRANT OPTION;


/* -------------------------------------------------------------------------------------------------------------
    IMPORTACION DE DATOS
------------------------------------------------------------------------------------------------------------- */
INSERT INTO RMSND (RMSND_ID, RMSND_A, RMSND_RMSN_ID, RMSND_DESC, RMSND_FCTR, RMSND_SBTTL, RMSND_FN, RMSND_USR_N) 

SELECT
CTLRMS_ID as RMSND_ID,
CTLRMS_A as RMSND_A,
CTLRMS_ID as RMSND_RMSN_ID,
SUBSTRING(CTLRMS_DDSCN FROM 1 FOR 60) as RMSND_DESC,
CTLRMS_FACPAC as RMSND_FCTR,
CTLRMS_DSCNT as RMSND_SBTTL,
(case CTLRMS_FN when NULL THEN CTLRMS_FN ELSE CTLRMS_FCEMSN end) as RMSND_FN,
CTLRMS_USU_N as RMSND_USR_N
FROM CTLRMS
order by CTLRMS_ID asc


/* **************************************************************************************************************
  +  TABLA DE COMPROBANTES RELACIONADOS (REMISIONES)
 ************************************************************************************************************** */
 CREATE TABLE RMSNR
 (
   RMSNR_ID Integer DEFAULT 0 NOT NULL,
   RMSNR_A Smallint DEFAULT 1 NOT NULL,
   RMSNR_RMSN_ID Integer,
   RMSNR_DRCTR_ID Integer,
   RMSNR_UUID Varchar(36) CHARACTER SET NONE,
   RMSNR_SERIE Varchar(25) CHARACTER SET NONE,
   RMSNR_FOLIO Integer,
   RMSNR_NOMR Varchar(255),
   RMSNR_FECEMS Date,
   RMSNR_TOTAL Numeric(18,4),
   RMSNR_USR_N Varchar(10),
   RMSNR_FN Timestamp,
   RMSNR_USR_M Varchar(10),
   RMSNR_FM Timestamp,
   CONSTRAINT PK_RMSNR_ID PRIMARY KEY (RMSNR_ID)
 );

 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RMSNR_ID' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RMSNR_A' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de remisiones'  where RDB$FIELD_NAME = 'RMSNR_RMSN_ID' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del directorio'  where RDB$FIELD_NAME = 'RMSNR_DRCTR_ID' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres'  where RDB$FIELD_NAME = 'RMSNR_SERIE' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.'  where RDB$FIELD_NAME = 'RMSNR_FOLIO' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre(s), primer apellido, segundo apellido, segun corresponda, denominacion o razón social del contribuyente, inscrito en el RFC, del receptor del comprobante.'  where RDB$FIELD_NAME = 'RMSNR_NOMR' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha y hora de expedición del comprobante.'  where RDB$FIELD_NAME = 'RMSNR_FECEMS' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe total del comprobante'  where RDB$FIELD_NAME = 'RMSNR_TOTAL' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'RMSNR_USR_N' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RMSNR_FN' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'RMSNR_USR_M' and RDB$RELATION_NAME = 'RMSNR';
 UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RMSNR_FM' and RDB$RELATION_NAME = 'RMSNR';
 CREATE INDEX IDX_RMSNR_CTRMS_ID ON RMSNR (RMSNR_RMSN_ID);
 CREATE INDEX IDX_RMSNR_DRCTR_ID ON RMSNR (RMSNR_DRCTR_ID);
 UPDATE RDB$RELATIONS set
 RDB$DESCRIPTION = 'comprobantes relacionados remisiones'
 where RDB$RELATION_NAME = 'RMSNR';
 GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
  ON RMSNR TO  SYSDBA WITH GRANT OPTION;

  CREATE GENERATOR GEN_RMSNR_ID;

  SET TERM !! ;
  CREATE TRIGGER RMSNR_BI FOR RMSNR
  ACTIVE BEFORE INSERT POSITION 0
  AS
  DECLARE VARIABLE tmp DECIMAL(18,0);
  BEGIN
  /* -------------------------------------------------------------------------------------------------------------
      Develop: ANHE1 09022022 1841
      Purpose: Disparador para indice incremental
  ------------------------------------------------------------------------------------------------------------- */
    IF (NEW.RMSNR_ID IS NULL) THEN
      NEW.RMSNR_ID = GEN_ID(GEN_RMSNR_ID, 1);
    ELSE
    BEGIN
      tmp = GEN_ID(GEN_RMSNR_ID, 0);
      if (tmp < new.RMSNR_ID) then
        tmp = GEN_ID(GEN_RMSNR_ID, new.RMSNR_ID-tmp);
    END
  END!!
  SET TERM ; !!


/* **************************************************************************************************************
  +  TABLA DE MOVIMIENTOS DE ALMACEN DE PRODUCTO TERMINADO (APT)
 ************************************************************************************************************** */
CREATE TABLE MVAPT
(
  MVAPT_ID Integer DEFAULT 0 NOT NULL,
  MVAPT_A Smallint DEFAULT 1 NOT NULL,
  MVAPT_DOC_ID Integer DEFAULT 0,
  MVAPT_CTALM_ID Integer DEFAULT 0,
  MVAPT_CTPRD_ID Integer DEFAULT 0,
  MVAPT_RMSN_ID Integer DEFAULT 0,
  MVAPT_CTDPT_ID Integer DEFAULT 0,
  MVAPT_CTMDL_ID Integer DEFAULT 0,
  MVAPT_CTPRC_ID Integer DEFAULT 0,
  MVAPT_CTESPC_ID Integer DEFAULT 0,
  MVAPT_CTUND_ID Integer DEFAULT 0,
  MVAPT_UNDF Numeric(18,4),
  MVAPT_UNDN Varchar(50),
  MVAPT_CANTE Numeric(18,4),
  MVAPT_CANTS Numeric(18,4),
  MVAPT_CTCLS Varchar(128),
  MVAPT_PRDN Varchar(128),
  MVAPT_MDLN Varchar(128),
  MVAPT_MRC Varchar(128),
  MVAPT_ESPC Varchar(128),
  MVAPT_ESPN Varchar(50),
  MVAPT_UNTC Numeric(18,4),
  MVAPT_UNDC Numeric(18,4),
  MVAPT_UNTR Numeric(18,4) DEFAULT 0,
  MVAPT_SBTTL Numeric(18,4) DEFAULT 0,
  MVAPT_TSIVA Numeric(18,4) DEFAULT 0,
  MVAPT_TRIVA Numeric(18,4) DEFAULT 0,
  MVAPT_IMPRT Numeric(18,4) DEFAULT 0,
  MVAPT_SKU Varchar(20),
  MVAPT_USR_N Varchar(10),
  MVAPT_FN Timestamp DEFAULT 'now',
  MVAPT_USR_M Varchar(10),
  MVAPT_FM Date,
  CONSTRAINT PK_MVAPT_ID PRIMARY KEY (MVAPT_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'MVAPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'MVAPT_A' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tipos de documento (26 = remisiones)'  where RDB$FIELD_NAME = 'MVAPT_DOC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de almacenes'  where RDB$FIELD_NAME = 'MVAPT_CTALM_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de productos'  where RDB$FIELD_NAME = 'MVAPT_CTPRD_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de remisiones'  where RDB$FIELD_NAME = 'MVAPT_RMSN_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo del departamento al tratarse de un documento diferente a remision (26)'  where RDB$FIELD_NAME = 'MVAPT_CTDPT_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de modelos'  where RDB$FIELD_NAME = 'MVAPT_CTMDL_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de precios'  where RDB$FIELD_NAME = 'MVAPT_CTPRC_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de unidades'  where RDB$FIELD_NAME = 'MVAPT_CTUND_ID' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'factor de unidad'  where RDB$FIELD_NAME = 'MVAPT_UNDF' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la unidad utilizada'  where RDB$FIELD_NAME = 'MVAPT_UNDN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad entrante'  where RDB$FIELD_NAME = 'MVAPT_CANTE' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad salida'  where RDB$FIELD_NAME = 'MVAPT_CANTS' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del catalogo'  where RDB$FIELD_NAME = 'MVAPT_CTCLS' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del modelo'  where RDB$FIELD_NAME = 'MVAPT_MDLN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la marca del producto o modelo'  where RDB$FIELD_NAME = 'MVAPT_MRC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'especificacion'  where RDB$FIELD_NAME = 'MVAPT_ESPC' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre de la descripcion'  where RDB$FIELD_NAME = 'MVAPT_ESPN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor unitario'  where RDB$FIELD_NAME = 'MVAPT_UNTR' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sub total'  where RDB$FIELD_NAME = 'MVAPT_SBTTL' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tasa del iva aplicable'  where RDB$FIELD_NAME = 'MVAPT_TSIVA' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del impuesto traslado IVA'  where RDB$FIELD_NAME = 'MVAPT_TRIVA' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que creo el registro'  where RDB$FIELD_NAME = 'MVAPT_USR_N' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'MVAPT_FN' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del utlimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'MVAPT_USR_M' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'MVAPT_FM' and RDB$RELATION_NAME = 'MVAPT';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'movimientos del almacen de producto terminado'
where RDB$RELATION_NAME = 'MVAPT';
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON MVAPT TO  SYSDBA WITH GRANT OPTION;

/* -------------------------------------------------------------------------------------------------------------
    GENERADOR DE INDICE
------------------------------------------------------------------------------------------------------------- */
CREATE GENERATOR GEN_MVAPT_ID;

SET TERM !! ;
CREATE TRIGGER MVAPT_BI FOR MVAPT
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 21072022 1430
    Purpose: Disparador para indice incremental
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.MVAPT_ID IS NULL) THEN
    NEW.MVAPT_ID = GEN_ID(GEN_MVAPT_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_MVAPT_ID, 0);
    if (tmp < new.MVAPT_ID) then
      tmp = GEN_ID(GEN_MVAPT_ID, new.MVAPT_ID-tmp);
  END
END!!
SET TERM ; !!

/* -------------------------------------------------------------------------------------------------------------
    IMPORTACIÓN DE LA INFORMACIÓN DE LA TABLA ANTERIOR
------------------------------------------------------------------------------------------------------------- */
INSERT INTO MVAPT (MVAPT_ID, MVAPT_A, MVAPT_DOC_ID, MVAPT_CTALM_ID, MVAPT_CTPRD_ID, MVAPT_RMSN_ID, MVAPT_CTDPT_ID, MVAPT_CTMDL_ID, MVAPT_CTPRC_ID, MVAPT_CTESPC_ID, MVAPT_CTUND_ID, MVAPT_UNDF, MVAPT_UNDN, MVAPT_CANTE, MVAPT_CANTS, MVAPT_CTCLS, MVAPT_PRDN, 
MVAPT_MDLN, MVAPT_MRC, MVAPT_ESPC, MVAPT_ESPN, MVAPT_UNTC, MVAPT_UNDC, MVAPT_UNTR, MVAPT_SBTTL, MVAPT_TSIVA, MVAPT_TRIVA, MVAPT_IMPRT, MVAPT_SKU, MVAPT_USR_N, MVAPT_FN, MVAPT_USR_M, MVAPT_FM)

SELECT 
MOVAPT_ID                   AS MVAPT_ID, 
MOVAPT_A                    AS MVAPT_A, 
MOVAPT_DOC_ID               AS MVAPT_DOC_ID, 
1                           AS MVAPT_CTALM_ID,
VWMDLS_PRDC_ID              AS MVAPT_CTPRD_ID,  
MOVAPT_CTLGRMSN_ID          AS MVAPT_RMSN_ID, 
MOVAPT_CTLGRMSNDPTO_ID      AS MVAPT_CTDPT_ID, 
MOVAPT_CTLGMDLS_ID          AS MVAPT_CTMDL_ID,
0                           AS MVAPT_CTPRC_ID,
MOVAPT_TAM_ID               AS MVAPT_CTESPC_ID, 
MOVAPT_UNDD_ID              AS MVAPT_CTUND_ID, 
MOVAPT_UNDD                 AS MVAPT_UNDF, 
a.LSTSYS_NOM                  AS MVAPT_UNDN, 
MOVAPT_CNTDD_ENTRDDPTO      AS MVAPT_CANTE, 
MOVAPT_CNTDD_SLD            AS MVAPT_CANTS, 
VWMDLS_CATALOGO             AS MVAPT_CTCLS, 
VWMDLS_PRDC                 AS MVAPT_PRDN, 
VWMDLS_DSCRP                AS MVAPT_MDLN, 
VWMDLS_MARCA                AS MVAPT_MRC, 
VWMDLS_ESPCF                AS MVAPT_ESPC, 
b.LSTSYS_NOM                AS MVAPT_ESPN,
MOVAPT_COST$                AS MVAPT_UNTC, 
MOVAPT_PRDCTV$              AS MVAPT_UNDC, 
MOVAPT_UNTR$                AS MVAPT_UNTR, 
MOVAPT_SBTT$                AS MVAPT_SBTTL, 
.16                         AS MVAPT_TSIVA,
MOVAPT_SBTT$ * .16          AS MVAPT_TRIVA,
MOVAPT_SBTT$ * 1.16         AS MVAPT_IMPRT,
((SUBSTRING('LT00000' FROM 1 FOR 7 -CHARACTER_LENGTH(VWMDLS_PRDC_ID)) || VWMDLS_PRDC_ID || SUBSTRING('-0000' FROM 1 FOR 5 -CHARACTER_LENGTH(MOVAPT_CTLGMDLS_ID)) || MOVAPT_CTLGMDLS_ID) || SUBSTRING('-00' FROM 1 FOR 3 -CHARACTER_LENGTH(MOVAPT_TAM_ID)) || MOVAPT_TAM_ID) AS MVAPT_SKU,
MOVAPT_USU_N                AS MVAPT_USR_N, 
MOVAPT_FN                   AS MVAPT_FN, 
MOVAPT_USU_M                AS MVAPT_USR_M, 
MOVAPT_FM                   AS MVAPT_FM
FROM MOVAPT
LEFT JOIN VWMDLS ON MOVAPT_CTLGMDLS_ID = VWMDLS_ID  AND VWMDLS_A = 1
LEFT JOIN LSTSYS b ON MOVAPT_TAM_ID = b.LSTSYS_SEC_ID AND b.LSTSYS_DOC_ID = 1091
LEFT JOIN LSTSYS a ON MOVAPT_UNDD_ID = a.LSTSYS_SEC_ID AND a.LSTSYS_DOC_ID = 1019 AND a.LSTSYS_A= 1
ORDER BY MOVAPT_ID ASC 