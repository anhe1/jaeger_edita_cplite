SET TERM ^ ;
CREATE PROCEDURE APLICAR_AUTORIACION_CORRECCION (
    INPUT_START DOM_ID DEFAULT 0,
    INPUT_END DOM_ID DEFAULT 0,
    INPUT_UPDATE DOM_ID DEFAULT 0 )
RETURNS (
    AUT_ID DOM_ID,
    REM_ID DOM_ID,
    REM_FOLIO DOM_ID,
    REM_STTS_ID DOM_ID,
    REM_STTS_AUT DOM_ID )
AS
BEGIN
  /* write your code here */ 
    for 
        select autdcs.autdcs_id, ctlrms.ctlrms_id, ctlrms.ctlrms_folio, ctlrms.ctlrms_sttsdcs_id, autdcs.autdcs_sttsaut_id
        from autdcs, ctlrms
        where autdcs.autdcs_a = 1
        and autdcs.autdcs_doc_id=26
        and autdcs.autdcs_ctlrms_id = ctlrms.ctlrms_id
        and autdcs.autdcs_sttsaut_id <> ctlrms.ctlrms_sttsdcs_id
        order by autdcs.autdcs_id
        into
        :aut_id , :rem_id , :rem_folio , :rem_stts_id , :rem_stts_aut 
    do
        begin
            update ctlrms 
                set ctlrms_sttsdcs_id = :rem_stts_aut
            where ctlrms_id = :rem_id;
            suspend;
        end
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE APLICAR_AUTORIACION_CORRECCION TO  SYSDBA;

