SET TERM ^ ;
CREATE TRIGGER CTLRMS_FOLIO_SERIE FOR CTLRMS ACTIVE
BEFORE INSERT POSITION 0
AS 
declare variable folio integer;
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 191120211510
    Purpose: crear folio de control interno
------------------------------------------------------------------------------------------------------------- */
    if (new.ctlrms_doc_id = 26) then 
        begin
            select max (ctlrms_folio) + 1
            from ctlrms where ctlrms_doc_id = new.ctlrms_doc_id
                into :folio;
                
            if (folio = 0 or folio is null) then 
                begin
                    new.ctlrms_folio = 1;
                end
            else 
                begin 
                    new.ctlRms_folio = folio;
                end
        end
END^
SET TERM ; ^
