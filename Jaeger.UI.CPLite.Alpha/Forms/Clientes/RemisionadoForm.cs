﻿using System;
using System.Linq;
using System.ComponentModel;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class RemisionadoForm : Almacen.PT.Forms.RemisionadoForm {
        #region declaraciones
        protected Aplication.Banco.IBancoService _BancoService;
        protected Aplication.Banco.IBancoMovientosSearchService _BancoSearchService;
        protected internal GridViewTemplate _GridCobranza = new GridViewTemplate() { Caption = "Cobranza" };
        protected internal RadMenuItem _ContextMenuReciboCobro = new RadMenuItem { Text = "Registrar Cobro", Name = "tcpl_gcliente_cobranza" };
        #endregion

        public RemisionadoForm(UIMenuElement menuElement) : base(menuElement) {
            this.ShowValues = true;
            this.Load += this.RemisionCatalogoForm_Load;
        }

        private void RemisionCatalogoForm_Load(object sender, EventArgs e) {
            this.Text = "Clientes: Remisionado";
            this.TRemision.menuContextual.Items.Add(_ContextMenuReciboCobro);
            var d3 = ConfigService.GeMenuElement(this._ContextMenuReciboCobro.Name);

            // permisos
            this._ContextMenuReciboCobro.Enabled = ConfigService.HasPermission(d3);

            this._ContextMenuReciboCobro.Click += ContextMenuReciboCobro_Click;
            this.TempleteCobranza();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e); 
            //this._Service = new Aplication.CPLite.Services.RemisionadoService();
        }

        public override void TImprimir_Comprobante_Click(object sender, EventArgs e) {
            var seleccionado = this.TRemision.GetCurrent<RemisionDetailModel>();
            if (!string.IsNullOrEmpty(seleccionado.IdDocumento)) {

                using (var espera = new Waiting1Form(this.GetPrinter)) {
                    espera.Text = "Espera un momento ...";
                    espera.ShowDialog(this);
                }
                // para la impresion de la remision en esta seccion utilizamos el formulario
                // del reporte de almacen para cumplir con el proceso original
                //if (seleccionado.Tag != null) {
                //    var printer = (RemisionDetailPrinter)(seleccionado.Tag);
                //    var reporte = new Almacen.ReporteForm(printer);
                //    reporte.Show();
                //}
            }
        }

        public override void TRemision_PorCobrar_Click(object sender, EventArgs e) {
            //var porCobrar = new RemisionadoPorCobrarForm(this._MenuElement, this._Service) { MdiParent = this.ParentForm };
            //porCobrar.Show();
        }

        private void ContextMenuReciboCobro_Click(object sender, EventArgs e) {
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            var correcto = seleccion.Where(it => it.Status == Domain.Almacen.PT.ValueObjects.RemisionStatusEnum.Por_Cobrar).Count();
            if (seleccion.Count != correcto) {
                RadMessageBox.Show(this, "No es posible crear el registro de cobranza. Es posible que uno o mas comprobantes no tengan el status adecuado para esta acción.",
                    "Atención", System.Windows.Forms.MessageBoxButtons.OK, RadMessageIcon.Info);
                return;
            }
            using (var espera = new Waiting1Form(this.CrearReciboCobro)) {
                espera.Text = "Espere ...";
                espera.ShowDialog(this);
            }
            var remision = this.Tag as MovimientoBancarioDetailModel;
            if (remision != null) {
                var _nuevoMovimiento = new ReciboCobroForm(remision, this._BancoService);
                _nuevoMovimiento.ShowDialog(this);
            }
            this.Tag = null;
        }

        private void CrearReciboCobro() {
            if (this._BancoService == null) this._BancoService = new Aplication.Banco.BancoService();
            var seleccion = this.TRemision.GetSeleccion<RemisionSingleModel>();
            if (seleccion != null) {
                if (seleccion.Count > 0) {
                    // crear el comprobante y luego la vista
                    var cfdiV33 = Aplication.Banco.BancoService.Create().New().Status();
                    foreach (var seleccionado in seleccion) {
                        //var remision = this._Service.GetById(seleccionado.IdRemision);
                     //   cfdiV33.Add(Helpers.ConversionHelper.ConverTo(seleccionado));
                    }
                    this.Tag = cfdiV33.Build();
                }
            }
        }

        private void TempleteCobranza() {
            this._GridCobranza.Columns.AddRange(new UI.Banco.Builder.GridViewBancoBuilder().Templetes().Cobranza().Build());
            this._GridCobranza.HierarchyDataProvider = new GridViewEventDataProvider(_GridCobranza);
            this.TRemision.GridData.RowSourceNeeded += this.GridData_RowSourceNeeded1;
            this.TRemision.GridData.MasterTemplate.Templates.Add(_GridCobranza);
            this._GridCobranza.Standard();
        }

        private void InfoCobranza() {
            var rowView = this.TRemision.GridData.CurrentRow.DataBoundItem as IRemisionSingleModel;
            if (rowView != null) {
                //if (this._BancoSearchService == null) this._BancoSearchService = new Aplication.CPLite.Service.BancoMovientosSearchService();
                //var tabla = this._BancoSearchService.GetSearchMovimientos(rowView.IdRemision, Domain.Banco.ValueObjects.MovimientoBancarioTipoComprobanteEnum.Remision);
                //if (tabla != null) {
                //    this.Tag = tabla;
                //}
            }
        }

        private void GridData_RowSourceNeeded1(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this._GridCobranza.Caption) {
                var rowView = e.ParentRow.DataBoundItem as IRemisionSingleModel;
                if (rowView != null) {
                    using (var espera = new Waiting1Form(this.InfoCobranza)) {
                        espera.Text = "Buscando información de cobranza";
                        espera.ShowDialog(this);
                    }
                    if (this.Tag != null) {
                        var tabla = (BindingList<MovimientoComprobanteMergeModelView>)this.Tag;
                        if (tabla != null) {
                            foreach (var item in tabla) {
                                var newRow = e.Template.Rows.NewRow();
                                e.SourceCollection.Add(UI.Banco.Builder.GridViewBancoBuilder.ConverTo(newRow, item));
                            }
                        }
                    }
                }
            }
        }
    }
}
