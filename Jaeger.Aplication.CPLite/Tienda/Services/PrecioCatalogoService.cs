﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.DataAccess.FB.Almacen.Repositories;

namespace Jaeger.Aplication.CPLite.Tienda.Services {
    public class PrecioCatalogoService : Aplication.Tienda.Services.PrecioCatalogoService, Aplication.Tienda.Contracts.IPrecioCatalogoService {

        public PrecioCatalogoService() {
            this.precioRepository = new SqlFbPrecioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.precioPartidaRepository = new SqlFbPrecioPartidaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.especificacionRepository = new SqlFbEspecificacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
