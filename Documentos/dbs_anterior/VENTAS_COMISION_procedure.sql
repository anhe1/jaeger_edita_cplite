SET TERM ^ ;
CREATE PROCEDURE VENTAS_COMISION (
    START_ID DOM_ID DEFAULT 0,
    END_ID DOM_ID DEFAULT 0,
    STATUS_ID DOM_ID DEFAULT 0 )
AS
declare variable     OUTPUT_ID DOM_ID;
declare variable     OUTPUT_FOLIO DOM_ID;
declare variable     OUTPUT_VNDDR_ID DOM_ID;
declare variable     OUTPUT_DSCNT DOM_MONEDA;
declare variable     OUTPUT_DSTRS DOM_ID;
declare variable     OUTPUT_FCEMSN DOM_FH;
declare variable     OUTPUT_NOM DOM_STR100;
declare variable     OUTPUT_FACTOR1 DOM_MONEDA;
declare variable     OUTPUT_FACNOM1 DOM_STR100;
declare variable     OUTPUT_FACTOR2 DOM_MONEDA;
declare variable     OUTPUT_FACNOM2 DOM_STR100 ;

declare variable ideal_facnom1 dom_str100;
declare variable ideal_facnom2 dom_str100;
declare variable ideal_factor1 dom_moneda;
declare variable ideal_factor2 dom_moneda;
declare variable ideal_cobrado dom_moneda;
declare variable rem_id     dom_id;
begin
/*
proposito: aplicar en modo de prueba politica de comisiones.
*/
    for
        select ctlrms.ctlrms_id,ctlrms.ctlrms_ctlgvddr_id,(ctlrms.ctlrms_divrr),ctlrms.ctlrms_dstrs,ctlrms.ctlrms_fcemsn, ctlrms.ctlrms_folio, ctlrms.ctlrms_cbsiva
        from   ctlrms
        where  ctlrms.ctlrms_a=1
           and ctlrms.ctlrms_sttsdcs_id = :status_id
           and ctlrms.ctlrms_id between :start_id and :end_id
           and ctlrms.ctlrms_catcms_id = 0
        order by ctlrms.ctlrms_id asc
        into
               :rem_id, :output_vnddr_id,:output_dscnt,:output_dstrs,:output_fcemsn,:output_folio, :ideal_cobrado
    do
        begin
            output_nom='';      -- nombre del catalogo de comisiones
            output_factor1=0;   -- factor de la comision
            output_facnom1='';  -- nivel o descripcion de la comision
            output_factor2=0;
            output_facnom2='';
            
            -- consulta al procedimiento para determinar la comision ideal
            select output_factor1, output_facnom1, output_factor2, output_facnom2
            from   comision_vendedor (:output_vnddr_id,1,1,:output_fcemsn)
            into  :ideal_factor1,:ideal_facnom1,:ideal_factor2,:ideal_facnom2;
            
            -- consulta al procedimiento para determinar la comision actualizada
            select output_id, output_nom, output_factor1, output_facnom1, output_factor2, output_facnom2
            from   comision_vendedor (:output_vnddr_id,:output_dscnt,:output_dstrs,:output_fcemsn)
            into  :output_id,:output_nom,:output_factor1,:output_facnom1,:output_factor2,:output_facnom2;
            if ( :output_facnom2 is null ) then
                begin
                    output_facnom2='';
                end 
            if ( output_id <> -1 ) then
                begin
                    update ctlrms set
                        ctlrms_cmsn  = '[' || :output_nom || '] , comisi�n:' || :output_facnom1 || ', corresponde:' || :output_facnom2,
                        ctlrms_cmsn$ = (:ideal_cobrado * :output_factor1 ) * :output_factor2,
                        ctlrms_faccom = :ideal_factor1
                    where ctlrms_id = :rem_id;
                end
            else
                begin
                    update ctlrms set
                        ctlrms_cmsn  = '[' || :output_nom || ']',
                        ctlrms_cmsn$ = 0,
                        ctlrms_faccom = 0
                    where ctlrms_id = :rem_id;
                end
            suspend;
        end

end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE VENTAS_COMISION TO  SYSDBA;

