﻿using System;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Banco {
    public class FormaPagoCatalogoForm : UI.Banco.Forms.FormaPagoCatalogoForm {
        public FormaPagoCatalogoForm() {
        }

        public FormaPagoCatalogoForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.FormaPagoCatalogoForm_Load;
        }

        private void FormaPagoCatalogoForm_Load(object sender, EventArgs e) {
            //this.Service = new BancoFormaPagoService();
        }

        public override void OnLoad() {
            base.OnLoad();  
        }
    }
}
