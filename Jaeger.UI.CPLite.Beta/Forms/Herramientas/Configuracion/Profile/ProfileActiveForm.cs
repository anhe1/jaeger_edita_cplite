﻿using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.CPLite.Forms.Herramientas.Configuracion.Profile {
    public class ProfileActiveForm : Kaiju.Forms.ProfileActiveForm {
        public ProfileActiveForm(UIMenuElement menuElement) : base(menuElement) {
            this.Load += this.ProfileActiveForm_Load;
        }

        private void ProfileActiveForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.CPLite.Services.ProfileService();
        }
    }
}
