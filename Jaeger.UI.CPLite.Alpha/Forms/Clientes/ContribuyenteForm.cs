﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Contribuyentes;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Contribuyentes.Entities;
using Jaeger.Aplication.Contribuyentes.Contracts;
using Jaeger.Aplication.Contribuyentes.Services;
using Jaeger.Aplication.Tienda.Contracts;
using Jaeger.Aplication.Tienda.Services;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    public class ContribuyenteForm : UI.Contribuyentes.Forms.ContribuyenteForm {
        protected IPrecioCatalogoService precios;
        protected IVendedoresService vendedoresService;
        private BindingList<Vendedor2DetailModel> vendedores;
        protected internal RadLabel lblCatalogo = new RadLabel() { Name = "lblCatalogo", Text = "Catálogo de Precios" };
        protected internal RadLabel lblPassword = new RadLabel() { Name = "lblPassword", Text = "Contraseña", Location = new System.Drawing.Point(10, 20) };
        protected internal RadLabel lblConfirmation = new RadLabel() { Name = "lblConfirmation", Text = "Confirmación", Location = new System.Drawing.Point(10, 45  ) };
        protected internal RadTextBox Password = new RadTextBox() { Name = "Password", NullText = "Password", Location = new System.Drawing.Point(90, 20), PasswordChar = '*' };
        protected internal RadTextBox Confirmation = new RadTextBox() { Name = "Confirmation", NullText = "Confirmación", Location = new System.Drawing.Point(90, 45), PasswordChar = '*' };
        protected internal RadCheckBox ShowPassword = new RadCheckBox() { Name = "ShowPassword", Text = "Ver", Location = new System.Drawing.Point(220, 20), AutoSize = true };
        protected internal RadMultiColumnComboBox CatalogoPrecio = new RadMultiColumnComboBox() {
            Name = "CatalogoPrecios",
            DisplayMember = "Descripcion",
            DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList,
            Size = new System.Drawing.Size(351, 20),
            Location = new System.Drawing.Point(465, 19),
            NullText = "Catálogo de precios",
            ValueMember = "IdPrecio",
            TabStop = false,
            AutoSize = true,
            AutoSizeDropDownToBestFit = true
        };
        protected internal RadGroupBox grupo = new RadGroupBox() {
            Name = "Grupo",
            Location = new System.Drawing.Point(465, 49),
            Size = new System.Drawing.Size(351, 80),
            Text = "Acceso a Tienda WEB"
        };
        public ContribuyenteForm(IContribuyenteService service, UIAction _permisos) : base(service, _permisos) {
            this.ComboCatalogoPrecios();
            this.Load += this.ContribuyenteForm_Load;
        }

        public ContribuyenteForm(IContribuyenteService service, ContribuyenteDetailModel model, UIAction _permisos) : base(service, model, _permisos) {
            this.ComboCatalogoPrecios();
            this.Load += this.ContribuyenteForm_Load;
        }

        private void ContribuyenteForm_Load(object sender, EventArgs e) {
            this.precios = new PrecioCatalogoService();
            this.vendedoresService = new VendedoresService();
            this.CatalogoPrecio.DataSource = this.precios.GetList(true);
            this.vendedores = this.vendedoresService.GetList(true);
            var cbVendedores = (GridViewMultiComboBoxColumn)this.GridVendedores.MasterTemplate.Columns["IdVendedor"];
            cbVendedores.DisplayMember = "Nombre";
            cbVendedores.ValueMember = "IdVendedor";
            cbVendedores.DataSource = this.vendedores;
        }

        public override void CreateBinding() {
            base.CreateBinding();
            this.CatalogoPrecio.DataBindings.Clear();
            this.CatalogoPrecio.DataBindings.Add("SelectedValue", this.currentPersona, "IdPrecio", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void ComboCatalogoPrecios() {
            this.grupo.Controls.Add(this.lblPassword);
            this.grupo.Controls.Add(this.lblConfirmation);
            this.grupo.Controls.Add(this.Password);
            this.grupo.Controls.Add(this.Confirmation);
            this.grupo.Controls.Add(this.ShowPassword);
            this.PageCredito.Controls.Add(this.lblCatalogo);
            this.lblCatalogo.Location = new System.Drawing.Point(351, 19);
            this.PageCredito.Controls.Add(this.CatalogoPrecio);
            this.PageCredito.Controls.Add(this.grupo);

            // cambios al control para precios
            this.CatalogoPrecio.EditorControl.MasterTemplate.Columns.AddRange(new GridViewDataColumn[] {
                new GridViewTextBoxColumn {
                    DataType = typeof(int),
                    FieldName = "IdPrecio",
                    HeaderText = "Id Precio",
                    Name = "IdPrecio",
                    IsVisible = true
                },
                 new GridViewTextBoxColumn {
                    FieldName = "Descripcion",
                    HeaderText = "Descripción",
                    Name = "Descripcion"
                }
            });

            this.CatalogoPrecio.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.CatalogoPrecio.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.CatalogoPrecio.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.CatalogoPrecio.EditorControl.MasterTemplate.EnableGrouping = false;
            this.CatalogoPrecio.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.CatalogoPrecio.EditorControl.ReadOnly = true;
            this.CatalogoPrecio.EditorControl.ShowGroupPanel = false;
            this.CatalogoPrecio.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.CatalogoPrecio.EditorControl.TabIndex = 0;
            this.CatalogoPrecio.ValueMember = "IdPrecio";
            this.CatalogoPrecio.DisplayMember = "Descripcion";
        }
    }
}
