SET TERM ^ ;
CREATE PROCEDURE APLICAR_CORRECCION_ULTFECPAGO
AS
declare variable lngIndex dom_id;
DECLARE VARIABLE dteFecha dom_fh;
begin
/*
    elabora: anhe 22042013
    proposito: actualizar la ultima fecha de cobro de las remisiones que esten en un estatus de cobrado
    y la remisi�n tenga un status de cobrado y el status del recibo de cobro que es de donde se obtiene
    la ultima fecha de cobro tiene que estar en el status de boveda.
*/
    for
        select ctlrms.ctlrms_id, ctlrms.ctlrms_fcultpg
        from ctlrms
        where ctlrms.ctlrms_fcultpg is null 
              and ctlrms.ctlrms_sttsdcs_id = 5
        into    :lngindex, :dtefecha
    do
        begin
            select first 1 rec.cntbl_feccbro
            from cntbl par, cntbl rec
            where par.cntbl_doc_id = 2800 
                and rec.cntbl_doc_id = 28
                and rec.cntbl_sttsdcs_id = 3
                and par.cntbl_rem_id = :lngIndex
                and par.cntbl_cntbl_id = rec.cntbl_id
            order by rec.cntbl_feccbro desc
            into :dteFecha;
            
            update ctlrms set
                ctlrms.ctlrms_fcultpg = :dtefecha
            where ctlrms_id = :lngIndex;
            
        end
end^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE APLICAR_CORRECCION_ULTFECPAGO TO  SYSDBA;

