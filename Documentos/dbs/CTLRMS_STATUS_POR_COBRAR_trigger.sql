SET TERM ^ ;
CREATE TRIGGER CTLRMS_STATUS_POR_COBRAR FOR CTLRMS ACTIVE
BEFORE UPDATE POSITION 0
AS 
declare variable subtotal$ numeric(18, 4);
declare variable dias integer;
declare variable diasCredito numeric(18, 4);
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: anhe 191120211138
    Purpose: Cambio de STATUS
    Para obtener el cambio de status 'Por Cobrar' se considera:
    1. Tipo de documento es 26
    2. Fecha de entrega valida, siempre mayor o igual a la fecha de emision
    3. Fecha de recepcion a cobranza valida, mayor o igual a la fecha de entrega
    4. El status anterior debe ser 'Entregado'
    5. El status actual no debe ser 'Cancelado'
    Y se crea la actulizacion del monto por cobrar
------------------------------------------------------------------------------------------------------------- */
	if (new.ctlrms_doc_id = 26 
        and new.ctlrms_fcentrg is not null 
         and new.ctlrms_fccbrnz is not null 
          and new.ctlrms_sttsdcs_id <> 0 
           and old.ctlrms_sttsdcs_id = 3 
            and new.ctlrms_aut_id = 0) then
        begin
            -- actualizar sub total por si las dudas
            subtotal$ = 0;
            select sum ( movapt.MOVAPT_SBTT$ )
            from movapt  where movapt.movapt_a = 1 and movapt.MOVAPT_CTLGRMSN_ID = new.CTLRMS_ID
            into :subtotal$;
            if ( subtotal$ is null or subtotal$ = 0 ) then
                begin
                    exception error_0031;
                end
            
            -- obtener los dias de credito del cliente
            --select drctr.drctr_dscrdt
            --from drctr where drctr.drctr_id = new.ctlrms_drctr_id
            --into :diasCredito;
            diasCredito = datediff(day from new.CTLRMS_FCENTRG to new.CTLRMS_FCHVNC);
            if (diasCredito is null) then
                begin
                    diasCredito = 0;
                end
                
            -- actualizamos los valores
            new.ctlrms_sttsdcs_id = 4;     -- nuevo status por cobrar
            new.ctlrms_sbttl$ = subtotal$; -- impote del subtotal
            new.ctlrms_impst$ = new.ctlrms_sbttl$ * new.ctlrms_impst; -- actualizar el importe de IVA
            new.ctlrms_catdsc_id = -1;     -- porque no pertenece a ninguna catalogo
            new.ctlrms_fccal = current_date;  -- fecha del ultimo calculo
            
            -- si los dias de credito son mayores a los dias transcurridos desde la fecha de entrega
            -- y el factor pactado es mayor de cero entonces el descuento esta autorizado
            if (:diasCredito >= new.CTLRMS_DSTRSA and new.CTLRMS_FACPAC > 0 and new.CTLRMS_FCEMSN >= '05/01/2013') then
                begin
                    new.ctlrms_ctldsc = 'Factor Pactado';
                    new.ctlrms_ddscn = 'Autorizado';
                    new.ctlrms_dscnt = (case when new.CTLRMS_FACPAC = 0 then 0 else new.CTLRMS_FACPAC end);
                    new.ctlrms_total$ = (new.ctlrms_sbttl$ + new.ctlrms_impst$) * (case when new.CTLRMS_FACPAC = 0 then 1 else new.CTLRMS_FACPAC end);
                end
            else 
                begin
                    -- en caso contrario no tiene descuentos
                    new.ctlrms_ctldsc = 'Sin descuento';
                    new.ctlrms_ddscn = 'Sin descuento';
                    new.ctlrms_dscnt = 0;
                    new.ctlrms_total$ = new.ctlrms_sbttl$ + + new.ctlrms_impst$;
                end
        end
END^
SET TERM ; ^
