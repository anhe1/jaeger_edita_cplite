SET TERM ^ ;
CREATE TRIGGER CTLRMS_FACTORES FOR CTLRMS ACTIVE
BEFORE INSERT POSITION 0
AS 
declare variable tasaIVA NUMERIC(18, 4);
declare variable factorPactado NUMERIC(18, 4);
BEGIN 
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 121120211419
    Purpose: asignar el factor pactado y la tasa del iva pactado al crear la remision 
    Revision 161120211103: se agrega CASE al factor del IVA pactado para que el valor sea cero (0) en caso
    de que el valor del directorio sea nulo
    Revision 171120212317: cuando el factor pactado es cero o nulo entonces el valor debe ser 1
------------------------------------------------------------------------------------------------------------- */
    if ( new.ctlrms_doc_id = 26 ) then
        begin
            select (case when drctr.drctr_ffctr is null then 0 else drctr.drctr_ffctr end), (case when drctr.drctr_factor is null then 0 else drctr.drctr_factor end) 
            from drctr 
            where drctr.drctr_id = new.ctlrms_drctr_id
            into :tasaIVA, :factorPactado;
            -- cuando el factor pactado es 0 entonces el valor debe ser 1 
            if (factorPactado = 0) then
                begin
                    factorPactado = 1;
                end 
            new.ctlrms_facpac = factorPactado; -- factor pactado del descueto
            new.ctlrms_impst = tasaIVA;
            new.ctlrms_impst$ = new.ctlrms_sbttl$ * tasaIVA ;
        end
END^
SET TERM ; ^
UPDATE RDB$TRIGGERS set
  RDB$DESCRIPTION = 'Antes de insertar se aplican los datos del factor pactado y el iva pactado del cliente'
  where RDB$TRIGGER_NAME = 'CTLRMS_FACTORES';
