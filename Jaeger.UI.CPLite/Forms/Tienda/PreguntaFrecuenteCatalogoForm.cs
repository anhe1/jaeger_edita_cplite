﻿using System;
using Jaeger.Aplication.CPLite.Services;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    public class PreguntaFrecuenteCatalogoForm : UI.Tienda.Forms.PreguntaFrecuenteCatalogoForm {
        protected internal Domain.Base.ValueObjects.UIAction _permisos;

        public PreguntaFrecuenteCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += this.PreguntaFrecuenteCatalogoForm_Load;
        }

        private void PreguntaFrecuenteCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new PreguntasFrecuentesService();
        }

        public override void Consultar() {
            this.datos = this.service.GetList(true);
        }
    }
}
