﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Almacen.Entities;

namespace Jaeger.UI.CPLite.Forms.Tienda {
    internal class SeccionesForm : UI.Tienda.Forms.CategoriaProductosForm {

        public SeccionesForm(UIMenuElement menuElement) : base() {
            this.Load += SeccionesForm_Load;
        }

        private void SeccionesForm_Load(object sender, EventArgs e) {
            this.Text = "Tienda: Categorías / Productos";
            this._Service = new Aplication.CPLite.Almacen.Services.CatalogoProductoService(AlmacenEnum.PT);
            this._CategoriasService = new Aplication.CPLite.Almacen.Services.CategoriasService();
            using (UI.Almacen.Builder.IProductoGridBuilder view = new UI.Almacen.Builder.ProductoGridBuilder()) {
               this.TControl.GridData.Columns.AddRange( view.Templetes().ModelosImportar1().Build());
            }
            // cambiamos el nombre porque la categoria a filtrar es la de la publicacion
            var catego = this.TControl.GridData.Columns["IdCategoria"] as GridViewComboBoxColumn;
            catego.FieldName = "IdCategoriaP";
            catego.Name = "IdCategoriaP";
        }

        protected override void TControl_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TControl.ValueMember = "IdCategoriaP";
            this.TControl.IndexChanged += TControl_IndexChanged;
            this.TControl.GridData.DataSource = this._DataSource;
            this.TControl.TreeData.DataSource = this._Categorias;
            this.TControl.TreeData.DisplayMember = "Clase";
            this.TControl.TreeData.ValueMember = "IdCategoria";
            this.TControl.TreeData.ParentMember = "IdSubCategoria";
            this.TControl.TreeData.ChildMember = "IdCategoria";
            this.TControl.TreeData.SelectedNode = this.TControl.TreeData.Nodes[0];

            // catalogo de unidades del almacen
            var cboUnidades = this.TControl.GridData.Columns["IdUnidad"] as GridViewComboBoxColumn;
            cboUnidades.DisplayMember = "Descripcion";
            cboUnidades.ValueMember = "IdUnidad";
            cboUnidades.AutoSizeMode = BestFitColumnMode.DisplayedCells;
            cboUnidades.DropDownStyle = RadDropDownStyle.DropDownList;
            cboUnidades.FilteringMode = GridViewFilteringMode.DisplayMember;
            cboUnidades.DataSource = this._Unidades;
        }

        private void TControl_IndexChanged(object sender, object e) {
            var d0 = e as ModeloXDetailModel;
            d0.Secuencia = this.TControl.NewIndex;
        }

        protected override void Consultar() {
            this._Categorias = new BindingList<CategoriaSingle>(this._CategoriasService.GetList<CategoriaSingle>(new List<Domain.Base.Builder.IConditional>()).ToList<CategoriaSingle>());
            this._Productos = new BindingList<CategoriaProductoModel>(this._Service.GetList<CategoriaProductoModel>(Aplication.Tienda.Services.CatalogoModeloService.Query().ByIdAlmacen(AlmacenEnum.PT).Activo(true).Build()).ToList());
            this._DataSource = new BindingList<ModeloXDetailModel>(this._Service.GetList<ModeloXDetailModel>(new Domain.Almacen.Builder.ModelosQueryBuilder().Activo().Visibilidad().Build()).ToList());
            this._Unidades = new BindingList<UnidadModel>(this._Service.GetList<UnidadModel>(Aplication.Almacen.Services.UnidadService.Query().ByAlmacen(this._Service.Almacen).Activo().Build()).ToList());
        }
    }
}
