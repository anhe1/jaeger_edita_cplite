﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.CPLite.Contracts;
using Jaeger.Aplication.CPLite.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Services;
using Jaeger.Domain.Ventas.Entities;
using Jaeger.Domain.Ventas.Builder;
using Jaeger.Domain.Tienda.Entities;
using Jaeger.Domain.Tienda.ValueObjects;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Banco.Contracts;
using Jaeger.Domain.Banco.Entities;
using Jaeger.Domain.Almacen.PT.Contracts;
using Jaeger.Domain.Almacen.PT.Entities;
using Jaeger.Domain.Almacen.PT.ValueObjects;
using Jaeger.DataAccess.FB.CPLite.Repositories;
using Jaeger.DataAccess.FB.Almacen.PT.Repositories;
using Jaeger.DataAccess.FB.Banco.Repositories;
using Jaeger.DataAccess.FB.Contribuyentes.Repositories;

namespace Jaeger.Aplication.CPLite.Ventas.Services {
    /// <summary>
    /// Catalogo de comisiones
    /// </summary>
    public class ComisionService : Aplication.Ventas.ComisionService, IComisionService, Aplication.Ventas.Contracts.IComisionService {
        protected ISqlMovimientoBancarioRepository movimientoBancarioRepository;

        public ComisionService() : base() { }

        public override void OnLoad() {
            this.movimientoBancarioRepository = new SqlFbMovimientoBancarioRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.comisionRemisionRepository = new SqlFbRemisionCRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.comisionCatalogoRepository = new SqlFbComisionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.vendedorRepository=new SqlFbVendedorRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.carteraRepository=new SqlFbVendedorCarteraRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public new BindingList<RemisionComisionDetailModel> GetComisionesPorPagar(int year, int idVendedor) {
            var query = new ComisionRemisionQueryBuilder().Year(year).IdVendedor(idVendedor).IdStatus(RemisionStatusEnum.Cobrado).IsDisponible(RemisionComisionStatusEnum.SinRelacion).Build();
            var _response = new BindingList<RemisionComisionDetailModel>();
            _response = new BindingList<RemisionComisionDetailModel>(this.comisionRemisionRepository.GetList<RemisionComisionDetailModel>(query).ToList());
            if (_response.Count > 0) {
                var indexs = _response.Select(it => it.IdRemision).ToArray();
                var mov = this.movimientoBancarioRepository.GetList<MovimientoComprobanteModel>(
                    new List<IConditional> {
                        new Conditional("BNCCMP_IDCOM", string.Join(",", indexs), ConditionalTypeEnum.In),
                        new Conditional("BNCCMP_A", "1")
                    }).ToList();
                if (mov != null) {
                    if (mov.Count > 0) {
                        for (int i = 0; i < _response.Count; i++) {
                            var index = mov.Where(it => it.IdComprobante == _response[i].IdRemision).ToList();
                            if (index != null) {
                                _response[i].Movimientos = new BindingList<RemisionMovimientoBancarioModel>();
                                foreach (var item in index) {
                                    _response[i].Movimientos.Add(item.MapProperties<RemisionMovimientoBancarioModel>());
                                }
                            }
                        }
                    }
                }
            }
            return _response;
        }

        // para comisiones de vendedores, solo aquellas remisiones que no se agregaron a 
        public BindingList<IRemisionComisionDetailModel> GetTo(List<IConditional> conditionals) {
            var d0 = this.comisionRemisionRepository.GetList<RemisionComisionDetailModel>(conditionals).ToList<IRemisionComisionDetailModel>();
            return new BindingList<IRemisionComisionDetailModel>(d0);
        }

        /// <summary>
        /// calcular comisiones a vendedor
        /// </summary>
        public void Calcular(ComisionDetailModel catalogo, BindingList<RemisionComisionDetailModel> remisiones) {
            for (int i = 0; i < remisiones.Count; i++) {
                var _current = remisiones[i];

                // que esten dentro de la vigencia del catalogo
                if (_current.FechaEmision >= catalogo.FechaInicio && _current.FechaEmision <= catalogo.FechaFin) {
                    // definimos el rango del factor del descuento
                    var _factor1 = _current.FactorRealCobrado;
                    _factor1 = 1 - _factor1;
                    if (_factor1 < 0)
                        _factor1 = 0;

                    try {
                        var _factorDescuento = new ComisionDescuentoModel();
                        var _comisionAl100 = new ComisionDescuentoModel();
                        try {
                            _factorDescuento = catalogo.Descuento.Where(it => _factor1 >= it.Inicio && _factor1 <= it.Final).First();
                            _comisionAl100 = catalogo.Descuento.First();
                        } catch (Exception ex) {
                            Console.WriteLine(ex.Message);
                            _factorDescuento = null;
                            _current.Comision = "No existe nivel para el descuento aplicado";
                            _current.ComisionFactor = 0;
                            _current.ComisionFactorActualizado = 0;
                            _current.ComisionPorPagar = 0;
                        }
                        if (_factorDescuento != null) {
                            // multa a la cartera vencida
                            var _diasTranscurridos = _current.DiasUltCobro;
                            if (_diasTranscurridos <= 0)
                                _diasTranscurridos = 1;
                            var _factorMulta = catalogo.Multa.Where(it => _diasTranscurridos >= it.Inicio && _diasTranscurridos <= it.Final).First();
                            if (_factorMulta != null) {
                                _current.IdComision = catalogo.IdComision;
                                _current.ComisionFactor = _comisionAl100.Factor;
                                _current.ComisionFactorActualizado = _factorDescuento.Factor;
                                _current.Comision = string.Format("{0} Com: {1} Multa: {2}", catalogo.Descripcion, _factorDescuento.Descripcion, _factorMulta.Descripcion);
                                _current.ComisionPorPagar = (_current.TotalCobrado * _factorDescuento.Factor) * _factorMulta.Factor;
                                Console.WriteLine(string.Format("{0} Com: {1} Multa: {2} {3}", catalogo.Descripcion, _factorDescuento.Descripcion, _factorMulta.Descripcion, ((_current.TotalCobrado * _factorDescuento.Factor) * _factorMulta.Factor)));
                                this.comisionRemisionRepository.Saveable(_current);
                            }
                        }
                    } catch (Exception ex) {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public ComisionDetailModel GetComision(int index) {
            return this.comisionCatalogoRepository.GetList< ComisionDetailModel>(
                new List<IConditional>() {
                    new Conditional("CMSNT_ID", index.ToString()),
                    new Conditional("CMSNT_A", "1")
                }).FirstOrDefault();
        }

        public BindingList<ComisionDetailModel> GetComisiones(bool onlyActive) {
            var _condicion = new List<IConditional>();
            if (onlyActive) {
                _condicion.Add(new Conditional("CMSNT_A", "1"));
            }
            return new BindingList<ComisionDetailModel>(this.comisionCatalogoRepository.GetList< ComisionDetailModel>(_condicion).ToList());
        }

        public ComisionDetailModel Save(ComisionDetailModel model) {
            return this.comisionCatalogoRepository.Save(model);
        }

        /// <summary>
        /// actualizar bandera de comisiones
        /// </summary>
        /// <param name="idRemision"></param>
        /// <param name="value">0 <-no esta agregada a ningun recibo, 1 <- comsion autorizada y esta agregada a un recibo, 2 <-comision pagada y ya esta autorizada en un recibo</param>
        public bool Update(int[] idRemision, int value) {
            return this.comisionRemisionRepository.Update(idRemision, value);
        }

        /// <summary>
        /// listado de prioridades
        /// </summary>
        public virtual List<PrioridadModel> GetPrioridad() {
            List<PrioridadModel> enums = ((CatalogoPrioridadEnum[])Enum.GetValues(typeof(CatalogoPrioridadEnum))).Select(c => new
            PrioridadModel() {
                Id = (int)c,
                Activo = 1,
                Descripcion = c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description
            }).ToList();
            return enums;
        }
    }
}
