﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.Localization;
using Telerik.WinControls;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Base;
using Jaeger.UI.Common.Services;
using Jaeger.UI.Login.Forms;

namespace Jaeger.UI.CPLite.Alpha {
    internal static class Program {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LocalizationProvider<RadMessageLocalizationProvider>.CurrentProvider = new LocalizationProviderCustomerMessageBox();

            // creamos y comprobamos los directorios para uso local
            ManagerPathService.CreatePaths();
            using (var login = new LoginForm(args) { LogoTipo = Properties.Resources.about_30px }) {
                login.ShowDialog();
            }

            if (!(ConfigService.Synapsis == null) & !(ConfigService.Piloto == null)) {
                Application.Run(new Forms.MainRibbonForm());
            }
        }
    }
}
