﻿using Jaeger.UI.Tienda.Forms;

namespace Jaeger.UI.CPLite.Forms.Clientes {
    partial class NotaDescuentoCancelarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.MotivoCancelacion = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.GroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.FechaEntrega = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaLabel = new Telerik.WinControls.UI.RadLabel();
            this.Cliente = new Telerik.WinControls.UI.RadTextBox();
            this.label7 = new Telerik.WinControls.UI.RadLabel();
            this.Nota = new Telerik.WinControls.UI.RadTextBox();
            this.NotaLabel = new Telerik.WinControls.UI.RadLabel();
            this.IdStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.lblStatus = new Telerik.WinControls.UI.RadLabel();
            this.Relacionado = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.NumPedidoLabel = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.Cerrar = new Telerik.WinControls.UI.RadButton();
            this.Autorizar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox)).BeginInit();
            this.GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotaLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relacionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPedidoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Autorizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // MotivoCancelacion
            // 
            this.MotivoCancelacion.AutoSizeDropDownHeight = true;
            this.MotivoCancelacion.AutoSizeDropDownToBestFit = true;
            this.MotivoCancelacion.DisplayMember = "Descriptor";
            this.MotivoCancelacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // MotivoCancelacion.NestedRadGridView
            // 
            this.MotivoCancelacion.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.MotivoCancelacion.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotivoCancelacion.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MotivoCancelacion.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn7.FieldName = "Id";
            gridViewTextBoxColumn7.HeaderText = "Clave";
            gridViewTextBoxColumn7.Name = "Clave";
            gridViewTextBoxColumn8.FieldName = "Descripcion";
            gridViewTextBoxColumn8.HeaderText = "Descripción";
            gridViewTextBoxColumn8.Name = "Descripcion";
            gridViewTextBoxColumn9.FieldName = "Descriptor";
            gridViewTextBoxColumn9.HeaderText = "Descriptor";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "Descriptor";
            this.MotivoCancelacion.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.MotivoCancelacion.EditorControl.MasterTemplate.EnableGrouping = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ShowRowHeaderColumn = false;
            this.MotivoCancelacion.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.MotivoCancelacion.EditorControl.Name = "NestedRadGridView";
            this.MotivoCancelacion.EditorControl.ReadOnly = true;
            this.MotivoCancelacion.EditorControl.ShowGroupPanel = false;
            this.MotivoCancelacion.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.MotivoCancelacion.EditorControl.TabIndex = 0;
            this.MotivoCancelacion.Location = new System.Drawing.Point(139, 89);
            this.MotivoCancelacion.Name = "MotivoCancelacion";
            this.MotivoCancelacion.NullText = "Selecciona";
            this.MotivoCancelacion.Size = new System.Drawing.Size(285, 20);
            this.MotivoCancelacion.TabIndex = 11;
            this.MotivoCancelacion.TabStop = false;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(12, 90);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(121, 18);
            this.radLabel6.TabIndex = 10;
            this.radLabel6.Text = "Motivo de cancelación:";
            // 
            // GroupBox
            // 
            this.GroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBox.Controls.Add(this.FechaEntrega);
            this.GroupBox.Controls.Add(this.FechaLabel);
            this.GroupBox.Controls.Add(this.Cliente);
            this.GroupBox.Controls.Add(this.label7);
            this.GroupBox.Controls.Add(this.Nota);
            this.GroupBox.Controls.Add(this.NotaLabel);
            this.GroupBox.Controls.Add(this.IdStatus);
            this.GroupBox.Controls.Add(this.lblStatus);
            this.GroupBox.Controls.Add(this.Relacionado);
            this.GroupBox.Controls.Add(this.radLabel2);
            this.GroupBox.Controls.Add(this.IdDocumento);
            this.GroupBox.Controls.Add(this.NumPedidoLabel);
            this.GroupBox.Controls.Add(this.radLabel6);
            this.GroupBox.Controls.Add(this.MotivoCancelacion);
            this.GroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupBox.HeaderText = "";
            this.GroupBox.Location = new System.Drawing.Point(0, 50);
            this.GroupBox.Name = "GroupBox";
            this.GroupBox.Size = new System.Drawing.Size(438, 149);
            this.GroupBox.TabIndex = 1;
            this.GroupBox.TabStop = false;
            // 
            // FechaEntrega
            // 
            this.FechaEntrega.Location = new System.Drawing.Point(226, 15);
            this.FechaEntrega.Name = "FechaEntrega";
            this.FechaEntrega.ReadOnly = true;
            this.FechaEntrega.Size = new System.Drawing.Size(199, 20);
            this.FechaEntrega.TabIndex = 3;
            this.FechaEntrega.TabStop = false;
            this.FechaEntrega.Text = "martes, 27 de julio de 2021";
            this.FechaEntrega.Value = new System.DateTime(2021, 7, 27, 22, 12, 11, 710);
            // 
            // FechaLabel
            // 
            this.FechaLabel.Location = new System.Drawing.Point(187, 16);
            this.FechaLabel.Name = "FechaLabel";
            this.FechaLabel.Size = new System.Drawing.Size(37, 18);
            this.FechaLabel.TabIndex = 2;
            this.FechaLabel.Text = "Fecha:";
            // 
            // Cliente
            // 
            this.Cliente.Location = new System.Drawing.Point(60, 64);
            this.Cliente.MaxLength = 100;
            this.Cliente.Name = "Cliente";
            this.Cliente.ReadOnly = true;
            this.Cliente.Size = new System.Drawing.Size(364, 20);
            this.Cliente.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Cliente:";
            // 
            // Nota
            // 
            this.Nota.Location = new System.Drawing.Point(99, 114);
            this.Nota.MaxLength = 100;
            this.Nota.Name = "Nota";
            this.Nota.Size = new System.Drawing.Size(325, 20);
            this.Nota.TabIndex = 13;
            // 
            // NotaLabel
            // 
            this.NotaLabel.Location = new System.Drawing.Point(12, 115);
            this.NotaLabel.Name = "NotaLabel";
            this.NotaLabel.Size = new System.Drawing.Size(81, 18);
            this.NotaLabel.TabIndex = 12;
            this.NotaLabel.Text = "Observaciones:";
            // 
            // IdStatus
            // 
            this.IdStatus.DisplayMember = "Descriptor";
            this.IdStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.IdStatus.Enabled = false;
            this.IdStatus.Location = new System.Drawing.Point(57, 15);
            this.IdStatus.Name = "IdStatus";
            this.IdStatus.Size = new System.Drawing.Size(120, 20);
            this.IdStatus.TabIndex = 1;
            this.IdStatus.ValueMember = "Id";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(12, 16);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 18);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Status:";
            // 
            // Relacionado
            // 
            this.Relacionado.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Relacionado.Location = new System.Drawing.Point(343, 40);
            this.Relacionado.Name = "Relacionado";
            this.Relacionado.NullText = "#Pedido";
            this.Relacionado.ReadOnly = true;
            this.Relacionado.Size = new System.Drawing.Size(81, 20);
            this.Relacionado.TabIndex = 7;
            this.Relacionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(233, 41);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(105, 18);
            this.radLabel2.TabIndex = 6;
            this.radLabel2.Text = "Pedido relacionado:";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdDocumento.Location = new System.Drawing.Point(106, 40);
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.NullText = "#Pedido";
            this.IdDocumento.ReadOnly = true;
            this.IdDocumento.Size = new System.Drawing.Size(81, 20);
            this.IdDocumento.TabIndex = 5;
            this.IdDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // NumPedidoLabel
            // 
            this.NumPedidoLabel.Location = new System.Drawing.Point(12, 41);
            this.NumPedidoLabel.Name = "NumPedidoLabel";
            this.NumPedidoLabel.Size = new System.Drawing.Size(90, 18);
            this.NumPedidoLabel.TabIndex = 4;
            this.NumPedidoLabel.Text = "Núm. de Pedido:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(438, 50);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // radLabel3
            // 
            this.radLabel3.BackColor = System.Drawing.Color.White;
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel3.Location = new System.Drawing.Point(12, 12);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(407, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Para cada pedido que desea cancelar debe capturar el motivo de cancelació";
            // 
            // Cerrar
            // 
            this.Cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Cerrar.Location = new System.Drawing.Point(316, 206);
            this.Cerrar.Name = "Cerrar";
            this.Cerrar.Size = new System.Drawing.Size(110, 24);
            this.Cerrar.TabIndex = 4;
            this.Cerrar.Text = "Cerrar";
            this.Cerrar.Click += new System.EventHandler(this.Cerrar_Click);
            // 
            // Autorizar
            // 
            this.Autorizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Autorizar.Location = new System.Drawing.Point(200, 206);
            this.Autorizar.Name = "Autorizar";
            this.Autorizar.Size = new System.Drawing.Size(110, 24);
            this.Autorizar.TabIndex = 4;
            this.Autorizar.Text = "Autorizar";
            this.Autorizar.Click += new System.EventHandler(this.Autorizar_Click);
            // 
            // NotaDescuentoCancelarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 242);
            this.Controls.Add(this.Autorizar);
            this.Controls.Add(this.Cerrar);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.GroupBox);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotaDescuentoCancelarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cancelación";
            this.Load += new System.EventHandler(this.PedidoCancelarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotivoCancelacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox)).EndInit();
            this.GroupBox.ResumeLayout(false);
            this.GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FechaEntrega)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotaLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Relacionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPedidoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Autorizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal Telerik.WinControls.UI.RadMultiColumnComboBox MotivoCancelacion;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadGroupBox GroupBox;
        private Telerik.WinControls.UI.RadLabel NumPedidoLabel;
        internal Telerik.WinControls.UI.RadTextBox Relacionado;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        internal Telerik.WinControls.UI.RadTextBox IdDocumento;
        internal Telerik.WinControls.UI.RadDropDownList IdStatus;
        private Telerik.WinControls.UI.RadLabel lblStatus;
        public Telerik.WinControls.UI.RadTextBox Nota;
        private Telerik.WinControls.UI.RadLabel NotaLabel;
        public Telerik.WinControls.UI.RadDateTimePicker FechaEntrega;
        private Telerik.WinControls.UI.RadLabel FechaLabel;
        public Telerik.WinControls.UI.RadTextBox Cliente;
        private Telerik.WinControls.UI.RadLabel label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton Cerrar;
        private Telerik.WinControls.UI.RadButton Autorizar;
    }
}