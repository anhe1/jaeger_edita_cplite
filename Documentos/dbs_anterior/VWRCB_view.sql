CREATE VIEW VWRCB (VWRCB_ID, VWRCB_A, VWRCB_DOC_ID, VWRCB_STTSDCS_ID, VWRCB_FRMPGS_ID, VWRCB_REM_ID, VWRCB_STA, VWRCB_FOLIO, VWRCB_PAGO, VWRCB_NOM, VWRCB_FECEMSN, VWRCB_FECDOCS, VWRCB_FECCBRO, VWRCB_CARGO$, VWRCB_ABONO$, VWRCB_FCTR2, VWRCB_RFRNC1, VWRCB_RFRNC2, VWRCB_SBTTL$, VWRCB_TOTAL$, VWRCB_DSCNT$, VWRCB_TRNS_ID)
AS             
/*
Elabora: anhe1 24122012
Proposito: mostrar una vista con todos los recibos con sus respectivas partidas.
Revisiones:
	251012012: se modifico la vista ya que en la primera version no se incluye el abono de cada partida eso crea confusion porque solo se mostraba el total.
*/     
select 
    rec.cntbl_id vwrcb_id, 
    rec.cntbl_a vwrcb_a, 
    rec.cntbl_doc_id vwrcb_doc_id, 
    rec.cntbl_sttsdcs_id vwrcb_sttsdcs_id, 
    rec.cntbl_frmpgs_id vwrcb_frmpgs_id, 
    par.cntbl_rem_id vwrcb_rem_id, 
    sta.VWSTATUS_NOM vwrcb_sta,
    rec.cntbl_folio vwrcb_folio, 
    pag.lstsys_nom vwrcb_pago, 
    dir.drctr_nom vwrcb_nom, 
    rec.cntbl_fecemsn vwrcb_fecemsn, 
    rec.cntbl_fecdocs vwrcb_fecdocs, 
    rec.cntbl_feccbro vwrcb_feccbro, 
    rec.cntbl_cargo$ vwrcb_cargo$,
    par.cntbl_abono$ vwrcb_abono$,
    par.cntbl_fctr2 vwrcb_fctr2,
    par.cntbl_rfrnc1 vwrcb_rfrnc1,
    par.cntbl_rfrnc2 vwrcb_rfrnc2,
    par.cntbl_sbtt$ vwrcb_sbttl$,
    par.cntbl_total$ vwrcb_total$,
    par.cntbl_dsct$ vwrcb_dscnt$,
    par.cntbl_trns_id vwrcb_trns_id
from 
    cntbl rec, cntbl par, vwstatus sta, lstsys pag, drctr dir
where 
    rec.cntbl_sttsdcs_id = sta.vwstatus_sec_id and 
    sta.vwstatus_doc_id = 28 and 
    rec.cntbl_doc_id = 28 and 
    par.cntbl_poliza = rec.cntbl_poliza and 
    rec.cntbl_frmpgs_id = pag.lstsys_sec_id and 
    pag.lstsys_doc_id = 1025 and 
    dir.drctr_id = rec.cntbl_drctr_id and
    par.cntbl_a > 0
order by rec.cntbl_id desc;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VWRCB TO  SYSDBA WITH GRANT OPTION;

