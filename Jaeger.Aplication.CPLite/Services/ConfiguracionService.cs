﻿using Jaeger.Domain.Tienda.Contracts;

namespace Jaeger.Aplication.CPLite.Services {
    /// <summary>
    /// Servicio para parametros de configuracion de la aplicacion
    /// </summary>
    public class ConfiguracionService : Empresa.Service.ConfigurationService, Empresa.Contracts.IConfigurationService, Empresa.Contracts.IEmpresaService {
        public ConfiguracionService() : base() { }

        public virtual IConfiguration Get() {
            var parametros = this.Get(Domain.Empresa.Enums.ConfigGroupEnum.CPLite);
            var configuration = new Aplication.Tienda.Builder.ConfigurationBuilder().Build(parametros);
            return (IConfiguration)configuration;
        }

        public virtual IConfiguration Set(IConfiguration configuration) {
            var parameters = new Aplication.Tienda.Builder.ConfigurationBuilder().Build(configuration);
            this.Set(parameters);
            return configuration;
        }
    }
}